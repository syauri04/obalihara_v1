/*
Template Name: Color Admin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3 & 4
Version: 4.0.0
Author: Sean Ngu
Website: http://www.seantheme.com/color-admin-v4.0/admin/
*/

var handleDataTableDefault = function() {
	"use strict";
    
    if ($('#data-table-default').length !== 0) {
        $('#data-table-default').DataTable({
            // responsive: true,
            ordering: false,
            filtering: false,
            searching: false,
            info: false,
            paging: false,
            scrollY: '400px',
            scrollCollapse: true,
        });
    }
};

var cancelOrder = function() {
    "use strict";

    if ($('.cancel').length !== 0) {
        $('.cancel').each(function(){
            $(this).on('click',function(){
                $(this).parent().parent().remove();
            })
            
        })
    }
}

var TableManageDefault = function () {
	"use strict";
    return {
        //main function
        init: function () {
            handleDataTableDefault();
            cancelOrder();
        }
    };
}();