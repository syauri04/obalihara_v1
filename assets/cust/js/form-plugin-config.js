var handleSelect2 = function() {
    $(".select2").select2();
};

var TableManageDefault = function () {
	"use strict";
    return {
        //main function
        init: function () {
            handleSelect2();
        }
    };
}();