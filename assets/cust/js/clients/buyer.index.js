/*
Template Name: Color Admin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3 & 4
Version: 4.0.0
Author: Sean Ngu
Website: http://www.seantheme.com/color-admin-v4.0/admin/
*/

var handleDataTableDefault = function() {
	"use strict";
    
    if ($('#data-table-default').length !== 0) {
        $('#data-table-default').DataTable({
            // "scrollX": true
        });
    }
};

var handleActionTable = function(){
    "use strict";

    var ownLink = $('#ownLink').text();

    $('.edit').on('click',function(){
        window.location.href = ownLink + 'edit/' + $(this).data('id');
    });

    $('.trash').on('click',function(){
        window.location.href = ownLink + 'delete/' + $(this).data('id');
    });
}

var BuyerHandlerFunc = function () {
	"use strict";
    return {
        //main function
        init: function () {
            handleDataTableDefault();
            handleActionTable();
        }
    };
}();