var ownLink = $('#ownLink').text();

var handleActionForm = function(){
    "use strict";

    $('#provinsiID').on('change',function(){
        $.post(ownLink+'getCity',{ id: $(this).val() },function(x){
            $('#kotaID').html(x);
            $('#kotaID').trigger("chosen:updated");
        });
    })

}

var TableManageDefault = function () {
	"use strict";
    return {
        //main function
        init: function () {
            handleActionForm();
        }
    };
}();