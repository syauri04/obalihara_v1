var oLink = $('#ownLink').text();
$('#wysihtml5').wysihtml5();
$(".chosen").chosen({
    // disable_search_threshold: 10,
    no_results_text: "Oops, nothing found!",
    width: "100%",
    height: "110%",
});

$('#varianPanel').hide();
$("#varian-panel :input").attr("disabled", true);

$(document).on('change','#catId',function(){
	$.post(oLink+'catChild',{ id: $(this).val()} ,function(x){
		var data = JSON.parse(x);
		$("#catId1 select").empty();
		$("#catId1 select").append('<option >- Pilih Kategori -</option>');
		$.each(data, function(){
        	$("#catId1 select").append('<option value="'+ this.id +'" data-tipe="'+this.tipe+'">'+ this.name +'</option>');
    	})
		$('#catId1').show();
	});
});

$(document).on('change','#catId1',function(){
	$.post(oLink+'catChild',{ id: $('#catId1 select').val()} ,function(x){
		var data = JSON.parse(x);
		if(data.length > 0 ){
			$("#catId2 select").empty();
			$("#catId2 select").append('<option >- Pilih Kategori -</option>');
			$.each(data, function(){
	        	$("#catId2 select").append('<option value="'+ this.id +'" data-tipe="'+this.tipe+'">'+ this.name +'</option>');
	    	})
			$('#catId2').show();
			$('#catId1 select').removeAttr('name');
			$('#catId2 select').attr('disabled', false);
		} else {
			$('#catId1 select').attr('name','catId');
			$('#catId2 select').attr('disabled', 'disabled');
			$('#catId2').hide();
		}		
	});

	var tipe = $('#catId1 select').find(':selected').data('tipe');
	if(tipe != 0){
		if(tipe == 3){
			$('.addVarian').hide();
		} else {
			$('.addVarian').show();
		}

		$('#varianPanel').show(500);
		$("#varian-panel :input").attr("disabled", false);

		$.post(oLink+'getWarna',function(x){ 
			$('.chs').html(x);
			$('.chs').chosen({width: "100%"});
			$('.chs').trigger('chosen:updated');
		});
	
	} else {
		$('#varianPanel').hide(500);
		$("#varian-panel :input").attr("disabled", true);
	}
});

$(document).on('change','#catId2',function(){

	var tipe = $('#catId2 select').find(':selected').data('tipe');
	if(tipe != 0){
		if(tipe == 3){
			$('.addVarian').hide();
		} else {
			$('.addVarian').show();
		}

		$.post(oLink+'getWarna',function(x){ 
			$('.chs').html(x);
			$('.chs').chosen({width: "100%"});
			$('.chs').trigger('chosen:updated');
		});

		$('#varianPanel').show(500);
		$("#varian-panel :input").attr("disabled", false);
	} else {
		$('#varianPanel').hide(500);
		$("#varian-panel :input").attr("disabled", true);
	}

});

// varian Barang Config

$(document).on('click','.addVarian',function(){

	$('select[name*=varianType] option').not(':selected').hide();

	var ele = '#catId1';
	if($('#catId2').is(":visible")){
		var ele = '#catId2';
	}

	var d = {val : $('select[name*=varianType]').val() , tipe : $(ele + ' select').find(':selected').data('tipe') };

	$.post(oLink+'verifyVarian',d,function(x){ 

		var $r = $(x);

		$r.filter('.chs').chosen();
        $r.find('.chs').chosen({width: "100%"});
		$r.insertAfter('div#varianEle').last();

		var c = $('div#varianEle').length;
		
		if(c == 2){
			$('.addVarian').hide();
		}

	});	

});

$(document).on('click','.cancel',function(){
    var p = $(this).parent().parent().remove();
    var c = $('div#varianEle').length;
    $('select[name*=varianType] option').not(':selected').show();
    if(c == 1){
		$('.addVarian').show();
	}
});

$(document).on('change',"select[name*='warna']",function(){
	

	var w = $('select[name*="ukuran"]');

	console.log($(this).val().length+' - '+w.val().length);

	var l = $(this).val().length;
	$('.panel-body2').show();
	$('.varianEleDet').remove();

	if( w.val().length != 0){

		for(var a = 0;a < l;a++){
			for (var r = 0;r < w.val().length;r++){
				var b = '<div class="form-group row varianEleDet" id="varianEleDet">';
				b += '<label class="col-form-label col-md-2 f-s-13 m-t-5">'+w.val()[r]+'</label>';
				b += '<label class="col-form-label col-md-2 f-s-13 m-t-5">'+$(this).val()[a]+'</label>';
				b += '<label class="col-form-label col-md-2"><input type="text" class="form-control" name="harga_varian[]"></label>';
				b += '<label class="col-form-label col-md-1"><input type="text" class="form-control" name="stok_varian[]"></label>';
				b += '<label class="col-form-label col-md-2"><input type="text" class="form-control" name="sku_varian[]"></label>';
				b += '<label class="col-form-label col-md-2"><button class="btn btn-danger btn-icon btn-circle cancel" ><i class="fa fa-trash "></i></button></label>';
				b += '</div>';

				$('#panel-varianDet').append(b);
			}
		}

	} else {
		for(var a = 0;a < l;a++){
			
			var b = '<div class="form-group row varianEleDet" id="varianEleDet">';
			b += '<label class="col-form-label col-md-2 f-s-13 m-t-5"> - </label>';
			b += '<label class="col-form-label col-md-2 f-s-13 m-t-5">'+$(this).val()[a]+'</label>';
			b += '<label class="col-form-label col-md-2"><input type="text" class="form-control" name="harga_varian[]"></label>';
			b += '<label class="col-form-label col-md-1"><input type="text" class="form-control" name="stok_varian[]"></label>';
			b += '<label class="col-form-label col-md-2"><input type="text" class="form-control" name="sku_varian[]"></label>';
			b += '<label class="col-form-label col-md-2"><button class="btn btn-danger btn-icon btn-circle cancel" ><i class="fa fa-trash "></i></button></label>';
			b += '</div>';

			$('#panel-varianDet').append(b);
			
		}
	}
	

});  

$(document).on('change',"select[name*='ukuran']",function(){
	console.log($(this).val()[0]);

	var w = $('select[name*="warna"]');

	var l = $(this).val().length;
	$('.panel-body2').show();
	$('.varianEleDet').remove();

	for(var a = 0;a < l;a++){
		for (var r = 0;r < w.val().length;r++){
			var b = '<div class="form-group row varianEleDet" id="varianEleDet">';
			b += '<label class="col-form-label col-md-2 f-s-13 m-t-5">'+$(this).val()[a]+'</label>';
			b += '<label class="col-form-label col-md-2 f-s-13 m-t-5">'+w.val()[r]+'</label>';
			b += '<label class="col-form-label col-md-2"><input type="text" class="form-control" name="harga_varian[]"></label>';
			b += '<label class="col-form-label col-md-1"><input type="text" class="form-control" name="stok_varian[]"></label>';
			b += '<label class="col-form-label col-md-2"><input type="text" class="form-control" name="sku_varian[]"></label>';
			b += '<label class="col-form-label col-md-2"><button class="btn btn-danger btn-icon btn-circle cancel" ><i class="fa fa-trash "></i></button></label>';
			b += '</div>';

			$('#panel-varianDet').append(b);
		}
	}
}); 


function varianDet(){

	var b = '';
	b += '<div class="form-group row" id="varianEleDet" style="display: none;">';
	b += '<label class="col-form-label col-md-2 f-s-13 m-t-5">34</label>';
	b += '<label class="col-form-label col-md-2 f-s-13 m-t-5">Hitam</label>';
	b += '<label class="col-form-label col-md-2"><input type="text" class="form-control" name="harga_varian[]"></label>';
	b += '<label class="col-form-label col-md-1"><input type="text" class="form-control" name="stok_varian[]"></label>';
	b += '<label class="col-form-label col-md-2"><input type="text" class="form-control" name="sku_varian[]"></label>';
	b += '<label class="col-form-label col-md-2"><button class="btn btn-danger btn-icon btn-circle cancel" ><i class="fa fa-trash "></i></button></label>';
	b += '</div>';



}