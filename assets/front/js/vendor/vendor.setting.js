var ownLink = $('#ownLink').text();

// $(document).bind("contextmenu",function(e){
//   return false;
// });

// document.onkeydown = function(e) {
//   if(event.keyCode == 123) {
//      return false;
//   }
//   if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
//      return false;
//   }
//   if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
//      return false;
//   }
//   if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
//      return false;
//   }
//   if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
//      return false;
//   }
// }   

$(document).ready(function(){

    if($('#provinsiID').val() != ''){
        $('#kotaID').hide();
        $.post(ownLink+'front/vendor/getCity',{ id: $('#provinsiID').val() , k: $('#kotaID').data('id') }).done(function(x){
            $('#kotaID').show();
            $('#kotaID').html(x);
            $('#kotaID').trigger("chosen:updated");
        });
    }

    if($('#kotaID').data('id') != ''){
        $('#kecID').hide();
        $.post(ownLink+'front/vendor/getSubdistrict',{ id: $('#kotaID').data('id') , k: $('#kecID').data('id') }).done(function(x){
            $('#kecID').show();
            $('#kecID').html(x);
            $('#kecID').trigger("chosen:updated");
        });
    }

});

$(document).on('change','#provinsiID',function(){
    $('#kotaID').hide();
    $.post(ownLink+'front/vendor/getCity',{ id: $(this).val() }).done(function(x){
        $('#kotaID').show();
        $('#kotaID').html(x);
        $('#kotaID').trigger("chosen:updated");
    });
})

$(document).on('change','#kotaID',function(){
    $('#kecID').hide();
    $.post(ownLink+'front/vendor/getSubdistrict',{ id: $(this).val() }).done(function(x){
        $('#kecID').show();
        $('#kecID').html(x);
        $('#kecID').trigger("chosen:updated");
    });
})

$(document).on('click','#saveSetting',function(){
    $('#settingForm').submit();
})

$(document).on('click','.delProduct',function(e){
  e.preventDefault();
  var konfirmasi = confirm("Apakah anda yakin menghapus barang ini?");
  var text = "";
  
  if(konfirmasi === true) {
    $.post(ownLink+'/front/vendor/deleteProduct',{id : $(this).data('id')},function(d){
      var a = JSON.parse(d);
      alert(a.msg);
      window.location.href = a.url;
    });
  }

});

Dropzone.options.demoUpload1 = {
  paramName: "file", // The name that will be used to transfer the file
  maxFiles: 1,
  maxFilesize: 6, // MB
  paramName: 'userImage',
  init: function(){
    this.on("sending", function(file, xhr, formData) {
       var value = $('form#demoUpload1 #key').val();
       formData.append("key", value); // Append all the additional input data of your form here!
    });
    this.on("maxfilesexceeded", function(file){
       alert("No more files please!");
    });
    this.on('success', function() {
      setTimeout(function(){
        location.reload();
      },2000);
    });
  }
};

//Dashboard Function
$(document).on('click','.processVendor',function(){
  $.blockUI({ css: { 
      border: 'none', 
      padding: '15px', 
      backgroundColor: '#000', 
      '-webkit-border-radius': '10px', 
      '-moz-border-radius': '10px', 
      opacity: .5, 
      color: '#fff' 
  } });

  var cartID = $(this).data('cartid');
  var tis = $(this);
  $.post(ownLink + '/front/vendor/processByVendor',{id : cartID},function(x){
    
    if(x == 'true'){
      tis.hide();
      setTimeout($.unblockUI, 200);
      location.reload();
    } else {
      alert('SOMETHING WRONG AS DEVELOPER!!');
    }
  });

});

$('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget); 
  var recipient = button.data('cartid'); 
  var modal = $(this);
  modal.find('.modal-body .cartid').val(recipient);
})

$('#resiModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget); 
  var recipient = button.data('cartid'); 
  var modal = $(this);
  modal.find('.modal-body .cartid').val(recipient);
})

$(document).on('click','.saveResi',function(){

  $.blockUI({ css: { 
      border: 'none', 
      padding: '15px', 
      backgroundColor: '#000', 
      '-webkit-border-radius': '10px', 
      '-moz-border-radius': '10px', 
      opacity: .5, 
      color: '#fff' 
  } });

  var noResi = $('.resi').val();
  var cartid = $('.cartid').val();
  $.post(ownLink + '/front/vendor/courierDelivery',{ id : cartid , resi : noResi },function(x){
    
    if(x == 'true'){
      $('.btnSend').hide();
      $('#resiModal').modal('hide');
      setTimeout($.unblockUI, 200);
      location.reload();
    } else {
      alert('SOMETHING WRONG AS DEVELOPER!!');
    }
  });

});

$(document).on('click','.btnTracking',function(e){
  e.preventDefault();

  $.blockUI({ css: { 
      border: 'none', 
      padding: '15px', 
      backgroundColor: '#000', 
      '-webkit-border-radius': '10px', 
      '-moz-border-radius': '10px', 
      opacity: .5, 
      color: '#fff' 
  } });

  var tis = $(this);
  var d = {

    receipt : $(this).data('receipt'),
    courier : $(this).data('courier')

  }

  if( tis.data('status') > 4) {
    $('.btnTerima').hide();
  }

  $.ajax({
    type:"POST",
    url:ownLink + 'front/buyer/cekResi',
    data:d,
    success: function(r){
      var parse = JSON.parse(r);
      var kode  = parse.rajaongkir.status.code;
      var res   = parse.rajaongkir.result;
      var html  = '';
      if(kode != 400){

        if(res.manifest.length > 0){

          if(res.delivery_status.status != 'DELIVERED'){
            $('.konsumen_kontak').hide();
          }

          var valuestart = res.delivery_status.pod_date+' '+res.delivery_status.pod_time;
          var valuestop = new Date($.now());

          //create date format          
          var timeStart = new Date(valuestart);
          var timeEnd = new Date(valuestop);

          var hourDiff = timeEnd.getTime() - timeStart.getTime();
          var hours = Math.floor(hourDiff / (60 * 60 * 1000));

          if( hours >= 24) {
            $('.btnTerima').hide();
          }

          $.post(ownLink + 'front/vendor/getBuyer',{id : tis.data('cartid')},function(d){
            var par = JSON.parse(d);
            $('.nama_konsumen b').text(par.nama_lengkap);
            $('.email_konsumen b').text(par.email);
            $('.phone_konsumen b').text(par.telp);
          });

          $('.resiStatus').html('Status Resi : '+res.delivery_status.status+','+res.delivery_status.pod_receiver+' '+res.delivery_status.pod_date+' '+res.delivery_status.pod_time);

          $.each(res.manifest.reverse(),function(i,v){
            html += '<ul>';
            html += '<li>'+res.manifest[i].manifest_date+' , '+res.manifest[i].manifest_time+'</li>';
            html += '<li>'+res.manifest[i].manifest_description+'</li>';
            html += '</ul>';
          });

        } else {

          html += '<ul>';
          html += '<li>';
          html += res.delivery_status.status;
          html += '</li>';
          html += '</ul>';

        }

        $('.manifest').html(html);
        $('#cartid').val(tis.data('cartid'));
        $('#trekingModal').modal('show');

        setTimeout($.unblockUI, 200);

      } else {

        alert('Seller Has not update waybill receipt!!');
        setTimeout($.unblockUI);

      }
    }
  });

});

$(document).on('click','.witdraw-btn',function(e){
  e.preventDefault();

  var a = spCharRemove($('.withdraw-total h5').text());
  var b = spCharRemove($('.withdraw_fee b').text());

  $('.withdraw_total b').text('Rp. '+parseInt(a-b).toLocaleString('id'));

  $('#witdrawModal').modal('show');

})

$(document).on('click','.withdraw-confirm',function(){

  $.blockUI({ css: { 
      border: 'none', 
      padding: '15px', 
      backgroundColor: '#000', 
      '-webkit-border-radius': '10px', 
      '-moz-border-radius': '10px', 
      opacity: .5, 
      color: '#fff' 
  } });
  
  var data = {
    'vendor_id' : $('.vendorid').val(),
    'user_id' : $('.userid').val(),
    'amount' : spCharRemove($('.withdraw_total b').text()),
    'fee'  : spCharRemove($('.vendor_fee b').text()),
    'bank_code' : $('.bank_vendor b').text(),
    'account_holder_name' : $('.nama_vendor b').text(),
    'account_number' : $('.rekening_vendor b').text(),
  };

  $.post(ownLink+'front/vendor/createDisbursement',data,function(res){
    var r = JSON.parse(res);

    $('#witdrawModal').modal('hide');

    if(r.msg == 'Success'){
      alert(r.text);
    } else {
      alert(r.text);
    }

    $.unblockUI();

  });

});

function spCharRemove(str){
  var a = parseInt($.trim(str).replace(/[\. ,:-]+/g, "").substr($.trim(str).replace(/[\. ,:-]+/g, "").indexOf("Rp") + 2));
  return a;
}