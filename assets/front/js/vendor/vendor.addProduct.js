var oLink = $('#ownLink').text()+'front/vendor/';
// $('#wysihtml5').wysihtml5();
// $(".chosen").chosen({
//     // disable_search_threshold: 10,
//     no_results_text: "Oops, nothing found!",
//     width: "100%",
//     height: "110%",
// });

// $('#varianPanel').hide();
// $("#varian-panel :input").attr("disabled", true);

// $(document).on('click','.btnBack',function(){
// 	window.history.back();
// });

$(window).keydown(function(event){
	if(event.keyCode == 13) {
	  event.preventDefault();
	  return false;
	}
});

$(document).ready(function(){

	if($("#errorMsg").text() != ''){
		alert($("#errorMsg").text());
	}

	if($("#barang_id").val() != '') {
		$.get( oLink+'getPhoto/'+$('#barang_id').val(), function( data ) {
		var d = JSON.parse(data);

			var l = d.length;

			for(var i = 0; i < l; i++){
				var t = '<div class="containerP">';
					t += '<img src="'+$('#ownLink').text()+'assets/img/goods/small/'+d[i].gambar_name+'" alt="Avatar" class="image" >';
					t += '<div class="middle">';
					t += '<div class="text"><a href="#" class="deletePhoto" data-id="'+d[i].id+'"><i class="fa fa-trash fa-2x"></i></a></div>';
					t += '<div class="text"><a href="#" class="starsPhoto" data-id="'+d[i].id+'"><i class="fa fa-star fa-2x"></i></a></div>';
					t += '</div>';
					t += '</div>';
				$('.imageRemove').append(t);
			}

		});
	}

});

setTimeout(function(){
	$('.deletePhoto').each(function(){
		$(this).click(function(){
			var p = $(this).parent().parent().parent()[0];
			$.post(oLink+'deletePhoto',{'id' : $(this).data('id')},function(e){
				var m = JSON.parse(e);

				if(m == 'd'){
					p.remove();
				} else {
					alert('Delete Error');
				}
			});
		});
	});
	$('.starsPhoto').each(function(){
		$(this).click(function(){
			var p = $(this).parent().parent().parent()[0];
			$.post(oLink+'starsPhoto',{'id' : $(this).data('id')},function(e){
				var m = JSON.parse(e);

				if(m != 'd'){
					alert('Change Error');
				}
			});
		});
	});
}, 1000);



if($('#demoUpload1').length){
	// alert('exist');
	Dropzone.options.demoUpload1 = {
		paramName: "file", // The name that will be used to transfer the file
		maxFile: 5,
		maxFilesize: 6, // MB
		paramName: 'userImage',
		// resizeWidth: 730,
		// resizeHeight: 395,
		addRemoveLinks: true,
		init: function(){
			this.on("sending", function(file, xhr, formData) {
			var value = $('form#demoUpload1 #key').val();
			formData.append("key", value); // Append all the additional input data of your form here!
			});
		}
    };
} else {

	const cat1 = $('#catId').data('val');
	const val = cat1.split(',');

	$(document).ready(function(){
			
		$.blockUI({ css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        } });

		setTimeout($.unblockUI, 6000); 

		if(cat1 != '' && val.length <= 2){
			$('#catId').val(val[0]);
	        $('#catId').trigger('change');
	        setTimeout(function () { $('#catId1').trigger('change'); }, 1500);
		} else if(val.length == 3) {
			$('#catId').val(val[0]);
	        $('#catId').trigger('change');
	        setTimeout(function () { $('#catId1').trigger('change'); }, 500);
			setTimeout(function () { $('#catId2').trigger('change'); }, 5000);
			
		}
	});

	$(document).on('change','.wp',function(){
		if(this.checked){
			// $(this).next('.wph').attr('disabled',false);
			$(this).next('.wph').attr('value',$(this).val());
		} else {
			// $(this).next('.wph').attr('disabled',true);
			$(this).next('.wph').attr('value','0');
		}
	})

	$(document).on('change','.up',function(){
		if(this.checked){
			// $(this).next('.uph').attr('disabled',false);
			$(this).next('.uph').attr('value',$(this).val());
		} else {
			// $(this).next('.uph').attr('disabled',true);
			$(this).next('.uph').attr('value','0');
		}
	})

	$(document).on('click','saveGoods',function(e){
		$('#sgForm').submit(e);
	});

	

	$(document).on('change','#catId',function(){
		$.post(oLink+'catChild',{ id: $(this).val()} ,function(x){
			var data = JSON.parse(x);

			if(data.length > 0){

				$("#catId1 select").empty();
				$("#catId1 select").append('<option selected>Select product subcategory</option>');
				$.each(data, function(){
					if(cat1 != ''){
						if(this.id == val[1]){
							var e = 'selected';
						}
					}
		        	$("#catId1 select").append('<option value="'+ this.id +'" data-tipe="'+this.tipe+'" '+e+' >'+ this.name +'</option>');
		    	})
				$('#catId1').show();
				$('#catId1 select').removeAttr('disabled','disabled');
			} else {

				$('#catId1').hide();
				$('#catId1 select').attr('disabled','disabled');

				var tipe = $('#catId1 select').find(':selected').data('tipe');
				var war = $('#warnaPicker').data('val');
				var uks = $('#ukuranPicker').data('val');

				if(tipe != 0){
					if(tipe == 3){
						$('.addVarian').hide();
					} else {
						$('.addVarian').show();
					}

					$('#varianPanel').show(500);
					$("#varian-panel :input").attr("disabled", false);

					if(war != '' && uks != '') {
						$.post(oLink+'getWarna',{t : tipe, w: war, u: uks},function(x){ 
							var a = JSON.parse(x)

							$('#warnaPicker').html(a.warna);
							$('#ukuranPicker').html(a.ukuran);
						});
					} else if(war != '' && uks == '') {
						$.post(oLink+'getWarna',{t : tipe, w: war},function(x){ 
							var a = JSON.parse(x)

							$('#warnaPicker').html(a.warna);
						});
					} else if(war == ''){
						$.post(oLink+'getWarna',{t : tipe},function(x){ 
							var a = JSON.parse(x)

							$('#warnaPicker').html(a.warna);
							$('#ukuranPicker').html(a.ukuran);
						});
					}

					
				
				} else {
					$('#varianPanel').hide(500);
					$("#varian-panel :input").attr("disabled", true);
				}

			}

		});
	});

	$(document).on('change','#catId1',function(){
		
		var id = ($('#catId1 select').val() != 0)? $('#catId1 select').val() : val[1];
		$('#warnaPicker div').remove();
		$('#ukuranPicker div').remove();

		setTimeout( function() { $.post(oLink+'catChild',{ id: id} ,function(x){
			var data = JSON.parse(x);
			console.log(data);
			if(data.length > 1 ){
				$("#catId2 select").empty();
				$("#catId2 select").append('<option >Select product subcategory</option>');
				$.each(data, function(){
					if(cat1 != ''){
						if(val[2] != undefined && this.id == val[2]){
							var e = 'selected';
						}
					}
		        	$("#catId2 select").append('<option value="'+ this.id +'" data-tipe="'+this.tipe+'" '+e+'>'+ this.name +'</option>');
		    	})
				$('#catId2').show();
				$('#catId1 select').removeAttr('name');
				$('#catId2 select').attr('disabled', false);
			} else {
				$('#catId1 select').attr('name','catId');
				$('#catId2 select').attr('disabled', 'disabled');
				$('#catId2').hide();
			}		
		}),5000});

		var tipe = $('#catId1 select').find(':selected').data('tipe');
		var war = $('#warnaPicker').data('val');
		var uks = $('#ukuranPicker').data('val');
		if(tipe != 0){
			if(tipe == 3){
				$('.addVarian').hide();
			} else {
				$('.addVarian').show();
			}

			$('#varianPanel').show(500);
			$("#varian-panel :input").attr("disabled", false);

			if(war != '' && uks != '') {
				$.post(oLink+'getWarna',{t : tipe, w: war, u: uks},function(x){ 
					var a = JSON.parse(x)

					$('#warnaPicker').html(a.warna);
					$('#ukuranPicker').html(a.ukuran);
				});
			} else if(war != '' && uks == '') {
				$.post(oLink+'getWarna',{t : tipe, w: war},function(x){ 
					var a = JSON.parse(x)

					$('#warnaPicker').html(a.warna);
				});
			} else if(war == ''){
				$.post(oLink+'getWarna',{t : tipe},function(x){ 
					var a = JSON.parse(x)

					$('#warnaPicker').html(a.warna);
					$('#ukuranPicker').html(a.ukuran);
				});
			}

			
		
		} else {
			$('#varianPanel').hide(500);
			$("#varian-panel :input").attr("disabled", true);
		}
	});

	$(document).on('change','#catId2',function(){

		$('#warnaPicker div').remove();
		$('#ukuranPicker div').remove();

		var tipe = $('#catId2 select').find(':selected').data('tipe');
		var war = $('#warnaPicker').data('val');
		var uks = $('#ukuranPicker').data('val');
		if(tipe != 0){
			if(tipe == 3){
				$('.addVarian').hide();
			} else {
				$('.addVarian').show();
			}

			if(war != '' && uks != '') {
				$.post(oLink+'getWarna',{t : tipe, w: war, u: uks},function(x){ 
					var a = JSON.parse(x)

					$('#warnaPicker').html(a.warna);
					$('#ukuranPicker').html(a.ukuran);
				});
			} else if(war != '' && uks == '') {
				$.post(oLink+'getWarna',{t : tipe, w: war},function(x){ 
					var a = JSON.parse(x)

					$('#warnaPicker').html(a.warna);
				});
			} else if(war == ''){
				$.post(oLink+'getWarna',{t : tipe},function(x){ 
					var a = JSON.parse(x)

					$('#warnaPicker').html(a.warna);
					$('#ukuranPicker').html(a.ukuran);
				});
			}

			$('#varianPanel').show();
			$("#varian-panel :input").attr("disabled", false);
		} else {
			$('#varianPanel').hide();
			$("#varian-panel :input").attr("disabled", true);
		}
	});

}