var ownLink = $('#ownLink').text();

// FUNCTION HELPER

function spCharRemove(str){
    var a = parseInt($.trim(str).replace(/[\. ,:-]+/g, "").substr($.trim(str).replace(/[\. ,:-]+/g, "").indexOf("Rp") + 2));
    return a;
}

function totalCart(){
    var sum = 0;

    $('.subTtl_item').each(function(){
        var col = '#'+$(this).parent().parent().parent()[0].id;
        if($(col).find('.checksp').is(':checked')){
            sum += +parseInt(spCharRemove( $(this).text()));
        }
    });

    return sum;
}

// FUNCTION CONFIG

$(document).ready(function(){

    $('.totalCart').text('Rp. 0');

    $('.originDest').each(function(){
        var tis = $(this);
        if($(this).data('val') != ''){
            $.post(ownLink+'front/home/cartCity',{ id: $(this).data('val') }).done(function(x){
                var response = JSON.parse(x);
                tis.find('span').text(response.type+' '+response.city_name)

            });
        }

    });    

    $('.priceDet').each(function(){
        var ttl = parseInt($(this).find('b.qty').text()) * parseInt($(this).find('.hargaSatuan').text());
        var a = $(this).find('.ttl_item').text('Rp. '+ttl.toLocaleString('ID'));
        $(this).parent().parent().parent().parent().find('.tdSubttl div span.subTtl_item').text(a.text());
    });
    
    $(document).on('keyup','.qtyText',function(e){
        e.preventDefault();
        var cols = $(this).parent().parent()[0].id;
        $('#'+cols+' .cityDest').text('Pilih Kota');
        $('#'+cols+' .courierDesc span').text('');
        $('#'+cols+' .courierPrice span b').text('');
        $('#'+cols+' .courierEtd span').text('');

        var a = $(this).parent().parent().find('td div.row div.priceDet span b.qty').text($(this).val());
        var b = $(this).parent().parent().find('td div.row div.priceDet span b.hargaSatuan').text();
        var ttl = parseInt($(this).val()) * parseInt(b);
        $(this).parent().parent().find('td div.row div.priceDet span b.ttl_item').text('Rp. '+ttl.toLocaleString('ID'))
        $(this).parent().parent().find('.tdSubttl div span.subTtl_item').text('Rp. '+ttl.toLocaleString('ID')).change();

        // $('.totalCart').text('Rp. 0');

        // $('#'+cols+' .checksp').trigger('change');
        // $('.checksp').trigger('change');
        var tc = totalCart();

        $('.totalCart').text('Rp. '+tc.toLocaleString('ID'));

    });

    $(document).on('change','.destination',function(){
        
        var cols = $('#colVal').val();

        $.post(ownLink+'front/home/cartCity',{ id: $(this).val() }).done(function(x){
            var response = JSON.parse(x);
            $('#'+cols+' .cityDest').text(response.type+" "+response.city_name);
        });

        $('select.courier').attr('disabled',false);

    });

    $(document).on('change','.courier',function(){

        var cols = $('#colVal').val();

        var data = {
            'origin'    : $('#'+cols+' div.originDest').data('val'),
            'dest'      : $('select.destination').val(),
            'weight'    : parseInt($('#'+cols+' input.weight').val()) * parseInt($('#'+cols+' b.qty').text()) ,
            'courier'   : $(this).val(),
        };

        $.post(ownLink+'front/home/cartCost',data).done(function(x){
            var r       = JSON.parse(x);
            var rCost   = r.costs.length;

            $('select.service').empty();

            for(var i = 0;i < rCost;i++){
                var opt = '<option data-service="'+r.costs[i].service+'" data-price="'+r.costs[i].cost[0].value+'" data-etd="'+r.costs[i].cost[0].etd.replace('HARI','')+'" data-desc="'+r.costs[i].description+'">'+r.costs[i].service+' ( Rp. '+r.costs[i].cost[0].value.toLocaleString('ID')+' , '+r.costs[i].cost[0].etd.replace('HARI','')+' Hari ) '+r.costs[i].description+' </option>';
                $('select.service').append(opt);
            }

        });

        $('select.service').attr('disabled',false);

    });

    $('.chooseDest').on('click',function(){

        var cols    = $('#colVal').val();
        var total   = spCharRemove($('#'+cols+' span.subTtl_item').text());

        var data = {
            'city'      : $('select.destination').find(':selected').text(),
            'courier'   : $('select.courier').find(':selected').text(),
            'service'   : $('select.service').find(':selected').data('service'),
            'price'     : $('select.service').find(':selected').data('price'),
            'etd'       : $('select.service').find(':selected').data('etd'),
            'desc'      : $('select.service').find(':selected').data('desc'),
            'total'     : parseInt(total) + parseInt($('select.service').find(':selected').data('price')),
        };

        $('#'+cols+' .cityDest').text(data.city);
        $('#'+cols+' .courierDesc span').text(data.courier+' '+data.desc);
        $('#'+cols+' .courierPrice span b').text('Rp. '+data.price.toLocaleString('ID'));
        $('#'+cols+' .courierEtd span').text(data.etd);
        $('#'+cols+' span.subTtl_item').text('Rp. '+data.total.toLocaleString('ID'));

        var tc = totalCart();

        $('.totalCart').text(tc.toLocaleString('ID'));

        $('#exampleModalCenter').modal('hide');

    });

    $(document).on('change','.checksp',function(e){
        var totalCart = parseInt(spCharRemove($('.totalCart').text()));
        var cols        = $(this).parent().parent()[0].id;
        if($(this).is(':checked')){
            
            var ttl_item    = parseInt(spCharRemove($('#'+cols+' .subTtl_item').text()));
            totalCart       = totalCart + ttl_item;
            $('.totalCart').text('Rp. '+totalCart.toLocaleString('ID'));
        } else {
            if(totalCart > 0){
                var ttl_item    =  parseInt(spCharRemove($('#'+cols+' .subTtl_item').text()));
                totalCart       = totalCart - ttl_item;
                $('.totalCart').text('Rp. '+totalCart.toLocaleString('ID'));
            }
            
        }
    });

    $(document).on('click','.delCart',function(e){
        e.preventDefault();
        var tis         = $(this);
        var cols        = $(this).parent().parent()[0].id;
        var konfirmasi  = confirm("Apakah anda yakin menghapus barang ini?");
        var text        = "";

        var data = {
            user_id : $(this).data('user'),
            barang_id : $(this).data('val'),
        }

        if(konfirmasi === true) {

            $.post(ownLink + 'front/home/deleteCart/',data,function(x){
                var a = JSON.parse(x);
                alert(a.msg);
                tis.parent().parent().remove();
                if($('#cartInfo span').length > 0){
                    var cartInfo = parseInt($('#cartInfo span').text()) - 1;
                    if(cartInfo == 0){
                        $('#cartInfo span').remove();
                    } else {
                        $('#cartInfo span').text(cartInfo);
                    }
                    
                }
                var tc = totalCart();

                $('.totalCart').text('Rp. '+tc.toLocaleString('ID'));
            });

        }
    })

    $(document).on('change','#checkAll',function(){

        if($(this).is(':checked')){
            $('input:checkbox').attr('checked','checked');
        } else {
            $('input:checkbox').removeAttr('checked');
        }

        var tc = totalCart();

        $('.totalCart').text('Rp. '+tc.toLocaleString('ID'));
    });

    $(document).on('click','.checkVendor',function(){
        // $('input:checkbox').not(this).attr('checked', this.checked);
        
        if($(this).is(':checked')){
            var a = $(this).data('vendor');
            $(this).attr('checked', 'checked');
            $('tr[data-vendor="'+a+'"] .checksp').attr('checked', 'checked');
        } else {
            var a = $(this).data('vendor');
            $(this).removeAttr('checked');
            $('tr[data-vendor="'+a+'"] .checksp').removeAttr('checked');
        }

        var tc = totalCart();

        $('.totalCart').text('Rp. '+tc.toLocaleString('ID'));
    });

    $('.checkOut').on('click',function(e){
        e.preventDefault();
        
        if($('.table .checksp:checked').length > 0){

            var d       = [];
            var qty     = [];
            var ttlItem = [];
            // var url = $(this).data('url');
            $('.checksp').each(function(){

                if($(this).is(':checked')){
                    var cols        = $(this).parent().parent().data('cart');
                    var cQty        = $(this).parent().parent().find('.qtyText').val();
                    var cttlItem    = spCharRemove($(this).parent().parent().find('.ttl_item').text());
                    qty.push(cQty);
                    d.push(cols);
                    ttlItem.push(cttlItem);
                }
            });
            
            $('#cartId').val(d);
            $('#qtyId').val(qty);
            $('#ttlItem').val(ttlItem);
            $('#formCartId').submit();

        } else {
            alert('Silahkan Pilih Barang Yang ingin di Bayar');
        }

        // window.location.href = ownLink+'/checkout';
    });

    $('#exampleModalCenter').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); 
        var recipient = button.data('val'); 
        var modal = $(this);
        modal.find('.modal-body #colVal').val(recipient);
    })
    
    $(".chosen").each(function(){
        $(this).chosen({
            no_results_text: "Oops, nothing found!",
            width: "100%"
        });
    })

});

