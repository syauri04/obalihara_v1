var ownLink = $('#ownLink').text();

$(document).on('click','.delWish',function(e){
    e.preventDefault();
    var tis = $(this);
    var data = {
        user_id : $(this).data('id'),
        barang_id : $(this).data('val'),
    }

    var konfirmasi = confirm("Apakah anda yakin menghapus barang ini?");
    var text = "";
    if(konfirmasi === true) {
        
        $.post(ownLink + 'front/home/deleteWishlist/',data,function(x){
            var a = JSON.parse(x);
            alert(a.msg);
            tis.parent().parent().remove();
            if($('#wishInfo span').length > 0){
                var wishInfo = parseInt($('#wishInfo span').text()) - 1;
                if(wishInfo == 0){
                    $('#wishInfo span').remove();
                } else {
                    $('#wishInfo span').text(wishInfo);
                }
                
            }
        });
    }
})