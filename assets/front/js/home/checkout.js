var ownLink = $('#ownLink').text();

// FUNCTION CONFIG

$(document).ready(function(){

    if($(".addressId").length == 0){
        $('a , :button').remove();
    }

    $('#totalProductPrice b').html('Rp. '+totalProductPrice().toLocaleString('ID'));

    $(document).on('click','.cAddress',function(e){
        e.preventDefault();
        var tis = $(this);
        var div = $(".cardAddress");
        var html = '';
        div.html('');
        
        html += '<div style="width:30%;">';
        html += '<h6>'+tis.data('title')+'</h6>';
        html += '<div><span><b>'+tis.data('nama_lengkap')+'</b></span></div>';
        html += '<div><span>'+tis.data('alamat')+'</span></div>';
        html += '<div><span>'+tis.data('province')+'</span> <br/> <span>'+tis.data('city')+'</span> , <span>'+tis.data('postal_code')+'</span> </div>';
        html += '<div class="mt-2"><span>'+tis.data('telp')+'</span></div>';
        html += '</div>';
        html += '<hr>';
        html += '<input type="hidden" class="userId" value="'+tis.data('userid')+'"/>';
        html += '<input type="hidden" class="addressId" value="'+tis.data('addressid')+'"/>';
        html += '<a href="#" data-toggle="modal" data-target="#addressModal" ><b>Ganti Alamat Pengirim</b></a>';

        div.html(html);

        $('#addressModal').modal('hide');

    });

    $(document).on('change','.destination',function(){
        
        var cols = $('#colVal').val();

        $.post(ownLink+'front/home/cartCity',{ id: $(this).val() }).done(function(x){
            var response = JSON.parse(x);
            $('#'+cols+' .courierName').text(response.type+" "+response.city_name);
        });

        $('select.courier').attr('disabled',false);

    });

    $(document).on('change','.courier',function(){

        var cols = $('#colVal').val();

        var w = totalWeight(cols);

        var data = {
            'origin'    : $('#'+cols+' .originDest').val(),
            'dest'      : $('select.destination').val(),
            'weight'    : w,
            'courier'   : $(this).val(),
        };

        $.post(ownLink+'front/home/cartCost',data).done(function(x){
            var r       = JSON.parse(x);
            var rCost   = r.costs.length;
            
            $('select.service').empty();

            for(var i = 0;i < rCost;i++){
                var opt = '<option data-service="'+r.costs[i].service+'" data-price="'+r.costs[i].cost[0].value+'" data-etd="'+r.costs[i].cost[0].etd.replace('HARI','')+'" data-desc="'+r.costs[i].description+'">'+r.costs[i].service+' ( Rp. '+r.costs[i].cost[0].value.toLocaleString('ID')+' , '+r.costs[i].cost[0].etd.replace('HARI','')+' Hari ) '+r.costs[i].description+' </option>';
                $('select.service').append(opt);
            }

        });

        $('select.service').attr('disabled',false);

    });

    $('.chooseDest').on('click',function(){

        var cols            = $('#colVal').val();
        var total           = parseInt(totalEachVendor(cols)) + parseInt($('select.service').find(':selected').data('price'));
        var cartIdentity    = cartid(cols); 

        var data = {
            'city'      : $('select.destination').find(':selected').text(),
            'courier'   : $('select.courier').find(':selected').text(),
            'service'   : $('select.service').find(':selected').data('service'),
            'price'     : $('select.service').find(':selected').data('price'),
            'etd'       : $('select.service').find(':selected').data('etd'),
            'desc'      : $('select.service').find(':selected').data('desc'),
            'total'     : total,
        };

        $('#'+cols+' .courierCity').text(data.city);
        $('#'+cols+' .courierName').text(data.courier);
        $('#'+cols+' .courierDesc').text(data.desc);
        $('#'+cols+' .courierCost').text('Rp. '+data.price.toLocaleString('ID'));
        $('#'+cols+' .courierEtd').text(data.etd+' Hari');
        $('#'+cols+' .subTotalVendor').text('Rp. '+data.total.toLocaleString('ID'));
        $('#'+cols+' .ces').data('cost',data.price);

        //Checkout Detail
        $('#'+cols+' #cartID').val(cartIdentity);
        $('#'+cols+' #totalCO').val(parseInt(totalEachVendor(cols)));
        $('#'+cols+' #courierCO').val(data.courier);
        $('#'+cols+' #courierServiceCO').val(data.desc);
        $('#'+cols+' #courierPriceCO').val(data.price);

        $('select.courier').val('0');
        $('select.destination').val('0').trigger('chosen:updated');
        $('select.service').empty();
        $('select.service').html('<option value="0">- Choose Service -</option>');
        $('select.courier').attr('disabled',true);
        $('select.service').attr('disabled',true);

        var tpp = totalProductPrice();
        var tep = totalExpeditionPrice();
        var ttl = ( parseInt(spCharRemove($('#totalExpeditionPrice b').text())) + tep );

        $('#totalProductPrice b').html('Rp. '+tpp.toLocaleString('ID'));
        $('#totalExpeditionPrice b').html('Rp. '+tep.toLocaleString('ID'));
        $('#subGrandTotal b').html('Rp. '+(tpp + tep).toLocaleString('ID'));
        $('.grandTotal').html('Rp. '+(tpp + tep).toLocaleString('ID'));
        // var tc = totalCart();

        // $('.totalCart').text(tc.toLocaleString('ID'));

        $('#exampleModalCenter').modal('hide');

    });

    $('#exampleModalCenter').on('show.bs.modal', function (event) {
        var button      = $(event.relatedTarget); 
        var recipient   = button.data('val'); 
        var modal       = $(this);
        var vendorid    = recipient.substr(9);
        modal.find('.modal-body #colVal').val(recipient);

        $.post(ownLink+'/front/home/getCourierList',{id : vendorid},function(res){
            
            $('.courier').html(res);

        });

    });
    
    $(".chosen").each(function(){
        $(this).chosen({
            no_results_text: "Oops, nothing found!",
            width: "100%"
        });
    })

    $(document).on('click','.paymentMethod',function(){

        var konfirmasi = confirm("Apakah anda sudah yakin?");

        if(konfirmasi === true) {

            var ic = orderCode();
            

            var data = {
                invoiceCode : ic,
                addressID : $('.addressId').val(),
                userID : $('.userId').val(),
                total_product_rate : spCharRemove($('#totalProductPrice b').text()),
                total_expedition_rate : spCharRemove($('#totalExpeditionPrice b').text()),
                sub_total : spCharRemove($('#subGrandTotal b').text()),
                discount : spCharRemove($('.discVoucherV b').text()),
                grand_total : spCharRemove($('.grandTotal').text())
            }

            var un = uniqueNumber(data.grand_total);

            if(data.total_expedition_rate > 0){
                $('#invoiceCode').val(data.invoiceCode);
                $('#addressID').val(data.addressID);
                $('#userID').val(data.userID);
                $('#total_product_rateCO').val(data.total_product_rate);
                $('#total_expedition_rateCO').val(data.total_expedition_rate);
                $('#sub_totalCO').val(data.sub_total);
                $('#discountCO').val(data.discount);
                $('#grand_totalCO').val(un);

                $('#coForm').submit();
            } else{

                alert('Something when wrong, check your courier service!!');
            
            }

            // $.post(ownLink + 'front/home/deleteCart/',data,function(x){
            //     var a = JSON.parse(x);
            //     alert(a.msg);
            //     tis.parent().parent().remove();
            //     if($('#cartInfo span').length > 0){
            //         var cartInfo = parseInt($('#cartInfo span').text()) - 1;
            //         if(cartInfo == 0){
            //             $('#cartInfo span').remove();
            //         } else {
            //             $('#cartInfo span').text(cartInfo);
            //         }
                    
            //     }
            //     var tc = totalCart();

            //     $('.totalCart').text('Rp. '+tc.toLocaleString('ID'));
            // });

        }

    });

    $('.coupon-btn').click(function(){

        $.blockUI({ css: { 
            border: 'none', 
            padding: '15px', 
            backgroundColor: '#000', 
            '-webkit-border-radius': '10px', 
            '-moz-border-radius': '10px', 
            opacity: .5, 
            color: '#fff' 
        } });

        if($('.coupon-text').val() != '' ){
            $.post(ownLink+'/front/home/checkCoupon',{id:$('.coupon-text').val()},function(r){
                var res = JSON.parse(r);
                if(res.s){

                    var a = discPriceTotal(res.p);
                    if(a > 0){
                        $('.couponel').show();
                        $('.discVoucherV b').html("Rp. "+parseInt(res.p).toLocaleString('ID'));
                        $('.grandTotal').html("Rp. "+a.toLocaleString('ID'));
                        setTimeout($.unblockUI, 1000);
                    } else {
                        alert('LOL Something Wrong!!');
                        setTimeout($.unblockUI, 1000);
                    }
                } else {
                    alert(res.m);
                    setTimeout($.unblockUI, 1000);
                }
            });
        } else {
            alert('Empty Text!!!');
            setTimeout($.unblockUI, 1000);
        }
        
    });

});

// FUNCTION HELPER

function discPriceTotal(p){

    var a = spCharRemove($('#subGrandTotal').text());
    var b = parseInt(a - p);

    return b;
}

function cartid(cols){

    var sum = '';

    $('#'+cols+' .cartid').each(function(){
        if(sum != ''){
           sum += ','+$(this).val(); 
        } else {
            sum += $(this).val(); 
        }
        
    });

    return sum;

}

function uniqueNumber(gt){
    
    var result = null;

    $.ajax({
        url: ownLink +'/front/home/uniqueNumber/'+gt,
        type: 'get',
        dataType: 'html',
        async: false,
        success: function(data) {
            result = JSON.parse(data);
        } 
     });

    return result;
}

function orderCode(){
    
    var result = null;

    $.ajax({
        url: ownLink +'/front/home/orderCode',
        type: 'get',
        dataType: 'html',
        async: false,
        success: function(data) {
            result = JSON.parse(data);
        } 
     });

    return result;
}

function spCharRemove(str){
    var a = parseInt($.trim(str).replace(/[\. ,:-]+/g, "").substr($.trim(str).replace(/[\. ,:-]+/g, "").indexOf("Rp") + 2));
    return a;
}

function totalEachVendor(cols){

    var sum = 0;

    $('#'+cols+' .subTtl_item').each(function(){
        sum += spCharRemove($(this).text());
    });

    return sum;

}

function totalProductPrice(){

    var sum = 0;

    $('.subTtl_item').each(function(){
        sum += spCharRemove($(this).text());
    });

    return sum;

}

function totalExpeditionPrice(){

    var sum = 0;

    $('.ces').each(function(){
        sum += $(this).data('cost');
    });

    return sum;
}

function totalWeight(cols){

    var sum = 0;

    $('#'+cols+' .weight').each(function(){
        sum += parseInt($(this).val());
    });

    return sum;

}

// function totalCart(){
//     var sum = 0;

//     $('.subTtl_item').each(function(){
//         var col = '#'+$(this).parent().parent().parent()[0].id;
//         if($(col).find('.checksp').is(':checked')){
//             sum += +parseInt(spCharRemove( $(this).text()));
//         }
//     });

//     return sum;
// }