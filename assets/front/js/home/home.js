var ownLink = $('#ownLink').text();

$(document).on('click','.wishlistButton',function(e){
    e.preventDefault();
    var tis = $(this);
    $.post(ownLink + 'front/home/wishlist/',{barang_id : $(this).data('id')},function(x){
        if(x == '') window.location.href = ownLink + 'login';
        var a = JSON.parse(x);
        if(a.status == 200){
            alert('Barang berhasil di tambahkan ke wishlist anda');
            $("."+tis[0].className+" span").removeClass();
            $("."+tis[0].className+" span").addClass('fa fa-heart');
            $("."+tis[0].className+" span").css('color','#c1355e');

            if($('#wishInfo span').length > 0){
                var wishInfo = parseInt($('#wishInfo span').text()) + 1;
                $('#wishInfo span').text(wishInfo);
            } else {
                $('#wishInfo').append('<span>1</span>');
            }

        } else if(a.status == 202) {
            alert(a.msg);
        } else {
            alert('Something Wrong Chat contact our customer service :)');
        }
    });
})

$(document).on('click','.cartButton',function(e){
    e.preventDefault();
    var data = {
        barang_id : $(this).data('id'),
        vendor_id : $(this).data('vendor'),
    };

    $.post(ownLink + 'front/home/cart/',data,function(x){

        var a = JSON.parse(x);

        if(x == '') window.location.href = ownLink + 'login';

        if(a.msg == 200) { 
            alert('Barang berhasil di tambahkan ke dalam keranjang');
            
            if($('#cartInfo span').length > 0){

                var cartInfo = parseInt($('#cartInfo span').text()) + 1;
                $('#cartInfo span').text(cartInfo);
            } else {
                $('#cartInfo').append('<span>1</span>');
            }
        } else {
            alert('Something Wrong Chat contact our customer service :)');
        }
    });
});