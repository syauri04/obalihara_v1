var ownLink = $('#ownLink').text();

$(document).ready(function(){
    if($('#ukuran_id option').length == 0){
        $('#ukuran_id').hide();
    }
});

$(document).on('click','#wishlistButton',function(e){
    e.preventDefault();
    var tis = $(this);
    $.post(ownLink + 'front/home/wishlist/',{barang_id : $('#barang_id').val()},function(x){
        if(x == '') window.location.href = ownLink + 'login';
        var a = JSON.parse(x);
        if(a.status == 200){
            alert('Barang berhasil di tambahkan ke wishlist anda');
            $("#"+tis[0].id+" i").removeClass();
            $("#"+tis[0].id+" i").addClass('fa fa-heart pull-right');
            $("#"+tis[0].id+" i").css('color','#c1355e').css('font-size','25px').css('margin-top','5px');
        } else if(a.status == 202) {
            alert(a.msg);
        } else {
            alert('Something Wrong Chat contact our customer service :)');
        }
    });
})

$(document).on('click','#warnaPicker',function(){

    $('.colour-pict').css("border","unset");
    $('#warna_id').val($(this).data('id'));
    $(this).css('border','3px solid #cccccc');

});

$(document).on('click','#cartButton',function(){

    var ukuran_id = ($('#ukuran_id option').length > 0) ? $('#ukuran_id').val() : 0;

    var data = {
        barang_id : $('#barang_id').val(),
        vendor_id : $('#vendor_id').val(),
        warna_id : $('#warna_id').val(),
        ukuran_id : ukuran_id,
        discount : $('#discount').val(),
    };

    if($('#warna_id').val() != '' && $('#colour-prev div').length > 0){
    
        $.post(ownLink + 'front/home/cart/',data,function(x){

            var a = JSON.parse(x);

            if(a.msg == 200) { 
                // window.location.href = ownLink + 'cart/gc?id=' + a.user_id;
                alert('Barang berhasil di tambahkan ke dalam keranjang');

                if($('#cartInfo span').length > 0){

                    var cartInfo = parseInt($('#cartInfo span').text()) + 1;
                    $('#cartInfo span').text(cartInfo);
                } else {
                    $('#cartInfo').append('<span>1</span>');
                }

            } else if(a == '500') {
                 window.location.href = ownLink + 'login'
            } else {
                alert('Something Wrong Chat contact our customer service :)');
            }
        });
    } else {
        alert('Silahkan Pilih warna!!');
    }
})

$(document).on('click','#buyButton',function(){

    var data = {
        barang_id : $('#barang_id').val(),
        vendor_id : $('#vendor_id').val(),
        warna_id : $('#warna_id').val(),
        ukuran_id : $('#ukuran_id').val(),
    };

    if($('#warna_id').val() != '' && $('#colour-prev div').length > 0){
        $.post(ownLink + 'front/home/cart/',data,function(x){

            var a = JSON.parse(x);

            if(a == '500') window.location.href = ownLink + 'login';

            if(a.msg == 200) { 
                window.location.href = ownLink + 'cart/gc?id=' + a.user_id;
            } else if(a == '500') {
                alert('Please do login');
            } else {
                alert('Something Wrong Chat contact our customer service :)');
            }
        });
    } else {
        alert('Silahkan Pilih warna!!');
    }
})

var productDetail = function () {
	"use strict";
    return {
        //main function
        init: function () {
            handleActionForm();
        }
    };
}();