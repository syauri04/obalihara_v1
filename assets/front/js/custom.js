$(document).ready(function () {
    
    var $easyzoom = $('.easyzoom').easyZoom();
    // Setup thumbnails example
    var api1 = $easyzoom.filter('.easyzoom--with-thumbnails').data('easyZoom');

    $('.thumbnails').on('click', 'a', function (e) {
        var $this = $(this);
        e.preventDefault();
        // Use EasyZoom's `swap` method
        api1.swap($this.data('standard'), $this.attr('href'));
    });

    var hash = window.location.hash;
    hash && $('ul.nav a[href="' + hash + '"]').tab('show');

    $('.nav-tabs a').click(function (e) {
        $(this).tab('show');
        var scrollmem = $('body').scrollTop() || $('html').scrollTop();
        window.location.hash = this.hash;
        $('html,body').scrollTop(scrollmem);
    });
    // $('.data-table').DataTable();
    $("#uploadForm").on('submit', (function (e) {
        e.preventDefault();
        $.ajax({
            url: "upload.php",
            type: "POST",
            data: new FormData(this),
            beforeSend: function () { $("#body-overlay").show(); },
            contentType: false,
            processData: false,
            success: function (data) {
                $("#targetLayer").html(data);
                $("#targetLayer").css('opacity', '1');
                setInterval(function () { $("#body-overlay").hide(); }, 500);
            },
            error: function () {
            }
        });
    }));
    // $('.select2').select2();
});

function showPreview(objFileInput) {
    if (objFileInput.files[0]) {
        var fileReader = new FileReader();
        fileReader.onload = function (e) {
            $("#targetLayer").html('<img src="' + e.target.result + '" width="200px" height="200px" class="upload-preview" />');
            $("#targetLayer").css('opacity', '0.7');
            $(".icon-choose-image").css('opacity', '0.5');
        }
        fileReader.readAsDataURL(objFileInput.files[0]);
    }
}

function add_shipping(){
    var html='';
    $("#box-shipping").appendTo(html);
}