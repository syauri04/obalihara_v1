var ownLink = $('#ownLink').text();

$(document).ready(function(){

    $('.provinsiID').each(function(){
        if($(this).val() != ''){
            var idParent = '#'+$(this).parent().parent().parent().parent().parent()[0].id;
            $(idParent+' .kotaID').hide();
            $.post(ownLink+'front/buyer/getCity',{ id: $(this).val() , k: $(idParent+' .kotaID').data('id') }).done(function(x){
                $(idParent+' .kotaID').show();
                $(idParent+' .kotaID').html(x);
                $(idParent+' .kotaID').trigger("chosen:updated");
            });
        }
    });
    
    $('.kotaID').each(function(){
        if($(this).val() != ''){
            var idParent = '#'+$(this).parent().parent().parent().parent().parent()[0].id;
            $(idParent+' .kecID').hide();
            $.post(ownLink+'front/buyer/getSubdistrict',{ id: $(this).data('id') , k: $(idParent+' .kecID').data('id') }).done(function(x){
                $(idParent+' .kecID').show();
                $(idParent+' .kecID').html(x);
                $(idParent+' .kecID').trigger("chosen:updated");
            });
        }
    });

});

$(document).on('change','.provinsiID',function(){
    
    var idParent = '#'+$(this).parent().parent().parent().parent().parent()[0].id;
    $.post(ownLink+'front/buyer/getCity',{ id: $(this).val() }).done(function(x){
        $(idParent+' .kotaID').show();
        $(idParent+' .kotaID').html(x);
        $(idParent+' .kotaID').trigger("chosen:updated");
    });
})

$(document).on('change','#kotaID',function(){
    
    var idParent = '#'+$(this).parent().parent().parent().parent().parent()[0].id;
    $(idParent+' .kecID').hide();
    $.post(ownLink+'front/buyer/getSubdistrict',{ id: $(this).val() }).done(function(x){
        $(idParent+' .kecID').show();
        $(idParent+' .kecID').html(x);
        $(idParent+' .kecID').trigger("chosen:updated");
    });
})

$(document).on('click','.cloneAdd',function(){
    
    var $div    = $('div[id^="box-shipping-"]:last');
    var num     = parseInt( $div.prop("id").match(/\d+/g), 10 ) +1;
    var $klon   = $div.clone().prop('id', 'box-shipping-'+num ).find("input, select").val("").end();

    $div.after( $klon );

    // $('#box-shipping-1').clone().;
})

$(document).on('click','.deleteAddress',function(){
    
    var id = $(this).data('id');
    $.post(ownLink+'front/buyer/deleteAddress',{ id: id }).done(function(x){
        alert(x);
        $('.addId'+id).remove();
    });
})



// $(document).on('click','#saveSetting',function(){
//     $('#settingForm').submit();
// })


// var productDetail = function () {
// 	"use strict";
//     return {
//         //main function
//         init: function () {
//             handleActionForm();
//         }
//     };
// }();