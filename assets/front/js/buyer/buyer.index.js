var ownLink = $('#ownLink').text();

$(document).on('click','.btnTerima',function(){
  alert('Barang Di Terima');
  $.post(ownLink+'front/buyer/terimaBarang',{cartid : $('#cartid').val()},function(res){
    $('#trekingModal').modal('hide');
  });
});

$(document).on('click','.btnTracking',function(e){
  e.preventDefault();

  $.blockUI({ css: { 
      border: 'none', 
      padding: '15px', 
      backgroundColor: '#000', 
      '-webkit-border-radius': '10px', 
      '-moz-border-radius': '10px', 
      opacity: .5, 
      color: '#fff' 
  } });

  var tis = $(this);
  var d = {

    receipt : $(this).data('receipt'),
    courier : $(this).data('courier')

  }

  if( tis.data('status') > 4) {
    $('.btnTerima').hide();
  }

  $.post(ownLink + 'front/buyer/cekResi',d,function(r){
    
    var parse = JSON.parse(r);
    var kode  = parse.rajaongkir.status.code;
    var res   = parse.rajaongkir.result;
    var html  = '';
    if(kode != 400){

      if(res.manifest.length > 0){

        $('.resiStatus').html('Status Resi : '+res.delivery_status.status+','+res.delivery_status.pod_receiver+' '+res.delivery_status.pod_date+' '+res.delivery_status.pod_time);

        $.each(res.manifest.reverse(),function(i,v){
          html += '<ul>';
          html += '<li>'+res.manifest[i].manifest_date+' , '+res.manifest[i].manifest_time+'</li>';
          html += '<li>'+res.manifest[i].manifest_description+'</li>';
          html += '</ul>';
        });

      } else {

        html += '<ul>';
        html += '<li>';
        html += res.delivery_status.status;
        html += '</li>';
        html += '</ul>';

      }

      $('.manifest').html(html);
      $('#cartid').val(tis.data('cartid'));
      $('#trekingModal').modal('show');

      setTimeout($.unblockUI, 200);

    } else {

      alert('Seller Has not update waybill receipt!!');
      setTimeout($.unblockUI);

    }

  })

});

// $(document).ready(function(){
//   $('.paymentPage').load('https://invoice-staging.xendit.co/web/invoices/5d665d0e3230f753835eded2');
// });