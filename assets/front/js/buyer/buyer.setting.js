var ownLink = $('#ownLink').text();

$(document).ready(function(){

    if($('#provinsiID').val() != ''){
        $.post(ownLink+'front/buyer/getCity',{ id: $('#provinsiID').val() , k: $('#kotaID').data('id') },function(x){
            $('#kotaID').html(x);
            $('#kotaID').trigger("chosen:updated");
        });
    }

    if($('#kotaID').data('id') != ''){
        $('#kecID').hide();
        $.post(ownLink+'front/buyer/getSubdistrict',{ id: $('#kotaID').data('id') , k: $('#kecID').data('id') }).done(function(x){
            $('#kecID').show();
            $('#kecID').html(x);
            $('#kecID').trigger("chosen:updated");
        });
    }

});

$(document).on('change','#provinsiID',function(){
    $.post(ownLink+'front/buyer/getCity',{ id: $(this).val() },function(x){
        $('#kotaID').html(x);
        $('#kotaID').trigger("chosen:updated");
    });
})

$(document).on('change','#kotaID',function(){
    $('#kecID').hide();
    $.post(ownLink+'front/buyer/getSubdistrict',{ id: $(this).val() }).done(function(x){
        $('#kecID').show();
        $('#kecID').html(x);
        $('#kecID').trigger("chosen:updated");
    });
})

Dropzone.options.demoUpload1 = {
  paramName: "file", // The name that will be used to transfer the file
  maxFiles: 1,
  maxFilesize: 6, // MB
  paramName: 'userImage',
  init: function(){
    this.on("sending", function(file, xhr, formData) {
       var value = $('form#demoUpload1 #key').val();
       formData.append("key", value); // Append all the additional input data of your form here!
    });
    this.on("maxfilesexceeded", function(file){
       alert("No more files please!");
    });
    this.on('success', function() {
      setTimeout(function(){
        location.reload();
      },2000);
    });
  }
};

// Dropzone.options.demoUpload1 = {
//     autoProcessQueue: false,
//     url: 'upload_files.php',
//     init: function () {

//         var myDropzone = this;

//         // Update selector to match your button
//         $("#button").click(function (e) {
//             e.preventDefault();
//             myDropzone.processQueue();
//         });

//         this.on('sending', function(file, xhr, formData) {
//             // Append all form inputs to the formData Dropzone will POST
//             var data = $('#demoUpload1').serializeArray();
//             $.each(data, function(key, el) {
//                 formData.append(el.name, el.value);
//             });
//         });
//     }
// }
