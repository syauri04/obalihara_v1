-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3307
-- Generation Time: Jun 18, 2019 at 04:10 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ciadmin`
--

-- --------------------------------------------------------

--
-- Table structure for table `cp_app_acl_accesses`
--

CREATE TABLE `cp_app_acl_accesses` (
  `acc_id` int(11) UNSIGNED NOT NULL,
  `acc_group` varchar(255) DEFAULT NULL,
  `acc_menu` varchar(255) NOT NULL,
  `acc_group_controller` varchar(255) NOT NULL,
  `acc_controller_name` varchar(255) NOT NULL,
  `acc_access_name` varchar(255) DEFAULT NULL,
  `acc_description` varchar(255) NOT NULL,
  `acc_by_order` int(11) DEFAULT NULL,
  `app_id` int(5) NOT NULL,
  `acc_css_class` varchar(50) NOT NULL,
  `acc_isshow` char(1) NOT NULL DEFAULT '1',
  `acc_active` int(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_app_acl_accesses`
--

INSERT INTO `cp_app_acl_accesses` (`acc_id`, `acc_group`, `acc_menu`, `acc_group_controller`, `acc_controller_name`, `acc_access_name`, `acc_description`, `acc_by_order`, `app_id`, `acc_css_class`, `acc_isshow`, `acc_active`) VALUES
(1, 'home', 'Dashboard > Home', 'welcome', 'welcome', 'Home', 'Home', 1, 1, 'fa fa-laptop', '1', 1),
(2, 'meme', 'Admin & Setting > Group User', 'meme', 'group', 'Admin & Setting', 'Group Access', 103, 1, 'fa fa-cog', '1', 1),
(3, 'meme', 'Admin & Setting > User & Contact', 'meme', 'user', 'Users', 'User', 104, 1, '', '0', 1),
(4, 'meme', 'Admin & Setting > Setting', 'meme', 'config', 'Configuration', 'Configuration', 102, 1, 'fa fa-cog', '0', 1),
(59, 'meme', 'Admin & Setting > News', 'meme', 'news', 'News', 'News', 109, 1, 'fa fa-laptop', '1', 1),
(63, 'meme', 'Admin & Setting > Company', 'meme', 'company', 'Company', 'Company', 105, 1, '', '0', 1),
(64, 'meme', 'Admin & Setting > Department', 'meme', 'department', 'Department', 'Department', 106, 1, '', '0', 1),
(65, 'meme', 'Admin & Setting > Topic', 'meme', 'topic', 'Topic', 'Topic', 107, 1, '', '1', 1),
(66, 'meme', 'Admin & Setting > To do list', 'meme', 'todolist', 'Todolist', 'Todolist', 108, 1, '', '0', 0),
(67, 'home', 'Dashboard > News', 'home', 'news', 'News', 'News', 2, 1, '', '1', 1),
(68, 'home', 'Dashboard > To do List', 'home', 'todolist', 'To do List', 'To do List', 3, 1, '', '0', 1),
(138, 'home', 'Master > Banner Image', 'master', 'banner', 'Banner Image', 'Banner Image', 5, 1, '', '1', 1),
(137, 'home', 'Master > Coupon', 'master', 'coupon', 'Coupon', 'Coupon', 5, 1, '', '1', 1),
(136, 'report', 'Report > Sales', 'report', 'report_sales', 'Report Sales', 'Report Sales', 72, 1, '', '1', 1),
(135, 'report', 'Report > Buyer', 'report', 'report_buyer', 'Report Buyer', 'Report Buyer', 71, 1, '', '1', 1),
(134, 'report', 'Report > Vendor', 'report', 'report_vendor', 'Report Vendor', 'Report Vendor', 50, 1, 'fa fa-book', '1', 1),
(133, 'home', 'Master > Menu', 'master', 'menu_front', 'Menu Front', 'Menu Front', 8, 1, '', '1', 1),
(132, 'home', 'Clients > Buyer', 'clients', 'buyer', 'Buyer', 'Buyer', 22, 1, 'fa fa-address-book', '1', 1),
(131, 'home', 'Clients > Vendor', 'clients', 'vendor', 'Vendor', 'Vendor', 21, 1, 'fa fa-address-book', '1', 1),
(130, 'home', 'Master > Kategori Barang', 'master', 'kategori', 'Kategori', 'Kategori', 7, 1, '', '1', 1),
(128, 'home', 'Master > Merek', 'master', 'merek', 'Merk', 'Merk', 5, 1, '', '0', 1),
(129, 'home', 'Master > Size', 'master', 'size', 'Size', 'Size', 6, 1, '', '0', 1),
(127, 'pos', 'POS > POS', 'pos', 'pos', 'POS', 'POS', 5, 1, 'fa fa-shopping-basket', '0', 1),
(126, 'home', 'Master > Member', 'master', 'member', 'Member', 'Member', 42, 1, '', '0', 1),
(125, 'home', 'Master > Supplier', 'master', 'supplier', 'Supplier', 'Supplier', 41, 1, '', '0', 1),
(124, 'home', 'Master > Barang', 'master', 'goods', 'Goods', 'Goods', 4, 1, 'fa fa-th-large', '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cp_app_acl_access_actions`
--

CREATE TABLE `cp_app_acl_access_actions` (
  `aca_id` int(11) UNSIGNED NOT NULL,
  `aca_access_id` int(11) NOT NULL,
  `aca_action_id` int(11) NOT NULL,
  `app_id` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_app_acl_access_actions`
--

INSERT INTO `cp_app_acl_access_actions` (`aca_id`, `aca_access_id`, `aca_action_id`, `app_id`) VALUES
(1, 1, 1, 1),
(2, 2, 1, 1),
(3, 2, 2, 1),
(4, 2, 3, 1),
(5, 2, 4, 1),
(6, 2, 6, 1),
(7, 3, 1, 1),
(8, 3, 2, 1),
(9, 3, 3, 1),
(10, 3, 4, 1),
(11, 4, 1, 1),
(249, 5, 1, 1),
(250, 5, 2, 1),
(251, 5, 3, 1),
(252, 5, 4, 1),
(253, 6, 1, 1),
(254, 6, 2, 1),
(255, 6, 3, 1),
(256, 6, 4, 1),
(257, 7, 1, 1),
(258, 7, 2, 1),
(259, 7, 3, 1),
(260, 7, 4, 1),
(261, 8, 1, 1),
(262, 8, 2, 1),
(263, 8, 3, 1),
(264, 8, 4, 1),
(265, 9, 1, 1),
(266, 9, 2, 1),
(267, 9, 3, 1),
(268, 9, 4, 1),
(269, 10, 1, 1),
(270, 10, 2, 1),
(271, 10, 3, 1),
(272, 10, 4, 1),
(273, 11, 1, 1),
(274, 11, 2, 1),
(275, 11, 3, 1),
(276, 11, 4, 1),
(277, 12, 1, 1),
(278, 12, 2, 1),
(279, 12, 3, 1),
(280, 12, 4, 1),
(281, 58, 1, 1),
(282, 58, 2, 1),
(283, 58, 3, 1),
(284, 58, 4, 1),
(285, 9, 5, 1),
(286, 59, 1, 1),
(287, 59, 2, 1),
(288, 60, 1, 1),
(289, 60, 2, 1),
(290, 61, 1, 1),
(291, 61, 2, 1),
(292, 63, 1, 1),
(293, 63, 2, 1),
(294, 63, 3, 1),
(295, 63, 4, 1),
(307, 65, 4, 1),
(306, 65, 3, 1),
(305, 65, 2, 1),
(304, 65, 1, 1),
(300, 64, 1, 1),
(301, 64, 2, 1),
(302, 64, 3, 1),
(303, 64, 4, 1),
(308, 63, 7, 1),
(309, 66, 1, 1),
(310, 66, 2, 1),
(311, 66, 3, 1),
(312, 66, 4, 1),
(313, 6, 5, 1),
(314, 6, 8, 1),
(315, 59, 3, 1),
(316, 59, 4, 1),
(317, 67, 1, 1),
(318, 68, 1, 1),
(319, 68, 2, 1),
(320, 68, 3, 1),
(321, 68, 4, 1),
(573, 138, 5, 1),
(572, 138, 4, 1),
(571, 138, 3, 1),
(570, 138, 2, 1),
(569, 138, 1, 1),
(568, 136, 1, 1),
(567, 136, 5, 1),
(566, 136, 4, 1),
(565, 136, 3, 1),
(564, 136, 2, 1),
(563, 135, 1, 1),
(562, 135, 5, 1),
(561, 135, 4, 1),
(560, 135, 3, 1),
(559, 135, 2, 1),
(558, 134, 5, 1),
(557, 134, 4, 1),
(556, 134, 3, 1),
(555, 134, 2, 1),
(554, 134, 1, 1),
(553, 137, 5, 1),
(552, 137, 4, 1),
(551, 137, 3, 1),
(550, 137, 2, 1),
(549, 137, 1, 1),
(548, 133, 5, 1),
(547, 133, 4, 1),
(546, 133, 3, 1),
(545, 133, 2, 1),
(544, 133, 1, 1),
(543, 132, 5, 1),
(542, 132, 4, 1),
(541, 132, 3, 1),
(540, 132, 2, 1),
(539, 132, 1, 1),
(538, 131, 5, 1),
(537, 131, 4, 1),
(536, 131, 3, 1),
(535, 131, 2, 1),
(534, 131, 1, 1),
(533, 130, 5, 1),
(532, 130, 4, 1),
(531, 130, 3, 1),
(530, 130, 2, 1),
(529, 130, 1, 1),
(528, 129, 5, 1),
(527, 129, 4, 1),
(526, 129, 3, 1),
(525, 129, 2, 1),
(524, 129, 1, 1),
(523, 128, 5, 1),
(522, 128, 4, 1),
(521, 128, 3, 1),
(520, 128, 2, 1),
(519, 128, 1, 1),
(518, 127, 5, 1),
(517, 127, 4, 1),
(516, 127, 3, 1),
(515, 127, 2, 1),
(514, 127, 1, 1),
(513, 126, 5, 1),
(512, 126, 4, 1),
(511, 126, 3, 1),
(510, 126, 2, 1),
(509, 126, 1, 1),
(508, 125, 5, 1),
(507, 125, 4, 1),
(506, 125, 3, 1),
(505, 125, 2, 1),
(504, 125, 1, 1),
(503, 124, 5, 1),
(502, 124, 4, 1),
(501, 124, 3, 1),
(500, 124, 2, 1),
(499, 124, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cp_app_acl_actions`
--

CREATE TABLE `cp_app_acl_actions` (
  `ac_id` int(11) UNSIGNED NOT NULL,
  `ac_action_name` varchar(255) NOT NULL,
  `ac_action` varchar(255) NOT NULL,
  `ac_action_image` varchar(255) DEFAULT NULL,
  `app_id` int(5) DEFAULT NULL,
  `ac_action_type` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_app_acl_actions`
--

INSERT INTO `cp_app_acl_actions` (`ac_id`, `ac_action_name`, `ac_action`, `ac_action_image`, `app_id`, `ac_action_type`) VALUES
(1, 'Search', 'index', 'list.png', 1, 'normal'),
(2, 'Add', 'add', 'add.png', 1, 'normal'),
(3, 'Edit', 'edit', 'edit', 1, 'normal'),
(4, 'Delete', 'delete', 'trash', 1, 'confirm'),
(5, 'Detail', 'detail', 'detail.png', 1, 'normal'),
(6, 'Access', 'access', 'access.png', 1, 'normal'),
(7, 'Department', 'department', 'detail.png', 1, 'normal'),
(8, 'Upload', 'upload', 'add.png', 1, 'normal');

-- --------------------------------------------------------

--
-- Table structure for table `cp_app_acl_group`
--

CREATE TABLE `cp_app_acl_group` (
  `ag_id` int(11) UNSIGNED NOT NULL,
  `ag_group_name` varchar(100) NOT NULL DEFAULT '0',
  `ag_group_desc` varchar(255) NOT NULL DEFAULT '',
  `ag_group_status` int(1) NOT NULL DEFAULT '0',
  `app_id` int(5) DEFAULT NULL,
  `ag_group_template` varchar(255) DEFAULT NULL,
  `is_trash` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_app_acl_group`
--

INSERT INTO `cp_app_acl_group` (`ag_id`, `ag_group_name`, `ag_group_desc`, `ag_group_status`, `app_id`, `ag_group_template`, `is_trash`) VALUES
(1, 'administrator', 'Administrator', 1, 1, 'default', 0),
(9, 'Atribute', 'Atribute', 1, NULL, NULL, 1),
(15, 'Admin Document', 'Admin Document', 1, NULL, NULL, 0),
(16, 'View', 'View Only', 1, NULL, NULL, 0),
(17, 'sales', 'Sales', 1, NULL, NULL, 0),
(18, 'sales admin', 'Sales Admin', 1, NULL, NULL, 0),
(19, 'Buyer', 'Buyer', 1, NULL, NULL, 0),
(20, 'Vendor', 'Vendor', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cp_app_acl_group_accesses`
--

CREATE TABLE `cp_app_acl_group_accesses` (
  `aga_id` int(11) UNSIGNED NOT NULL,
  `aga_access_id` int(11) NOT NULL,
  `aga_group_id` int(11) NOT NULL,
  `app_id` int(5) DEFAULT NULL,
  `aga_action_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_app_acl_group_accesses`
--

INSERT INTO `cp_app_acl_group_accesses` (`aga_id`, `aga_access_id`, `aga_group_id`, `app_id`, `aga_action_id`) VALUES
(3365, 67, 15, NULL, 1),
(3364, 1, 15, NULL, 1),
(3366, 68, 15, NULL, 1),
(3367, 68, 15, NULL, 2),
(3368, 68, 15, NULL, 3),
(3369, 68, 15, NULL, 4),
(3371, 6, 15, NULL, 1),
(3372, 6, 15, NULL, 2),
(3373, 6, 15, NULL, 3),
(3374, 6, 15, NULL, 4),
(3375, 6, 15, NULL, 5),
(3376, 6, 15, NULL, 8),
(3377, 8, 15, NULL, 1),
(3378, 8, 15, NULL, 2),
(3379, 8, 15, NULL, 3),
(3380, 8, 15, NULL, 4),
(3500, 8, 16, NULL, 4),
(3499, 8, 16, NULL, 3),
(3498, 8, 16, NULL, 2),
(3497, 8, 16, NULL, 1),
(3496, 6, 16, NULL, 8),
(3495, 6, 16, NULL, 5),
(3494, 6, 16, NULL, 4),
(3493, 6, 16, NULL, 3),
(3492, 6, 16, NULL, 2),
(3491, 6, 16, NULL, 1),
(3489, 68, 16, NULL, 4),
(3488, 68, 16, NULL, 3),
(3487, 68, 16, NULL, 2),
(3486, 68, 16, NULL, 1),
(3485, 67, 16, NULL, 1),
(3484, 1, 16, NULL, 1),
(3505, 9, 16, NULL, 1),
(3506, 9, 16, NULL, 2),
(3507, 9, 16, NULL, 3),
(3508, 9, 16, NULL, 4),
(3509, 9, 16, NULL, 5),
(7790, 136, 1, NULL, 5),
(7786, 136, 1, NULL, 1),
(7789, 136, 1, NULL, 4),
(7787, 136, 1, NULL, 2),
(7602, 1, 20, NULL, 1),
(7601, 1, 19, NULL, 1),
(7758, 127, 1, NULL, 3),
(7757, 127, 1, NULL, 2),
(7756, 127, 1, NULL, 1),
(7755, 125, 1, NULL, 5),
(7788, 136, 1, NULL, 3),
(7795, 134, 1, NULL, 5),
(7794, 134, 1, NULL, 4),
(7793, 134, 1, NULL, 3),
(7792, 134, 1, NULL, 2),
(7791, 134, 1, NULL, 1),
(7800, 135, 1, NULL, 5),
(7796, 135, 1, NULL, 1),
(7754, 125, 1, NULL, 4),
(7753, 125, 1, NULL, 3),
(7752, 125, 1, NULL, 2),
(7797, 135, 1, NULL, 2),
(7798, 135, 1, NULL, 3),
(7799, 135, 1, NULL, 4),
(7803, 138, 1, NULL, 3),
(7804, 138, 1, NULL, 4),
(7802, 138, 1, NULL, 2),
(7801, 138, 1, NULL, 1),
(7805, 138, 1, NULL, 5),
(7765, 137, 1, NULL, 5),
(7764, 137, 1, NULL, 4),
(7763, 137, 1, NULL, 3),
(7762, 137, 1, NULL, 2),
(7761, 137, 1, NULL, 1),
(6672, 68, 17, NULL, 4),
(6671, 68, 17, NULL, 3),
(6670, 68, 17, NULL, 2),
(6669, 3, 17, NULL, 4),
(6668, 3, 17, NULL, 3),
(6667, 3, 17, NULL, 2),
(6666, 65, 17, NULL, 4),
(6665, 65, 17, NULL, 3),
(6664, 65, 17, NULL, 2),
(6663, 59, 17, NULL, 4),
(6662, 59, 17, NULL, 3),
(6661, 59, 17, NULL, 2),
(6660, 2, 17, NULL, 6),
(6659, 2, 17, NULL, 4),
(6658, 2, 17, NULL, 3),
(6657, 2, 17, NULL, 2),
(6656, 64, 17, NULL, 4),
(6655, 64, 17, NULL, 3),
(6654, 64, 17, NULL, 2),
(6653, 63, 17, NULL, 7),
(6652, 63, 17, NULL, 4),
(6651, 63, 17, NULL, 3),
(6650, 63, 17, NULL, 2),
(7751, 125, 1, NULL, 1),
(7750, 129, 1, NULL, 5),
(7749, 129, 1, NULL, 4),
(7760, 127, 1, NULL, 5),
(7759, 127, 1, NULL, 4),
(7748, 129, 1, NULL, 3),
(7747, 129, 1, NULL, 2),
(7746, 129, 1, NULL, 1),
(7745, 128, 1, NULL, 5),
(7744, 128, 1, NULL, 4),
(7743, 128, 1, NULL, 3),
(7742, 128, 1, NULL, 2),
(7741, 128, 1, NULL, 1),
(7740, 133, 1, NULL, 5),
(7739, 133, 1, NULL, 4),
(7738, 133, 1, NULL, 3),
(7737, 133, 1, NULL, 2),
(7736, 133, 1, NULL, 1),
(7735, 126, 1, NULL, 5),
(7734, 126, 1, NULL, 4),
(7733, 126, 1, NULL, 3),
(7732, 126, 1, NULL, 2),
(7731, 126, 1, NULL, 1),
(7730, 130, 1, NULL, 5),
(7729, 130, 1, NULL, 4),
(7728, 130, 1, NULL, 3),
(7727, 130, 1, NULL, 2),
(7726, 130, 1, NULL, 1),
(7725, 124, 1, NULL, 5),
(7724, 124, 1, NULL, 4),
(7723, 124, 1, NULL, 3),
(7722, 124, 1, NULL, 2),
(7721, 124, 1, NULL, 1),
(7720, 68, 1, NULL, 4),
(7719, 68, 1, NULL, 3),
(7718, 68, 1, NULL, 2),
(7717, 68, 1, NULL, 1),
(7716, 67, 1, NULL, 1),
(7715, 1, 1, NULL, 1),
(7714, 131, 1, NULL, 5),
(7713, 131, 1, NULL, 4),
(7712, 131, 1, NULL, 3),
(7711, 131, 1, NULL, 2),
(7710, 131, 1, NULL, 1),
(7709, 132, 1, NULL, 5),
(7708, 132, 1, NULL, 4),
(7707, 132, 1, NULL, 3),
(7706, 132, 1, NULL, 2),
(7705, 132, 1, NULL, 1),
(7704, 3, 1, NULL, 4),
(7703, 3, 1, NULL, 3),
(7702, 3, 1, NULL, 2),
(7701, 3, 1, NULL, 1),
(7700, 65, 1, NULL, 4),
(7699, 65, 1, NULL, 3),
(7698, 65, 1, NULL, 2),
(7697, 4, 1, NULL, 1),
(7696, 59, 1, NULL, 4),
(7695, 59, 1, NULL, 3),
(7694, 59, 1, NULL, 2),
(7693, 2, 1, NULL, 6),
(7692, 2, 1, NULL, 4),
(7691, 2, 1, NULL, 3),
(7690, 2, 1, NULL, 2),
(7689, 2, 1, NULL, 1),
(7688, 64, 1, NULL, 4),
(7341, 68, 18, NULL, 4),
(7340, 68, 18, NULL, 3),
(7339, 68, 18, NULL, 2),
(7687, 64, 1, NULL, 3),
(7686, 64, 1, NULL, 2),
(7685, 64, 1, NULL, 1),
(7684, 63, 1, NULL, 7),
(7338, 68, 18, NULL, 1),
(7337, 67, 18, NULL, 1),
(7336, 1, 18, NULL, 1),
(7335, 3, 18, NULL, 1),
(7334, 65, 18, NULL, 1),
(7333, 4, 18, NULL, 1),
(7332, 59, 18, NULL, 1),
(7331, 2, 18, NULL, 1),
(7330, 64, 18, NULL, 1),
(7329, 63, 18, NULL, 1),
(7683, 63, 1, NULL, 4),
(7682, 63, 1, NULL, 3),
(7681, 63, 1, NULL, 2),
(7680, 63, 1, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cp_app_city`
--

CREATE TABLE `cp_app_city` (
  `id` int(10) UNSIGNED NOT NULL,
  `provinsi_id` int(11) UNSIGNED NOT NULL,
  `kd_kota` varchar(10) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `nama_kota` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `kota_kab` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `keterangan` text CHARACTER SET latin1,
  `status` char(1) CHARACTER SET latin1 NOT NULL DEFAULT '1',
  `is_trash` char(1) CHARACTER SET latin1 NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `cp_app_city`
--

INSERT INTO `cp_app_city` (`id`, `provinsi_id`, `kd_kota`, `nama_kota`, `kota_kab`, `keterangan`, `status`, `is_trash`) VALUES
(1, 1, '1', 'Badung', 'Kab', '', '1', '0'),
(2, 1, '2', 'Bangli', 'Kab', '', '1', '0'),
(3, 1, '3', 'Buleleng', 'Kab', '', '1', '0'),
(4, 1, '4', 'Denpasar', 'Kota', '', '1', '0'),
(5, 1, '5', 'Gianyar', 'Kab', '', '1', '0'),
(6, 1, '6', 'Jembrana', 'Kab', '', '1', '0'),
(7, 1, '7', 'Karangasem', 'Kab', '', '1', '0'),
(8, 1, '8', 'Klungkung', 'Kab', '', '1', '0'),
(9, 1, '9', 'Tabanan', 'Kab', '', '1', '0'),
(10, 2, '1', 'Bangka', 'Kab', '', '1', '0'),
(11, 2, '2', 'Bangka Barat', 'Kab', '', '1', '0'),
(12, 2, '3', 'Bangka Selatan', 'Kab', '', '1', '0'),
(13, 2, '4', 'Bangka Tengah', 'Kab', '', '1', '0'),
(14, 2, '5', 'Belitung', 'Kab', '', '1', '0'),
(15, 2, '6', 'Belitung Timur', 'Kab', '', '1', '0'),
(16, 2, '7', 'Pangkal Pinang', 'Kota', '', '1', '0'),
(17, 3, '1', 'Cilegon', 'Kota', '', '1', '0'),
(18, 3, '2', 'Lebak', 'Kab', '', '1', '0'),
(19, 3, '3', 'Pandeglang', 'Kab', '', '1', '0'),
(20, 3, '4', 'Serang', 'Kab', '', '1', '0'),
(21, 3, '4', 'Serang', 'Kota', '', '1', '0'),
(22, 3, '5', 'Tangerang', 'Kab', '', '1', '0'),
(23, 3, '5', 'Tangerang', 'Kota', '', '1', '0'),
(24, 3, '6', 'Tangerang Selatan', 'Kota', '', '1', '0'),
(25, 4, '1', 'Bengkulu', 'Kota', '', '1', '0'),
(26, 4, '2', 'Bengkulu Selatan', 'Kab', '', '1', '0'),
(27, 4, '3', 'Bengkulu Tengah', 'Kab', '', '1', '0'),
(28, 4, '4', 'Bengkulu Utara', 'Kab', '', '1', '0'),
(29, 4, '5', 'Kaur', 'Kab', '', '1', '0'),
(30, 4, '6', 'Kepahiang', 'Kab', '', '1', '0'),
(31, 4, '7', 'Lebong', 'Kab', '', '1', '0'),
(32, 4, '8', 'Muko Muko', 'Kab', '', '1', '0'),
(33, 4, '9', 'Rejang Lebong', 'Kab', '', '1', '0'),
(34, 4, '10', 'Seluma', 'Kab', '', '1', '0'),
(35, 5, '1', 'Bantul', 'Kab', '', '1', '0'),
(36, 5, '2', 'Gunung Kidul', 'Kab', '', '1', '0'),
(37, 5, '3', 'Kulon Progo', 'Kab', '', '1', '0'),
(38, 5, '4', 'Sleman', 'Kab', '', '1', '0'),
(39, 5, '5', 'Yogyakarta', 'Kota', '', '1', '0'),
(40, 6, '1', 'Jakarta Barat', 'Kota', '', '1', '0'),
(41, 6, '2', 'Jakarta Pusat', 'Kota', '', '1', '0'),
(42, 6, '3', 'Jakarta Selatan', 'Kota', '', '1', '0'),
(43, 6, '4', 'Jakarta Timur', 'Kota', '', '1', '0'),
(44, 6, '5', 'Jakarta Utara', 'Kota', '', '1', '0'),
(45, 6, '6', 'Kepulauan Seribu', 'Kab', '', '1', '0'),
(46, 7, '1', 'Boalemo', 'Kab', '', '1', '0'),
(47, 7, '2', 'Bone Bolango', 'Kab', '', '1', '0'),
(48, 7, '3', 'Gorontalo', 'Kab', '', '1', '0'),
(49, 7, '3', 'Gorontalo', 'Kota', '', '1', '0'),
(50, 7, '4', 'Gorontalo Utara', 'Kab', '', '1', '0'),
(51, 7, '5', 'Pohuwato', 'Kab', '', '1', '0'),
(52, 8, '1', 'Batang Hari', 'Kab', '', '1', '0'),
(53, 8, '2', 'Bungo', 'Kab', '', '1', '0'),
(54, 8, '3', 'Jambi', 'Kota', '', '1', '0'),
(55, 8, '4', 'Kerinci', 'Kab', '', '1', '0'),
(56, 8, '5', 'Merangin', 'Kab', '', '1', '0'),
(57, 8, '6', 'Muaro Jambi', 'Kab', '', '1', '0'),
(58, 8, '7', 'Sarolangun', 'Kab', '', '1', '0'),
(59, 8, '9', 'Sungaipenuh', 'Kota', '', '1', '0'),
(60, 8, '10', 'Tanjung Jabung Barat', 'Kab', '', '1', '0'),
(61, 8, '11', 'Tanjung Jabung Timur', 'Kab', '', '1', '0'),
(62, 8, '12', 'Tebo', 'Kab', '', '1', '0'),
(63, 9, '1', 'Bandung', 'Kab', '', '1', '0'),
(64, 9, '1', 'Bandung', 'Kota', '', '1', '0'),
(65, 9, '2', 'Bandung Barat', 'Kab', '', '1', '0'),
(66, 9, '3', 'Bekasi', 'Kab', '', '1', '0'),
(67, 9, '3', 'Bekasi', 'Kota', '', '1', '0'),
(68, 9, '4', 'Bogor', 'Kab', '', '1', '0'),
(69, 9, '4', 'Bogor', 'Kota', '', '1', '0'),
(70, 9, '5', 'Ciamis', 'Kab', '', '1', '0'),
(71, 9, '6', 'Cianjur', 'Kab', '', '1', '0'),
(72, 9, '7', 'Cimahi', 'Kota', '', '1', '0'),
(73, 9, '8', 'Cirebon', 'Kab', '', '1', '0'),
(74, 9, '8', 'Cirebon', 'Kota', '', '1', '0'),
(75, 9, '9', 'Depok', 'Kota', '', '1', '0'),
(76, 9, '10', 'Garut', 'Kab', '', '1', '0'),
(77, 9, '11', 'Indramayu', 'Kab', '', '1', '0'),
(78, 9, '12', 'Karawang', 'Kab', '', '1', '0'),
(79, 9, '13', 'Kuningan', 'Kab', '', '1', '0'),
(80, 9, '14', 'Majalengka', 'Kab', '', '1', '0'),
(81, 9, '15', 'Pangandaran', 'Kab', '', '1', '0'),
(82, 9, '16', 'Purwakarta', 'Kab', '', '1', '0'),
(83, 9, '17', 'Subang', 'Kab', '', '1', '0'),
(84, 9, '18', 'Sukabumi', 'Kab', '', '1', '0'),
(85, 9, '18', 'Sukabumi', 'Kota', '', '1', '0'),
(86, 9, '19', 'Sumedang', 'Kab', '', '1', '0'),
(87, 9, '20', 'Tasikmalaya', 'Kab', '', '1', '0'),
(88, 9, '20', 'Tasikmalaya', 'Kota', '', '1', '0'),
(89, 9, '21', 'Banjar', 'Kota', '', '1', '0'),
(90, 10, '1', 'Banjarnegara', 'Kab', '', '1', '0'),
(91, 10, '2', 'Banyumas', 'Kab', '', '1', '0'),
(92, 10, '3', 'Batang', 'Kab', '', '1', '0'),
(93, 10, '4', 'Blora', 'Kab', '', '1', '0'),
(94, 10, '5', 'Boyolali', 'Kab', '', '0', '0'),
(95, 10, '6', 'Brebes', 'Kab', '', '1', '0'),
(96, 10, '7', 'Cilacap', 'Kab', '', '1', '0'),
(97, 10, '8', 'Demak', 'Kab', '', '1', '0'),
(98, 10, '9', 'Grobogan', 'Kab', '', '1', '0'),
(99, 10, '10', 'Jepara', 'Kab', '', '1', '0'),
(100, 10, '11', 'Karanganyar', 'Kab', '', '1', '0'),
(101, 10, '12', 'Kebumen', 'Kab', '', '1', '0'),
(102, 10, '13', 'Kendal', 'Kab', '', '1', '0'),
(103, 10, '14', 'Klaten', 'Kab', '', '1', '0'),
(104, 10, '15', 'Kudus', 'Kab', '', '1', '0'),
(105, 10, '16', 'Magelang', 'Kab', '', '1', '0'),
(106, 10, '16', 'Magelang', 'Kota', '', '1', '0'),
(107, 10, '17', 'Pati', 'Kab', '', '1', '0'),
(108, 10, '18', 'Pekalongan', 'Kab', '', '1', '0'),
(109, 10, '18', 'Pekalongan', 'Kota', '', '1', '0'),
(110, 10, '19', 'Pemalang', 'Kab', '', '1', '0'),
(111, 10, '20', 'Purbalingga', 'Kab', '', '1', '0'),
(112, 10, '21', 'Purworejo', 'Kab', '', '1', '0'),
(113, 10, '22', 'Rembang', 'Kab', '', '1', '0'),
(114, 10, '23', 'Salatiga', 'Kota', '', '1', '0'),
(115, 10, '24', 'Semarang', 'Kab', '', '1', '0'),
(116, 10, '24', 'Semarang', 'Kota', '', '1', '0'),
(117, 10, '25', 'Sragen', 'Kab', '', '1', '0'),
(118, 10, '26', 'Sukoharjo', 'Kab', '', '1', '0'),
(119, 10, '27', 'Surakarta', 'Kota', '', '1', '0'),
(120, 10, '28', 'Tegal', 'Kab', '', '1', '0'),
(121, 10, '28', 'Tegal', 'Kota', '', '1', '0'),
(122, 10, '29', 'Temanggung', 'Kab', '', '1', '0'),
(123, 10, '30', 'Wonogiri', 'Kab', '', '1', '0'),
(124, 10, '31', 'Wonosobo', 'Kab', '', '1', '0'),
(125, 11, '1', 'Bangkalan', 'Kab', '', '1', '0'),
(126, 11, '2', 'Banyuwangi', 'Kab', '', '1', '0'),
(127, 11, '3', 'Batu', 'Kota', '', '1', '0'),
(128, 11, '4', 'Blitar', 'Kab', '', '1', '0'),
(129, 11, '4', 'Blitar', 'Kota', '', '1', '0'),
(130, 11, '5', 'Bojonegoro', 'Kab', '', '1', '0'),
(131, 11, '6', 'Bondowoso', 'Kab', '', '1', '0'),
(132, 11, '7', 'Gresik', 'Kab', '', '1', '0'),
(133, 11, '8', 'Jember', 'Kab', '', '1', '0'),
(134, 11, '9', 'Jombang', 'Kab', '', '1', '0'),
(135, 11, '10', 'Kediri', 'Kab', '', '1', '0'),
(136, 11, '10', 'Kediri', 'Kota', '', '1', '0'),
(137, 11, '11', 'Lamongan', 'Kab', '', '1', '0'),
(138, 11, '12', 'Lumajang', 'Kab', '', '1', '0'),
(139, 11, '13', 'Madiun', 'Kab', '', '1', '0'),
(140, 11, '13', 'Madiun', 'Kota', '', '1', '0'),
(141, 11, '14', 'Magetan', 'Kab', '', '1', '0'),
(142, 11, '15', 'Malang', 'Kab', '', '1', '0'),
(143, 11, '15', 'Malang', 'Kota', '', '1', '0'),
(144, 11, '16', 'Mojokerto', 'Kab', '', '1', '0'),
(145, 11, '16', 'Mojokerto', 'Kota', '', '1', '0'),
(146, 11, '17', 'Nganjuk', 'Kab', '', '1', '0'),
(147, 11, '18', 'Ngawi', 'Kab', '', '1', '0'),
(148, 11, '19', 'Pacitan', 'Kab', '', '1', '0'),
(149, 11, '20', 'Pamekasan', 'Kab', '', '1', '0'),
(150, 11, '21', 'Pasuruan', 'Kab', '', '1', '0'),
(151, 11, '21', 'Pasuruan', 'Kota', '', '1', '0'),
(152, 11, '22', 'Ponorogo', 'Kab', '', '1', '0'),
(153, 11, '23', 'Probolinggo', 'Kab', '', '1', '0'),
(154, 11, '23', 'Probolinggo', 'Kota', '', '1', '0'),
(155, 11, '24', 'Sampang', 'Kab', '', '1', '0'),
(156, 11, '25', 'Sidoarjo', 'Kab', '', '1', '0'),
(157, 11, '26', 'Situbondo', 'Kab', '', '1', '0'),
(158, 11, '27', 'Sumenep', 'Kab', '', '1', '0'),
(159, 11, '28', 'Surabaya', 'Kota', '', '1', '0'),
(160, 11, '29', 'Trenggalek', 'Kab', '', '1', '0'),
(161, 11, '30', 'Tuban', 'Kab', '', '1', '0'),
(162, 11, '31', 'Tulungagung', 'Kab', '', '1', '0'),
(163, 12, '1', 'Bengkayang', 'Kab', '', '1', '0'),
(164, 12, '2', 'Kapuas Hulu', 'Kab', '', '1', '0'),
(165, 12, '3', 'Kayong Utara', 'Kab', '', '1', '0'),
(166, 12, '4', 'Ketapang', 'Kab', '', '1', '0'),
(167, 12, '5', 'Kubu Raya', 'Kab', '', '1', '0'),
(168, 12, '6', 'Landak', 'Kab', '', '1', '0'),
(169, 12, '7', 'Melawi', 'Kab', '', '1', '0'),
(170, 12, '8', 'Mempawah', 'Kab', '', '1', '0'),
(171, 12, '9', 'Pontianak', 'Kota', '', '1', '0'),
(172, 12, '10', 'Sambas', 'Kab', '', '1', '0'),
(173, 12, '11', 'Sanggau', 'Kab', '', '1', '0'),
(174, 12, '12', 'Sekadau', 'Kab', '', '1', '0'),
(175, 12, '13', 'Singkawang', 'Kota', '', '1', '0'),
(176, 12, '14', 'Sintang', 'Kab', '', '1', '0'),
(177, 13, '1', 'Balangan', 'Kab', '', '1', '0'),
(178, 13, '2', 'Banjarbaru', 'Kota', '', '1', '0'),
(179, 13, '3', 'Banjarmasin', 'Kota', '', '1', '0'),
(180, 13, '4', 'Barito Kuala', 'Kab', '', '1', '0'),
(181, 13, '5', 'Hulu Sungai Selatan', 'Kab', '', '1', '0'),
(182, 13, '6', 'Hulu Sungai Tengah', 'Kab', '', '1', '0'),
(183, 13, '7', 'Hulu Sungai Utara', 'Kab', '', '1', '0'),
(184, 13, '8', 'Kotabaru', 'Kab', '', '1', '0'),
(185, 13, '9', 'Tabalong', 'Kab', '', '1', '0'),
(186, 13, '10', 'Tanah Bumbu', 'Kab', '', '1', '0'),
(187, 13, '11', 'Tanah Laut', 'Kab', '', '1', '0'),
(188, 13, '12', 'Tapin', 'Kab', '', '1', '0'),
(189, 13, '21', 'Banjar', 'Kab', '', '1', '0'),
(190, 14, '1', 'Barito Selatan', 'Kab', '', '1', '0'),
(191, 14, '2', 'Barito Timur', 'Kab', '', '1', '0'),
(192, 14, '3', 'Barito Utara', 'Kab', '', '1', '0'),
(193, 14, '4', 'Gunung Mas', 'Kab', '', '1', '0'),
(194, 14, '5', 'Kapuas', 'Kab', '', '1', '0'),
(195, 14, '6', 'Katingan', 'Kab', '', '1', '0'),
(196, 14, '7', 'Kotawaringin Barat', 'Kab', '', '1', '0'),
(197, 14, '8', 'Kotawaringin Timur', 'Kab', '', '1', '0'),
(198, 14, '9', 'Lamandau', 'Kab', '', '1', '0'),
(199, 14, '10', 'Murung Raya', 'Kab', '', '1', '0'),
(200, 14, '11', 'Palangka Raya', 'Kota', '', '1', '0'),
(201, 14, '12', 'Pulang Pisau', 'Kab', '', '1', '0'),
(202, 14, '13', 'Seruyan', 'Kab', '', '1', '0'),
(203, 14, '14', 'Sukamara', 'Kab', '', '1', '0'),
(204, 15, '1', 'Balikpapan', 'Kota', '', '1', '0'),
(205, 15, '2', 'Berau', 'Kab', '', '1', '0'),
(206, 15, '3', 'Bontang', 'Kota', '', '1', '0'),
(207, 15, '4', 'Kutai Barat', 'Kab', '', '1', '0'),
(208, 15, '5', 'Kutai Kartanegara', 'Kab', '', '1', '0'),
(209, 15, '6', 'Kutai Timur', 'Kab', '', '1', '0'),
(210, 15, '7', 'Mahakam Ulu', 'Kab', '', '1', '0'),
(211, 15, '8', 'Paser', 'Kab', '', '1', '0'),
(212, 15, '9', 'Penajam Paser Utara', 'Kab', '', '1', '0'),
(213, 15, '10', 'Samarinda', 'Kota', '', '1', '0'),
(214, 16, '1', 'Bulungan', 'Kab', '', '1', '0'),
(215, 16, '2', 'Malinau', 'Kab', '', '1', '0'),
(216, 16, '3', 'Nunukan', 'Kab', '', '1', '0'),
(217, 16, '4', 'Tana Tidung', 'Kab', '', '1', '0'),
(218, 16, '5', 'Tarakan', 'Kota', '', '1', '0'),
(219, 17, '1', 'Batam', 'Kota', '', '1', '0'),
(220, 17, '2', 'Bintan', 'Kab', '', '1', '0'),
(221, 17, '3', 'Karimun', 'Kab', '', '1', '0'),
(222, 17, '4', 'Kepulauan Anambas', 'Kab', '', '1', '0'),
(223, 17, '5', 'Lingga', 'Kab', '', '1', '0'),
(224, 17, '6', 'Natuna', 'Kab', '', '1', '0'),
(225, 17, '7', 'Tanjung Pinang', 'Kota', '', '1', '0'),
(226, 18, '1', 'Bandar Lampung', 'Kota', '', '1', '0'),
(227, 18, '2', 'Lampung Barat', 'Kab', '', '1', '0'),
(228, 18, '3', 'Lampung Selatan', 'Kab', '', '1', '0'),
(229, 18, '4', 'Lampung Tengah', 'Kab', '', '1', '0'),
(230, 18, '5', 'Lampung Timur', 'Kab', '', '1', '0'),
(231, 18, '6', 'Lampung Utara', 'Kab', '', '1', '0'),
(232, 18, '7', 'Mesuji', 'Kab', '', '1', '0'),
(233, 18, '8', 'Metro', 'Kota', '', '1', '0'),
(234, 18, '9', 'Pesawaran', 'Kab', '', '1', '0'),
(235, 18, '10', 'Pesisir Barat', 'Kab', '', '1', '0'),
(236, 18, '11', 'Pringsewu', 'Kab', '', '1', '0'),
(237, 18, '12', 'Tanggamus', 'Kab', '', '1', '0'),
(238, 18, '13', 'Tulang Bawang', 'Kab', '', '1', '0'),
(239, 18, '14', 'Tulang Bawang Barat', 'Kab', '', '1', '0'),
(240, 18, '15', 'Way Kanan', 'Kab', '', '1', '0'),
(241, 19, '1', 'Ambon', 'Kota', '', '1', '0'),
(242, 19, '2', 'Buru', 'Kab', '', '1', '0'),
(243, 19, '3', 'Buru Selatan', 'Kab', '', '1', '0'),
(244, 19, '4', 'Kepulauan Aru', 'Kab', '', '1', '0'),
(245, 19, '5', 'Maluku Barat Daya', 'Kab', '', '1', '0'),
(246, 19, '6', 'Maluku Tengah', 'Kab', '', '1', '0'),
(247, 19, '7', 'Maluku Tenggara', 'Kab', '', '1', '0'),
(248, 19, '8', 'Maluku Tenggara Barat', 'Kab', '', '1', '0'),
(249, 19, '9', 'Seram Bagian Barat', 'Kab', '', '1', '0'),
(250, 19, '10', 'Seram Bagian Timur', 'Kab', '', '1', '0'),
(251, 19, '11', 'Tual', 'Kota', '', '1', '0'),
(252, 20, '1', 'Halmahera Barat', 'Kab', '', '1', '0'),
(253, 20, '2', 'Halmahera Selatan', 'Kab', '', '1', '0'),
(254, 20, '3', 'Halmahera Tengah', 'Kab', '', '1', '0'),
(255, 20, '4', 'Halmahera Timur', 'Kab', '', '1', '0'),
(256, 20, '5', 'Halmahera Utara', 'Kab', '', '1', '0'),
(257, 20, '6', 'Kepulauan Sula', 'Kab', '', '1', '0'),
(258, 20, '7', 'Pulau Morotai', 'Kab', '', '1', '0'),
(259, 20, '8', 'Pulau Taliabu', 'Kab', '', '1', '0'),
(260, 20, '9', 'Ternate', 'Kota', '', '1', '0'),
(261, 20, '10', 'Tidore Kepulauan', 'Kota', '', '1', '0'),
(262, 21, '1', 'Aceh Barat', 'Kab', '', '1', '0'),
(263, 21, '2', 'Aceh Barat Daya', 'Kab', '', '1', '0'),
(264, 21, '3', 'Aceh Besar', 'Kab', '', '1', '0'),
(265, 21, '4', 'Aceh Jaya', 'Kab', '', '1', '0'),
(266, 21, '5', 'Aceh Selatan', 'Kab', '', '1', '0'),
(267, 21, '6', 'Aceh Singkil', 'Kab', '', '1', '0'),
(268, 21, '7', 'Aceh Tamiang', 'Kab', '', '1', '0'),
(269, 21, '8', 'Aceh Tengah', 'Kab', '', '1', '0'),
(270, 21, '9', 'Aceh Tenggara', 'Kab', '', '1', '0'),
(271, 21, '10', 'Aceh Timur', 'Kab', '', '1', '0'),
(272, 21, '11', 'Aceh Utara', 'Kab', '', '1', '0'),
(273, 21, '12', 'Banda Aceh', 'Kota', '', '1', '0'),
(274, 21, '13', 'Bener Meriah', 'Kab', '', '1', '0'),
(275, 21, '14', 'Bireuen', 'Kab', '', '1', '0'),
(276, 21, '15', 'Gayo Lues', 'Kab', '', '1', '0'),
(277, 21, '16', 'Lhokseumawe', 'Kota', '', '1', '0'),
(278, 21, '17', 'Nagan Raya', 'Kab', '', '1', '0'),
(279, 21, '18', 'Pidie', 'Kab', '', '1', '0'),
(280, 21, '19', 'Pidie Jaya', 'Kab', '', '1', '0'),
(281, 21, '20', 'Sabang', 'Kota', '', '1', '0'),
(282, 21, '21', 'Simeulue', 'Kab', '', '1', '0'),
(283, 21, '22', 'Subulussalam', 'Kota', '', '1', '0'),
(284, 22, '1', 'Bima', 'Kab', '', '1', '0'),
(285, 22, '1', 'Bima', 'Kota', '', '1', '0'),
(286, 22, '2', 'Dompu', 'Kab', '', '1', '0'),
(287, 22, '3', 'Lombok Barat', 'Kab', '', '1', '0'),
(288, 22, '4', 'Lombok Tengah', 'Kab', '', '1', '0'),
(289, 22, '5', 'Lombok Timur', 'Kab', '', '1', '0'),
(290, 22, '6', 'Lombok Utara', 'Kab', '', '1', '0'),
(291, 22, '7', 'Mataram', 'Kota', '', '1', '0'),
(292, 22, '8', 'Sumbawa', 'Kab', '', '1', '0'),
(293, 22, '9', 'Sumbawa Barat', 'Kab', '', '1', '0'),
(294, 23, '2', ' Flores Timur', 'Kab', '', '1', '0'),
(295, 23, '3', 'Alor', 'Kab', '', '1', '0'),
(296, 23, '4', 'Belu', 'Kab', '', '1', '0'),
(297, 23, '5', 'Ende', 'Kab', '', '1', '0'),
(298, 23, '7', 'Kupang', 'Kab', '', '1', '0'),
(299, 23, '7', 'Kupang', 'Kota', '', '1', '0'),
(300, 23, '8', 'Lembata', 'Kab', '', '1', '0'),
(301, 23, '9', 'Malaka', 'Kab', '', '1', '0'),
(302, 23, '10', 'Manggarai', 'Kab', '', '1', '0'),
(303, 23, '11', 'Manggarai Barat', 'Kab', '', '1', '0'),
(304, 23, '12', 'Manggarai Timur', 'Kab', '', '1', '0'),
(305, 23, '13', 'Nagekeo', 'Kab', '', '1', '0'),
(306, 23, '14', 'Ngada', 'Kab', '', '1', '0'),
(307, 23, '15', 'Rote Ndao', 'Kab', '', '1', '0'),
(308, 23, '16', 'Sabu Raijua', 'Kab', '', '1', '0'),
(309, 23, '17', 'Sikka', 'Kab', '', '1', '0'),
(310, 23, '18', 'Sumba Barat', 'Kab', '', '1', '0'),
(311, 23, '19', 'Sumba Barat Daya', 'Kab', '', '1', '0'),
(312, 23, '20', 'Sumba Tengah', 'Kab', '', '1', '0'),
(313, 23, '21', 'Sumba Timur', 'Kab', '', '1', '0'),
(314, 23, '22', 'Timor Tengah Selatan', 'Kab', '', '1', '0'),
(315, 23, '23', 'Timor Tengah Utara', 'Kab', '', '1', '0'),
(316, 24, '1', 'Asmat', 'Kab', '', '1', '0'),
(317, 24, '2', 'Biak Numfor', 'Kab', '', '1', '0'),
(318, 24, '3', 'Boven Digoel', 'Kab', '', '1', '0'),
(319, 24, '4', 'Deiyai', 'Kab', '', '1', '0'),
(320, 24, '5', 'Dogiyai', 'Kab', '', '1', '0'),
(321, 24, '6', 'Intan Jaya', 'Kab', '', '1', '0'),
(322, 24, '7', 'Jayapura', 'Kab', '', '1', '0'),
(323, 24, '7', 'Jayapura', 'Kota', '', '1', '0'),
(324, 24, '8', 'Jayawijaya', 'Kab', '', '1', '0'),
(325, 24, '9', 'Keerom', 'Kab', '', '1', '0'),
(326, 24, '10', 'Kepulauan Yapen', 'Kab', '', '1', '0'),
(327, 24, '11', 'Lanny Jaya', 'Kab', '', '1', '0'),
(328, 24, '12', 'Mamberamo Raya', 'Kab', '', '1', '0'),
(329, 24, '13', 'Mamberamo Tengah', 'Kab', '', '1', '0'),
(330, 24, '14', 'Mappi', 'Kab', '', '1', '0'),
(331, 24, '15', 'Merauke', 'Kab', '', '1', '0'),
(332, 24, '16', 'Mimika', 'Kab', '', '1', '0'),
(333, 24, '17', 'Nabire', 'Kab', '', '1', '0'),
(334, 24, '18', 'Nduga', 'Kab', '', '1', '0'),
(335, 24, '19', 'Paniai', 'Kab', '', '1', '0'),
(336, 24, '20', 'Pegunungan Bintang', 'Kab', '', '1', '0'),
(337, 24, '21', 'Puncak', 'Kab', '', '1', '0'),
(338, 24, '22', 'Puncak Jaya', 'Kab', '', '1', '0'),
(339, 24, '23', 'Sarmi', 'Kab', '', '1', '0'),
(340, 24, '24', 'Supiori', 'Kab', '', '1', '0'),
(341, 24, '25', 'Tolikara', 'Kab', '', '1', '0'),
(342, 24, '26', 'Waropen', 'Kab', '', '1', '0'),
(343, 24, '27', 'Yahukimo', 'Kab', '', '1', '0'),
(344, 24, '28', 'Yalimo', 'Kab', '', '1', '0'),
(345, 25, '1', 'Fakfak', 'Kab', '', '1', '0'),
(346, 25, '2', 'Kaimana', 'Kab', '', '1', '0'),
(347, 25, '3', 'Manokwari', 'Kab', '', '1', '0'),
(348, 25, '4', 'Manokwari Selatan', 'Kab', '', '1', '0'),
(349, 25, '5', 'Maybrat', 'Kab', '', '1', '0'),
(350, 25, '6', 'Pegunungan Arfak', 'Kab', '', '1', '0'),
(351, 25, '7', 'Raja Ampat', 'Kab', '', '1', '0'),
(352, 25, '8', 'Sorong', 'Kab', '', '1', '0'),
(353, 25, '8', 'Sorong', 'Kota', '', '1', '0'),
(354, 25, '9', 'Sorong Selatan', 'Kab', '', '1', '0'),
(355, 25, '10', 'Tambrauw', 'Kab', '', '1', '0'),
(356, 25, '11', 'Teluk Bintuni', 'Kab', '', '1', '0'),
(357, 25, '12', 'Teluk Wondama', 'Kab', '', '1', '0'),
(358, 26, '1', 'Bengkalis', 'Kab', '', '1', '0'),
(359, 26, '2', 'Dumai', 'Kota', '', '1', '0'),
(360, 26, '3', 'Indragiri Hilir', 'Kab', '', '1', '0'),
(361, 26, '4', 'Indragiri Hulu', 'Kab', '', '1', '0'),
(362, 26, '5', 'Kampar', 'Kab', '', '1', '0'),
(363, 26, '6', 'Kepulauan Meranti', 'Kab', '', '1', '0'),
(364, 26, '7', 'Kuantan Singingi', 'Kab', '', '1', '0'),
(365, 26, '8', 'Pekanbaru', 'Kota', '', '1', '0'),
(366, 26, '9', 'Pelalawan', 'Kab', '', '1', '0'),
(367, 26, '10', 'Rokan Hilir', 'Kab', '', '1', '0'),
(368, 26, '11', 'Rokan Hulu', 'Kab', '', '1', '0'),
(369, 26, '12', 'Siak', 'Kab', '', '1', '0'),
(370, 27, '1', 'Majene', 'Kab', '', '1', '0'),
(371, 27, '2', 'Mamasa', 'Kab', '', '1', '0'),
(372, 27, '3', 'Mamuju', 'Kab', '', '1', '0'),
(373, 27, '4', 'Mamuju Tengah', 'Kab', '', '1', '0'),
(374, 27, '5', 'Mamuju Utara', 'Kab', '', '1', '0'),
(375, 27, '6', 'Polewali Mandar', 'Kab', '', '1', '0'),
(376, 28, '1', 'Bantaeng', 'Kab', '', '1', '0'),
(377, 28, '2', 'Barru', 'Kab', '', '1', '0'),
(378, 28, '3', 'Bone', 'Kab', '', '1', '0'),
(379, 28, '4', 'Bulukumba', 'Kab', '', '1', '0'),
(380, 28, '5', 'Enrekang', 'Kab', '', '1', '0'),
(381, 28, '6', 'Gowa', 'Kab', '', '1', '0'),
(382, 28, '7', 'Jeneponto', 'Kab', '', '1', '0'),
(383, 28, '8', 'Kepulauan Selayar', 'Kab', '', '1', '0'),
(384, 28, '9', 'Luwu', 'Kab', '', '1', '0'),
(385, 28, '10', 'Luwu Timur', 'Kab', '', '1', '0'),
(386, 28, '11', 'Luwu Utara', 'Kab', '', '1', '0'),
(387, 28, '12', 'Makassar', 'Kota', '', '1', '0'),
(388, 28, '13', 'Maros', 'Kab', '', '1', '0'),
(389, 28, '14', 'Palopo', 'Kota', '', '1', '0'),
(390, 28, '15', 'Pangkajene Kepulauan', 'Kab', '', '1', '0'),
(391, 28, '16', 'Parepare', 'Kota', '', '1', '0'),
(392, 28, '17', 'Pinrang', 'Kab', '', '1', '0'),
(393, 28, '18', 'Sidenreng Rappang', 'Kab', '', '1', '0'),
(394, 28, '19', 'Sinjai', 'Kab', '', '1', '0'),
(395, 28, '20', 'Soppeng', 'Kab', '', '1', '0'),
(396, 28, '21', 'Takalar', 'Kab', '', '1', '0'),
(397, 28, '22', 'Tana Toraja', 'Kab', '', '1', '0'),
(398, 28, '23', 'Toraja Utara', 'Kab', '', '1', '0'),
(399, 28, '24', 'Wajo', 'Kab', '', '1', '0'),
(400, 29, '1', 'Banggai', 'Kab', '', '1', '0'),
(401, 29, '2', 'Banggai Kepulauan', 'Kab', '', '1', '0'),
(402, 29, '3', 'Banggai Laut', 'Kab', '', '1', '0'),
(403, 29, '4', 'Buol', 'Kab', '', '1', '0'),
(404, 29, '5', 'Donggala', 'Kab', '', '1', '0'),
(405, 29, '6', 'Morowali', 'Kab', '', '1', '0'),
(406, 29, '7', 'Morowali Utara', 'Kab', '', '1', '0'),
(407, 29, '8', 'Palu', 'Kota', '', '1', '0'),
(408, 29, '9', 'Parigi Moutong', 'Kab', '', '1', '0'),
(409, 29, '10', 'Poso', 'Kab', '', '1', '0'),
(410, 29, '11', 'Sigi', 'Kab', '', '1', '0'),
(411, 29, '12', 'Tojo Una-Una', 'Kab', '', '1', '0'),
(412, 29, '13', 'Toli-Toli', 'Kab', '', '1', '0'),
(413, 30, '1', 'Bau-Bau', 'Kota', '', '1', '0'),
(414, 30, '2', 'Bombana', 'Kab', '', '1', '0'),
(415, 30, '3', 'Buton', 'Kab', '', '1', '0'),
(416, 30, '4', 'Buton Selatan', 'Kab', '', '1', '0'),
(417, 30, '5', 'Buton Tengah', 'Kab', '', '1', '0'),
(418, 30, '6', 'Buton Utara', 'Kab', '', '1', '0'),
(419, 30, '7', 'Kendari', 'Kota', '', '1', '0'),
(420, 30, '8', 'Kolaka', 'Kab', '', '1', '0'),
(421, 30, '9', 'Kolaka Timur', 'Kab', '', '1', '0'),
(422, 30, '10', 'Kolaka Utara', 'Kab', '', '1', '0'),
(423, 30, '11', 'Konawe', 'Kab', '', '1', '0'),
(424, 30, '12', 'Konawe Kepulauan', 'Kab', '', '1', '0'),
(425, 30, '13', 'Konawe Selatan', 'Kab', '', '1', '0'),
(426, 30, '14', 'Konawe Utara', 'Kab', '', '1', '0'),
(427, 30, '15', 'Muna', 'Kab', '', '1', '0'),
(428, 30, '16', 'Muna Barat', 'Kab', '', '1', '0'),
(429, 30, '17', 'Wakatobi', 'Kab', '', '1', '0'),
(430, 31, '1', 'Bitung', 'Kota', '', '1', '0'),
(431, 31, '2', 'Bolaang Mongondow', 'Kab', '', '1', '0'),
(432, 31, '3', 'Bolaang Mongondow Selatan', 'Kab', '', '1', '0'),
(433, 31, '4', 'Bolaang Mongondow Timur', 'Kab', '', '1', '0'),
(434, 31, '5', 'Bolaang Mongondow Utara', 'Kab', '', '1', '0'),
(435, 31, '6', 'Kepulauan Sangihe', 'Kab', '', '1', '0'),
(436, 31, '7', 'Kepulauan Siau Tagulandang Biaro (Sitaro)', 'Kab', '', '1', '0'),
(437, 31, '8', 'Kepulauan Talaud', 'Kab', '', '1', '0'),
(438, 31, '9', 'Kotamobagu', 'Kota', '', '1', '0'),
(439, 31, '10', 'Manado', 'Kota', '', '1', '0'),
(440, 31, '11', 'Minahasa', 'Kab', '', '1', '0'),
(441, 31, '12', 'Minahasa Selatan', 'Kab', '', '1', '0'),
(442, 31, '13', 'Minahasa Tenggara', 'Kab', '', '1', '0'),
(443, 31, '14', 'Minahasa Utara', 'Kab', '', '1', '0'),
(444, 31, '15', 'Tomohon', 'Kota', '', '1', '0'),
(445, 32, '1', 'Agam', 'Kab', '', '1', '0'),
(446, 32, '2', 'Bukittinggi', 'Kota', '', '1', '0'),
(447, 32, '3', 'Dharmasraya', 'Kab', '', '1', '0'),
(448, 32, '4', 'Kepulauan Mentawai', 'Kab', '', '1', '0'),
(449, 32, '5', 'Lima Puluh Kota', 'Kab', '', '1', '0'),
(450, 32, '6', 'Padang', 'Kota', '', '1', '0'),
(451, 32, '7', 'Padang Panjang', 'Kota', '', '1', '0'),
(452, 32, '8', 'Padang Pariaman', 'Kab', '', '1', '0'),
(453, 32, '9', 'Pariaman', 'Kota', '', '1', '0'),
(454, 32, '10', 'Pasaman', 'Kab', '', '1', '0'),
(455, 32, '11', 'Pasaman Barat', 'Kab', '', '1', '0'),
(456, 32, '12', 'Payakumbuh', 'Kota', '', '1', '0'),
(457, 32, '13', 'Pesisir Selatan', 'Kab', '', '1', '0'),
(458, 32, '14', 'Sawah Lunto', 'Kota', '', '1', '0'),
(459, 32, '15', 'Sijunjung', 'Kab', '', '1', '0'),
(460, 32, '16', 'Solok', 'Kab', '', '1', '0'),
(461, 32, '16', 'Solok', 'Kota', '', '1', '0'),
(462, 32, '17', 'Solok Selatan', 'Kab', '', '1', '0'),
(463, 32, '18', 'Tanah Datar', 'Kab', '', '1', '0'),
(464, 33, '1', 'Banyuasin', 'Kab', '', '1', '0'),
(465, 33, '2', 'Empat Lawang', 'Kab', '', '1', '0'),
(466, 33, '3', 'Lahat', 'Kab', '', '1', '0'),
(467, 33, '4', 'Lubuk Linggau', 'Kota', '', '1', '0'),
(468, 33, '5', 'Muara Enim', 'Kab', '', '1', '0'),
(469, 33, '6', 'Musi Banyuasin', 'Kab', '', '1', '0'),
(470, 33, '7', 'Musi Rawas', 'Kab', '', '1', '0'),
(471, 33, '8', 'Musi Rawas Utara', 'Kab', '', '1', '0'),
(472, 33, '9', 'Ogan Ilir', 'Kab', '', '1', '0'),
(473, 33, '10', 'Ogan Komering Ilir', 'Kab', '', '1', '0'),
(474, 33, '11', 'Ogan Komering Ulu', 'Kab', '', '1', '0'),
(475, 33, '12', 'Ogan Komering Ulu Selatan', 'Kab', '', '1', '0'),
(476, 33, '13', 'Ogan Komering Ulu Timur', 'Kab', '', '1', '0'),
(477, 33, '14', 'Pagar Alam', 'Kota', '', '1', '0'),
(478, 33, '15', 'Palembang', 'Kota', '', '1', '0'),
(479, 33, '16', 'Penukal Abab Lematang Ilir', 'Kab', '', '1', '0'),
(480, 33, '17', 'Prabumulih', 'Kota', '', '1', '0'),
(481, 34, '1', 'Asahan', 'Kab', '', '1', '0'),
(482, 34, '2', 'Batu Bara', 'Kab', '', '1', '0'),
(483, 34, '3', 'Binjai', 'Kota', '', '1', '0'),
(484, 34, '4', 'Dairi', 'Kab', '', '1', '0'),
(485, 34, '5', 'Deli Serdang', 'Kab', '', '1', '0'),
(486, 34, '6', 'Gunungsitoli', 'Kota', '', '1', '0'),
(487, 34, '7', 'Humbang Hasundutan', 'Kab', '', '1', '0'),
(488, 34, '8', 'Karo', 'Kab', '', '1', '0'),
(489, 34, '9', 'Labuhanbatu', 'Kab', '', '1', '0'),
(490, 34, '10', 'Labuhanbatu Selatan', 'Kab', '', '1', '0'),
(491, 34, '11', 'Labuhanbatu Utara', 'Kab', '', '1', '0'),
(492, 34, '12', 'Langkat', 'Kab', '', '1', '0'),
(493, 34, '13', 'Mandailing Natal', 'Kab', '', '1', '0'),
(494, 34, '14', 'Medan', 'Kota', '', '1', '0'),
(495, 34, '15', 'Nias', 'Kab', '', '1', '0'),
(496, 34, '16', 'Nias Barat', 'Kab', '', '1', '0'),
(497, 34, '17', 'Nias Selatan', 'Kab', '', '1', '0'),
(498, 34, '18', 'Nias Utara', 'Kab', '', '1', '0'),
(499, 34, '19', 'Padang Lawas', 'Kab', '', '1', '0'),
(500, 34, '20', 'Padang Lawas Utara', 'Kab', '', '1', '0'),
(501, 34, '21', 'Padang Sidempuan', 'Kota', '', '1', '0'),
(502, 34, '22', 'Pakpak Bharat', 'Kab', '', '1', '0'),
(503, 34, '23', 'Pematang Siantar', 'Kota', '', '1', '0'),
(504, 34, '24', 'Samosir', 'Kab', '', '1', '0'),
(505, 34, '25', 'Serdang Bedagai', 'Kab', '', '1', '0'),
(506, 34, '26', 'Sibolga', 'Kota', '', '1', '0'),
(507, 34, '27', 'Simalungun', 'Kab', '', '1', '0'),
(508, 34, '28', 'Tanjung Balai', 'Kota', '', '1', '0'),
(509, 34, '29', 'Tapanuli Selatan', 'Kab', '', '1', '0'),
(510, 34, '30', 'Tapanuli Tengah', 'Kab', '', '1', '0'),
(511, 34, '31', 'Tapanuli Utara', 'Kab', '', '1', '0'),
(512, 34, '32', 'Tebing Tinggi', 'Kota', '', '1', '0'),
(513, 34, '33', 'Toba Samosir', 'Kab', '', '1', '0'),
(514, 10, '', 'PURWOKERTO', 'Kab', '-', '1', '0'),
(516, 9, '', 'Purwakarta', 'Kab', '', '0', '0'),
(517, 21, '', 'LANGSA', 'Kota', '-', '1', '0'),
(518, 3, '', 'Cilegon', 'Kota', '', '1', '0'),
(519, 10, '', 'sidoarjo', 'Kab', '', '1', '0'),
(520, 17, '', 'Kubu Raya', 'Kab', '', '1', '0'),
(521, 11, '', 'AREMA MALANG', 'Kota', '', '1', '1'),
(522, 10, '', 'Boyolali', 'Kab', '-', '1', '0');

-- --------------------------------------------------------

--
-- Table structure for table `cp_app_config`
--

CREATE TABLE `cp_app_config` (
  `id` int(11) UNSIGNED NOT NULL,
  `config_name` varchar(200) DEFAULT NULL,
  `config_value` varchar(200) DEFAULT NULL,
  `config_label` varchar(200) DEFAULT NULL,
  `config_obj` varchar(255) DEFAULT 'text',
  `config_obj_value` text,
  `config_obj_attr` varchar(255) DEFAULT NULL,
  `app_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_app_config`
--

INSERT INTO `cp_app_config` (`id`, `config_name`, `config_value`, `config_label`, `config_obj`, `config_obj_value`, `config_obj_attr`, `app_id`) VALUES
(1, 'app_name', 'Mithan.com', 'Aplication Name', 'text', NULL, NULL, 1),
(2, 'app_email', 'admin@app.com', 'Email Admin', 'text', NULL, NULL, 2),
(3, 'app_email_finance', 'finance@app.com', 'Email Finance', 'text', NULL, NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `cp_app_country`
--

CREATE TABLE `cp_app_country` (
  `country_id` int(5) NOT NULL,
  `iso2` char(2) DEFAULT NULL,
  `short_name` varchar(80) NOT NULL DEFAULT '',
  `long_name` varchar(80) NOT NULL DEFAULT '',
  `iso3` char(3) DEFAULT NULL,
  `numcode` varchar(6) DEFAULT NULL,
  `un_member` varchar(12) DEFAULT NULL,
  `calling_code` varchar(8) DEFAULT NULL,
  `cctld` varchar(5) DEFAULT NULL,
  `is_trash` int(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `cp_app_country`
--

INSERT INTO `cp_app_country` (`country_id`, `iso2`, `short_name`, `long_name`, `iso3`, `numcode`, `un_member`, `calling_code`, `cctld`, `is_trash`) VALUES
(251, 'AF', 'Afghanistan', 'Islamic Republic of Afghanistan', 'AFG', '004', 'yes', '93', '.af', 0),
(2, 'AX', 'Aland Islands', '&Aring;land Islands', 'ALA', '248', 'no', '358', '.ax', 0),
(3, 'AL', 'Albania', 'Republic of Albania', 'ALB', '008', 'yes', '355', '.al', 0),
(4, 'DZ', 'Algeria', 'People\'s Democratic Republic of Algeria', 'DZA', '012', 'yes', '213', '.dz', 0),
(5, 'AS', 'American Samoa', 'American Samoa', 'ASM', '016', 'no', '1+684', '.as', 0),
(6, 'AD', 'Andorra', 'Principality of Andorra', 'AND', '020', 'yes', '376', '.ad', 0),
(7, 'AO', 'Angola', 'Republic of Angola', 'AGO', '024', 'yes', '244', '.ao', 0),
(8, 'AI', 'Anguilla', 'Anguilla', 'AIA', '660', 'no', '1+264', '.ai', 0),
(9, 'AQ', 'Antarctica', 'Antarctica', 'ATA', '010', 'no', '672', '.aq', 0),
(10, 'AG', 'Antigua and Barbuda', 'Antigua and Barbuda', 'ATG', '028', 'yes', '1+268', '.ag', 0),
(11, 'AR', 'Argentina', 'Argentine Republic', 'ARG', '032', 'yes', '54', '.ar', 0),
(12, 'AM', 'Armenia', 'Republic of Armenia', 'ARM', '051', 'yes', '374', '.am', 0),
(13, 'AW', 'Aruba', 'Aruba', 'ABW', '533', 'no', '297', '.aw', 0),
(14, 'AU', 'Australia', 'Commonwealth of Australia', 'AUS', '036', 'yes', '61', '.au', 0),
(15, 'AT', 'Austria', 'Republic of Austria', 'AUT', '040', 'yes', '43', '.at', 0),
(16, 'AZ', 'Azerbaijan', 'Republic of Azerbaijan', 'AZE', '031', 'yes', '994', '.az', 0),
(17, 'BS', 'Bahamas', 'Commonwealth of The Bahamas', 'BHS', '044', 'yes', '1+242', '.bs', 0),
(18, 'BH', 'Bahrain', 'Kingdom of Bahrain', 'BHR', '048', 'yes', '973', '.bh', 0),
(19, 'BD', 'Bangladesh', 'People\'s Republic of Bangladesh', 'BGD', '050', 'yes', '880', '.bd', 0),
(20, 'BB', 'Barbados', 'Barbados', 'BRB', '052', 'yes', '1+246', '.bb', 0),
(21, 'BY', 'Belarus', 'Republic of Belarus', 'BLR', '112', 'yes', '375', '.by', 0),
(22, 'BE', 'Belgium', 'Kingdom of Belgium', 'BEL', '056', 'yes', '32', '.be', 0),
(23, 'BZ', 'Belize', 'Belize', 'BLZ', '084', 'yes', '501', '.bz', 0),
(24, 'BJ', 'Benin', 'Republic of Benin', 'BEN', '204', 'yes', '229', '.bj', 0),
(25, 'BM', 'Bermuda', 'Bermuda Islands', 'BMU', '060', 'no', '1+441', '.bm', 0),
(26, 'BT', 'Bhutan', 'Kingdom of Bhutan', 'BTN', '064', 'yes', '975', '.bt', 0),
(27, 'BO', 'Bolivia', 'Plurinational State of Bolivia', 'BOL', '068', 'yes', '591', '.bo', 0),
(28, 'BQ', 'Bonaire, Sint Eustatius and Saba', 'Bonaire, Sint Eustatius and Saba', 'BES', '535', 'no', '599', '.bq', 0),
(29, 'BA', 'Bosnia and Herzegovina', 'Bosnia and Herzegovina', 'BIH', '070', 'yes', '387', '.ba', 0),
(30, 'BW', 'Botswana', 'Republic of Botswana', 'BWA', '072', 'yes', '267', '.bw', 0),
(31, 'BV', 'Bouvet Island', 'Bouvet Island', 'BVT', '074', 'no', 'NONE', '.bv', 0),
(32, 'BR', 'Brazil', 'Federative Republic of Brazil', 'BRA', '076', 'yes', '55', '.br', 0),
(33, 'IO', 'British Indian Ocean Territory', 'British Indian Ocean Territory', 'IOT', '086', 'no', '246', '.io', 0),
(34, 'BN', 'Brunei', 'Brunei Darussalam', 'BRN', '096', 'yes', '673', '.bn', 0),
(35, 'BG', 'Bulgaria', 'Republic of Bulgaria', 'BGR', '100', 'yes', '359', '.bg', 0),
(36, 'BF', 'Burkina Faso', 'Burkina Faso', 'BFA', '854', 'yes', '226', '.bf', 0),
(37, 'BI', 'Burundi', 'Republic of Burundi', 'BDI', '108', 'yes', '257', '.bi', 0),
(38, 'KH', 'Cambodia', 'Kingdom of Cambodia', 'KHM', '116', 'yes', '855', '.kh', 0),
(39, 'CM', 'Cameroon', 'Republic of Cameroon', 'CMR', '120', 'yes', '237', '.cm', 0),
(40, 'CA', 'Canada', 'Canada', 'CAN', '124', 'yes', '1', '.ca', 0),
(41, 'CV', 'Cape Verde', 'Republic of Cape Verde', 'CPV', '132', 'yes', '238', '.cv', 0),
(42, 'KY', 'Cayman Islands', 'The Cayman Islands', 'CYM', '136', 'no', '1+345', '.ky', 0),
(43, 'CF', 'Central African Republic', 'Central African Republic', 'CAF', '140', 'yes', '236', '.cf', 0),
(44, 'TD', 'Chad', 'Republic of Chad', 'TCD', '148', 'yes', '235', '.td', 0),
(45, 'CL', 'Chile', 'Republic of Chile', 'CHL', '152', 'yes', '56', '.cl', 0),
(46, 'CN', 'China', 'People\'s Republic of China', 'CHN', '156', 'yes', '86', '.cn', 0),
(47, 'CX', 'Christmas Island', 'Christmas Island', 'CXR', '162', 'no', '61', '.cx', 0),
(48, 'CC', 'Cocos (Keeling) Islands', 'Cocos (Keeling) Islands', 'CCK', '166', 'no', '61', '.cc', 0),
(49, 'CO', 'Colombia', 'Republic of Colombia', 'COL', '170', 'yes', '57', '.co', 0),
(50, 'KM', 'Comoros', 'Union of the Comoros', 'COM', '174', 'yes', '269', '.km', 0),
(51, 'CG', 'Congo', 'Republic of the Congo', 'COG', '178', 'yes', '242', '.cg', 0),
(52, 'CK', 'Cook Islands', 'Cook Islands', 'COK', '184', 'some', '682', '.ck', 0),
(53, 'CR', 'Costa Rica', 'Republic of Costa Rica', 'CRI', '188', 'yes', '506', '.cr', 0),
(54, 'CI', 'Cote d\'ivoire (Ivory Coast)', 'Republic of C&ocirc;te D\'Ivoire (Ivory Coast)', 'CIV', '384', 'yes', '225', '.ci', 0),
(55, 'HR', 'Croatia', 'Republic of Croatia', 'HRV', '191', 'yes', '385', '.hr', 0),
(56, 'CU', 'Cuba', 'Republic of Cuba', 'CUB', '192', 'yes', '53', '.cu', 0),
(57, 'CW', 'Curacao', 'Cura&ccedil;ao', 'CUW', '531', 'no', '599', '.cw', 0),
(58, 'CY', 'Cyprus', 'Republic of Cyprus', 'CYP', '196', 'yes', '357', '.cy', 0),
(59, 'CZ', 'Czech Republic', 'Czech Republic', 'CZE', '203', 'yes', '420', '.cz', 0),
(60, 'CD', 'Democratic Republic of the Congo', 'Democratic Republic of the Congo', 'COD', '180', 'yes', '243', '.cd', 0),
(61, 'DK', 'Denmark', 'Kingdom of Denmark', 'DNK', '208', 'yes', '45', '.dk', 0),
(62, 'DJ', 'Djibouti', 'Republic of Djibouti', 'DJI', '262', 'yes', '253', '.dj', 0),
(63, 'DM', 'Dominica', 'Commonwealth of Dominica', 'DMA', '212', 'yes', '1+767', '.dm', 0),
(64, 'DO', 'Dominican Republic', 'Dominican Republic', 'DOM', '214', 'yes', '1+809, 8', '.do', 0),
(65, 'EC', 'Ecuador', 'Republic of Ecuador', 'ECU', '218', 'yes', '593', '.ec', 0),
(66, 'EG', 'Egypt', 'Arab Republic of Egypt', 'EGY', '818', 'yes', '20', '.eg', 0),
(67, 'SV', 'El Salvador', 'Republic of El Salvador', 'SLV', '222', 'yes', '503', '.sv', 0),
(68, 'GQ', 'Equatorial Guinea', 'Republic of Equatorial Guinea', 'GNQ', '226', 'yes', '240', '.gq', 0),
(69, 'ER', 'Eritrea', 'State of Eritrea', 'ERI', '232', 'yes', '291', '.er', 0),
(70, 'EE', 'Estonia', 'Republic of Estonia', 'EST', '233', 'yes', '372', '.ee', 0),
(71, 'ET', 'Ethiopia', 'Federal Democratic Republic of Ethiopia', 'ETH', '231', 'yes', '251', '.et', 0),
(72, 'FK', 'Falkland Islands (Malvinas)', 'The Falkland Islands (Malvinas)', 'FLK', '238', 'no', '500', '.fk', 0),
(73, 'FO', 'Faroe Islands', 'The Faroe Islands', 'FRO', '234', 'no', '298', '.fo', 0),
(74, 'FJ', 'Fiji', 'Republic of Fiji', 'FJI', '242', 'yes', '679', '.fj', 0),
(75, 'FI', 'Finland', 'Republic of Finland', 'FIN', '246', 'yes', '358', '.fi', 0),
(76, 'FR', 'France', 'French Republic', 'FRA', '250', 'yes', '33', '.fr', 0),
(77, 'GF', 'French Guiana', 'French Guiana', 'GUF', '254', 'no', '594', '.gf', 0),
(78, 'PF', 'French Polynesia', 'French Polynesia', 'PYF', '258', 'no', '689', '.pf', 0),
(79, 'TF', 'French Southern Territories', 'French Southern Territories', 'ATF', '260', 'no', NULL, '.tf', 0),
(80, 'GA', 'Gabon', 'Gabonese Republic', 'GAB', '266', 'yes', '241', '.ga', 0),
(81, 'GM', 'Gambia', 'Republic of The Gambia', 'GMB', '270', 'yes', '220', '.gm', 0),
(82, 'GE', 'Georgia', 'Georgia', 'GEO', '268', 'yes', '995', '.ge', 0),
(83, 'DE', 'Germany', 'Federal Republic of Germany', 'DEU', '276', 'yes', '49', '.de', 0),
(84, 'GH', 'Ghana', 'Republic of Ghana', 'GHA', '288', 'yes', '233', '.gh', 0),
(85, 'GI', 'Gibraltar', 'Gibraltar', 'GIB', '292', 'no', '350', '.gi', 0),
(86, 'GR', 'Greece', 'Hellenic Republic', 'GRC', '300', 'yes', '30', '.gr', 0),
(87, 'GL', 'Greenland', 'Greenland', 'GRL', '304', 'no', '299', '.gl', 0),
(88, 'GD', 'Grenada', 'Grenada', 'GRD', '308', 'yes', '1+473', '.gd', 0),
(89, 'GP', 'Guadaloupe', 'Guadeloupe', 'GLP', '312', 'no', '590', '.gp', 0),
(90, 'GU', 'Guam', 'Guam', 'GUM', '316', 'no', '1+671', '.gu', 0),
(91, 'GT', 'Guatemala', 'Republic of Guatemala', 'GTM', '320', 'yes', '502', '.gt', 0),
(92, 'GG', 'Guernsey', 'Guernsey', 'GGY', '831', 'no', '44', '.gg', 0),
(93, 'GN', 'Guinea', 'Republic of Guinea', 'GIN', '324', 'yes', '224', '.gn', 0),
(94, 'GW', 'Guinea-Bissau', 'Republic of Guinea-Bissau', 'GNB', '624', 'yes', '245', '.gw', 0),
(95, 'GY', 'Guyana', 'Co-operative Republic of Guyana', 'GUY', '328', 'yes', '592', '.gy', 0),
(96, 'HT', 'Haiti', 'Republic of Haiti', 'HTI', '332', 'yes', '509', '.ht', 0),
(97, 'HM', 'Heard Island and McDonald Islands', 'Heard Island and McDonald Islands', 'HMD', '334', 'no', 'NONE', '.hm', 0),
(98, 'HN', 'Honduras', 'Republic of Honduras', 'HND', '340', 'yes', '504', '.hn', 0),
(99, 'HK', 'Hong Kong', 'Hong Kong', 'HKG', '344', 'no', '852', '.hk', 0),
(100, 'HU', 'Hungary', 'Hungary', 'HUN', '348', 'yes', '36', '.hu', 0),
(101, 'IS', 'Iceland', 'Republic of Iceland', 'ISL', '352', 'yes', '354', '.is', 0),
(102, 'IN', 'India', 'Republic of India', 'IND', '356', 'yes', '91', '.in', 0),
(1, 'ID', 'Indonesia', 'Republic of Indonesia', 'IDN', '360', 'yes', '62', '.id', 0),
(104, 'IR', 'Iran', 'Islamic Republic of Iran', 'IRN', '364', 'yes', '98', '.ir', 0),
(105, 'IQ', 'Iraq', 'Republic of Iraq', 'IRQ', '368', 'yes', '964', '.iq', 0),
(106, 'IE', 'Ireland', 'Ireland', 'IRL', '372', 'yes', '353', '.ie', 0),
(107, 'IM', 'Isle of Man', 'Isle of Man', 'IMN', '833', 'no', '44', '.im', 0),
(108, 'IL', 'Israel', 'State of Israel', 'ISR', '376', 'yes', '972', '.il', 0),
(109, 'IT', 'Italy', 'Italian Republic', 'ITA', '380', 'yes', '39', '.jm', 0),
(110, 'JM', 'Jamaica', 'Jamaica', 'JAM', '388', 'yes', '1+876', '.jm', 0),
(111, 'JP', 'Japan', 'Japan', 'JPN', '392', 'yes', '81', '.jp', 0),
(112, 'JE', 'Jersey', 'The Bailiwick of Jersey', 'JEY', '832', 'no', '44', '.je', 0),
(113, 'JO', 'Jordan', 'Hashemite Kingdom of Jordan', 'JOR', '400', 'yes', '962', '.jo', 0),
(114, 'KZ', 'Kazakhstan', 'Republic of Kazakhstan', 'KAZ', '398', 'yes', '7', '.kz', 0),
(115, 'KE', 'Kenya', 'Republic of Kenya', 'KEN', '404', 'yes', '254', '.ke', 0),
(116, 'KI', 'Kiribati', 'Republic of Kiribati', 'KIR', '296', 'yes', '686', '.ki', 0),
(117, 'XK', 'Kosovo', 'Republic of Kosovo', '---', '---', 'some', '381', '', 0),
(118, 'KW', 'Kuwait', 'State of Kuwait', 'KWT', '414', 'yes', '965', '.kw', 0),
(119, 'KG', 'Kyrgyzstan', 'Kyrgyz Republic', 'KGZ', '417', 'yes', '996', '.kg', 0),
(120, 'LA', 'Laos', 'Lao People\'s Democratic Republic', 'LAO', '418', 'yes', '856', '.la', 0),
(121, 'LV', 'Latvia', 'Republic of Latvia', 'LVA', '428', 'yes', '371', '.lv', 0),
(122, 'LB', 'Lebanon', 'Republic of Lebanon', 'LBN', '422', 'yes', '961', '.lb', 0),
(123, 'LS', 'Lesotho', 'Kingdom of Lesotho', 'LSO', '426', 'yes', '266', '.ls', 0),
(124, 'LR', 'Liberia', 'Republic of Liberia', 'LBR', '430', 'yes', '231', '.lr', 0),
(125, 'LY', 'Libya', 'Libya', 'LBY', '434', 'yes', '218', '.ly', 0),
(126, 'LI', 'Liechtenstein', 'Principality of Liechtenstein', 'LIE', '438', 'yes', '423', '.li', 0),
(127, 'LT', 'Lithuania', 'Republic of Lithuania', 'LTU', '440', 'yes', '370', '.lt', 0),
(128, 'LU', 'Luxembourg', 'Grand Duchy of Luxembourg', 'LUX', '442', 'yes', '352', '.lu', 0),
(129, 'MO', 'Macao', 'The Macao Special Administrative Region', 'MAC', '446', 'no', '853', '.mo', 0),
(130, 'MK', 'Macedonia', 'The Former Yugoslav Republic of Macedonia', 'MKD', '807', 'yes', '389', '.mk', 0),
(131, 'MG', 'Madagascar', 'Republic of Madagascar', 'MDG', '450', 'yes', '261', '.mg', 0),
(132, 'MW', 'Malawi', 'Republic of Malawi', 'MWI', '454', 'yes', '265', '.mw', 0),
(133, 'MY', 'Malaysia', 'Malaysia', 'MYS', '458', 'yes', '60', '.my', 0),
(134, 'MV', 'Maldives', 'Republic of Maldives', 'MDV', '462', 'yes', '960', '.mv', 0),
(135, 'ML', 'Mali', 'Republic of Mali', 'MLI', '466', 'yes', '223', '.ml', 0),
(136, 'MT', 'Malta', 'Republic of Malta', 'MLT', '470', 'yes', '356', '.mt', 0),
(137, 'MH', 'Marshall Islands', 'Republic of the Marshall Islands', 'MHL', '584', 'yes', '692', '.mh', 0),
(138, 'MQ', 'Martinique', 'Martinique', 'MTQ', '474', 'no', '596', '.mq', 0),
(139, 'MR', 'Mauritania', 'Islamic Republic of Mauritania', 'MRT', '478', 'yes', '222', '.mr', 0),
(140, 'MU', 'Mauritius', 'Republic of Mauritius', 'MUS', '480', 'yes', '230', '.mu', 0),
(141, 'YT', 'Mayotte', 'Mayotte', 'MYT', '175', 'no', '262', '.yt', 0),
(142, 'MX', 'Mexico', 'United Mexican States', 'MEX', '484', 'yes', '52', '.mx', 0),
(143, 'FM', 'Micronesia', 'Federated States of Micronesia', 'FSM', '583', 'yes', '691', '.fm', 0),
(144, 'MD', 'Moldava', 'Republic of Moldova', 'MDA', '498', 'yes', '373', '.md', 0),
(145, 'MC', 'Monaco', 'Principality of Monaco', 'MCO', '492', 'yes', '377', '.mc', 0),
(146, 'MN', 'Mongolia', 'Mongolia', 'MNG', '496', 'yes', '976', '.mn', 0),
(147, 'ME', 'Montenegro', 'Montenegro', 'MNE', '499', 'yes', '382', '.me', 0),
(148, 'MS', 'Montserrat', 'Montserrat', 'MSR', '500', 'no', '1+664', '.ms', 0),
(149, 'MA', 'Morocco', 'Kingdom of Morocco', 'MAR', '504', 'yes', '212', '.ma', 0),
(150, 'MZ', 'Mozambique', 'Republic of Mozambique', 'MOZ', '508', 'yes', '258', '.mz', 0),
(151, 'MM', 'Myanmar (Burma)', 'Republic of the Union of Myanmar', 'MMR', '104', 'yes', '95', '.mm', 0),
(152, 'NA', 'Namibia', 'Republic of Namibia', 'NAM', '516', 'yes', '264', '.na', 0),
(153, 'NR', 'Nauru', 'Republic of Nauru', 'NRU', '520', 'yes', '674', '.nr', 0),
(154, 'NP', 'Nepal', 'Federal Democratic Republic of Nepal', 'NPL', '524', 'yes', '977', '.np', 0),
(155, 'NL', 'Netherlands', 'Kingdom of the Netherlands', 'NLD', '528', 'yes', '31', '.nl', 0),
(156, 'NC', 'New Caledonia', 'New Caledonia', 'NCL', '540', 'no', '687', '.nc', 0),
(157, 'NZ', 'New Zealand', 'New Zealand', 'NZL', '554', 'yes', '64', '.nz', 0),
(158, 'NI', 'Nicaragua', 'Republic of Nicaragua', 'NIC', '558', 'yes', '505', '.ni', 0),
(159, 'NE', 'Niger', 'Republic of Niger', 'NER', '562', 'yes', '227', '.ne', 0),
(160, 'NG', 'Nigeria', 'Federal Republic of Nigeria', 'NGA', '566', 'yes', '234', '.ng', 0),
(161, 'NU', 'Niue', 'Niue', 'NIU', '570', 'some', '683', '.nu', 0),
(162, 'NF', 'Norfolk Island', 'Norfolk Island', 'NFK', '574', 'no', '672', '.nf', 0),
(163, 'KP', 'North Korea', 'Democratic People\'s Republic of Korea', 'PRK', '408', 'yes', '850', '.kp', 0),
(164, 'MP', 'Northern Mariana Islands', 'Northern Mariana Islands', 'MNP', '580', 'no', '1+670', '.mp', 0),
(165, 'NO', 'Norway', 'Kingdom of Norway', 'NOR', '578', 'yes', '47', '.no', 0),
(166, 'OM', 'Oman', 'Sultanate of Oman', 'OMN', '512', 'yes', '968', '.om', 0),
(167, 'PK', 'Pakistan', 'Islamic Republic of Pakistan', 'PAK', '586', 'yes', '92', '.pk', 0),
(168, 'PW', 'Palau', 'Republic of Palau', 'PLW', '585', 'yes', '680', '.pw', 0),
(169, 'PS', 'Palestine', 'State of Palestine (or Occupied Palestinian Territory)', 'PSE', '275', 'some', '970', '.ps', 0),
(170, 'PA', 'Panama', 'Republic of Panama', 'PAN', '591', 'yes', '507', '.pa', 0),
(171, 'PG', 'Papua New Guinea', 'Independent State of Papua New Guinea', 'PNG', '598', 'yes', '675', '.pg', 0),
(172, 'PY', 'Paraguay', 'Republic of Paraguay', 'PRY', '600', 'yes', '595', '.py', 0),
(173, 'PE', 'Peru', 'Republic of Peru', 'PER', '604', 'yes', '51', '.pe', 0),
(174, 'PH', 'Phillipines', 'Republic of the Philippines', 'PHL', '608', 'yes', '63', '.ph', 0),
(175, 'PN', 'Pitcairn', 'Pitcairn', 'PCN', '612', 'no', 'NONE', '.pn', 0),
(176, 'PL', 'Poland', 'Republic of Poland', 'POL', '616', 'yes', '48', '.pl', 0),
(177, 'PT', 'Portugal', 'Portuguese Republic', 'PRT', '620', 'yes', '351', '.pt', 0),
(178, 'PR', 'Puerto Rico', 'Commonwealth of Puerto Rico', 'PRI', '630', 'no', '1+939', '.pr', 0),
(179, 'QA', 'Qatar', 'State of Qatar', 'QAT', '634', 'yes', '974', '.qa', 0),
(180, 'RE', 'Reunion', 'R&eacute;union', 'REU', '638', 'no', '262', '.re', 0),
(181, 'RO', 'Romania', 'Romania', 'ROU', '642', 'yes', '40', '.ro', 0),
(182, 'RU', 'Russia', 'Russian Federation', 'RUS', '643', 'yes', '7', '.ru', 0),
(183, 'RW', 'Rwanda', 'Republic of Rwanda', 'RWA', '646', 'yes', '250', '.rw', 0),
(184, 'BL', 'Saint Barthelemy', 'Saint Barth&eacute;lemy', 'BLM', '652', 'no', '590', '.bl', 0),
(185, 'SH', 'Saint Helena', 'Saint Helena, Ascension and Tristan da Cunha', 'SHN', '654', 'no', '290', '.sh', 0),
(186, 'KN', 'Saint Kitts and Nevis', 'Federation of Saint Christopher and Nevis', 'KNA', '659', 'yes', '1+869', '.kn', 0),
(187, 'LC', 'Saint Lucia', 'Saint Lucia', 'LCA', '662', 'yes', '1+758', '.lc', 0),
(188, 'MF', 'Saint Martin', 'Saint Martin', 'MAF', '663', 'no', '590', '.mf', 0),
(189, 'PM', 'Saint Pierre and Miquelon', 'Saint Pierre and Miquelon', 'SPM', '666', 'no', '508', '.pm', 0),
(190, 'VC', 'Saint Vincent and the Grenadines', 'Saint Vincent and the Grenadines', 'VCT', '670', 'yes', '1+784', '.vc', 0),
(191, 'WS', 'Samoa', 'Independent State of Samoa', 'WSM', '882', 'yes', '685', '.ws', 0),
(192, 'SM', 'San Marino', 'Republic of San Marino', 'SMR', '674', 'yes', '378', '.sm', 0),
(193, 'ST', 'Sao Tome and Principe', 'Democratic Republic of S&atilde;o Tom&eacute; and Pr&iacute;ncipe', 'STP', '678', 'yes', '239', '.st', 0),
(194, 'SA', 'Saudi Arabia', 'Kingdom of Saudi Arabia', 'SAU', '682', 'yes', '966', '.sa', 0),
(195, 'SN', 'Senegal', 'Republic of Senegal', 'SEN', '686', 'yes', '221', '.sn', 0),
(196, 'RS', 'Serbia', 'Republic of Serbia', 'SRB', '688', 'yes', '381', '.rs', 0),
(197, 'SC', 'Seychelles', 'Republic of Seychelles', 'SYC', '690', 'yes', '248', '.sc', 0),
(198, 'SL', 'Sierra Leone', 'Republic of Sierra Leone', 'SLE', '694', 'yes', '232', '.sl', 0),
(199, 'SG', 'Singapore', 'Republic of Singapore', 'SGP', '702', 'yes', '65', '.sg', 0),
(200, 'SX', 'Sint Maarten', 'Sint Maarten', 'SXM', '534', 'no', '1+721', '.sx', 0),
(201, 'SK', 'Slovakia', 'Slovak Republic', 'SVK', '703', 'yes', '421', '.sk', 0),
(202, 'SI', 'Slovenia', 'Republic of Slovenia', 'SVN', '705', 'yes', '386', '.si', 0),
(203, 'SB', 'Solomon Islands', 'Solomon Islands', 'SLB', '090', 'yes', '677', '.sb', 0),
(204, 'SO', 'Somalia', 'Somali Republic', 'SOM', '706', 'yes', '252', '.so', 0),
(205, 'ZA', 'South Africa', 'Republic of South Africa', 'ZAF', '710', 'yes', '27', '.za', 0),
(206, 'GS', 'South Georgia and the South Sandwich Islands', 'South Georgia and the South Sandwich Islands', 'SGS', '239', 'no', '500', '.gs', 0),
(207, 'KR', 'South Korea', 'Republic of Korea', 'KOR', '410', 'yes', '82', '.kr', 0),
(208, 'SS', 'South Sudan', 'Republic of South Sudan', 'SSD', '728', 'yes', '211', '.ss', 0),
(209, 'ES', 'Spain', 'Kingdom of Spain', 'ESP', '724', 'yes', '34', '.es', 0),
(210, 'LK', 'Sri Lanka', 'Democratic Socialist Republic of Sri Lanka', 'LKA', '144', 'yes', '94', '.lk', 0),
(211, 'SD', 'Sudan', 'Republic of the Sudan', 'SDN', '729', 'yes', '249', '.sd', 0),
(212, 'SR', 'Suriname', 'Republic of Suriname', 'SUR', '740', 'yes', '597', '.sr', 0),
(213, 'SJ', 'Svalbard and Jan Mayen', 'Svalbard and Jan Mayen', 'SJM', '744', 'no', '47', '.sj', 0),
(214, 'SZ', 'Swaziland', 'Kingdom of Swaziland', 'SWZ', '748', 'yes', '268', '.sz', 0),
(215, 'SE', 'Sweden', 'Kingdom of Sweden', 'SWE', '752', 'yes', '46', '.se', 0),
(216, 'CH', 'Switzerland', 'Swiss Confederation', 'CHE', '756', 'yes', '41', '.ch', 0),
(217, 'SY', 'Syria', 'Syrian Arab Republic', 'SYR', '760', 'yes', '963', '.sy', 0),
(218, 'TW', 'Taiwan', 'Republic of China (Taiwan)', 'TWN', '158', 'former', '886', '.tw', 0),
(219, 'TJ', 'Tajikistan', 'Republic of Tajikistan', 'TJK', '762', 'yes', '992', '.tj', 0),
(220, 'TZ', 'Tanzania', 'United Republic of Tanzania', 'TZA', '834', 'yes', '255', '.tz', 0),
(221, 'TH', 'Thailand', 'Kingdom of Thailand', 'THA', '764', 'yes', '66', '.th', 0),
(222, 'TL', 'Timor-Leste (East Timor)', 'Democratic Republic of Timor-Leste', 'TLS', '626', 'yes', '670', '.tl', 0),
(223, 'TG', 'Togo', 'Togolese Republic', 'TGO', '768', 'yes', '228', '.tg', 0),
(224, 'TK', 'Tokelau', 'Tokelau', 'TKL', '772', 'no', '690', '.tk', 0),
(225, 'TO', 'Tonga', 'Kingdom of Tonga', 'TON', '776', 'yes', '676', '.to', 0),
(226, 'TT', 'Trinidad and Tobago', 'Republic of Trinidad and Tobago', 'TTO', '780', 'yes', '1+868', '.tt', 0),
(227, 'TN', 'Tunisia', 'Republic of Tunisia', 'TUN', '788', 'yes', '216', '.tn', 0),
(228, 'TR', 'Turkey', 'Republic of Turkey', 'TUR', '792', 'yes', '90', '.tr', 0),
(229, 'TM', 'Turkmenistan', 'Turkmenistan', 'TKM', '795', 'yes', '993', '.tm', 0),
(230, 'TC', 'Turks and Caicos Islands', 'Turks and Caicos Islands', 'TCA', '796', 'no', '1+649', '.tc', 0),
(231, 'TV', 'Tuvalu', 'Tuvalu', 'TUV', '798', 'yes', '688', '.tv', 0),
(232, 'UG', 'Uganda', 'Republic of Uganda', 'UGA', '800', 'yes', '256', '.ug', 0),
(233, 'UA', 'Ukraine', 'Ukraine', 'UKR', '804', 'yes', '380', '.ua', 0),
(234, 'AE', 'United Arab Emirates', 'United Arab Emirates', 'ARE', '784', 'yes', '971', '.ae', 0),
(235, 'GB', 'United Kingdom', 'United Kingdom of Great Britain and Nothern Ireland', 'GBR', '826', 'yes', '44', '.uk', 0),
(236, 'US', 'United States', 'United States of America', 'USA', '840', 'yes', '1', '.us', 0),
(237, 'UM', 'United States Minor Outlying Islands', 'United States Minor Outlying Islands', 'UMI', '581', 'no', 'NONE', 'NONE', 0),
(238, 'UY', 'Uruguay', 'Eastern Republic of Uruguay', 'URY', '858', 'yes', '598', '.uy', 0),
(239, 'UZ', 'Uzbekistan', 'Republic of Uzbekistan', 'UZB', '860', 'yes', '998', '.uz', 0),
(240, 'VU', 'Vanuatu', 'Republic of Vanuatu', 'VUT', '548', 'yes', '678', '.vu', 0),
(241, 'VA', 'Vatican City', 'State of the Vatican City', 'VAT', '336', 'no', '39', '.va', 0),
(242, 'VE', 'Venezuela', 'Bolivarian Republic of Venezuela', 'VEN', '862', 'yes', '58', '.ve', 0),
(243, 'VN', 'Vietnam', 'Socialist Republic of Vietnam', 'VNM', '704', 'yes', '84', '.vn', 0),
(244, 'VG', 'Virgin Islands, British', 'British Virgin Islands', 'VGB', '092', 'no', '1+284', '.vg', 0),
(245, 'VI', 'Virgin Islands, US', 'Virgin Islands of the United States', 'VIR', '850', 'no', '1+340', '.vi', 0),
(246, 'WF', 'Wallis and Futuna', 'Wallis and Futuna', 'WLF', '876', 'no', '681', '.wf', 0),
(247, 'EH', 'Western Sahara', 'Western Sahara', 'ESH', '732', 'no', '212', '.eh', 0),
(248, 'YE', 'Yemen', 'Republic of Yemen', 'YEM', '887', 'yes', '967', '.ye', 0),
(249, 'ZM', 'Zambia', 'Republic of Zambia', 'ZMB', '894', 'yes', '260', '.zm', 0),
(250, 'ZW', 'Zimbabwe', 'Republic of Zimbabwe', 'ZWE', '716', 'yes', '263', '.zw', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cp_app_education`
--
-- Error reading structure for table ciadmin.cp_app_education: #1932 - Table 'ciadmin.cp_app_education' doesn't exist in engine
-- Error reading data for table ciadmin.cp_app_education: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `ciadmin`.`cp_app_education`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `cp_app_identity`
--
-- Error reading structure for table ciadmin.cp_app_identity: #1932 - Table 'ciadmin.cp_app_identity' doesn't exist in engine
-- Error reading data for table ciadmin.cp_app_identity: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `ciadmin`.`cp_app_identity`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `cp_app_log`
--

CREATE TABLE `cp_app_log` (
  `log_id` int(11) NOT NULL,
  `log_date` datetime DEFAULT NULL,
  `log_class` varchar(100) DEFAULT NULL,
  `log_function` varchar(100) DEFAULT NULL,
  `log_user_name` varchar(100) DEFAULT NULL,
  `log_user_id` int(11) DEFAULT NULL,
  `log_role` varchar(70) DEFAULT NULL,
  `log_ip` varchar(100) DEFAULT NULL,
  `log_user_agent` varchar(255) DEFAULT NULL,
  `log_url` varchar(255) NOT NULL,
  `log_var_get` text,
  `log_var_post` text,
  `app_id` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cp_app_province`
--

CREATE TABLE `cp_app_province` (
  `id` int(10) UNSIGNED NOT NULL,
  `kd_provinsi` varchar(10) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `nama_provinsi` varchar(100) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `keterangan` text CHARACTER SET latin1,
  `status` char(1) CHARACTER SET latin1 NOT NULL DEFAULT '1',
  `is_trash` char(1) CHARACTER SET latin1 NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `cp_app_province`
--

INSERT INTO `cp_app_province` (`id`, `kd_provinsi`, `nama_provinsi`, `keterangan`, `status`, `is_trash`) VALUES
(1, '1', 'Bali', '', '1', '0'),
(2, '2', 'Bangka Belitung', '', '1', '0'),
(3, '3', 'Banten', '', '1', '0'),
(4, '4', 'Bengkulu', '', '1', '0'),
(5, '5', 'DI Yogyakarta', '', '1', '0'),
(6, '6', 'DKI Jakarta', '', '1', '0'),
(7, '7', 'Gorontalo', '', '1', '0'),
(8, '8', 'Jambi', '', '1', '0'),
(9, '9', 'Jawa Barat', '', '1', '0'),
(10, '10', 'Jawa Tengah', '', '1', '0'),
(11, '11', 'Jawa Timur', '', '1', '0'),
(12, '12', 'Kalimantan Barat', '', '1', '0'),
(13, '13', 'Kalimantan Selatan', '', '1', '0'),
(14, '14', 'Kalimantan Tengah', '', '1', '0'),
(15, '15', 'Kalimantan Timur', '', '1', '0'),
(16, '16', 'Kalimantan Utara', '', '1', '0'),
(17, '17', 'Kepulauan Riau', '', '1', '0'),
(18, '18', 'Lampung', '', '1', '0'),
(19, '19', 'Maluku', '', '1', '0'),
(20, '20', 'Maluku Utara', '', '1', '0'),
(21, '21', 'Aceh', '', '1', '0'),
(22, '22', 'Nusa Tenggara Barat (NTB)', '', '1', '0'),
(23, '23', 'Nusa Tenggara Timur (NTT)', '', '1', '0'),
(24, '24', 'Papua', '', '1', '0'),
(25, '25', 'Papua Barat', '', '1', '0'),
(26, '26', 'Riau', '', '1', '0'),
(27, '27', 'Sulawesi Barat', '', '1', '0'),
(28, '28', 'Sulawesi Selatan', '', '1', '0'),
(29, '29', 'Sulawesi Tengah', '', '1', '0'),
(30, '30', 'Sulawesi Tenggara', '', '1', '0'),
(31, '31', 'Sulawesi Utara', '', '1', '0'),
(32, '32', 'Sumatera Barat', '', '1', '0'),
(33, '33', 'Sumatera Selatan', '', '1', '0'),
(34, '34', 'Sumatera Utara', 'Sumatera Utara', '1', '0');

-- --------------------------------------------------------

--
-- Table structure for table `cp_app_sessions`
--

CREATE TABLE `cp_app_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(50) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `cp_app_sessions`
--

INSERT INTO `cp_app_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('8c021dfdcd9b9b74ee44c2d00f2dd270', '8.37.232.70', 'Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) Ap', 1548208081, 'a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"jcfg\";a:16:{s:9:\"cp_app_id\";s:1:\"1\";s:11:\"cp_app_name\";s:0:\"\";s:8:\"is_login\";i:0;s:4:\"view\";a:2:{s:4:\"data\";s:3:\"all\";s:1:\"t\";s:3:\"all\";}s:4:\"user\";a:9:{s:2:\"id\";s:0:\"\";s:4:\"name\";s:5:\"guest\";s:8:\"fullname\";s:5:\"Guest\";s:5:\"level\";s:0:\"\";s:6:\"is_all\";i:0;s:5:\"color\";s:4:\"mine\";s:2:\"bg\";s:6:\"ptrn_e\";s:10:\"ujian_type\";s:0:\"\";s:5:\"email\";s:0:\"\";}s:4:\"menu\";a:0:{}s:13:\"current_class\";s:0:\"\";s:15:\"current_funtion\";s:0:\"\";s:11:\"mod_rewrite\";i:1;s:5:\"theme\";s:10:\"front/wika\";s:6:\"search\";a:9:{s:5:\"class\";s:0:\"\";s:10:\"date_start\";s:0:\"\";s:8:\"date_end\";s:0:\"\";s:6:\"status\";s:0:\"\";s:8:\"per_page\";i:20;s:8:\"order_by\";s:0:\"\";s:5:\"colum\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:9:\"order_dir\";s:3:\"ASC\";}s:7:\"referer\";s:0:\"\";s:11:\"chat_online\";a:0:{}s:6:\"access\";a:0:{}s:4:\"lang\";s:3:\"ind\";s:7:\"captcha\";a:0:{}}}'),
('ded4270270458b1fa2394ab8aede5c80', '180.246.221.239', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0)', 1548208597, 'a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"jcfg\";a:16:{s:9:\"cp_app_id\";s:1:\"1\";s:11:\"cp_app_name\";s:0:\"\";s:8:\"is_login\";i:0;s:4:\"view\";a:2:{s:4:\"data\";s:3:\"all\";s:1:\"t\";s:3:\"all\";}s:4:\"user\";a:9:{s:2:\"id\";s:0:\"\";s:4:\"name\";s:5:\"guest\";s:8:\"fullname\";s:5:\"Guest\";s:5:\"level\";s:0:\"\";s:6:\"is_all\";i:0;s:5:\"color\";s:4:\"mine\";s:2:\"bg\";s:6:\"ptrn_e\";s:10:\"ujian_type\";s:0:\"\";s:5:\"email\";s:0:\"\";}s:4:\"menu\";a:0:{}s:13:\"current_class\";s:0:\"\";s:15:\"current_funtion\";s:0:\"\";s:11:\"mod_rewrite\";i:1;s:5:\"theme\";s:10:\"front/wika\";s:6:\"search\";a:9:{s:5:\"class\";s:0:\"\";s:10:\"date_start\";s:0:\"\";s:8:\"date_end\";s:0:\"\";s:6:\"status\";s:0:\"\";s:8:\"per_page\";i:20;s:8:\"order_by\";s:0:\"\";s:5:\"colum\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:9:\"order_dir\";s:3:\"ASC\";}s:7:\"referer\";s:0:\"\";s:11:\"chat_online\";a:0:{}s:6:\"access\";a:0:{}s:4:\"lang\";s:3:\"ind\";s:7:\"captcha\";a:0:{}}}'),
('b0cf88290ab6d28a26b29a83eb67cf66', '103.83.173.13', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1548215389, 'a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"jcfg\";a:16:{s:9:\"cp_app_id\";s:1:\"1\";s:11:\"cp_app_name\";s:0:\"\";s:8:\"is_login\";i:0;s:4:\"view\";a:2:{s:4:\"data\";s:3:\"all\";s:1:\"t\";s:3:\"all\";}s:4:\"user\";a:9:{s:2:\"id\";s:0:\"\";s:4:\"name\";s:5:\"guest\";s:8:\"fullname\";s:5:\"Guest\";s:5:\"level\";s:0:\"\";s:6:\"is_all\";i:0;s:5:\"color\";s:4:\"mine\";s:2:\"bg\";s:6:\"ptrn_e\";s:10:\"ujian_type\";s:0:\"\";s:5:\"email\";s:0:\"\";}s:4:\"menu\";a:0:{}s:13:\"current_class\";s:0:\"\";s:15:\"current_funtion\";s:0:\"\";s:11:\"mod_rewrite\";i:1;s:5:\"theme\";s:10:\"front/wika\";s:6:\"search\";a:9:{s:5:\"class\";s:0:\"\";s:10:\"date_start\";s:0:\"\";s:8:\"date_end\";s:0:\"\";s:6:\"status\";s:0:\"\";s:8:\"per_page\";i:20;s:8:\"order_by\";s:0:\"\";s:5:\"colum\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:9:\"order_dir\";s:3:\"ASC\";}s:7:\"referer\";s:0:\"\";s:11:\"chat_online\";a:0:{}s:6:\"access\";a:0:{}s:4:\"lang\";s:3:\"ind\";s:7:\"captcha\";a:0:{}}}'),
('50c5a40d56dffdda8e7ff189a29503fd', '207.180.218.247', 'Mozilla/5.0 (compatible; MJ12bot/v1.4.8; http://mj', 1548215580, 'a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"jcfg\";a:16:{s:9:\"cp_app_id\";s:1:\"1\";s:11:\"cp_app_name\";s:0:\"\";s:8:\"is_login\";i:0;s:4:\"view\";a:2:{s:4:\"data\";s:3:\"all\";s:1:\"t\";s:3:\"all\";}s:4:\"user\";a:9:{s:2:\"id\";s:0:\"\";s:4:\"name\";s:5:\"guest\";s:8:\"fullname\";s:5:\"Guest\";s:5:\"level\";s:0:\"\";s:6:\"is_all\";i:0;s:5:\"color\";s:4:\"mine\";s:2:\"bg\";s:6:\"ptrn_e\";s:10:\"ujian_type\";s:0:\"\";s:5:\"email\";s:0:\"\";}s:4:\"menu\";a:0:{}s:13:\"current_class\";s:0:\"\";s:15:\"current_funtion\";s:0:\"\";s:11:\"mod_rewrite\";i:1;s:5:\"theme\";s:10:\"front/wika\";s:6:\"search\";a:9:{s:5:\"class\";s:0:\"\";s:10:\"date_start\";s:0:\"\";s:8:\"date_end\";s:0:\"\";s:6:\"status\";s:0:\"\";s:8:\"per_page\";i:20;s:8:\"order_by\";s:0:\"\";s:5:\"colum\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:9:\"order_dir\";s:3:\"ASC\";}s:7:\"referer\";s:0:\"\";s:11:\"chat_online\";a:0:{}s:6:\"access\";a:0:{}s:4:\"lang\";s:3:\"ind\";s:7:\"captcha\";a:0:{}}}'),
('b0360190b66e3d404ea5c83669217c61', '103.83.173.13', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1548216325, 'a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"jcfg\";a:16:{s:9:\"cp_app_id\";s:1:\"1\";s:11:\"cp_app_name\";s:0:\"\";s:8:\"is_login\";i:0;s:4:\"view\";a:2:{s:4:\"data\";s:3:\"all\";s:1:\"t\";s:3:\"all\";}s:4:\"user\";a:9:{s:2:\"id\";s:0:\"\";s:4:\"name\";s:5:\"guest\";s:8:\"fullname\";s:5:\"Guest\";s:5:\"level\";s:0:\"\";s:6:\"is_all\";i:0;s:5:\"color\";s:4:\"mine\";s:2:\"bg\";s:6:\"ptrn_e\";s:10:\"ujian_type\";s:0:\"\";s:5:\"email\";s:0:\"\";}s:4:\"menu\";a:0:{}s:13:\"current_class\";s:0:\"\";s:15:\"current_funtion\";s:0:\"\";s:11:\"mod_rewrite\";i:1;s:5:\"theme\";s:10:\"front/wika\";s:6:\"search\";a:9:{s:5:\"class\";s:0:\"\";s:10:\"date_start\";s:0:\"\";s:8:\"date_end\";s:0:\"\";s:6:\"status\";s:0:\"\";s:8:\"per_page\";i:20;s:8:\"order_by\";s:0:\"\";s:5:\"colum\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:9:\"order_dir\";s:3:\"ASC\";}s:7:\"referer\";s:0:\"\";s:11:\"chat_online\";a:0:{}s:6:\"access\";a:0:{}s:4:\"lang\";s:3:\"ind\";s:7:\"captcha\";a:0:{}}}'),
('fb18bb48896fba42940546f8b139f35b', '207.46.13.13', 'Mozilla/5.0 (compatible; bingbot/2.0; +http://www.', 1548220079, 'a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"jcfg\";a:16:{s:9:\"cp_app_id\";s:1:\"1\";s:11:\"cp_app_name\";s:0:\"\";s:8:\"is_login\";i:0;s:4:\"view\";a:2:{s:4:\"data\";s:3:\"all\";s:1:\"t\";s:3:\"all\";}s:4:\"user\";a:9:{s:2:\"id\";s:0:\"\";s:4:\"name\";s:5:\"guest\";s:8:\"fullname\";s:5:\"Guest\";s:5:\"level\";s:0:\"\";s:6:\"is_all\";i:0;s:5:\"color\";s:4:\"mine\";s:2:\"bg\";s:6:\"ptrn_e\";s:10:\"ujian_type\";s:0:\"\";s:5:\"email\";s:0:\"\";}s:4:\"menu\";a:0:{}s:13:\"current_class\";s:0:\"\";s:15:\"current_funtion\";s:0:\"\";s:11:\"mod_rewrite\";i:1;s:5:\"theme\";s:10:\"front/wika\";s:6:\"search\";a:9:{s:5:\"class\";s:0:\"\";s:10:\"date_start\";s:0:\"\";s:8:\"date_end\";s:0:\"\";s:6:\"status\";s:0:\"\";s:8:\"per_page\";i:20;s:8:\"order_by\";s:0:\"\";s:5:\"colum\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:9:\"order_dir\";s:3:\"ASC\";}s:7:\"referer\";s:0:\"\";s:11:\"chat_online\";a:0:{}s:6:\"access\";a:0:{}s:4:\"lang\";s:3:\"ind\";s:7:\"captcha\";a:0:{}}}'),
('eeabbf6f82cfccf085c241f19a42e954', '110.136.67.162', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1548222644, 'a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"jcfg\";a:16:{s:9:\"cp_app_id\";s:1:\"1\";s:11:\"cp_app_name\";s:0:\"\";s:8:\"is_login\";i:0;s:4:\"view\";a:2:{s:4:\"data\";s:3:\"all\";s:1:\"t\";s:3:\"all\";}s:4:\"user\";a:9:{s:2:\"id\";s:0:\"\";s:4:\"name\";s:5:\"guest\";s:8:\"fullname\";s:5:\"Guest\";s:5:\"level\";s:0:\"\";s:6:\"is_all\";i:0;s:5:\"color\";s:4:\"mine\";s:2:\"bg\";s:6:\"ptrn_e\";s:10:\"ujian_type\";s:0:\"\";s:5:\"email\";s:0:\"\";}s:4:\"menu\";a:0:{}s:13:\"current_class\";s:0:\"\";s:15:\"current_funtion\";s:0:\"\";s:11:\"mod_rewrite\";i:1;s:5:\"theme\";s:10:\"front/wika\";s:6:\"search\";a:9:{s:5:\"class\";s:0:\"\";s:10:\"date_start\";s:0:\"\";s:8:\"date_end\";s:0:\"\";s:6:\"status\";s:0:\"\";s:8:\"per_page\";i:20;s:8:\"order_by\";s:0:\"\";s:5:\"colum\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:9:\"order_dir\";s:3:\"ASC\";}s:7:\"referer\";s:0:\"\";s:11:\"chat_online\";a:0:{}s:6:\"access\";a:0:{}s:4:\"lang\";s:3:\"ind\";s:7:\"captcha\";a:0:{}}}'),
('b5b9b788429d1436b7cb0a4ee0d8e94c', '176.74.192.71', 'Mozilla/5.0 (X11; Linux x86_64; rv:63.0) Gecko/201', 1548224419, 'a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"jcfg\";a:16:{s:9:\"cp_app_id\";s:1:\"1\";s:11:\"cp_app_name\";s:0:\"\";s:8:\"is_login\";i:0;s:4:\"view\";a:2:{s:4:\"data\";s:3:\"all\";s:1:\"t\";s:3:\"all\";}s:4:\"user\";a:9:{s:2:\"id\";s:0:\"\";s:4:\"name\";s:5:\"guest\";s:8:\"fullname\";s:5:\"Guest\";s:5:\"level\";s:0:\"\";s:6:\"is_all\";i:0;s:5:\"color\";s:4:\"mine\";s:2:\"bg\";s:6:\"ptrn_e\";s:10:\"ujian_type\";s:0:\"\";s:5:\"email\";s:0:\"\";}s:4:\"menu\";a:0:{}s:13:\"current_class\";s:0:\"\";s:15:\"current_funtion\";s:0:\"\";s:11:\"mod_rewrite\";i:1;s:5:\"theme\";s:10:\"front/wika\";s:6:\"search\";a:9:{s:5:\"class\";s:0:\"\";s:10:\"date_start\";s:0:\"\";s:8:\"date_end\";s:0:\"\";s:6:\"status\";s:0:\"\";s:8:\"per_page\";i:20;s:8:\"order_by\";s:0:\"\";s:5:\"colum\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:9:\"order_dir\";s:3:\"ASC\";}s:7:\"referer\";s:0:\"\";s:11:\"chat_online\";a:0:{}s:6:\"access\";a:0:{}s:4:\"lang\";s:3:\"ind\";s:7:\"captcha\";a:0:{}}}'),
('4ecc212e2e7fd8fa7f80c38d422478fc', '110.136.67.162', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1548227334, 'a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"jcfg\";a:16:{s:9:\"cp_app_id\";s:1:\"1\";s:11:\"cp_app_name\";s:0:\"\";s:8:\"is_login\";i:0;s:4:\"view\";a:2:{s:4:\"data\";s:3:\"all\";s:1:\"t\";s:3:\"all\";}s:4:\"user\";a:9:{s:2:\"id\";s:0:\"\";s:4:\"name\";s:5:\"guest\";s:8:\"fullname\";s:5:\"Guest\";s:5:\"level\";s:0:\"\";s:6:\"is_all\";i:0;s:5:\"color\";s:4:\"mine\";s:2:\"bg\";s:6:\"ptrn_e\";s:10:\"ujian_type\";s:0:\"\";s:5:\"email\";s:0:\"\";}s:4:\"menu\";a:0:{}s:13:\"current_class\";s:0:\"\";s:15:\"current_funtion\";s:0:\"\";s:11:\"mod_rewrite\";i:1;s:5:\"theme\";s:10:\"front/wika\";s:6:\"search\";a:9:{s:5:\"class\";s:0:\"\";s:10:\"date_start\";s:0:\"\";s:8:\"date_end\";s:0:\"\";s:6:\"status\";s:0:\"\";s:8:\"per_page\";i:20;s:8:\"order_by\";s:0:\"\";s:5:\"colum\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:9:\"order_dir\";s:3:\"ASC\";}s:7:\"referer\";s:0:\"\";s:11:\"chat_online\";a:0:{}s:6:\"access\";a:0:{}s:4:\"lang\";s:3:\"ind\";s:7:\"captcha\";a:0:{}}}'),
('8dfd885a63744208d2a7d3d2ad52923b', '110.136.67.162', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWeb', 1548227413, 'a:2:{s:9:\"user_data\";s:0:\"\";s:4:\"jcfg\";a:16:{s:9:\"cp_app_id\";s:1:\"1\";s:11:\"cp_app_name\";s:0:\"\";s:8:\"is_login\";i:0;s:4:\"view\";a:2:{s:4:\"data\";s:3:\"all\";s:1:\"t\";s:3:\"all\";}s:4:\"user\";a:9:{s:2:\"id\";s:0:\"\";s:4:\"name\";s:5:\"guest\";s:8:\"fullname\";s:5:\"Guest\";s:5:\"level\";s:0:\"\";s:6:\"is_all\";i:0;s:5:\"color\";s:4:\"mine\";s:2:\"bg\";s:6:\"ptrn_e\";s:10:\"ujian_type\";s:0:\"\";s:5:\"email\";s:0:\"\";}s:4:\"menu\";a:0:{}s:13:\"current_class\";s:0:\"\";s:15:\"current_funtion\";s:0:\"\";s:11:\"mod_rewrite\";i:1;s:5:\"theme\";s:10:\"front/wika\";s:6:\"search\";a:9:{s:5:\"class\";s:0:\"\";s:10:\"date_start\";s:0:\"\";s:8:\"date_end\";s:0:\"\";s:6:\"status\";s:0:\"\";s:8:\"per_page\";i:20;s:8:\"order_by\";s:0:\"\";s:5:\"colum\";s:0:\"\";s:7:\"keyword\";s:0:\"\";s:9:\"order_dir\";s:3:\"ASC\";}s:7:\"referer\";s:0:\"\";s:11:\"chat_online\";a:0:{}s:6:\"access\";a:0:{}s:4:\"lang\";s:3:\"ind\";s:7:\"captcha\";a:0:{}}}');

-- --------------------------------------------------------

--
-- Table structure for table `cp_app_user`
--

CREATE TABLE `cp_app_user` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `username` varchar(100) NOT NULL DEFAULT '',
  `user_password` varchar(255) NOT NULL DEFAULT '',
  `user_status` int(1) DEFAULT NULL,
  `time_add` datetime DEFAULT NULL,
  `time_update` datetime DEFAULT NULL,
  `user_add` varchar(50) DEFAULT NULL,
  `user_update` varchar(50) DEFAULT NULL,
  `user_logindate` datetime DEFAULT NULL,
  `app_id` int(5) DEFAULT NULL,
  `is_trash` int(11) NOT NULL DEFAULT '0',
  `user_group` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_app_user`
--

INSERT INTO `cp_app_user` (`user_id`, `username`, `user_password`, `user_status`, `time_add`, `time_update`, `user_add`, `user_update`, `user_logindate`, `app_id`, `is_trash`, `user_group`) VALUES
(1, 'admin', '4297f44b13955235245b2497399d7a93', 1, NULL, '2015-10-01 07:23:22', NULL, '1', '2019-06-18 10:02:18', 1, 0, 1),
(3, 'buyer1', 'd41d8cd98f00b204e9800998ecf8427e', 1, '2019-05-05 00:57:32', '2019-05-07 14:37:32', '1', '1', '0000-00-00 00:00:00', NULL, 0, 2),
(2, 'Vendor1', '4297f44b13955235245b2497399d7a93', 1, '2019-05-05 00:57:32', '2019-05-05 00:57:32', '1', '1', NULL, NULL, 0, 3),
(4, 'buyer2', '4297f44b13955235245b2497399d7a93', 1, '2019-05-05 00:57:32', '2019-05-05 00:57:32', '1', '1', NULL, NULL, 0, 3);

-- --------------------------------------------------------

--
-- Table structure for table `cp_article`
--

CREATE TABLE `cp_article` (
  `article_id` int(11) NOT NULL,
  `article_date` datetime NOT NULL,
  `article_date_revisi` datetime NOT NULL,
  `article_status` char(1) NOT NULL DEFAULT '1',
  `article_istrash` char(1) NOT NULL DEFAULT '0',
  `article_user_id` int(11) NOT NULL,
  `article_publishdate` datetime NOT NULL,
  `article_title` varchar(255) NOT NULL,
  `article_content` longtext NOT NULL,
  `article_file` varchar(255) NOT NULL,
  `article_category_id` int(11) NOT NULL,
  `article_departement_id` int(11) NOT NULL,
  `article_company_id` int(11) NOT NULL,
  `article_meta_description` varchar(255) NOT NULL,
  `article_meta_keywords` varchar(255) NOT NULL,
  `article_count` int(11) NOT NULL,
  `article_lead` varchar(255) NOT NULL,
  `article_comment_count` varchar(11) NOT NULL DEFAULT '0',
  `article_ext` varchar(50) NOT NULL,
  `article_topik` text NOT NULL,
  `public` int(2) NOT NULL,
  `private` int(2) NOT NULL,
  `share_dept` int(2) NOT NULL,
  `article_terkait` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_article`
--

INSERT INTO `cp_article` (`article_id`, `article_date`, `article_date_revisi`, `article_status`, `article_istrash`, `article_user_id`, `article_publishdate`, `article_title`, `article_content`, `article_file`, `article_category_id`, `article_departement_id`, `article_company_id`, `article_meta_description`, `article_meta_keywords`, `article_count`, `article_lead`, `article_comment_count`, `article_ext`, `article_topik`, `public`, `private`, `share_dept`, `article_terkait`) VALUES
(1, '2015-08-10 13:06:19', '0000-00-00 00:00:00', '1', '0', 1, '2015-08-10 13:06:00', 'Document SIPKD2', '<p>Document SIPKD2</p>\r\n', '', 5, 8, 2, '', '', 0, '', '0', '', 'project 5, project1', 1, 0, 0, '5, 4'),
(2, '2015-08-10 18:27:27', '0000-00-00 00:00:00', '1', '0', 1, '2015-08-10 18:26:00', 'test doc', '<p>test error</p>\r\n', '', 1, 2, 1, '', '', 0, '', '0', '', 'project9', 1, 0, 0, '8'),
(3, '2015-08-11 14:02:54', '0000-00-00 00:00:00', '1', '0', 1, '2015-08-11 14:02:00', 'Document test', '<p>Document test</p>\r\n', '', 1, 2, 1, '', '', 0, '', '0', '', 'project8', 1, 0, 0, '7');

-- --------------------------------------------------------

--
-- Table structure for table `cp_article_category`
--

CREATE TABLE `cp_article_category` (
  `category_id` int(11) UNSIGNED NOT NULL,
  `category_date` datetime NOT NULL,
  `category_status` char(1) NOT NULL DEFAULT '1',
  `category_istrash` char(1) NOT NULL DEFAULT '0',
  `category_title` varchar(255) NOT NULL,
  `category_desc` varchar(255) NOT NULL,
  `category_parent_id` int(11) NOT NULL,
  `category_order` int(5) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_article_category`
--

INSERT INTO `cp_article_category` (`category_id`, `category_date`, `category_status`, `category_istrash`, `category_title`, `category_desc`, `category_parent_id`, `category_order`) VALUES
(1, '2015-04-25 11:31:49', '1', '0', 'File Kantor', 'Berisi File Kantor', 0, 0),
(2, '2015-01-25 13:56:24', '1', '0', 'Gallery', 'gallery', 3, 0),
(3, '2015-01-25 13:56:11', '1', '0', 'Sertifikat', 'Sertifikat', 1, 0),
(4, '2015-01-26 12:36:10', '1', '0', 'Presentasi', 'Presentasi', 0, 0),
(5, '2015-01-26 12:36:16', '1', '0', 'Brosur / White Paper', 'Brosur / White Paper', 0, 0),
(6, '2015-01-26 12:36:23', '1', '0', 'Film', 'Film', 0, 0),
(7, '2015-01-26 12:36:29', '1', '0', 'SK', 'SK', 0, 0),
(8, '2015-01-26 12:36:39', '1', '0', 'Peraturan', 'Peraturan', 0, 0),
(9, '2015-01-26 12:36:47', '1', '0', 'SK Manajer', 'SK Manajer', 0, 0),
(10, '2015-01-26 12:36:57', '1', '0', 'Kontrak', 'Kontrak', 0, 0),
(11, '2015-01-26 12:37:03', '1', '0', 'Sertifikat', 'Sertifikat', 0, 0),
(12, '2015-04-01 17:30:00', '1', '0', 'powerpoint', 'powerpoint', 4, 0),
(13, '2015-04-20 12:41:21', '1', '0', 'apache', 'dfdfdfdf', 3, 0),
(14, '2015-08-10 15:53:22', '1', '0', 'News', 'News', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cp_article_category_link`
--
-- Error reading structure for table ciadmin.cp_article_category_link: #1932 - Table 'ciadmin.cp_article_category_link' doesn't exist in engine
-- Error reading data for table ciadmin.cp_article_category_link: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `ciadmin`.`cp_article_category_link`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `cp_banner`
--

CREATE TABLE `cp_banner` (
  `id` int(11) NOT NULL,
  `carousel` int(1) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `side` int(1) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `is_trash` int(1) DEFAULT NULL,
  `status` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_banner`
--

INSERT INTO `cp_banner` (`id`, `carousel`, `url`, `side`, `image`, `is_trash`, `status`) VALUES
(1, 1, NULL, 0, '1560080798-slide3.jpg', 0, 1),
(2, 1, NULL, 0, '1560080799-slide4.jpg', 0, 1),
(3, 1, NULL, 0, '1560080799-slide1.jpg', 0, 1),
(4, 1, NULL, 0, '1560080799-slide2.jpg', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cp_barang`
--

CREATE TABLE `cp_barang` (
  `id` int(11) NOT NULL,
  `nama_barang` varchar(255) NOT NULL,
  `kategori_barang` int(11) NOT NULL,
  `deskripsi` longtext,
  `product_strength` varchar(255) DEFAULT NULL,
  `outlet_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_publish` datetime DEFAULT NULL,
  `is_stock` int(1) DEFAULT NULL,
  `is_new` int(1) DEFAULT NULL,
  `is_trash` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_barang`
--

INSERT INTO `cp_barang` (`id`, `nama_barang`, `kategori_barang`, `deskripsi`, `product_strength`, `outlet_id`, `user_id`, `date_publish`, `is_stock`, `is_new`, `is_trash`) VALUES
(1, 'Sepatu Slip On GDNS', 15, 'Sepatu Slip On GDNS', 'SHOES', 1, 1, '2019-05-27 16:33:05', 1, 1, 0),
(2, 'Sepatu Slip On Viral Goodness', 15, 'Sepatu Slip On Viral Goodness', 'SHOES BEROOOOO', 1, 1, '2019-05-25 16:33:13', 2, 1, 0),
(4, 'Kaos Walhi', 13, 'Kaos Walhi', 'kaos', 2, 4, '2019-05-26 16:33:18', 2, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cp_barang_detail`
--

CREATE TABLE `cp_barang_detail` (
  `id` int(11) NOT NULL,
  `barang_id` int(11) NOT NULL,
  `harga_satuan` int(11) NOT NULL,
  `discount` int(4) DEFAULT '0',
  `perkiraan_berat` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `pembeliaan_min` int(11) NOT NULL,
  `pembelian_max` int(11) DEFAULT NULL,
  `kondisi_barang` int(11) NOT NULL,
  `barang_impor` int(11) DEFAULT NULL,
  `merek_id` int(11) NOT NULL,
  `video_url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_barang_detail`
--

INSERT INTO `cp_barang_detail` (`id`, `barang_id`, `harga_satuan`, `discount`, `perkiraan_berat`, `stok`, `pembeliaan_min`, `pembelian_max`, `kondisi_barang`, `barang_impor`, `merek_id`, `video_url`) VALUES
(1, 1, 100000, 0, 400, 100, 2, 100, 1, NULL, 0, '0'),
(2, 2, 1000000, 0, 400, 100, 2, 100, 1, NULL, 0, '0'),
(4, 4, 1000000, 0, 100, 100, 0, NULL, 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cp_barang_gambar`
--

CREATE TABLE `cp_barang_gambar` (
  `id` int(11) NOT NULL,
  `barang_id` int(11) DEFAULT NULL,
  `gambar_name` text,
  `stars` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_barang_gambar`
--

INSERT INTO `cp_barang_gambar` (`id`, `barang_id`, `gambar_name`, `stars`) VALUES
(5, 2, 'slipon4.jpg', 0),
(6, 2, 'slipon2.jpg', 0),
(7, 2, 'slipon1.jpg', 0),
(18, 1, '1559237520-slipon4.jpg', 1),
(19, 1, '1559237521-slipon6.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cp_barang_varian`
--

CREATE TABLE `cp_barang_varian` (
  `id` int(11) NOT NULL,
  `barang_id` int(11) NOT NULL,
  `jenis_varian` varchar(255) NOT NULL,
  `varian_barang` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_barang_varian`
--

INSERT INTO `cp_barang_varian` (`id`, `barang_id`, `jenis_varian`, `varian_barang`) VALUES
(7, 4, 'ukuran', '1,2,3,4,0,0,0'),
(8, 4, 'warna', '1,2,3,0,0,0,0,0,0'),
(21, 2, 'ukuran', '9,10,0,0,13,14,0,16,17,0,0,0'),
(22, 2, 'warna', '0,2,3,0,5,6,7,0,0'),
(23, 1, 'ukuran', '0,0,0,0,0,0,0,0,0,0,0,0'),
(24, 1, 'warna', '0,0,0,0,0,0,0,0,0');

-- --------------------------------------------------------

--
-- Table structure for table `cp_barang_varian_detail`
--

CREATE TABLE `cp_barang_varian_detail` (
  `id` int(11) NOT NULL,
  `barang_id` int(11) NOT NULL,
  `ukuran_id` int(11) NOT NULL,
  `warna_id` int(11) DEFAULT NULL,
  `harga` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `sku` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_barang_varian_detail`
--

INSERT INTO `cp_barang_varian_detail` (`id`, `barang_id`, `ukuran_id`, `warna_id`, `harga`, `stok`, `sku`) VALUES
(1, 1, 9, 1, 100000, 10, '0'),
(2, 1, 11, 1, 100000, 10, '0'),
(3, 1, 13, 1, 100000, 10, '0'),
(4, 1, 9, 2, 120000, 10, '0'),
(5, 1, 11, 2, 120000, 10, '0'),
(6, 1, 13, 2, 120000, 10, '0'),
(7, 2, 9, 1, 100000, 10, '0'),
(8, 2, 9, 3, 100000, 5, '0'),
(9, 2, 11, 1, 110000, 15, '0'),
(10, 2, 11, 3, 115000, 10, '0');

-- --------------------------------------------------------

--
-- Table structure for table `cp_buyer`
--

CREATE TABLE `cp_buyer` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `nama_lengkap` varchar(255) DEFAULT NULL,
  `telp` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `provinsi_id` int(1) DEFAULT NULL,
  `kota_id` int(1) DEFAULT NULL,
  `post_code` varchar(255) DEFAULT NULL,
  `alamat` mediumtext,
  `country` int(11) DEFAULT NULL,
  `is_trash` int(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_buyer`
--

INSERT INTO `cp_buyer` (`id`, `user_id`, `nama_lengkap`, `telp`, `email`, `photo`, `provinsi_id`, `kota_id`, `post_code`, `alamat`, `country`, `is_trash`) VALUES
(1, 1, 'Risa Sungkan Dari', '089766541243', 'risa@gmail.com', '1559314573-download__2_.jpg', 9, 76, '19877', 'Jl. Hutan Pinus 1 Blok C3 No.14 Bogor Utara', 1, 0),
(2, 2, 'Kira Rizuka', '088722127654', 'kira@gmail.com', NULL, NULL, NULL, NULL, NULL, 1, 0),
(3, 4, 'walhi', '089766541243', 'walhi@gmail.com', '', NULL, NULL, NULL, NULL, 0, 0),
(4, 6, 'buyer3', '089765461234', 'buyer3@gmail.com', NULL, 5, 35, '98872', 'Jl. Nusa Dua , Blok C7 No. 18 Taman Angsa Cicurug , Kab. Bogor', 1, 0),
(7, 9, 'Afghani Rakamuraz', NULL, 'jokerhell945@gmail.com', 'https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=2433977153333901&height=50&width=50&ext=1562794205&hash=AeTQrb9hi7yIdSXe', NULL, NULL, NULL, NULL, NULL, 0),
(8, 10, 'vendor2', '09899899898', 'vendor2@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cp_buyer_address`
--

CREATE TABLE `cp_buyer_address` (
  `id` int(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `buyer_id` int(11) NOT NULL,
  `provinsi_id` int(11) DEFAULT NULL,
  `kota_id` int(11) DEFAULT NULL,
  `alamat` mediumtext,
  `post_code` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_buyer_address`
--

INSERT INTO `cp_buyer_address` (`id`, `title`, `buyer_id`, `provinsi_id`, `kota_id`, `alamat`, `post_code`) VALUES
(3, 'Rumah 1', 2, 9, 63, 'Jl. Buleleng No. 19 Blok E4 Taman Angsa', '15990'),
(4, 'Rumah 2', 2, 27, 370, 'Jl. Kemayoran Kembar No. 19 Blok CC4 Kampung Sunter', '12022'),
(16, 'Alamat Kantor', 2, 9, 63, 'Jl. Hutan Pinus 1 Blok C3 No.14 Bogor Utara', '19877'),
(17, 'Alamat Kantor', 1, 9, 22, 'Jl. Nusa Dua , Blok C7 No. 18 Taman Angsa Cicurug , Kab. Bandung', '98872'),
(18, 'Alamat Rumah 2', 1, 9, 78, 'Jl. Gagak Blok C99 No.18 Tm.Pagelaran , Ciomas , Bogor', '16110');

-- --------------------------------------------------------

--
-- Table structure for table `cp_cart`
--

CREATE TABLE `cp_cart` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `barang_id` int(1) DEFAULT NULL,
  `warna_id` int(1) DEFAULT NULL,
  `ukuran_id` int(1) DEFAULT NULL,
  `qty` int(3) DEFAULT NULL,
  `status_invoice` int(1) DEFAULT NULL,
  `tanggal_order` datetime DEFAULT NULL,
  `tanggal_checkout` datetime DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `tax` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_cart`
--

INSERT INTO `cp_cart` (`id`, `user_id`, `barang_id`, `warna_id`, `ukuran_id`, `qty`, `status_invoice`, `tanggal_order`, `tanggal_checkout`, `discount`, `tax`) VALUES
(5, 1, 1, 2, 9, 1, 1, '2019-05-18 23:49:00', NULL, 0, 0),
(6, 1, 2, 3, 11, 1, 1, '2019-05-19 00:23:45', NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cp_cart_detail`
--

CREATE TABLE `cp_cart_detail` (
  `id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL,
  `tanggal_order` datetime DEFAULT NULL,
  `barang_id` int(11) NOT NULL,
  `warna_id` int(3) NOT NULL,
  `ukuran_id` int(3) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `status_invoice` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cp_company_setting`
--

CREATE TABLE `cp_company_setting` (
  `nama` varchar(255) DEFAULT NULL,
  `deskripsi` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_company_setting`
--

INSERT INTO `cp_company_setting` (`nama`, `deskripsi`, `alamat`, `phone_number`, `email`) VALUES
('OBALIHARA', 'Marketplace Produk Wilayah Kelola Rakyat', 'Jl. Kapten Tendean no.46, Jakarta Pusat', '(0231) 2371 8346', ' Hallo@Obalihara.com');

-- --------------------------------------------------------

--
-- Table structure for table `cp_configuration`
--
-- Error reading structure for table ciadmin.cp_configuration: #1932 - Table 'ciadmin.cp_configuration' doesn't exist in engine
-- Error reading data for table ciadmin.cp_configuration: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `ciadmin`.`cp_configuration`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `cp_goods`
--

CREATE TABLE `cp_goods` (
  `id` int(11) NOT NULL,
  `goods_name` varchar(255) NOT NULL,
  `goods_details` mediumtext NOT NULL,
  `goods_price` int(11) NOT NULL,
  `goods_image` varchar(255) NOT NULL,
  `goods_status` int(11) NOT NULL,
  `goods_istrash` int(11) NOT NULL,
  `created_user` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_user` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `deleted_user` int(11) NOT NULL,
  `deleted_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_goods`
--

INSERT INTO `cp_goods` (`id`, `goods_name`, `goods_details`, `goods_price`, `goods_image`, `goods_status`, `goods_istrash`, `created_user`, `created_date`, `updated_user`, `updated_date`, `deleted_user`, `deleted_date`) VALUES
(1, 'TEST', 'TEST', 100000, '', 1, 0, 1, '2019-04-04 14:46:49', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cp_kategori`
--

CREATE TABLE `cp_kategori` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `parent_id` int(2) DEFAULT NULL,
  `order` int(2) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `is_trash` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_kategori`
--

INSERT INTO `cp_kategori` (`id`, `title`, `desc`, `parent_id`, `order`, `status`, `is_trash`) VALUES
(1, 'Home', 'home', 0, 1, 1, 0),
(12, 'Shop', 'shop', 0, 2, 1, 0),
(13, 'Page', 'page', 0, 3, 1, 0),
(14, 'Blog', 'blog', 0, 4, 1, 0),
(15, 'Fashion Pria', 'Fashion Pria', 12, 1, 1, 0),
(16, 'Fashion Wanita', 'Fashion Wanita', 12, 2, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cp_kategori_barang`
--

CREATE TABLE `cp_kategori_barang` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `tipe` int(3) DEFAULT NULL,
  `clicked` int(11) DEFAULT NULL,
  `is_trash` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_kategori_barang`
--

INSERT INTO `cp_kategori_barang` (`id`, `parent_id`, `name`, `tipe`, `clicked`, `is_trash`) VALUES
(12, 0, 'Fashion Pria', 0, 0, 0),
(13, 12, 'Kaos Pria', 1, 1, 0),
(14, 12, 'Sepatu Pria', 0, 2, 0),
(15, 14, 'Slip On Pria', 2, 5, 0),
(16, 12, 'Topi Pria', 3, 3, 0),
(17, 0, 'Fashion Wanita', 0, 0, 0),
(18, 17, 'Dress Wanita', 1, 1, 0),
(19, 17, 'Sepatu Wanita', 0, 3, 0),
(20, 19, 'Slip On Wanita', 2, 4, 0),
(21, 17, 'Beanie Wanita', 3, 2, 0),
(22, 0, 'Kopi Nusantara', 0, 0, 0),
(23, 22, 'Kopi Toraja', 1, 6, 0),
(24, 22, 'Kopi Aceh Gayo', 1, 7, 0),
(25, 22, 'Kopi Sumsel', 1, 5, 0),
(26, 14, 'Sneakers', 2, 2, 0),
(27, 14, 'Pantofel', 2, 2, 0),
(28, 0, 'Pangan', 0, 0, 0),
(29, 0, 'Kain Nusantara', 0, 0, 0),
(30, 0, 'Kerajinan', 0, 0, 0),
(31, 0, 'Merchandise', 0, 0, 0),
(32, 0, 'Aksesoris', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cp_member`
--
-- Error reading structure for table ciadmin.cp_member: #1932 - Table 'ciadmin.cp_member' doesn't exist in engine
-- Error reading data for table ciadmin.cp_member: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `ciadmin`.`cp_member`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `cp_merek`
--

CREATE TABLE `cp_merek` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cp_template`
--
-- Error reading structure for table ciadmin.cp_template: #1932 - Table 'ciadmin.cp_template' doesn't exist in engine
-- Error reading data for table ciadmin.cp_template: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `ciadmin`.`cp_template`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `cp_ukuran`
--

CREATE TABLE `cp_ukuran` (
  `id` int(11) NOT NULL,
  `kategori_barang_tipe` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_ukuran`
--

INSERT INTO `cp_ukuran` (`id`, `kategori_barang_tipe`, `name`) VALUES
(1, 1, 'XS'),
(2, 1, 'S'),
(3, 1, 'M'),
(4, 1, 'L'),
(5, 1, 'XL'),
(6, 1, 'XXL'),
(7, 1, 'XXXL'),
(9, 2, '36'),
(10, 2, '37.5'),
(11, 2, '38'),
(12, 2, '39'),
(13, 2, '40'),
(14, 2, '41'),
(15, 2, '42'),
(16, 2, '43'),
(17, 2, '44'),
(18, 2, '45'),
(19, 2, 'Tan'),
(20, 2, '37');

-- --------------------------------------------------------

--
-- Table structure for table `cp_user`
--

CREATE TABLE `cp_user` (
  `id` int(11) NOT NULL,
  `oauth_provider` varchar(255) DEFAULT NULL,
  `oauth_uid` varchar(100) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone_number` varchar(30) DEFAULT NULL,
  `changePasswordDate` datetime DEFAULT NULL,
  `logindate` datetime DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `is_trash` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_user`
--

INSERT INTO `cp_user` (`id`, `oauth_provider`, `oauth_uid`, `email`, `username`, `password`, `phone_number`, `changePasswordDate`, `logindate`, `createdDate`, `status`, `is_trash`) VALUES
(1, NULL, NULL, 'vendor1@gmail.com', 'vendor1', '6a1373860f34c59e734d372ab48c56fb', '08919191919', '2019-06-06 19:40:33', '2019-06-18 20:28:06', NULL, 1, 0),
(2, NULL, NULL, 'buyer1@gmail.com', 'buyer1', '4297f44b13955235245b2497399d7a93', '09123819231', NULL, NULL, NULL, 1, 0),
(3, NULL, NULL, 'buyer2@gmail.com', 'buyer2', '4297f44b13955235245b2497399d7a93', '09123881239', NULL, NULL, NULL, 1, 0),
(4, NULL, NULL, 'walhi@gmail.com', 'walhi', '4297f44b13955235245b2497399d7a93', '12332132232', NULL, '2019-05-28 02:11:16', NULL, 1, NULL),
(6, NULL, NULL, 'buyer3@gmail.com', 'buyer3', '4297f44b13955235245b2497399d7a93', '089765461234', NULL, '2019-05-28 16:20:18', NULL, 1, NULL),
(9, 'facebook', '2433977153333901', 'jokerhell945@gmail.com', NULL, NULL, NULL, NULL, '2019-06-12 19:58:37', '2019-06-11 04:30:02', 1, 0),
(10, NULL, NULL, 'vendor2@gmail.com', 'vendor2', '4297f44b13955235245b2497399d7a93', '09899899898', NULL, '2019-06-18 20:23:14', NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cp_vendor`
--

CREATE TABLE `cp_vendor` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `register_date` date DEFAULT NULL,
  `fee` varchar(10) DEFAULT NULL,
  `no_rek` varchar(100) DEFAULT NULL,
  `nama_lengkap` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `instagram` varchar(255) DEFAULT NULL,
  `deskripsi` longtext,
  `alamat` mediumtext,
  `telp` varchar(30) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `postcode` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `provinsi_id` int(11) DEFAULT NULL,
  `kota_id` int(11) DEFAULT NULL,
  `shipping_metode` varchar(255) DEFAULT NULL,
  `is_trash` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_vendor`
--

INSERT INTO `cp_vendor` (`id`, `user_id`, `register_date`, `fee`, `no_rek`, `nama_lengkap`, `facebook`, `twitter`, `instagram`, `deskripsi`, `alamat`, `telp`, `email`, `postcode`, `photo`, `provinsi_id`, `kota_id`, `shipping_metode`, `is_trash`) VALUES
(1, 1, '2019-06-09', '2,5', '9877787288272', 'Vendor 1', '/vendor1', '@vendor1', '@vendor1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\r\n\r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Jl. Nusa Dua , Blok C7 No. 18 Taman Angsa', '089788887722', 'vendor@mail.com', '16780', '1557202467-gm_logo_approved_url.jpg', 9, 22, 'tiki,pos,jne', 0),
(2, 4, '2019-05-22', '1,0', '9877787223288', 'Walhi Mart', '/vendor1', '@vendor1', '@vendor1', 'TEATEATEATEATEATEAETEA', 'mesjid amaliah ciawi', '089788887722', 'walhi@gmai.com', '16552', NULL, 9, 68, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cp_warna`
--

CREATE TABLE `cp_warna` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cp_warna`
--

INSERT INTO `cp_warna` (`id`, `name`, `value`) VALUES
(1, 'Merah', 'Red'),
(2, 'Biru', 'Blue'),
(3, 'Hijau', 'Green'),
(4, 'Kuning', 'Yellow'),
(5, 'Coklat', 'Brown'),
(6, 'Ungu', 'Purple'),
(7, 'Pink', 'Pink'),
(8, 'Putih', 'White'),
(9, 'Hitam', 'Black');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cp_app_acl_accesses`
--
ALTER TABLE `cp_app_acl_accesses`
  ADD PRIMARY KEY (`acc_id`);

--
-- Indexes for table `cp_app_acl_access_actions`
--
ALTER TABLE `cp_app_acl_access_actions`
  ADD PRIMARY KEY (`aca_id`);

--
-- Indexes for table `cp_app_acl_actions`
--
ALTER TABLE `cp_app_acl_actions`
  ADD PRIMARY KEY (`ac_id`);

--
-- Indexes for table `cp_app_acl_group`
--
ALTER TABLE `cp_app_acl_group`
  ADD PRIMARY KEY (`ag_id`);

--
-- Indexes for table `cp_app_acl_group_accesses`
--
ALTER TABLE `cp_app_acl_group_accesses`
  ADD PRIMARY KEY (`aga_id`);

--
-- Indexes for table `cp_app_city`
--
ALTER TABLE `cp_app_city`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `provinsi_id` (`provinsi_id`) USING BTREE;

--
-- Indexes for table `cp_app_config`
--
ALTER TABLE `cp_app_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_app_country`
--
ALTER TABLE `cp_app_country`
  ADD PRIMARY KEY (`country_id`) USING BTREE;

--
-- Indexes for table `cp_app_log`
--
ALTER TABLE `cp_app_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `cp_app_province`
--
ALTER TABLE `cp_app_province`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `cp_app_sessions`
--
ALTER TABLE `cp_app_sessions`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `cp_app_user`
--
ALTER TABLE `cp_app_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `cp_article`
--
ALTER TABLE `cp_article`
  ADD PRIMARY KEY (`article_id`);

--
-- Indexes for table `cp_article_category`
--
ALTER TABLE `cp_article_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `cp_banner`
--
ALTER TABLE `cp_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_barang`
--
ALTER TABLE `cp_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_barang_detail`
--
ALTER TABLE `cp_barang_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_barang_gambar`
--
ALTER TABLE `cp_barang_gambar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_barang_varian`
--
ALTER TABLE `cp_barang_varian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_barang_varian_detail`
--
ALTER TABLE `cp_barang_varian_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_buyer`
--
ALTER TABLE `cp_buyer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_buyer_address`
--
ALTER TABLE `cp_buyer_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_cart`
--
ALTER TABLE `cp_cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_cart_detail`
--
ALTER TABLE `cp_cart_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_goods`
--
ALTER TABLE `cp_goods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_kategori`
--
ALTER TABLE `cp_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_kategori_barang`
--
ALTER TABLE `cp_kategori_barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_merek`
--
ALTER TABLE `cp_merek`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_ukuran`
--
ALTER TABLE `cp_ukuran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_user`
--
ALTER TABLE `cp_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_vendor`
--
ALTER TABLE `cp_vendor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cp_warna`
--
ALTER TABLE `cp_warna`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cp_app_acl_accesses`
--
ALTER TABLE `cp_app_acl_accesses`
  MODIFY `acc_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT for table `cp_app_acl_access_actions`
--
ALTER TABLE `cp_app_acl_access_actions`
  MODIFY `aca_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=574;

--
-- AUTO_INCREMENT for table `cp_app_acl_actions`
--
ALTER TABLE `cp_app_acl_actions`
  MODIFY `ac_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `cp_app_acl_group`
--
ALTER TABLE `cp_app_acl_group`
  MODIFY `ag_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `cp_app_acl_group_accesses`
--
ALTER TABLE `cp_app_acl_group_accesses`
  MODIFY `aga_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7806;

--
-- AUTO_INCREMENT for table `cp_app_city`
--
ALTER TABLE `cp_app_city`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=523;

--
-- AUTO_INCREMENT for table `cp_app_config`
--
ALTER TABLE `cp_app_config`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cp_app_country`
--
ALTER TABLE `cp_app_country`
  MODIFY `country_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=252;

--
-- AUTO_INCREMENT for table `cp_app_log`
--
ALTER TABLE `cp_app_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cp_app_province`
--
ALTER TABLE `cp_app_province`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `cp_app_user`
--
ALTER TABLE `cp_app_user`
  MODIFY `user_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `cp_article`
--
ALTER TABLE `cp_article`
  MODIFY `article_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cp_article_category`
--
ALTER TABLE `cp_article_category`
  MODIFY `category_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `cp_banner`
--
ALTER TABLE `cp_banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cp_barang`
--
ALTER TABLE `cp_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cp_barang_detail`
--
ALTER TABLE `cp_barang_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cp_barang_gambar`
--
ALTER TABLE `cp_barang_gambar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `cp_barang_varian`
--
ALTER TABLE `cp_barang_varian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `cp_barang_varian_detail`
--
ALTER TABLE `cp_barang_varian_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `cp_buyer`
--
ALTER TABLE `cp_buyer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `cp_buyer_address`
--
ALTER TABLE `cp_buyer_address`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `cp_cart`
--
ALTER TABLE `cp_cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cp_cart_detail`
--
ALTER TABLE `cp_cart_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cp_goods`
--
ALTER TABLE `cp_goods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cp_kategori`
--
ALTER TABLE `cp_kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `cp_kategori_barang`
--
ALTER TABLE `cp_kategori_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `cp_ukuran`
--
ALTER TABLE `cp_ukuran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `cp_user`
--
ALTER TABLE `cp_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `cp_vendor`
--
ALTER TABLE `cp_vendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cp_warna`
--
ALTER TABLE `cp_warna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cp_app_city`
--
ALTER TABLE `cp_app_city`
  ADD CONSTRAINT `cp_app_city_ibfk_1` FOREIGN KEY (`provinsi_id`) REFERENCES `cp_app_province` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
