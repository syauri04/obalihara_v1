<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH."libraries/AdminController.php");

class Report_sales extends AdminController {

	
	function __construct()    
	{
		parent::__construct();    
		$this->_set_action();
		$this->_set_action(array("edit","delete"),"ITEM");
		$this->_set_title( 'Report Sales ' );
		$this->load->model('report_sales_model','SM');
		$this->folder_view = "Report Sales";
		$this->DATA->table = "cp_checkout";

	}
	
	public function index()
	{

		$p = [];
		if($_POST && $_POST['dateRange'] != ''){

			$d = explode(' - ',$_POST['dateRange']);

			$p = [
				'vendor_id' => $_POST['vendor_id'],
				'dateStart' => dateRe(str_replace('/', '-',$d[0])),
				'dateEnd' 	=> dateRe(str_replace('/', '-',$d[1])),
			];
		}

		$data = [
			'salesReport' => $this->SM->getData($p),
			'filter'	  => $p,
			'vendor'	  => $this->db->get_where('cp_vendor',['is_trash' => 0 , 'activate' => 1])->result(),
		];
		// d($data);
		$this->_v($this->folder_view.'/index',$data);
	}

}
