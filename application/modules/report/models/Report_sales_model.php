<?php

class Report_sales_model extends CI_Model{

    function __construct(){
    	parent::__construct();
  	}

    function getData($p=[]){

        $this->db->select('c.*,SUM(c.qty) as total_qty,SUM(cd.total) as total,SUM(cd.courier_cost) as courier_cost')
        ->join('cp_checkout_detail cd','cd.cart_id = c.id')
        ->where('c.status_invoice >',4);

        if(!empty($p) && $p['dateStart'] != ''){
            $this->db->where('c.tanggal_order >=',$p['dateStart']);
            $this->db->where('c.tanggal_order <=',$p['dateEnd']);
        }

        if(!empty($p) && $p['vendor_id'] != 0){
            $this->db->where('c.vendor_id',$p['vendor_id']);
        }
        $this->db->group_by('c.vendor_id');
        $data = $this->db->get('cp_cart c')->result_array();

        // d($data);

        // $result = array();
        // $t = 0;
        // foreach ($data as $element => $k) {
            
        //     $result[$k['vendor_id']] = $k;
        // }

        // d($result);
        
        return $data;
    }

}
