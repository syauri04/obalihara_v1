

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.2/d3.min.js"></script>
<script src="<?= base_url()?>assets/def/plugins/nvd3/build/nv.d3.js"></script>
<script src="<?= base_url()?>assets/def/plugins/jquery-jvectormap/jquery-jvectormap.min.js"></script>
<script src="<?= base_url()?>assets/def/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js"></script>
<script src="<?= base_url()?>assets/def/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js"></script>
<script src="<?= base_url()?>assets/def/plugins/gritter/js/jquery.gritter.js"></script>
<script src="<?= base_url()?>assets/def/js/demo/dashboard-v2.min.js"></script> -->
<!-- ================== END PAGE LEVEL JS ================== -->


<div class="panel panel-inverse" data-sortable-id="table-basic-1">
	<!-- begin panel-heading -->
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
        <h4 class="panel-title"><a href="<?= $this->own_link.'add'?>"><button type="button" class="btn btn-lime btn-sm">Add</button></a></h4>
    </div>
    <!-- end panel-heading -->
    <!-- begin panel-body -->
    <div class="panel-body">
    	<form action="" method="post" id="formFilter">
	    	<div class="row" style="margin-bottom: 10px;">
	    		
		    		<div class="col-md-4">
		    			<select class="form-control" name="vendor_id">
		    				<option value="0">- Pilih Vendor -</option>
		    				<?php

		    					foreach ($vendor as $k => $v) {
		    						echo '<option value="'.$v->id.'">'.$v->nama_lengkap.'</option>';
		    					}

		    				?>
		    			</select>
		    		</div>
		    		<div class="col-md-4">
		    			<input type="text" id="demo" class="form-control" name="dateRange" >
		    			<input type="hidden" class="dateStart" value="<?= isset($filter['dateStart'])?dateRe(str_replace('-', '/',$filter['dateStart']),'reverse'):''; ?>">
		    			<input type="hidden" class="dateEnd" value="<?= isset($filter['dateEnd'])?dateRe(str_replace('-', '/',$filter['dateEnd']),'reverse'):''; ?>">
		    		</div>
		    		<div class="col-md-4">
		    			<button class="btn btn-outline-success">Apply</button>
		    			<a class="btn btn-default reset" href="#">Reset</a>
		    		</div>
	    		
	    	</div>
    	</form>
    	<!-- begin table-responsive -->
		<table id="data-table-default" class="table table-striped ">
			<thead>
                <tr>
                	<th>Vendor</th>					
					<th>Total Item Sold</th>
					<th>Total Sales</th>
					<th>Fee</th>
                </tr>
            </thead>
			<tbody>
				<?php
					// d($salesReport);
					foreach ($salesReport as $k => $v) {
						$tp 	= $v['total'];
						$fee	= feePrice($v['total'],getOutlet($v['vendor_id'],'fee'));
				?>
				<tr>
					<td><?= getOutlet($v['vendor_id'],'nama_lengkap');?></td>
					<td><?= $v['total_qty'];?></td>
					<td><i><?= myNum($tp,'Rp. ')?></i></td>
					<td><?= myNum($fee,'Rp. ');?></td>			
				</tr>
				<?php
					}
				?>
			</tbody>
		</table>
		<!-- end table-responsive -->
    </div>
    <!-- end panel-body -->
</div>

<link href="<?= base_url()?>assets/def/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="<?= base_url()?>assets/def/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="<?= base_url()?>assets/def/plugins/DataTables/extensions/Buttons/css/buttons.dataTables.min.css" rel="stylesheet" />

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?= base_url()?>assets/def/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?= base_url()?>assets/def/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url()?>assets/def/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="<?= base_url()?>assets/def/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url()?>assets/def/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js"></script>
<script src="<?= base_url()?>assets/def/plugins/DataTables/extensions/Buttons/js/jszip.min.js"></script>
<script src="<?= base_url()?>assets/def/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js"></script>
<script src="<?= base_url()?>assets/def/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js"></script>
<script src="<?= base_url()?>assets/def/plugins/moment/moment.min.js"></script>
<script src="<?= base_url()?>assets/def/plugins/moment/moment-with-locales.min.js"></script>
<script src="<?= base_url()?>assets/def/js/demo/table-manage-default.demo.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script type="text/javascript">
	// TableManageDefault.init();
	
	$(document).ready(function(){

		var start = ( $('.dateStart').val() != '' ) ? $('.dateStart').val() : moment().format('DD/MM/YYYY');
    	var end = ( $('.dateEnd').val() != '' ) ? $('.dateEnd').val() : moment().format('DD/MM/YYYY');
		var drange = start+' / '+end;

		$('#data-table-default').DataTable({
	        responsive: true,
	        searchHighlight: true,
	        dom: 'Bfrtip',
	        buttons: [
	            {
	                extend: 'excel',
	                filename: 'REPORT SALES OBALIHARA '+drange,
	          }
	        ]
	    });
	});

	$(document).on('click','.reset',function(e){
		e.preventDefault();
		$('#demo').val('');
		$('#formFilter').submit();
	});

	$(function() {

		var start = ( $('.dateStart').val() != '' ) ? $('.dateStart').val() : moment().format('DD/MM/YYYY');
    	var end = ( $('.dateEnd').val() != '' ) ? $('.dateEnd').val() : moment().format('DD/MM/YYYY');

    	function cb(start, end) {
	        $('#demo span').html(start + ' - ' + end);
	    }

		$('#demo').daterangepicker({
			// autoUpdateInput: true,
			startDate: start,
			endDate: end,
			locale: {
	            format: 'DD/MM/YYYY'
	        },        
		},cb);

		cb(start, end);

		// function(start, end, label) {
		// 	$('.dateFrom').val(start.format('YYYY-MM-DD'));
		// 	$('.dateTo').val(end.format('YYYY-MM-DD'));
		// }

	});


	
</script>