<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once(APPPATH."libraries/AdminController.php");
class Department extends AdminController {  
	function __construct()    
	{
		parent::__construct();    
		$this->_set_action();
		$this->_set_action(array("edit","delete"),"ITEM");
		$this->_set_title( 'Department' );
		$this->DATA->table="cp_departement";
		$this->folder_view = "meme/";
		$this->prefix_view = strtolower($this->_getClass());
		$this->breadcrumb[] = array(
				"title"		=> "Company",
				"url"		=> site_url('meme/company')
			);
		$this->breadcrumb[] = array(
				"title"		=> "Department",
				"url"		=> $this->own_link."/index/".$this->session->flashdata('id_company')
			);

		$this->upload_path="./assets/collections/departement/";

		$this->upload_resize  = array(
				array('name'	=> 'thumb','width'	=> 1200, 'height'	=> 450, 'quality'	=> '100%')
			);
		$this->image_size_str = "1200px x 450px";
	}

	function index($id=''){
		$this->breadcrumb[] = array(
				"title"		=> "List"
		);

		$this->session->set_flashdata('id_company', $id);
		$data['data'] = $this->DATA->_getall(array(
				'departement_istrash !=' => 1,
				'departement_company' => $id
			),array(
				"by"	=> "departement_id",
				"dir"	=> "desc"
			));
		$this->_v($this->folder_view.$this->prefix_view,$data);
	}

	
	function add(){	
		$id= $this->session->flashdata('id_company');
		if(empty($id)){
			redirect(site_url('meme/company'));
		}	
		$this->breadcrumb[] = array(
				"title"		=> "Add"
			);		
		$this->session->set_flashdata('id_company', $id);
		$this->_v($this->folder_view.$this->prefix_view."_form");
	}
	
	function edit($id=''){

		$this->breadcrumb[] = array(
				"title"		=> "Edit"
			);

		$id=dbClean(trim($id));
		
		if(trim($id)!=''){
			$this->data_form = $this->DATA->data_id(array(
					'departement_id'	=> $id
				));
			$this->_v($this->folder_view.$this->prefix_view."_form");
		}else{
			redirect($this->own_link);
		}
	}
	
	function delete($id=''){
		$id=dbClean(trim($id));		
		if(trim($id) != ''){
			/*$o = $this->DATA->_delete(
				array("departement_id"	=> idClean($id))
			);*/
			$this->db->update("cp_departement",array("departement_istrash"=>1),array("departement_id"=>$id));
		}
		redirect($this->own_link."?msg=".urldecode('Delete data departement succes')."&type_msg=success");
	}

	function save(){
		$data = array(
			'departement_title'		=> dbClean($_POST['departement_title']),
			'departement_date'		=> date('Y-m-d H:i:s'),
			'departement_status'	=> dbClean($_POST['departement_status']),
			'departement_company'	=> dbClean($_POST['departement_company']),
		);		
		$a = $this->_save_master( 
			$data,
			array(
				'departement_id' => dbClean($_POST['departement_id'])
			),
			dbClean($_POST['departement_id'])			
		);

		redirect($this->own_link."/index/".$_POST['departement_company']."?msg=".urldecode('Save data departement succes')."&type_msg=success");
	}

}
