<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once(APPPATH."libraries/AdminController.php");
class News extends AdminController {  
	function __construct()    
	{
		parent::__construct();    
		$this->_set_action();
		$this->_set_action(array("edit","delete"),"ITEM");
		$this->_set_title( 'List News' );
		$this->DATA->table = "cp_news";
		$this->folder_view = "meme/";
		$this->prefix_view = strtolower($this->_getClass());
		$this->load->model("mdl_news","M");
		$this->upload_path="./assets/collections/article/";
		$this->breadcrumb[] = array(
				"title"		=> "List News",
				"url"		=> $this->own_link
			);
		$this->cat_search = array(
			''									=> 'Search By',
			'cp_news.news_title'				=> 'Title',
		); 
	}
	
	function _reset(){
		$this->jCfg['search'] = array(
			'class'		=> $this->_getClass(),
			'name'		=> 'news',
			'date_start'=> '',
			'date_end'	=> '',
			'status'	=> '',
			'order_by'  => 'news_id',
			'order_dir' => 'DESC',
			'colum'		=> '',
			'keyword'	=> ''
		);
		$this->_releaseSession();
	}

	function index(){
		$hal = isset($this->jCfg['search']['name'])?$this->jCfg['search']['name']:"home";
		if($hal != 'news'){
			$this->_reset();
		}
		
		$this->breadcrumb[] = array(
				"title"		=> "List"
			);
		//debugCode($this->jCfg['search']);	
		if($this->input->post('btn_search')){
			if($this->input->post('date_start') && trim($this->input->post('date_start'))!="")
				$this->jCfg['search']['date_start'] = $this->input->post('date_start');

			if($this->input->post('date_end') && trim($this->input->post('date_end'))!="")
				$this->jCfg['search']['date_end'] = $this->input->post('date_end');

			if($this->input->post('colum') && trim($this->input->post('colum'))!="")
				$this->jCfg['search']['colum'] = $this->input->post('colum');
			else
				$this->jCfg['search']['colum'] = "";	

			if($this->input->post('keyword') && trim($this->input->post('keyword'))!="")
				$this->jCfg['search']['keyword'] = $this->input->post('keyword');
			else
				$this->jCfg['search']['keyword'] = "";

			$this->_releaseSession();
		}

		if($this->input->post('btn_reset')){
			$this->_reset();
		}

		$this->per_page = 20;
		$par_filter = array(
				"offset"	=> $this->uri->segment($this->uri_segment),
				"limit"		=> $this->per_page,
				"param"		=> $this->cat_search
			);
		$this->data_table = $this->M->data_news($par_filter);
		$data = $this->_data(array(
				"base_url"	=> $this->own_link.'/index'
			));

		$this->_v($this->folder_view.$this->prefix_view,$data);
	}

	function add()
	{	
		$this->breadcrumb[] = array(
			"title"		=> "Add"
		);		
		$this->_v($this->folder_view.$this->prefix_view."_form");
	}

	function edit($id=''){
		$this->breadcrumb[] = array(
				"title"		=> "Edit"
			);
		$id=dbClean(trim($id));
		if(trim($id)!=''){
			$this->data_form = $this->DATA->data_id(array(
					'news_id'	=> $id
				));
			$this->_v($this->folder_view.$this->prefix_view."_form");
		}else{
			redirect($this->own_link);
		}
	}

	function delete($id=''){
		$id=dbClean(trim($id));		
		if(trim($id) != ''){
			$this->db->update("cp_news",array("news_istrash"=>1),array("news_id"=>$id));
		}
		redirect($this->own_link."?msg=".urldecode('Delete data news success')."&type_msg=success");
	}

	function save(){
		$data = array(
			'news_title'			=> dbClean($_POST['news_title']),
			'news_content'			=> dbClean($_POST['news_content']),
			'news_status'			=> dbClean($_POST['news_status'])
		);		
		$datePub = explode("/", dbClean($_POST['pdate'])) ;

		$data['news_publishdate'] = $datePub['2'].'-'.$datePub['0'].'-'.$datePub['1'].' '.'00:00:00'; 
		if (dbClean($_POST['news_id']) == "") {
			$data['news_date'] = date("Y-m-d H:i:s");
			$data['news_user_id'] = $this->jCfg['user']['id'];
		}
		
		//debugCode($data);
		$a = $this->_save_master( 
			$data,
			array(
				'news_id' => dbClean($_POST['news_id'])
			),
			dbClean($_POST['news_id'])			
		);
		$id = $a['id'];
		$this->_uploaded(
		array(
			'id'		=> $id ,
			'input'		=> 'news_image',
			'param'		=> array(
							'field' => 'news_file', 
							'par'	=> array('news_id' => $id)
						)
		));

		redirect($this->own_link."/?msg=".urldecode('Save data news success')."&type_msg=success");
	}
	
}



