<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once(APPPATH."libraries/AdminController.php");
class Todolist extends AdminController {  
	function __construct()    
	{
		parent::__construct();    
		$this->_set_action();
		$this->_set_action(array("edit","delete"),"ITEM");
		$this->_set_title( 'To do list' );
		$this->DATA->table = "cp_todolist";
		$this->folder_view = "meme/";
		$this->prefix_view = strtolower($this->_getClass());
		$this->load->model("mdl_todolist","M");
		$this->breadcrumb[] = array(
				"title"		=> "To do list",
				"url"		=> $this->own_link
			);
		$this->cat_search = array(
			''									=> 'Search By',
			'cp_topik.topik_title'				=> 'Title',
			'cp_topik.topik_summary'			=> 'Summary',
		); 
	}
	
	function _reset(){
		$this->jCfg['search'] = array(
			'class'		=> $this->_getClass(),
			'name'		=> 'topik',
			'date_start'=> '',
			'date_end'	=> '',
			'status'	=> '',
			'order_by'  => 'topik_id',
			'order_dir' => 'DESC',
			'colum'		=> '',
			'keyword'	=> ''
		);
		$this->_releaseSession();
	}

	function index(){
		//echo "masul";exit;
		$this->breadcrumb[] = array(
			"title"		=> "Add"
		);	
		/* $this->data_form = $this->DATA->data_id(array(
					'user_id'	=> $id
				));	 */	
		$this->_v($this->folder_view.$this->prefix_view."_form",array(
			'company'		=> $this->db->get_where("cp_company",array(
				))->result(),
					'departement'	=> $this->db->get_where("cp_departement",array(
					//"departement_company" 		=> $this->user_company,
					"departement_istrash <>" 		=> "1"
				))->result()
		));
	}

	function add()
	{	
		$this->breadcrumb[] = array(
			"title"		=> "Add"
		);		
		$this->_v($this->folder_view.$this->prefix_view."_form");
	}

	function edit($id=''){
		$this->breadcrumb[] = array(
				"title"		=> "Edit"
			);
		$id=dbClean(trim($id));
		if(trim($id)!=''){
			$this->data_form = $this->DATA->data_id(array(
					'topik_id'	=> $id
				));
			$this->_v($this->folder_view.$this->prefix_view."_form");
		}else{
			redirect($this->own_link);
		}
	}

	function delete($id=''){
		$id=dbClean(trim($id));		
		if(trim($id) != ''){
			$this->db->update("cp_topik",array("topik_istrash"=>1),array("topik_id"=>$id));
		}
		redirect($this->own_link."?msg=".urldecode('Delete data Topik success')."&type_msg=success");
	}

	function save(){
		$data = array(
			'user'			=> dbClean($_POST['user_id']),
			'todo'			=> dbClean($_POST['todo']),
			'status'		=> dbClean($_POST['status'])
		);		


		$datePub1 = explode("/", dbClean($_POST['startDate'])) ;
		$data['start_date'] = $datePub1['2'].'-'.$datePub1['0'].'-'.$datePub1['1']; 

		$datePub = explode("/", dbClean($_POST['endDate'])) ;
		$data['end_date'] = $datePub['2'].'-'.$datePub['0'].'-'.$datePub['1']; 

		if (dbClean($_POST['id']) == "") {
			$data['created_date'] = date("Y-m-d H:i:s");
			
		}
		
		$a = $this->_save_master( 
			$data,
			array(
				'id' => dbClean($_POST['id'])
			),
			dbClean($_POST['id'])			
		);

		$base = base_url();
		$link = $_POST['link'];
		redirect($base.$link."?msg=".urldecode('Save data to do list success')."&type_msg=success");
	}
	function setStatus(){
		$data = array(
			'status'			=> dbClean($_POST['status'])
		);				
		$a = $this->_save_master( 
			$data,
			array(
				'id' => dbClean($_POST['id'])
			),
			dbClean($_POST['id'])			
		);

		$base = base_url();
		$link = $_POST['link'];
		redirect($base.$link."?msg=".urldecode('Save data to do list success')."&type_msg=success");
	

	}
	
}



