<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once(APPPATH."libraries/AdminController.php");
class Company extends AdminController {  
	function __construct()    
	{
		parent::__construct();    
		$this->_set_action();
		$this->_set_action(array("department","edit","delete"),"ITEM");
		$this->_set_title( 'Company' );
		$this->DATA->table = "cp_company";
		$this->folder_view = "meme/";
		$this->prefix_view = strtolower($this->_getClass());
		$this->load->model("mdl_company","M");
		$this->breadcrumb[] = array(
				"title"		=> "Company",
				"url"		=> $this->own_link
			);
		$this->cat_search = array(
			''									=> 'Search By',
			'cp_company.company_title'				=> 'Title'
		); 
	}
	
	function _reset(){
		$this->jCfg['search'] = array(
			'class'		=> $this->_getClass(),
			'name'		=> 'company',
			'date_start'=> '',
			'date_end'	=> '',
			'status'	=> '',
			'order_by'  => 'company_id',
			'order_dir' => 'DESC',
			'colum'		=> '',
			'keyword'	=> ''
		);
		$this->_releaseSession();
	}

	function index(){
		$hal = isset($this->jCfg['search']['name'])?$this->jCfg['search']['name']:"home";
		if($hal != 'company'){
			$this->_reset();
		}
		
		$this->breadcrumb[] = array(
				"title"		=> "List"
			);
		//debugCode($this->jCfg['search']);	
		if($this->input->post('btn_search')){
			if($this->input->post('date_start') && trim($this->input->post('date_start'))!="")
				$this->jCfg['search']['date_start'] = $this->input->post('date_start');

			if($this->input->post('date_end') && trim($this->input->post('date_end'))!="")
				$this->jCfg['search']['date_end'] = $this->input->post('date_end');

			if($this->input->post('colum') && trim($this->input->post('colum'))!="")
				$this->jCfg['search']['colum'] = $this->input->post('colum');
			else
				$this->jCfg['search']['colum'] = "";	

			if($this->input->post('keyword') && trim($this->input->post('keyword'))!="")
				$this->jCfg['search']['keyword'] = $this->input->post('keyword');
			else
				$this->jCfg['search']['keyword'] = "";

			$this->_releaseSession();
		}

		if($this->input->post('btn_reset')){
			$this->_reset();
		}

		$this->per_page = 20;
		$par_filter = array(
				"offset"	=> $this->uri->segment($this->uri_segment),
				"limit"		=> $this->per_page,
				"param"		=> $this->cat_search
			);
		$this->data_table = $this->M->data_company($par_filter);
		$data = $this->_data(array(
				"base_url"	=> $this->own_link.'/index'
			));
			
		$data['url'] = base_url()."meme/company/index";

		$this->_v($this->folder_view.$this->prefix_view,$data);
	}

	function add()
	{	
		$this->breadcrumb[] = array(
			"title"		=> "Add"
		);		
		$this->_v($this->folder_view.$this->prefix_view."_form",array(
			'country'		=> $this->db->get_where("cp_country",array(
				"country_istrash <>" 		=> "1"
			))->result(),
			'company_type'	=> $this->db->get_where("cp_company_type",array(
			))->result()
		));
	}

	function edit($id=''){
		$this->breadcrumb[] = array(
				"title"		=> "Edit"
			);
		$id=dbClean(trim($id));
		if(trim($id)!=''){
			$this->data_form = $this->DATA->data_id(array(
					'company_id'	=> $id
				));
			//debugCode($this->data_form);
			$this->_v($this->folder_view.$this->prefix_view."_form",array(
				'country'		=> $this->db->get_where("cp_country",array(
					"country_istrash <>" 		=> "1"
				))->result(),
				'province'		=> $this->db->get_where("cp_app_province",array(
					"province_country" 		=> $this->data_form->company_negara
				))->result(),
				'city'			=> $this->db->get_where("cp_app_city",array(
				"city_province_id" 		=> $this->data_form->company_province
				))->result(),
				'company_type'	=> $this->db->get_where("cp_company_type",array(
				))->result()
			));
		}else{
			redirect($this->own_link);
		}
	}

	function delete($id=''){
		$id=dbClean(trim($id));		
		if(trim($id) != ''){
			$this->db->update("cp_company",array("company_istrash"=>1),array("company_id"=>$id));
		}
		redirect($this->own_link."?msg=".urldecode('Delete data Company success')."&type_msg=success");
	}

	function save(){
		$data = array(
			'company_title'			=> dbClean($_POST['company_title']),
			'company_desc'			=> dbClean($_POST['company_desc']),
			'company_status'		=> dbClean($_POST['company_status']),
			'company_alamat'		=> dbClean($_POST['company_alamat']),
			'company_type'			=> dbClean($_POST['company_type']),
			'company_negara'		=> dbClean($_POST['company_negara']),
			'company_kode_pos'		=> dbClean($_POST['company_kode_pos']),
			'company_telepon'		=> dbClean($_POST['company_telepon']),
			'company_fax'			=> dbClean($_POST['company_fax']),
			'company_email'			=> dbClean($_POST['company_email']),
			'company_website'		=> dbClean($_POST['company_website']),
			'company_fb'			=> dbClean($_POST['company_fb']),
			'company_twitter'		=> dbClean($_POST['company_twitter'])
		);		

		if($_POST['company_negara'] !== '1'){

			$data['company_province_other'] 	= dbClean($_POST['company_province_text']); 
			$data['company_kota_other']		= dbClean($_POST['company_kota_text']);

		}else{

			$data['company_province'] 	= dbClean($_POST['company_province']); 
			$data['company_kota']		= dbClean($_POST['company_kota']);

		}

		if (dbClean($_POST['company_id']) == "") {
			$data['company_date'] = date("Y-m-d H:i:s");
		}
		
		$a = $this->_save_master( 
			$data,
			array(
				'company_id' => dbClean($_POST['company_id'])
			),
			dbClean($_POST['company_id'])			
		);

		redirect($this->own_link."?msg=".urldecode('Save data company success')."&type_msg=success");
	}

	function department($id=''){
		$this->session->set_flashdata('id_company', $id);
		redirect(site_url('meme/department/index/'.$id));
	}
	
}



