<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once(APPPATH."libraries/AdminController.php");
class config extends AdminController {  
	function __construct()    
	{
		parent::__construct();
			    
		$this->_set_action();
		$this->_set_action(array("detail"),"ITEM");
		$this->_set_title( 'Setting' );
		$this->DATA->table = "cp_configuration";
		$this->folder_view = "meme/";
		$this->prefix_view = strtolower($this->_getClass());
		$this->breadcrumb[] = array(
			"title"		=> "Setting",
			"url"		=> $this->own_link
		);
		$this->upload_types = "image";
		$this->upload_path="./assets/collections/logo/";
		$this->upload_resize  = array(
			array('name'	=> 'thumb','width'	=> 150, 'height'	=> 150, 'quality'	=> '75%'),
			array('name'	=> 'small','width'	=> 350, 'height'	=> 200, 'quality'	=> '85%'),
			array('name'	=> 'large','width'	=> 850, 'height'	=> 440, 'quality'	=> '75%')
		);
		$this->image_size_str = "800px x 450px";
	}
	
	function index(){
		$data['configuration'] = $this->db->get("cp_configuration")->row();
		$this->_v($this->folder_view.$this->prefix_view,$data);
	}

	function save(){
		$data = array(
			'configuration_name'			=> dbClean($_POST['configuration_name']),	
			'configuration_about'			=> dbClean($_POST['configuration_about']),
			'configuration_alamat'			=> dbClean($_POST['configuration_alamat']),	
			'configuration_email'			=> dbClean($_POST['configuration_email']),	
			'configuration_telp'			=> dbClean($_POST['configuration_telp']),	
			'configuration_fax'				=> dbClean($_POST['configuration_fax']),	
			'configuration_website'			=> dbClean($_POST['configuration_website']),	
			'configuration_fb'				=> dbClean($_POST['configuration_fb']),	
			'configuration_tw'				=> dbClean($_POST['configuration_tw']),	
			'configuration_meta_title'		=> dbClean($_POST['configuration_meta_title']),
			'configuration_meta_keyword'	=> dbClean($_POST['configuration_meta_keyword']),
			'configuration_meta_desc'		=> dbClean($_POST['configuration_meta_desc']),			
		);		

		if (dbClean($_POST['configuration_id']) == "") {
			$data['configuration_date'] = date("Y-m-d H:i:s");
		}
		$a = $this->_save_master( 
			$data,
			array(
				'configuration_id' => dbClean($_POST['configuration_id'])
			),
			dbClean($_POST['configuration_id'])			
		);
		$id = $a['id'];
		$this->_uploaded(
		array(
			'id'		=> $id ,
			'input'		=> 'configuration_logo',
			'param'		=> array(
							'field' => 'configuration_logo', 
							'par'	=> array('configuration_id' => $id)
						)
		));
		
		redirect($this->own_link."/index?msg=".urldecode('Save data success')."&type_msg=success");
	}
}



