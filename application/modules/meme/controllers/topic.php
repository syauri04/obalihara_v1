<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once(APPPATH."libraries/AdminController.php");
class Topic extends AdminController {  
	function __construct()    
	{
		parent::__construct();    
		$this->_set_action();
		$this->_set_action(array("edit","delete"),"ITEM");
		$this->_set_title( 'List Topic' );
		$this->DATA->table = "cp_topik";
		$this->folder_view = "meme/";
		$this->prefix_view = strtolower($this->_getClass());
		$this->load->model("mdl_topik","M");
		$this->breadcrumb[] = array(
				"title"		=> "List Topic",
				"url"		=> $this->own_link
			);
		$this->cat_search = array(
			''									=> 'Search By',
			'cp_topik.topik_title'				=> 'Title',
		); 
	}
	
	function _reset(){
		$this->jCfg['search'] = array(
			'class'		=> $this->_getClass(),
			'name'		=> 'topik',
			'date_start'=> '',
			'date_end'	=> '',
			'status'	=> '',
			'order_by'  => 'topik_id',
			'order_dir' => 'DESC',
			'colum'		=> '',
			'keyword'	=> ''
		);
		$this->_releaseSession();
	}

	function index(){
		$hal = isset($this->jCfg['search']['name'])?$this->jCfg['search']['name']:"home";
		if($hal != 'topik'){
			$this->_reset();
		}
		
		$this->breadcrumb[] = array(
				"title"		=> "List"
			);
		//debugCode($this->jCfg['search']);	
		if($this->input->post('btn_search')){
			if($this->input->post('date_start') && trim($this->input->post('date_start'))!="")
				$this->jCfg['search']['date_start'] = $this->input->post('date_start');

			if($this->input->post('date_end') && trim($this->input->post('date_end'))!="")
				$this->jCfg['search']['date_end'] = $this->input->post('date_end');

			if($this->input->post('colum') && trim($this->input->post('colum'))!="")
				$this->jCfg['search']['colum'] = $this->input->post('colum');
			else
				$this->jCfg['search']['colum'] = "";	

			if($this->input->post('keyword') && trim($this->input->post('keyword'))!="")
				$this->jCfg['search']['keyword'] = $this->input->post('keyword');
			else
				$this->jCfg['search']['keyword'] = "";

			$this->_releaseSession();
		}

		if($this->input->post('btn_reset')){
			$this->_reset();
		}

		$this->per_page = 20;
		$par_filter = array(
				"offset"	=> $this->uri->segment($this->uri_segment),
				"limit"		=> $this->per_page,
				"param"		=> $this->cat_search
			);
		$this->data_table = $this->M->data_topik($par_filter);
		$data = $this->_data(array(
				"base_url"	=> $this->own_link.'/index'
			));
			
		$data['url'] = base_url()."meme/topik/index";

		$this->_v($this->folder_view.$this->prefix_view,$data);
	}

	function add()
	{	
		$this->breadcrumb[] = array(
			"title"		=> "Add"
		);		
		$this->_v($this->folder_view.$this->prefix_view."_form");
	}

	function edit($id=''){
		$this->breadcrumb[] = array(
				"title"		=> "Edit"
			);
		$id=dbClean(trim($id));
		if(trim($id)!=''){
			$this->data_form = $this->DATA->data_id(array(
					'topik_id'	=> $id
				));
			$this->_v($this->folder_view.$this->prefix_view."_form");
		}else{
			redirect($this->own_link);
		}
	}

	function delete($id=''){
		$id=dbClean(trim($id));		
		if(trim($id) != ''){
			$this->db->update("cp_topik",array("topik_istrash"=>1),array("topik_id"=>$id));
		}
		redirect($this->own_link."?msg=".urldecode('Delete data Topik success')."&type_msg=success");
	}

	function save(){
		$data = array(
			'topik_title'			=> dbClean($_POST['topik_title']),
			'topik_status'			=> dbClean($_POST['topik_status'])
		);		

		if (dbClean($_POST['topik_id']) == "") {
			$data['topik_date'] = date("Y-m-d H:i:s");
			$data['topik_user'] = $this->jCfg['user']['id'];
		}
		
		$a = $this->_save_master( 
			$data,
			array(
				'topik_id' => dbClean($_POST['topik_id'])
			),
			dbClean($_POST['topik_id'])			
		);

		redirect($this->own_link."/add?msg=".urldecode('Save data topik success')."&type_msg=success");
	}
	function save2(){
		$data = array(
			'topik_title'			=> dbClean($_POST['topik_title']),
			'topik_status'			=> dbClean($_POST['topik_status'])
		);		

		if (dbClean($_POST['topik_id']) == "") {
			$data['topik_date'] = date("Y-m-d H:i:s");
			$data['topik_user'] = $this->jCfg['user']['id'];
		}
		
		$a = $this->_save_master( 
			$data,
			array(
				'topik_id' => dbClean($_POST['topik_id'])
			),
			dbClean($_POST['topik_id'])			
		);
		
		$base = base_url();
		$link = $_POST['link'];
		redirect($base.$link);
	}
	
}



