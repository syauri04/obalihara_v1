<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once(APPPATH."libraries/AdminController.php");

class Me extends AdminController {

	function __construct()
	{
		parent::__construct(); 
		$this->_set_title( 'Dashboard' );
		$this->DATA->table="cp_app_user";
		$this->upload_path="./assets/collections/photo/";

		//$this->output->enable_profilr(TRUE);

	} 

	function set_chart_type(){

		$this->jCfg['dashboard']['chart_type'] = isset($_GET['v'])?dbClean($_GET['v']):'';
		$this->_releaseSession();

		$go = isset($_GET['next'])?$_GET['next']:$this->own_link;
		redirect($go);

	}

	function set_report_type(){

		$this->jCfg['dashboard']['report_type'] = isset($_GET['v'])?dbClean($_GET['v']):'';
		$this->_releaseSession();

		$go = isset($_GET['next'])?$_GET['next']:$this->own_link;
		redirect($go);

	}

	function index(){ 	
			$data = array();
			//$this->_v("index",$data);
			$this->_v("home/home",$data);
	}
	function index2(){ 	
			$data = array();
			$this->load->view("admin/template/master",$data);
			//$this->_v("master",$data);
	}
	
	function user_guide(){
		$this->_set_title( 'User Guide' );
		$this->_v("user_guide",array());
	}

	function background(){
		$this->_set_title( 'Ganti Latar Belakang & Warna' );

		if( isset($_POST['simpan']) ){
			$color = isset($_POST['opt_color'])?$_POST['opt_color']:'mine';
			$bg = isset($_POST['opt_bg'])?$_POST['opt_bg']:'ptrn_e';

			$this->db->update("cp_app_user",array(
					'user_background' 	=> $bg,
					'user_themes'		=> $color
				),array(
					'user_id'		=> $this->jCfg['user']['id']
				));

			//set sesstion...
			$this->jCfg['user']['bg']		= $bg;
			$this->jCfg['user']['color']	= $color;
			$this->_releaseSession();
			
			redirect($this->own_link."/background?msg=".urldecode('Update background & color succes')."&type_msg=success");

		}

		$this->_v("background",array());
	}

	function profil(){
		$this->_set_title($this->jCfg['user']['fullname'].' Profil');
		$this->breadcrumb[] = array(
				"title"		=> "Profil"
			);
		$this->data_form =  $this->db->get_where("cp_app_user",array(
					"user_id"	=> $this->jCfg['user']['id']
				))->row();

		$this->_v("view-profil",array(
			"data"	=> $this->db->get_where("cp_app_user",array(
					"user_id"	=> $this->jCfg['user']['id']
				))->row(),
					'country'		=> $this->db->get_where("cp_country",array(
					"country_istrash <>" 		=> "1",
					"country_id" 		=> $this->data_form->user_negara
				))->result(),
					'province'		=> $this->db->get_where("cp_app_province",array(
					"province_id" 		=> $this->data_form->user_province
				))->result(),
					'city'			=> $this->db->get_where("cp_app_city",array(
					"city_id" 		=> $this->data_form->user_kota
				))->result(),
					'agama'			=> $this->db->get_where("cp_app_agama",array(
					"agama_id" 		=> $this->data_form->user_agama
				))->result(),
					'company'		=> $this->db->get_where("cp_company",array(
					"company_id" 		=> $this->data_form->user_company
				))->result(),
					'departement'	=> $this->db->get_where("cp_departement",array(
					"departement_id" 		=> $this->data_form->user_departement,
					"departement_istrash <>" 		=> "1"
				))->result()
		));
	}

	function edit_profil(){

		$this->_set_title('Update'.$this->jCfg['user']['fullname'].' Profil');
		$this->breadcrumb[] = array(
				"title"		=> "Profil",
				"url"		=> $this->own_link
			);

		$this->breadcrumb[] = array(
				"title"		=> "Update Profil"
			);



		if( isset($_POST['update']) ){
			
			$this->own_link = 'meme/me/profil';
			$this->save();

		}
		$this->data_form =  $this->db->get_where("cp_app_user",array(
					"user_id"	=> $this->jCfg['user']['id']
				))->row();
		$this->_v("edit-profil",array(
				"data"	=> $this->db->get_where("cp_app_user",array(
					"user_id"	=> $this->jCfg['user']['id']
				))->row(),
					'country'		=> $this->db->get_where("cp_country",array(
					"country_istrash <>" 		=> "1"
				))->result(),
					'province'		=> $this->db->get_where("cp_app_province",array(
					"province_country" 		=> $this->data_form->user_negara
				))->result(),
					'city'			=> $this->db->get_where("cp_app_city",array(
					"city_province_id" 		=> $this->data_form->user_province
				))->result(),
					'agama'			=> $this->db->get_where("cp_app_agama",array(
				))->result(),
					'company'		=> $this->db->get_where("cp_company",array(
				))->result(),
					'departement'	=> $this->db->get_where("cp_departement",array(
					"departement_company" 		=> $this->data_form->user_company,
					"departement_istrash <>" 		=> "1"
				))->result()
		));
	}

	function change_password(){
		$this->breadcrumb[] = array(
				"title"		=> "Change Password",
				"url"		=> $this->own_link
			);

		$pesan="";
		if(isset($_POST['btn_simpan'])){
			
			if($_POST['new_pass'] != $_POST['conf_new_pass']){
				
				$pesan = "Confirmation passwords do not match";
				$mtype = "error";
				
				$this->session->set_flashdata('oldPassword',$_POST['old_pass']);
				$this->session->set_flashdata('newPassword',$_POST['old_pass']);
				$this->session->set_flashdata('confNewPassword',$_POST['old_pass']);
				
				redirect($this->own_link."/change_password?msg=".urldecode($pesan)."&type_msg=".$mtype);
			}

			$pass_lama = md5(dbClean($_POST['old_pass']));
			$this->DATA->table="cp_app_user";
			$m1 = $this->DATA->_getall(array(
				"user_name"		=> $this->jCfg['user']['name'],
				"user_password"	=> $pass_lama
			));
			if(count($m1)>0){
				$pass_baru = md5(dbClean($_POST['new_pass']));
				$mx = $this->DATA->_update(
					array(
						"user_name"		=> $this->jCfg['user']['name']
					),array(
						"user_password" => $pass_baru
					)
				);
				$pesan = ($mx)?"Success update your password":"Success update your password";
				$mtype = ($mx)?"success":"error";
			}else{
				$pesan ="Your old password is not correctly!";
				$mtype = "error";
			}

			redirect($this->own_link."/change_password?msg=".urldecode($pesan)."&type_msg=".$mtype);

			
		}
		
		$this->_set_title('Change '.$this->jCfg['user']['fullname'].' Password');
		$this->_v("change-password",array(
			"pesan"	=> $pesan
		));
	}
		
	function bug(){
		if(isset($_POST['btn_simpan'])){
			$pesan = dbClean($_POST['pesan']);
			$url   = isset($_GET['url'])?$_GET['url']:'';
			$by    = $this->jCfg['user']['name'];
			$tgl   = date("Y-m-d H:i:s");
			$msg   = "Telah Terjadi Error Pada ".$tgl." Dilaporkan Oleh : ".$by."\n";
			$msg  .= "Error Pada ".$url." \n Pesan : ".$pesan."\n";
			
			$this->sendEmail(array(
				'from'		=> 'web@'.$this->domain,
				'to'		=> array(getCfgApp('bug_email')),
				'subject'	=> 'Bolanews Bug',
				'priority'	=> 1,
				'message'	=> $msg
			));
			
			echo "<script>parent.location.reload(true);</script>";
		}
		$this->_v("report_bug",array(
			"url"	=> isset($_GET['url'])?$_GET['url']:''
		),FALSE); 
	}
	function save(){
		$data = array(
			'user_fullname'		=> dbClean($_POST['user_fullname']),
			'user_name'			=> dbClean($_POST['user_name']),
			'user_email'		=> dbClean($_POST['user_email']),
			'user_company'		=> dbClean($_POST['user_company']),
			'user_departement'	=> dbClean($_POST['user_departement']),
			'user_jabatan'		=> dbClean($_POST['user_jabatan']),
			'user_alamat'		=> dbClean($_POST['user_alamat']),
			'user_negara'		=> dbClean($_POST['user_negara']),
			'user_kode_pos'		=> dbClean($_POST['user_kode_pos']),
			'user_agama'		=> dbClean($_POST['user_agama']),
			'user_fb'			=> dbClean($_POST['user_fb']),
			'user_twitter'		=> dbClean($_POST['user_twitter']),
			'user_status'		=> isset($_POST['user_status'])?1:0,
			'is_show_all'		=> isset($_POST['is_show_all'])?1:0
		);		

		if($_POST['user_negara'] !== '1'){

			$data['user_province_other'] 	= dbClean($_POST['user_province_text']); 
			$data['user_kota_other']		= dbClean($_POST['user_kota_text']);

		}else{

			$data['user_province'] 	= dbClean($_POST['user_province']); 
			$data['user_kota']		= dbClean($_POST['user_kota']);

		}

		$utl = explode("/",dbClean($_POST['user_tgl_lahir']));
		$user_tgl_lahir = $utl['2']."-".$utl['0']."-".$utl['1'];
		$data['user_tgl_lahir'] = $user_tgl_lahir;
		
		if( isset($_POST['user_password']) && trim($_POST['user_password']) != ''){
			$data['user_password'] = md5(dbClean($_POST['user_password']));
		}
		//debugCode($_FILES);
		
		$a = $this->_save_master( 
			$data,
			array(
				'user_id' => dbClean($_POST['user_id'])
			),
			dbClean($_POST['user_id'])			
		);

		$id = $a['id'];
		$this->_uploaded(
		array(
			'id'		=> $id ,
			'input'		=> 'user_photo',
			'param'		=> array(
							'field' => 'user_photo', 
							'par'	=> array('user_id' => $id)
						)
		));
	
		redirect($this->own_link."?msg=".urldecode('Save data user succes')."&type_msg=success");
	}
}

