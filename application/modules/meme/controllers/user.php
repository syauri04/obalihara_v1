<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once(APPPATH."libraries/AdminController.php");
class User extends AdminController {  
	function __construct()   
	{
		parent::__construct();     
		$this->_set_action();
		$this->_set_action(array("edit","delete"),"ITEM");
		$this->_set_title('User');
		$this->DATA->table="cp_app_user";
		$this->folder_view = "meme/";
		$this->prefix_view = strtolower($this->_getClass());

		$this->upload_path="./assets/collections/photo/";

		$this->breadcrumb[] = array(
				"title"		=> "User & Contact",
				"url"		=> $this->own_link
			);

	}

	function index(){

		$this->breadcrumb[] = array(
				"title"		=> "List"
			);
		
		$data['data'] = $this->DATA->_getall(array(
				'is_trash !=' => 1
			));	
					
		$this->_v($this->folder_view.$this->prefix_view,$data);
	}
		
	function cek_user(){
		echo _ajax_cek(array(
			"field" => "user_name",
			"table"	=> "iapi_user"
		));
	}
	
	function add(){		

		$this->breadcrumb[] = array(
				"title"		=> "Add"
			);
		$this->_v($this->folder_view.$this->prefix_view."_form",array(
			'group'			=> $this->db->get_where("cp_app_acl_group",array(
				"ag_group_status"	=>	"1",
				"is_trash <>" 		=> "1"
			))->result(),
			'country'		=> $this->db->get_where("cp_country",array(
				"country_istrash <>" 		=> "1"
			))->result(),
			'agama'			=> $this->db->get_where("cp_app_agama",array(
			))->result(),
			'company'		=> $this->db->get_where("cp_company",array(
			))->result()
		));
	}
	
	function edit($id=''){

		$this->breadcrumb[] = array(
				"title"		=> "Edit"
			);
		$id=dbClean(trim($id));
		if(trim($id)!=''){
			$this->data_form = $this->DATA->data_id(array(
					'user_id'	=> $id
				));
			//debugCode($this->data_form);						
			$this->_v($this->folder_view.$this->prefix_view."_form",array(
					'group'		=> $this->db->get_where("cp_app_acl_group",array(
					"ag_group_status"	=>	"1",
					"is_trash <>" => "1"	
				))->result(),
					'country'		=> $this->db->get_where("cp_country",array(
					"country_istrash <>" 		=> "1"
				))->result(),
					'province'		=> $this->db->get_where("cp_app_province",array(
					"province_country" 		=> $this->data_form->user_negara
				))->result(),
					'city'			=> $this->db->get_where("cp_app_city",array(
					"city_province_id" 		=> $this->data_form->user_province
				))->result(),
					'agama'			=> $this->db->get_where("cp_app_agama",array(
				))->result(),
					'company'		=> $this->db->get_where("cp_company",array(
				))->result(),
					'departement'	=> $this->db->get_where("cp_departement",array(
					"departement_company" 		=> $this->data_form->user_company,
					"departement_istrash <>" 		=> "1"
				))->result()
			));
		}else{
			redirect($this->own_link);
		}
	}
	
	function delete($id=''){
		$id=dbClean(trim($id));		
		if(trim($id) != ''){
			$o = $this->DATA->_delete(
				array("user_id"	=> idClean($id))
			);
			
		}
		redirect($this->own_link."?msg=".urldecode('Delete data user succes')."&type_msg=success");
	}

	function save(){
		$data = array(
			'user_fullname'		=> dbClean($_POST['user_fullname']),
			'user_name'			=> dbClean($_POST['user_name']),
			'user_email'		=> dbClean($_POST['user_email']),
			'user_company'		=> dbClean($_POST['user_company']),
			'user_departement'	=> dbClean($_POST['user_departement']),
			'user_jabatan'		=> dbClean($_POST['user_jabatan']),
			'user_alamat'		=> dbClean($_POST['user_alamat']),
			'user_negara'		=> dbClean($_POST['user_negara']),
			'user_kode_pos'		=> dbClean($_POST['user_kode_pos']),
			'user_agama'		=> dbClean($_POST['user_agama']),
			'user_fb'			=> dbClean($_POST['user_fb']),
			'user_twitter'		=> dbClean($_POST['user_twitter']),
			'user_status'		=> isset($_POST['user_status'])?1:0,
			'is_show_all'		=> isset($_POST['is_show_all'])?1:0,
			'user_group'		=> dbClean($_POST['user_group'])
		);		

		if($_POST['user_negara'] !== '1'){

			$data['user_province_other'] 	= dbClean($_POST['user_province_text']); 
			$data['user_kota_other']		= dbClean($_POST['user_kota_text']);

		}else{

			$data['user_province'] 	= dbClean($_POST['user_province']); 
			$data['user_kota']		= dbClean($_POST['user_kota']);

		}

		$utl = explode("/",dbClean($_POST['user_tgl_lahir']));
		$user_tgl_lahir = $utl['2']."-".$utl['0']."-".$utl['1'];
		$data['user_tgl_lahir'] = $user_tgl_lahir;
		
		if( isset($_POST['user_password']) && trim($_POST['user_password']) != ''){
			$data['user_password'] = md5(dbClean($_POST['user_password']));
		}
		
		$a = $this->_save_master( 
			$data,
			array(
				'user_id' => dbClean($_POST['user_id'])
			),
			dbClean($_POST['user_id'])			
		);

		$id = $a['id'];
		$this->_uploaded(
		array(
			'id'		=> $id ,
			'input'		=> 'user_photo',
			'param'		=> array(
							'field' => 'user_photo', 
							'par'	=> array('user_id' => $id)
						)
		));
	
		redirect($this->own_link."?msg=".urldecode('Save data user succes')."&type_msg=success");
	}

}
