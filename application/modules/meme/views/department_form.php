 <div class="row">
        <!-- begin col-6 -->
        <div class="col-md-12">
          <!-- begin panel -->
          <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
            <div class="panel-heading">
              <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
              </div>
              <h4 class="panel-title">Department Form</h4>
            </div>
            <div class="panel-body">
              <form id="form-validated" class="form-horizontal" enctype="multipart/form-data"  action="<?php echo $own_links;?>/save" method="post">
                  <input type="hidden" name="departement_id" id="departement_id" value="<?php echo isset($val->departement_id)?$val->departement_id:'';?>" />
                  <input type="hidden" name="departement_company" id="departement_company" value="<?php echo isset($val->departement_company)?$val->departement_company:$this->session->flashdata('id_company');?>" />
                  
                  <div class="form-group">
                  <label class="col-md-3 control-label">Department Name</label>
                  <div class="col-md-7">
                    <input type="text" id="departement_title" name="departement_title" class="form-control" value="<?php echo isset($val->departement_title)?$val->departement_title:'';?>" />
                  </div>
                </div>


                <div class="form-group">
                  <label class="col-md-3 control-label">Status</label>
                  <div class="col-md-3">
                      <select name="departement_status" id="departement_status" class="form-control">
                      <option value="1" <?php echo ((! empty($val)) && ($val->departement_status == 1)) ? " selected='selected' " : ""; ?>>Aktif</option>
                      <option value="0" <?php echo ((! empty($val)) && ($val->departement_status == 0)) ? " selected='selected' " : ""; ?>>Nonaktif</option>
                    </select>
                    
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-md-3 control-label"></label>
                  <div class="col-md-9">
                    <button type="submit" class="btn btn-sm btn-success">Save</button>
                    <button type="submit" class="btn btn-sm btn-default">Cancle</button>
                  </div>
                </div>
                </form>
            </div>
          </div>
          <!-- end panel -->
        </div>
        <!-- end col-6 -->
        <!-- begin col-6 -->

        <!-- end col-6 -->
      </div>
  