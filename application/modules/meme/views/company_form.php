 <div class="row">
	<!-- begin col-6 -->
	<div class="col-md-12">
		<!-- begin panel -->
		<div class="panel panel-inverse" data-sortable-id="form-stuff-1">
			<div class="panel-heading">
				<div class="panel-heading-btn">
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				</div>
				<h4 class="panel-title">Company Form</h4>
			</div>
			<div class="panel-body">
				<form id="form-validated" class="form-horizontal" enctype="multipart/form-data"  action="<?php echo $own_links;?>/save" method="post" data-parsley-validate="true">
						<input type="hidden" name="company_id" id="company_id" value="<?php echo isset($val->company_id)?$val->company_id:'';?>" />
                    <div class="row">
                        <div class="col-xs-6">
							<div class="form-group">
								<label class="col-md-3 control-label">Company Name</label>
								<div class="col-md-9">
									<input type="text" id="company_title" name="company_title" class="form-control" value="<?php echo isset($val->company_title)?$val->company_title:'';?>" data-parsley-required="true" data-parsley-id="0166"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Description</label>
								<div class="col-md-9">
									<textarea class="form-control" id="company_desc" name="company_desc" data-parsley-required="true" style="height:60px;"><?php echo isset($val->company_desc)?$val->company_desc:'';?></textarea>
								</div>
							</div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Address</label>
                                <div class="col-md-9">
                                	<textarea name="company_alamat" class="form-control" data-parsley-required="true" style="height:60px;"><?php echo isset($val->company_alamat)?$val->company_alamat:'';?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Country</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="company_negara" id="company_negara" data-parsley-required="true" onchange="comboCPC('#company_negara','#company_province','ajax/getProvince')">
		                                <option value=""> - select - </option>
										<?php
										if(count($country)>0){
											foreach($country as $r){
												//debugCode($val);
												$t = isset($val->company_negara)?$val->company_negara:"";
												$s = ($r->country_id==$t)?"selected='selected'":'';
												echo "<option value='".$r->country_id."' $s >".$r->country_title."</option>";
											}
										} 
										?>
	                                </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Province</label>
                                <div class="col-md-9">
                                    <input type="text" name="company_province_text" id="company_province_text" class="form-control" value="<?php echo isset($val->company_province_other)?$val->company_province_other:'';?>">
                                    <select class="form-control" name="company_province" id="company_province" onchange="comboCPC('#company_province','#company_kota','ajax/getCity')">
		                                <option value=""> - select - </option>
										<?php
										if(count($province)>0){
											foreach($province as $r){
												//debugCode($val);
												$t = isset($val->company_province)?$val->company_province:"";
												$s = ($r->province_id==$t)?"selected='selected'":'';
												echo "<option value='".$r->province_id."' $s >".$r->province_title."</option>";
											}
										} 
										?>
	                                </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">City</label>
                                <div class="col-md-9">
                                    <input type="text" name="company_kota_text" id="company_kota_text" class="form-control" value="<?php echo isset($val->company_kota_other)?$val->company_kota_other:'';?>">
                                    <select class="form-control" name="company_kota" id="company_kota" >
		                                <option value=""> - select - </option>
										<?php
											if(count($city)>0){
											foreach($city as $r){
												//debugCode($val);
												$t = isset($val->company_kota)?$val->company_kota:"";
												$s = ($r->city_id==$t)?"selected='selected'":'';
												echo "<option value='".$r->city_id."' $s >".$r->city_title."</option>";
											}
										} 
										?>
	                                </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Postal Code</label>
                                <div class="col-md-9">
                                    <input type="text" name="company_kode_pos" class="form-control" value="<?php echo isset($val->company_kode_pos)?$val->company_kode_pos:'';?>"  data-parsley-required="true">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Telephone</label>
                                <div class="col-md-9">
                                    <input type="text" name="company_telepon" class="form-control" value="<?php echo isset($val->company_telepon)?$val->company_telepon:'';?>"  data-parsley-required="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Fax</label>
                                <div class="col-md-9">
                                    <input type="text" name="company_fax" class="form-control" value="<?php echo isset($val->company_fax)?$val->company_fax:'';?>"  data-parsley-required="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Email</label>
                                <div class="col-md-9">
                                    <input type="text" name="company_email" class="form-control" value="<?php echo isset($val->company_email)?$val->company_email:'';?>"  data-parsley-required="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Website</label>
                                <div class="col-md-9">
                                    <input type="text" name="company_website" class="form-control" value="<?php echo isset($val->company_company)?$val->company_company:'';?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Fb</label>
                                <div class="col-md-9">
                                    <input type="text" name="company_fb" class="form-control" value="<?php echo isset($val->company_departemen)?$val->company_departemen:'';?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Twitter</label>
                                <div class="col-md-9">
                                    <input type="text" name="company_twitter" class="form-control" value="<?php echo isset($val->company_jabatan)?$val->company_jabatan:'';?>">
                                </div>
                            </div>
							<div class="form-group">
								<label class="col-md-3 control-label">Status</label>
								<div class="col-md-9">
									<select name="company_status" id="company_status" class="form-control" data-parsley-required="true">
										<option value="1" <?php echo ((! empty($val)) && ($val->company_status == 1)) ? " selected='selected' " : ""; ?>>Aktif</option>
										<option value="0" <?php echo ((! empty($val)) && ($val->company_status == 0)) ? " selected='selected' " : ""; ?>>Nonaktif</option>
									</select>									
								</div>
							</div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Company Type</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="company_type" id="company_type" data-parsley-required="true">
		                                <option value=""> - select - </option>
										<?php
										if(count($company_type)>0){
											foreach($company_type as $r){
												//debugCode($val);
												$t = isset($val->company_type)?$val->company_type:"";
												$s = ($r->company_type_id==$t)?"selected='selected'":'';
												echo "<option value='".$r->company_type_id."' $s >".$r->company_type_title."</option>";
											}
										} 
										?>
	                                </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
					<div class="form-group">
						<div class="col-md-9">
							<button type="submit" class="btn btn-sm btn-success">Save</button>
							<button type="submit" class="btn btn-sm btn-default">Cancle</button>
						</div>
					</div>
					</form>
			</div>
		</div>
		<!-- end panel -->
	</div>
	<!-- end col-6 -->
	<!-- begin col-6 -->

	<!-- end col-6 -->
</div>
  