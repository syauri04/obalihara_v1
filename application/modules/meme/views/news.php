   <div class="row">
				<!-- begin col-12 -->
				<div class="col-md-12">
					<!-- begin panel -->
					<div class="panel panel-inverse ">
						<div class="panel-heading">
							<div class="panel-heading-btn">
								<?php isset($links)?getLink2($links):'';?> 
        				  		<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
							</div>
							<h4 class="panel-title">News Management</h4>
						</div>
						<div class="panel-body">

							<div class="table-responsive">

								<?php getFormSearch();?>
								<table class="table table-striped table-bordered">
						
									<thead>
										<tr>
											<th width="20">No</th>
											<th>Title</th>
											<th>Status</th>
											<th width="80">Action</th>
										</tr>
									</thead>
									<tbody>
										  <?php
												if(count($data) > 0){
												  $pageNumber = $this->uri->segment(4);
												  if(!empty($pageNumber)){
													  $no = $pageNumber+1;
												  }else{
													  $no=1;
												  }	 
												  //debugCode($data);
												  foreach($data as $r){?>
													<tr valign="top">
													  <td><center><?php echo $no++;?></center></td>			 
													  <td><?php echo $r->news_title;?></td>
													  <td><?php echo ($r->news_status=='1')?'Aktif':'Non Aktif';?></td>
													  <td align="right">
														  <?php link_action($links_table_item,$r->news_id);?>
													  </td>
													</tr>
												<?php } 
												}
												?>       
									</tbody>
								</table>

								<?php
									echo $paging;
								?>

							</div>
						</div>
					</div>
					<!-- end panel -->
				</div>
				<!-- end col-12 -->
			</div>


  