<div class="row">
  <div class="col-md-12 ui-sortable">
      <!-- begin panel -->
      <div class="panel panel-inverse" data-sortable-id="table-basic-1">
          <div class="panel-heading">
              <div class="panel-heading-btn">
                  <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                  <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                  <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
              </div>
              <h4 class="panel-title">List Module</h4>
          </div>
          <div class="panel-body">
            <form action="" method="post" id="myform" class="input">
              <table class="table table-striped table-bordered dTableR">
                <thead>
                  <tr height="20px">
                    <th width="30" rowspan="2">No</th>
                    <th width="439" rowspan="2">Module Name</th>
                    <th colspan="<?php echo count($actions);?>"><center>Action</center></th>
                  </tr>
                  <tr style="font-size:11px; font-weight:normal;">
                    <?php if(count($actions)>0){
                      foreach($actions as $m){
                    ?>
                    <th width="10px" style="padding:5px;">
                      <center>
                      <a href="#" class="south ttip_t" style="color:#222; text-decoration:none;" title="<?php echo $m->ac_action_name;?>"><?php echo $m->ac_action_name;?></a>
                      </center>
                    </th>
                    
                    <?php } 
                      }
                    ?>
                  </tr> 
                </thead>
                <tbody>  
                  <?php if(count($access) > 0){
                    $no=1;
                    foreach($access as $r){
                    //debugCode($r);
                  
                  ?>
                  <tr>
                    <td align="center"><?php echo $no++;?>.</td>
                    <td>
                      <button type="button" class="btn btn-xs btn-default m-r-5 m-b-5"><?php echo $r['group_menu'];?></button>
                      <?php echo $r['module_name'];?>
                    </td>
                      <?php 
                        if(count($r['action'])>0){
                          foreach($r['action'] as $acc){

                            if($acc['show']==1){
                              $chk = ($acc['value']==1)?'checked="checked"':'';
                              $name_chk = "acc_name[".$r['id_module']."][".$acc['id']."]";
                              echo "<td align='center'><input type='checkbox' class='ttip_t' title='".$acc['name']."' $chk name='$name_chk' value='".$acc['id']."' style='cursor:pointer;' class='south' title='".$acc['name']."'></td>";
                            }else{
                              echo "<td align='center'>-</td>";
                            }
                          } 
                        }
                      ?>
                  </tr> 
                  <?php } 
                    }     
                  ?>

                </tbody>
              </table>
              
              <hr>

              <div class="form-group">
                  <div class="col-md-12">
                      <input type="submit" value="Save" class="btn btn-sm btn-success" name="simpan"/>
                      <a href="<?php echo $own_links;?>" class="btn btn-sm btn-default">Cancel</a>
                  </div>

              </div>

            </form>
          </div>
      </div>
      <!-- end panel -->
  </div>
</div>
