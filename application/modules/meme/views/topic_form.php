<?php //getTinymce();?>  
<?php js_validate(); ?>
<?php js_picker();?>        
 
 <div class="row">
				<!-- begin col-6 -->
				<div class="col-md-12">
					<!-- begin panel -->
					<div class="panel panel-inverse" data-sortable-id="form-stuff-1">
						<div class="panel-heading">
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
							<h4 class="panel-title">Topic Management</h4>
						</div>
						<div class="panel-body">
							<form id="form-validated" class="form-horizontal" enctype="multipart/form-data"  action="<?php echo $own_links;?>/save" method="post" data-parsley-validate="true" >
									<input type="hidden" name="topik_id" id="topik_id" value="<?php echo isset($val->topik_id)?$val->topik_id:'';?>" />
									<div class="form-group">
									<label class="col-md-3 control-label">Title</label>
									<div class="col-md-7">
										<input type="text" id="topik_title" name="topik_title" class="form-control" value="<?php echo isset($val->topik_title)?$val->topik_title:'';?>" data-parsley-required="true"/>
									</div>
								</div>




								<div class="form-group">
									<label class="col-md-3 control-label">Status</label>
									<div class="col-md-3">
										  <select name="topik_status" id="topik_status" class="form-control" data-parsley-required="true">
											<option value="1" <?php echo ((! empty($val)) && ($val->topik_status == 1)) ? " selected='selected' " : ""; ?>>Aktif</option>
											<option value="0" <?php echo ((! empty($val)) && ($val->topik_status == 0)) ? " selected='selected' " : ""; ?>>Nonaktif</option>
										</select>
										
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label"></label>
									<div class="col-md-9">
										<button type="submit" class="btn btn-sm btn-success">Save</button>
										<button type="submit" class="btn btn-sm btn-default">Cancle</button>
									</div>
								</div>
								</form>
						</div>
					</div>
					<!-- end panel -->
				</div>
				<!-- end col-6 -->
				<!-- begin col-6 -->

				<!-- end col-6 -->
			</div>
  

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/tinymce/tinymce.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugin/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea.tinymce",
	height:400,
	width:712,
    theme: "modern",
    plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor moxiemanager"
    ],
    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    toolbar2: "print preview media | forecolor backcolor emoticons",
    templates: [
        {title: 'Test template 1', content: 'Test 1'},
        {title: 'Test template 2', content: 'Test 2'}
    ],
	
	content_css: "css/content.min.css",
	style_formats: [
		{
			title: 'Table Style',
			selector: 'table',
			styles: {
				'table-layout': 'fixed', 
			}
		 },
		 {
			 title: 'Td Style',
			 selector: 'td', 
			 styles: {
				 'word-wrap':'break-word', 
			 }
		 }
	]
});
</script>
<script language="javascript">
		$('#selecttags').click(function() {
			 $('#disptags').show('slow');	
    		return false;
		});	 			
		$('#topik_section_id').change(function(){
            $.post("<?php echo base_url();?>toko/article/category/"+$('#topik_section_id').val(),{},function(obj){
                $('#topik_category_id').html(obj);
            });
        });
</script>
<script language="javascript" type="text/javascript">
 
    $(document).ready( function () {
	maxLL = $("textarea#topik_lead").attr("maxlength");
		$("textarea#topik_lead").after("<div><em><span id='topik_lead_length'>" 
                  + maxLL + "</span> Character.</em></div>");
        $("#topik_lead").bind("keyup change", function(){
			//checkMaxLength($(this).attr("id"),  $(($(this).attr("id")).attr("maxlength")); 
			idEl = "#"+$(this).attr("id");
			mL = $(idEl).attr("maxlength");
			currentLengthInTextarea = $(idEl).val().length;
			$(idEl+"_length").text(parseInt(mL) - parseInt(currentLengthInTextarea));
	 
			if (currentLengthInTextarea > (mL)) { 
	 
				// Trim the field current length over the maxlength.
				$(idEl).val($(idEl).val().slice(0, maxLength));
				$(idEl+"_length").text(0);
	 
			}
		});
		
 
        //$("textarea#topik_teaser").bind("keyup change", function(){checkMaxLT("topik_teaser",  maxLT); } )
 
    });
</script>
<?php gebo_choosen();?>