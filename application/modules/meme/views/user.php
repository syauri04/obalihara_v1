<div class="row">
  <div class="col-md-12 ui-sortable">
      <!-- begin panel -->
      <div class="panel panel-inverse" data-sortable-id="table-basic-1">
          <div class="panel-heading">
              <div class="panel-heading-btn">
				  <?php isset($links)?getLink2($links):'';?> 
        		  <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                  <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                  <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
              </div>
              <h4 class="panel-title">User</h4>
          </div>
          <div class="panel-body">
          <div class="table-responsive">
            <table id="data-table" class="table table-striped table-bordered">
              <thead>
                <tr>
					<th>Foto</th>
					<th>Username</th>
					<th>Full Nama</th>
			   		<th>Email</th>
					<th>Status</th>
					<th>Action</th>
                </tr>
              </thead>
              <tbody>
				<?php 
				$ujian = cfg('exam_type');
				if(count($data) > 0){
					$no=0;
					foreach($data as $r){
				?>
						<tr>
							<!-- <td><?php echo ++$no;?></td> -->
							<td>
								<a href="<?php echo get_image(base_url()."assets/collections/photo/".$r->user_photo);?>" title="Photo <?php echo $r->username;?>" class="act_modal" rel="600|350">
									<img alt="" src="<?php echo get_image(base_url()."assets/collections/photo/".$r->user_photo);?>" class="img-polaroid" style="height:50px;width:50px">
								</a>
							</td>
							<td><?php echo $r->username;?></td>
							<td><?php echo $r->user_fullname;?></td>
							<td><?php echo $r->user_email;?></td>
							<td><?php echo ($r->user_status==1)?'Aktif':'Non Aktif';?></td>
							<td align="center">
								<?php link_action($links_table_item,$r->user_id);?>
							</td>
						</tr>
				<?php } 
				}
				?>
              </tbody>
            </table>
          </div>

          </div>
      </div>
      <!-- end panel -->
  </div>
</div>