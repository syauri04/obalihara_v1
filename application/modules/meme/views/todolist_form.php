<div class="row">
				<!-- begin col-6 -->
				<div class="col-md-12">
					<!-- begin panel -->
					<div class="panel panel-inverse" data-sortable-id="form-stuff-1">
						<div class="panel-heading">
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
							<h4 class="panel-title">To do list</h4>
						</div>
						<div class="panel-body">
						 <?php
							$date = isset($val->article_publishdate) ? date("Y-m-d H i",strtotime($val->article_publishdate)) : date("Y-m-d H i");
							$explode_date = explode(" ",$date);
							$out = explode("-",$explode_date[0]);
							$date_now = $out[1]."/".$out[2]."/".$out[0];
						?>
							<form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo $own_links;?>/save">
								 <input type="hidden" name="id" id="id" value="<?php echo isset($val->id)?$val->id:'';?>" />
								
								
								<div class="form-group">
									<label class="col-md-3 control-label">Username</label>
									<div class="col-md-7">
										<input type="text" class="form-control" id="username" name="username" maxlength="70"  value="<?php echo isset($val->username)?$val->username:'';?>" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label">To do list</label>
									<div class="col-md-7">
										<input type="text" class="form-control" id="todo" name="todo" maxlength="70" value="<?php echo isset($val->todo)?$val->todo:'';?>" />
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-md-3 control-label">Status</label>
									<div class="col-md-3">
										<select class="form-control" name="status" id="status" class="validate[required]">
											<option value="1" <?php echo ((! empty($val)) && ($val->status == 1)) ? " selected='selected' " : ""; ?>>Aktif</option>
											<option value="0" <?php echo ((! empty($val)) && ($val->status == 0)) ? " selected='selected' " : ""; ?>>Nonaktif</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-3 control-label"></label>
									<div class="col-md-9">
										<button type="submit" class="btn btn-sm btn-success">Save</button>
										<button type="submit" class="btn btn-sm btn-default">Cancel</button>
										
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- end panel -->
				</div>
	<div class="modal fade" id="modal-dialog-topic">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Add Topic</h4>
        </div>
		<form id="form-validated" class="form-horizontal" enctype="multipart/form-data"  action="<?php echo base_url();?>meme/topic/save2" method="post">
			<div class="modal-body">
			 <div class="panel-body">
					
							<input type="hidden" name="link" id="link" value="content/upload" />
							<input type="hidden" name="topik_id" id="topik_id" value="<?php echo isset($val->topik_id)?$val->topik_id:'';?>" />
							<div class="form-group">
							<label class="col-md-3 control-label">Title</label>
							<div class="col-md-7">
								<input type="text" id="topik_title" name="topik_title" class="form-control" value="<?php echo isset($val->topik_title)?$val->topik_title:'';?>" />
							</div>
						</div>




						<div class="form-group">
							<label class="col-md-3 control-label">Status</label>
							<div class="col-md-3">
								  <select name="topik_status" id="topik_status" class="form-control">
									<option value="1" <?php echo ((! empty($val)) && ($val->topik_status == 1)) ? " selected='selected' " : ""; ?>>Aktif</option>
									<option value="0" <?php echo ((! empty($val)) && ($val->topik_status == 0)) ? " selected='selected' " : ""; ?>>Nonaktif</option>
								</select>
								
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label"></label>
							<div class="col-md-9">
								
							</div>
						</div>
						
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-sm btn-success" >Save</button>
				<button type="submit" class="btn btn-sm btn-white" data-dismiss="modal">Cancle</button>
			 
			</div>
		</form>
      </div>
    </div>
  </div>

<script src="<?php echo base_url(); ?>dashboard/assets/plugins/jquery-tag-it/js/tag-it.min.js"></script>

	
