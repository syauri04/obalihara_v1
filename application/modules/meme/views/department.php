<div class="row">
  <div class="col-md-12 ui-sortable">
      <!-- begin panel -->
      <div class="panel panel-inverse" data-sortable-id="table-basic-1">
          <div class="panel-heading">
              <div class="panel-heading-btn">
                  <?php isset($links)?getLink2($links):'';?> 
                  <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                  <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                  <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
              </div>
              <h4 class="panel-title">Department</h4>
          </div>
          <div class="panel-body">
          <div class="table-responsive">
            <table id="data-table" class="table table-striped table-bordered">
              <thead>
                <tr>
          <th width="30px">No</th>
          <th>Department Name</th>
          <th>Status</th>
          <th width="120">Action</th>
                </tr>
              </thead>
              <tbody>
        <?php 
        if(count($data) > 0){
          $no=0;
            //debugCode($data);
          foreach($data as $r){?>
            <tr>
              <td><?php echo ++$no;?></td>
              <td><?php echo $r->departement_title;?></td>
              <td><?php echo ($r->departement_status==1)?'Aktif':'Non Aktif';?></td>
              <td align="center">
                <?php link_action($links_table_item,$r->departement_id);?>
              </td>
            </tr>
        <?php } 
        }
        ?>
              </tbody>
            </table>
          </div>

          </div>
      </div>
      <!-- end panel -->
  </div>
</div>