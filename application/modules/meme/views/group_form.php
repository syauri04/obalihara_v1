<div class="row">
  <!-- begin col-6 -->
  <div class="col-md-12 ui-sortable">
      <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">User Group Form</h4>
                </div>
                <div class="panel-body">
                      <form action="<?php echo $own_links;?>/save" method="post" class="form-horizontal" data-parsley-validate="true" name="form">
                        
                        <input type="hidden" name="group_id" id="group_id" value="<?php echo isset($val->ag_id)?$val->ag_id:'';?>" />

                        <div class="form-group">
                            <label class="col-md-3 control-label">User Group Name</label>
                            <div class="col-md-9">
                                <input type="text" name="group_name" class="form-control" value="<?php echo isset($val->ag_group_name)?$val->ag_group_name:'';?>"  data-parsley-required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Description</label>
                            <div class="col-md-9">
                                <input type="text" name="group_desc" class="form-control" value="<?php echo isset($val->ag_group_desc)?$val->ag_group_desc:'';?>"  data-parsley-required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Status</label>
                            <div class="col-md-9">
                                <select class="form-control" name="group_status" data-parsley-required="true">
                                  <option value="1" <?php echo (isset($val->ag_group_status)&& $val->ag_group_status=="1")?'selected="selected"':'';?>>Active</option>
                                  <option value="0"  <?php echo (isset($val->ag_group_status)&& $val->ag_group_status=="0")?'selected="selected"':'';?> >Non Active</option>
                                </select>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-sm btn-success">Save</button>
                                <a href="<?php echo $own_links;?>" class="btn btn-sm btn-default">Cancel</a>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->
    </div>
    <!-- end row -->
