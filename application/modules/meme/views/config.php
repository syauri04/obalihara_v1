<div class="row">
  <!-- begin col-6 -->
  <div class="col-md-12 ui-sortable">
      <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">Setting</h4>
                </div>
                <div class="panel-body">

                    <form action="<?php echo $own_links;?>/save" enctype="multipart/form-data" method="post" class="form-horizontal" data-parsley-validate="true" name="form">
                        <input type="hidden" name="configuration_id" id="configuration_id" value="<?php echo isset($configuration->configuration_id)?$configuration->configuration_id:""?>">
                        <div class="row">
                          <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Logo</label>
                                <div class="col-md-9">
                                  <span class="btn btn-success fileinput-button">
                                    <i class="fa fa-plus"></i>
                                    <span>Choose files...</span>
                                    <input type="file" name="configuration_logo" id="configuration_logo" value="<?php echo isset($configuration->configuration_id)?$configuration->configuration_id:""?>">
                                  </span>
                                  &nbsp;
                                  <?php if( isset($configuration->configuration_logo) && trim($configuration->configuration_logo)!="" ){?>
                                  <a href="<?php echo get_image(base_url()."assets/collections/logo/large/".$configuration->configuration_logo);?>" title="Image Photo" class="act_modal" rel="700|400">
                                      <img alt="" src="<?php echo get_image(base_url()."assets/collections/logo/small/".$configuration->configuration_logo);?>" style="max-height:30px;" class="img-polaroid">
                                  </a>
                                  <?php } ?>
                                </div>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Nama Website</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" name="configuration_name" id="configuration_name" value="<?php echo isset($configuration->configuration_name)?$configuration->configuration_name:""?>" data-parsley-required="true">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Tentang Kami</label>
                                <div class="col-md-9">
                                  <textarea class="form-control"  name="configuration_about" id="configuration_about"><?php echo isset($configuration->configuration_about)?$configuration->configuration_about:""?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Alamat</label>
                                <div class="col-md-9">
                                  <textarea class="form-control" name="configuration_alamat" id="configuration_alamat"><?php echo isset($configuration->configuration_alamat)?$configuration->configuration_alamat:""?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Email</label>
                                <div class="col-md-9">
                                  <input class="form-control" type="text" name="configuration_email" id="configuration_email" value="<?php echo isset($configuration->configuration_email)?$configuration->configuration_email:""?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Telpon</label>
                                <div class="col-md-9">
                                   <input class="form-control" type="text" name="configuration_telp" id="configuration_telp" value="<?php echo isset($configuration->configuration_telp)?$configuration->configuration_telp:""?>">
                              </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Fax</label>
                                <div class="col-md-9">
                                  <input class="form-control" type="text" name="configuration_fax" id="configuration_fax" value="<?php echo isset($configuration->configuration_fax)?$configuration->configuration_fax:""?>">
                                </div>
                            </div>
                          </div>
                          <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Website</label>
                                <div class="col-md-9">
                                  <input class="form-control" type="text" name="configuration_website" id="configuration_website" value="<?php echo isset($configuration->configuration_website)?$configuration->configuration_website:""?>">  
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Facebook</label>
                                <div class="col-md-9">
                                  <input class="form-control" type="text" name="configuration_fb" id="configuration_fb" value="<?php echo isset($configuration->configuration_fb)?$configuration->configuration_fb:""?>">  
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Twitter</label>
                                <div class="col-md-9">
                                  <input type="text" class="form-control" name="configuration_tw" id="configuration_tw" value="<?php echo isset($configuration->configuration_tw)?$configuration->configuration_tw:""?>">  
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Meta Title</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" name="configuration_meta_title" id="configuration_meta_title" value="<?php echo isset($configuration->configuration_meta_title)?$configuration->configuration_meta_title:""?>">
                                </div>
                            </div>
             
                            <div class="form-group">
                                <label class="col-md-3 control-label">Meta Keyword</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" name="configuration_meta_keyword" id="configuration_meta_keyword" value="<?php echo isset($configuration->configuration_meta_keyword)?$configuration->configuration_meta_keyword:""?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Meta Description</label>
                                <div class="col-md-9">
                                    <textarea class="form-control" name="configuration_meta_desc" id="configuration_meta_desc"><?php echo isset($configuration->configuration_meta_desc)?$configuration->configuration_meta_desc:""?></textarea>
                                </div>
                            </div>
                          </div>
                        </div>

                        <hr>

                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-sm btn-success">Save</button>
                                <a href="<?php echo $own_links;?>" class="btn btn-sm btn-default">Cancel</a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->
    </div>
    <!-- end row -->
 
          