<div class="row">
	<!-- begin col-6 -->
	<div class="col-md-12">
		<!-- begin panel -->
		<div class="panel panel-inverse" data-sortable-id="form-stuff-1">
			<div class="panel-heading">
				<div class="panel-heading-btn">
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
				</div>
				<h4 class="panel-title">News Management</h4>
			</div>
			<div class="panel-body">
			 <?php
				$date = isset($val->news_publishdate) ? date("Y-m-d H i",strtotime($val->news_publishdate)) : date("Y-m-d H i");
				$explode_date = explode(" ",$date);
				$out = explode("-",$explode_date[0]);
				$date_now = $out[1]."/".$out[2]."/".$out[0];
			?>
				<form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo $own_links;?>/save" data-parsley-validate="true" >
					 <input type="hidden" name="news_id" id="news_id" value="<?php echo isset($val->news_id)?$val->news_id:'';?>" />
					<div class="form-group">
						<label class="col-md-3 control-label">Publish Date</label>
						<div class="col-md-2">
							<input type="text" class="form-control" id="datepicker-default" name="pdate"  placeholder=""  value="<?php echo $date_now;?>"   data-parsley-required="true"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Title</label>
						<div class="col-md-7">
							<input type="text" class="form-control" id="news_title" name="news_title" value="<?php echo isset($val->news_title)?$val->news_title:'';?>"   data-parsley-required="true"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Description</label>
						<div class="col-md-7">
							<textarea class="ckeditor" id="news_content" name="news_content" value="<?php echo isset($val->news_content)?$val->news_content:'';?>" rows="20"   data-parsley-required="true">
							<?php echo isset($val->news_content)?$val->news_content:'';?>
							</textarea>
						</div>
					</div>
					<!--<div class="form-group">
						<label class="col-md-3 control-label">File</label>
						<div class="col-md-3">
							<input type="file" id="news_file" name="news_file" class="filestyle form-control f-upload" data-icon="false">
						</div>
					</div>!-->
					<div class="form-group">
						<label class="col-md-3 control-label">Image</label>
						<div class="col-md-3">
							<input type="file" name="news_image" accept="image/*"   data-parsley-required="true"> 
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Status</label>
						<div class="col-md-2">
							
								  <select name="news_status" id="news_status" class="form-control"  data-parsley-required="true">
									
								<option value="1" <?php echo ((! empty($val)) && ($val->news_status == 1)) ? " selected='selected' " : ""; ?>>Aktif</option>
								<option value="0" <?php echo ((! empty($val)) && ($val->news_status == 0)) ? " selected='selected' " : ""; ?>>Nonaktif</option>
								</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label"></label>
						<div class="col-md-9">
							<button type="submit" class="btn btn-sm btn-success">Save</button>
							<button type="submit" class="btn btn-sm btn-default">Cancle</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- end panel -->
	</div>
