<?php js_validate();?>
<style>
	.box-theme{
		float:left;
		margin:10px;
	}
	.box-img{
		width:120px;
		height:120px;
		border:1px solid rgba(0,0,0,0.1);	
	}
	#lightbox{
		background-color:#eee;
		padding: 10px;
		border-bottom: 2px solid #666;
		border-right: 2px solid #666;
		}
	#lightboxDetails{
		font-size: 0.8em;
		padding-top: 0.4em;
		}	
	#lightboxCaption{ float: left; }
	#keyboardMsg{ float: right; }
	
	#lightbox img{ border: none; } 
	#overlay img{ border: none; }
	#overlay{ border: none;background:rgba(0,0,0,0.8) }
</style>
<form name="form-validated" id="form-validated" method="post" action="<?php echo $own_links;?>/save" class="input" enctype="multipart/form-data">
<input type="hidden" name="template_id" id="template_id" value="<?php echo isset($template->template_id)?$template->template_id:""?>">
  <div class="well" style="background-color:#fff;">
  <?php 
  if(!empty($templatelist)){
	  foreach($templatelist as $key => $t){?>
	<div class="box-theme">
    	<div>
        	<a href="<?php echo themeDefaultUrl().$key?>/large.png" rel="lightbox" title="<?php echo $key?>">
        		<img src="<?php echo themeDefaultUrl().$key?>/thumb.png" class="box-img" >
            </a>
        </div>
    	<div style="padding:5px">
        	<input type="radio" name="template_name" value="<?php echo $key?>" id="template_name" <?php echo (isset($template->template_name)&& $template->template_name==$key)?'checked="checked"':'';?> > <strong><?php echo $key?></strong>
        </div>
    </div>
    <?php }}?>
    <div style="clear:both"></div>
  </div>
  <div class="well" style="background-color:#fff;">
  <table>
    <tr>
      <td style="width:200px;"><input type="reset" value="reset" class="btn btn-simpan"  /></td>
      <td><input type="submit" value="Save" name="submit" class="btn btn-primary btn-simpan" /></td>
    </tr>
  </table>
  </div>
</form>
<script src="<?php echo themeUrl()?>js/lightbox.js"></script>
                   