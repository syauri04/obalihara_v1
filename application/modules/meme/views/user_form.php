<?php 

    if(isset($val->user_tgl_lahir)){
        $d=explode("-", $val->user_tgl_lahir);
        $user_tgl_lahir=$d['1']."/".$d['2']."/".$d['0'];
    }else{
        $dateNow = date('m/j/Y');
    }
?>

<div class="row">
  <!-- begin col-6 -->
  <div class="col-md-12 ui-sortable">
      <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">User Form</h4>
                </div>
                <div class="panel-body">
                      <form enctype="multipart/form-data"  action="<?php echo $own_links;?>/save" method="post" class="form-horizontal" data-parsley-validate="true" name="form">
                        <h4>Main Info</h4>
                        <hr>
                        <input type="hidden" name="user_id" id="user_id" value="<?php echo isset($val->user_id)?$val->user_id:'';?>" />
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Fullname</label>
                                    <div class="col-md-9">
                                        <input type="text" id="user_name" name="user_fullname"  class="form-control" value="<?php echo isset($val->user_fullname)?$val->user_fullname:'';?>" data-parsley-required="true" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Email</label>
                                    <div class="col-md-9">
                                        <input type="text" name="user_email" class="form-control" value="<?php echo isset($val->user_email)?$val->user_email:'';?>"  data-parsley-required="true">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Address</label>
                                    <div class="col-md-9">
                                        <textarea name="user_alamat" class="form-control" data-parsley-required="true"><?php echo isset($val->user_alamat)?$val->user_alamat:'';?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Country</label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="user_negara" id="user_negara" data-parsley-required="true" onchange="comboCPC('#user_negara','#user_province','ajax/getProvince')">
                                            <option value=""> - select - </option>
                                            <?php
                                            if(count($country)>0){
                                                foreach($country as $r){
                                                    //debugCode($val);
                                                    $t = isset($val->user_negara)?$val->user_negara:"";
                                                    $s = ($r->country_id==$t)?"selected='selected'":'';
                                                    echo "<option value='".$r->country_id."' $s >".$r->country_title."</option>";
                                                }
                                            } 
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Province</label>
                                    <div class="col-md-9" id="frmProvince">
                                        <input type="text" name="user_province_text" id="user_province_text" class="form-control" value="<?php echo isset($val->user_province_other)?$val->user_province_other:'';?>">
                                        <select class="form-control" name="user_province" id="user_province" onchange="comboCPC('#user_province','#user_kota','ajax/getCity')">
                                            <option value=""> - select - </option>
                                            <?php
                                            if(count($province)>0){
                                                foreach($province as $r){
                                                    //debugCode($val);
                                                    $t = isset($val->user_province)?$val->user_province:"";
                                                    $s = ($r->province_id==$t)?"selected='selected'":'';
                                                    echo "<option value='".$r->province_id."' $s >".$r->province_title."</option>";
                                                }
                                            } 
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">City</label>
                                    <div class="col-md-9" id="frmCity">
                                        <input type="text" name="user_kota_text" id="user_kota_text" class="form-control" value="<?php echo isset($val->user_kota_other)?$val->user_kota_other:'';?>">
                                        <select class="form-control" name="user_kota" id="user_kota" >
                                            <option value=""> - select - </option>
                                            <?php
                                                if(count($city)>0){
                                                foreach($city as $r){
                                                    //debugCode($val);
                                                    $t = isset($val->user_kota)?$val->user_kota:"";
                                                    $s = ($r->city_id==$t)?"selected='selected'":'';
                                                    echo "<option value='".$r->city_id."' $s >".$r->city_title."</option>";
                                                }
                                            } 
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Postal Code</label>
                                    <div class="col-md-9">
                                        <input type="text" name="user_kode_pos" class="form-control" value="<?php echo isset($val->user_kode_pos)?$val->user_kode_pos:'';?>"  data-parsley-required="true" data-parsley-type="number" data-parsley-id="3354">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Religion</label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="user_agama" id="user_agama" data-parsley-required="true">
                                            <option value=""> - select - </option>
                                            <?php
                                                if(count($agama)>0){
                                                foreach($agama as $r){
                                                    //debugCode($val);
                                                    $t = isset($val->user_agama)?$val->user_agama:"";
                                                    $s = ($r->agama_id==$t)?"selected='selected'":'';
                                                    echo "<option value='".$r->agama_id."' $s >".$r->agama_title."</option>";
                                                }
                                            } 
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Company</label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="user_company" id="user_company" data-parsley-required="true"     onchange="comboCPC('#user_company','#user_departement','ajax/getDepartement')">
                                            <option value=""> - select - </option>
                                            <?php
                                                if(count($company)>0){
                                                foreach($company as $r){
                                                    //debugCode($val);
                                                    $t = isset($val->user_company)?$val->user_company:"";
                                                    $s = ($r->company_id==$t)?"selected='selected'":'';
                                                    echo "<option value='".$r->company_id."' $s >".$r->company_title."</option>";
                                                }
                                            } 
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Department</label>
                                    <div class="col-md-9">
                                        <select class="form-control" name="user_departement" id="user_departement" data-parsley-required="true">
                                            <option value=""> - select - </option>
                                            <?php
                                                if(count($departement)>0){
                                                foreach($departement as $r){
                                                    //debugCode($val);
                                                    $t = isset($val->user_departement)?$val->user_departement:"";
                                                    $s = ($r->departement_id==$t)?"selected='selected'":'';
                                                    echo "<option value='".$r->departement_id."' $s >".$r->departement_title."</option>";
                                                }
                                            } 
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Position</label>
                                    <div class="col-md-9">
                                        <input type="text" name="user_jabatan" class="form-control" value="<?php echo isset($val->user_jabatan)?$val->user_jabatan:'';?>"  data-parsley-required="true">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Date Of Birth</label>
                                    <div class="col-md-9">
                                        <input type="text" name="user_tgl_lahir" class="form-control" id="datepicker-default" placeholder="" value="<?php echo isset($val->user_tgl_lahir)?$user_tgl_lahir:$dateNow;?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Fb</label>
                                    <div class="col-md-9">
                                        <input type="text" name="user_fb" class="form-control" value="<?php echo isset($val->user_fb)?$val->user_fb:'';?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Twitter</label>
                                    <div class="col-md-9">
                                        <input type="text" name="user_twitter" class="form-control" value="<?php echo isset($val->user_twitter)?$val->user_twitter:'';?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4>Login</h4>
                        <hr>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Access User</label>
                            <div class="col-md-9">
                                <select class="form-control" name="user_group" id="user_group" data-parsley-required="true">
	                                <option value=""> - select - </option>
									<?php
									if(count($group)>0){
										foreach($group as $r){
											$t = isset($val->user_group)?$val->user_group:"";
											$s = ($r->ag_id==$t)?"selected='selected'":'';
											echo "<option value='".$r->ag_id."' $s >".$r->ag_group_name."</option>";
										}
									} 
									?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Username</label>
                            <div class="col-md-9">

								<?php if(!isset($val->user_name)){?>
									<input class="form-control" type="text" value="<?php echo isset($val->user_name)?$val->user_name:'';?>" name="user_name">
								<?php }else{ 
									echo "
										<div class='radio'>
											<label>
												<b>";
									echo isset($val->user_name)?$val->user_name:'';
									echo "		<b>
											</label>
										</div>";
									?>
									
										
									<input class="form-control" type="hidden" value="<?php echo isset($val->user_name)?$val->user_name:'';?>" name="user_name">
									<?php
								}?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Password</label>
                            <div class="col-md-9">
                                <input type="password" id="password" name="user_password" placeholder="Your password" class="form-control" required="" data-parsley-id="5957" <?php echo isset($val->user_password)?'':'class="validate[required,length[6,11]]"';?>>
                                <ul class="parsley-errors-list" id="parsley-id-5957"></ul>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Confirmation Password</label>
                            <div class="col-md-9 controls">
                                <input type="password" name="user_password2" id="password2" placeholder="Confirmed password" class="form-control" required="" data-parsley-id="5757" <?php echo isset($val->user_password)?'':'class="validate[required,confirm[user_password]]"';?> >
                                <ul class="parsley-errors-list" id="parsley-id-5757"></ul>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Foto</label>
                            <div class="col-md-9 controls">
                            	<span class="btn btn-success fileinput-button">
                                    <i class="fa fa-plus"></i>
                                    <span>Choose files...</span>
                                    <input type="file" name="user_photo" id="user_photo" value="<?php echo isset($val->configuration_id)?$val->configuration_id:""?>">
                                  </span>
                                  &nbsp;
                                  <?php if( isset($val->user_photo) && trim($val->user_photo)!="" ){?>
                                  <a href="<?php echo get_image(base_url()."assets/collections/photo/large/".$val->user_photo);?>" title="Image Photo" class="act_modal" rel="700|400">
                                      <img alt="" src="<?php echo get_image(base_url()."assets/collections/photo/".$val->user_photo);?>" style="height:30px;width:30px" class="img-polaroid">
                                  </a>
                                  <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Can See All Data</label>
                            <div class="col-md-9">
                                <label class="checkbox-inline">
                                	<input type="checkbox" id="is_show_all" <?php echo isset($val) && $val->is_show_all=="1"?'checked="checked"':'';?> name="is_show_all"> Yes
								</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Active</label>
                            <div class="col-md-9">
                                <label class="checkbox-inline">
                                	<input type="checkbox" id="user_status" <?php echo isset($val) && $val->user_status=="1"?'checked="checked"':'';?> name="user_status"> Yes
                                </label>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-sm btn-success">Save</button>
                                <a href="<?php echo $own_links;?>" class="btn btn-sm btn-default">Cancel</a>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->
    </div>
    <!-- end row -->
