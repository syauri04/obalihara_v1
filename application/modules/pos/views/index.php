

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.2/d3.min.js"></script>
<script src="<?= base_url()?>assets/def/plugins/nvd3/build/nv.d3.js"></script>
<script src="<?= base_url()?>assets/def/plugins/jquery-jvectormap/jquery-jvectormap.min.js"></script>
<script src="<?= base_url()?>assets/def/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js"></script>
<script src="<?= base_url()?>assets/def/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js"></script>
<script src="<?= base_url()?>assets/def/plugins/gritter/js/jquery.gritter.js"></script>
<script src="<?= base_url()?>assets/def/js/demo/dashboard-v2.min.js"></script> -->
<!-- ================== END PAGE LEVEL JS ================== -->

<div class="row">
	<div class="col-lg-5">
		<div class="panel panel-inverse" data-sortable-id="table-basic-1">
		<!-- begin panel-heading -->
		<!-- <div class="panel-heading">
		    <div class="panel-heading-btn">
		        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
		        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
		        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
		        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
		    </div>
		    <h4 class="panel-title"><a href="#"><button type="button" class="btn btn-lime btn-sm">Add</button></a></h4>
		</div> -->
		<!-- end panel-heading -->
		<!-- begin panel-body -->
		<div class="panel-body">
			<!-- begin table-responsive -->
			<table id="data-table-default" class="table table-striped ">
				<thead>
					<tr>
						<th>#</th>
						<th>Goods Name</th>
						<th>Price</th>
						<th>Qty</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>Nicky Almera</td>
						<td>2</td>
						<td>$50</td>
						<td><a href="#" class="btn btn-danger btn-icon btn-circle cancel" ><i class="fa fa-times "></i></a></td>
					</tr>
					<tr>
						<td>2</td>
						<td>Edmund Wong</td>
						<td>1</td>
						<td>$10</td>
						<td><a href="#" class="btn btn-danger btn-icon btn-circle cancel" ><i class="fa fa-times "></i></a></td>
					</tr>
					<tr>
						<td>3</td>
						<td>Harvinder Singh</td>
						<td>2</td>
						<td>$20</td>
						<td><a href="#" class="btn btn-danger btn-icon btn-circle cancel" ><i class="fa fa-times "></i></a></td>
					</tr>
					<tr>
						<td>4</td>
						<td>Harvinder Singh</td>
						<td>4</td>
						<td>$40</td>
						<td><a href="#" class="btn btn-danger btn-icon btn-circle cancel" ><i class="fa fa-times "></i></a></td>
					</tr>
					<tr>
						<td>5</td>
						<td>Harvinder Singh</td>
						<td>5</td>
						<td>$100</td>
						<td><a href="#" class="btn btn-danger btn-icon btn-circle cancel" ><i class="fa fa-times "></i></a></td>
					</tr>
					<tr>
						<td>5</td>
						<td>Harvinder Singh</td>
						<td>5</td>
						<td>$100</td>
						<td><a href="#" class="btn btn-danger btn-icon btn-circle cancel" ><i class="fa fa-times "></i></a></td>
					</tr>
					<tr>
						<td>5</td>
						<td>Harvinder Singh</td>
						<td>5</td>
						<td>$100</td>
						<td><a href="#" class="btn btn-danger btn-icon btn-circle cancel" ><i class="fa fa-times "></i></a></td>
					</tr>
					<tr>
						<td colspan="3" align="right"><h5>Discount</h5></td>
						<td colspan="2"><h5>- $74</h5></td>
					</tr>
					<tr>
						<td colspan="3" align="right"><h5>Subtotal</h5></td>
						<td colspan="2"><h5>$420</h5></td>
					</tr>
					<tr>
						<td colspan="3" align="right"><h5>Tax</h5></td>
						<td colspan="2"><h5>$42</h5></td>
					</tr>
					<tr>
						<td colspan="3" align="right"><h5>Total</h5></td>
						<td colspan="2"><h5>$462</h5></td>
					</tr>
				</tbody>
			</table>
			<!-- end table-responsive -->
		</div>
		<!-- end panel-body -->
		</div>
	</div>
	<div class="col-lg-7">
		<div class="panel panel-inverse" data-sortable-id="table-basic-1">
		
		<div class="panel-body">
			<div class="row row-space-10">
				<div class="col-md-4">
					<div class="card card-inverse">
						<img src="<?= loadImage('goods','thumb','coffeee.jpg')?>" alt class="card-img">
						<div class="card-img-overlay">
							<h4 class="card-title text-aqua-darker">Cappucino</h4>
						</div>
					</div>
					<div class="card card-inverse">
						<img src="<?= loadImage('goods','thumb','non-coffee.jpg')?>" alt class="card-img">
						<div class="card-img-overlay">
							<h4 class="card-title text-aqua-darker">Vanilla Latte</h4>
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="card card-inverse">
						<img src="<?= loadImage('goods','thumb','coffeee.jpg')?>" alt class="card-img">
						<div class="card-img-overlay">
							<h4 class="card-title">Cappucino</h4>
						</div>
					</div>
					<div class="card card-inverse">
						<img src="<?= loadImage('goods','thumb','coffeee.jpg')?>" alt class="card-img">
						<div class="card-img-overlay">
							<h4 class="card-title">Cappucino</h4>
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="card card-inverse">
						<img src="<?= loadImage('goods','thumb','coffeee.jpg')?>" alt class="card-img">
						<div class="card-img-overlay">
							<h4 class="card-title">Cappucino</h4>
						</div>
					</div>
					<div class="card card-inverse">
						<img src="<?= loadImage('goods','thumb','coffeee.jpg')?>" alt class="card-img">
						<div class="card-img-overlay">
							<h4 class="card-title">Cappucino</h4>
						</div>
					</div>
				</div>
				
			</div>
			
		</div>
		<!-- end panel-body -->
		</div>
	</div>
</div>

<link href="<?= base_url()?>assets/def/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="<?= base_url()?>assets/def/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?= base_url()?>assets/def/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?= base_url()?>assets/def/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url()?>assets/def/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="<?= base_url()?>assets/cust/js/pos/table-manage-pos.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script type="text/javascript">
	TableManageDefault.init();
</script>