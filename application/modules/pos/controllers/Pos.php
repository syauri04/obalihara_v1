<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH."libraries/AdminController.php");

class Pos extends AdminController {

	
	function __construct()    
	{
		parent::__construct();    
		$this->_set_action();
		$this->_set_action(array("edit","delete"),"ITEM");
		$this->_set_title( 'Goods List ' );

	}
	
	public function index()
	{
		// $data = [
		// 	'd' => $this->db->get('cp_goods')->result(),
		// ];

		$this->_v('index');
	}
}
