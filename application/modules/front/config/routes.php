<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Routes Dummy for sample html
// $route['category'] 					= 'front/dummy/category';
// $route['detail'] 					= 'front/dummy/detail';
// $route['cart'] 						= 'front/dummy/cart';
// $route['whislist'] 					= 'front/dummy/whislist';

$route['login'] 					= 'front/dummy/login';
// $route['vendor'] 					= 'front/dummy/vendor';
// $route['vendor/order_detail'] 		= 'front/dummy/vorder_detail';
// $route['vendor/add_product'] 		= 'front/dummy/vadd_product';
// $route['vendor/add_product_photo'] 	= 'front/dummy/vadd_product_photo';

// $route['buyer'] 					= 'front/dummy/buyer';
// $route['buyer/personal'] 			= 'front/dummy/bpersonal';
// $route['buyer/shipping'] 			= 'front/dummy/bshipping';
// $route['buyer/order'] 				= 'front/dummy/border';
// $route['buyer/change_password'] 	= 'front/dummy/bchange_password';

$route['checkout'] 					= 'front/home/checkout';

$route['home'] 		= 'front/home/index';
$route['wishlist'] 	= 'front/home/wishlist';

$route['login'] 	= 'front/signin/index';
$route['reset'] 	= 'front/signin/reset';
$route['login/forgot'] 	= 'front/signin/forgot';
$route['login-google'] 	= 'front/signin/loginG';
$route['logout'] 	= 'front/signin/logout';
$route['logout-google'] 	= 'front/signin/logoutG';

$route['auth'] 		= 'front/auth/index';
$route['auth/(.*)'] = 'front/auth/out';

$route['cart'] 		= 'front/home/cart';
$route['cart/(.*)'] = 'front/home/cart/$1';

$route['vendors'] 				= 'front/vendor/index';
$route['vendor/vd'] 			= 'front/vendor/index';
$route['vendor/addProduct'] 	= 'front/vendor/addProduct';
$route['vendor/ep'] 			= 'front/vendor/editProduct';
$route['vendor/pp']				= 'front/vendor/productPhoto';
$route['vendor/sm']				= 'front/vendor/shippingMetode';
$route['vendor/deleteProduct']	= 'front/vendor/deleteProduct';

$route['buyer'] 				= 'front/buyer/index';
$route['buyer/personal'] 		= 'front/buyer/personal';
$route['buyer/shipping'] 		= 'front/buyer/shippingAddress';
$route['buyer/change_password'] = 'front/buyer/changePassword';
$route['buyer/order'] 			= 'front/buyer/order';
$route['buyer/payment'] 		= 'front/buyer/paymentPage';
// $route['buyer/addProduct'] 	= 'front/buyer/addProduct';
// $route['buyer/ep'] 			= 'front/buyer/editProduct';

$route['vendorDetail/(.*)'] = 'front/home/vendorDetail/$1';
$route['category/(.*)'] 	= 'front/home/category/$1';
$route['detail/(.*)'] 		= 'front/home/detail/$1';

$route['privacy'] 		= 'front/home/privacy';
$route['terms'] 		= 'front/home/terms';
$route['about'] 		= 'front/home/about';
$route['service'] 		= 'front/home/service';
$route['faq'] 		= 'front/home/faq';
$route['privacy/(.*)'] 		= 'front/home/privacy/$1';
// $route['dashboard/performance/(.*)'] = 'dashboard/Performance/$1';