<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH."libraries/FrontController.php");

class Vendor extends FrontController {

	
	function __construct()    
	{
		parent::__construct();
		$this->load->model('vendor_model','VM');
		$this->load->model('home_model','HM');
		$this->folder_view = 'vendor';
		$this->menu = $this->HM->getCatPM();

		$this->upload_resize  = array(
			array('name'	=> 'thumb','width'	=> 100, 'height'	=> 80, 'quality'	=> '75%'),
			array('name'	=> 'small','width'	=> 160, 'height'	=> 200, 'quality'	=> '85%'),
			array('name'	=> 'large','width'	=> 330, 'height'	=> 400, 'quality'	=> '100%')
		);

		$this->upload_path="./assets/img/goods/";
	}
	
	public function index()
	{

		$idV = getOutletByUser(codeDecrypt($_GET['id']),'id');
		$idU = codeDecrypt($_GET['id']);

		$data = [
  			'dashboard' 	=> '',
  			'product'   	=> $this->VM->getProduct($idU),
  			'order'			=> $this->VM->getOrders($idV),
  			'orderIn'		=> $this->VM->getOrders($idV,'orderIn'),
  			'orderShipped'	=> $this->VM->getOrders($idV,'orderShipped'),
  			'orderRecent'	=> $this->VM->getOrders($idV,'orderRecent'),
  			'salesReport'	=> $this->VM->getSales($idV),
  			'setting'		=> $this->VM->getSetting($idU),
  			'province'		=> $this->_raongProvince('p')['rajaongkir']['results'],
  			'userId'		=> $idV,
  			'earning'		=> $this->VM->getEarning($idV),
  			//'city'		=> $this->VM->getCity($data['setting']->provinsi_id),
  		];
  		// d($data);
		$this->_v($this->folder_view.'/vendor',$data);
	}

	// DASHBOARD CONFIG
	function processByVendor(){
		
		$a = $this->db->update('cp_cart',['status_invoice'=>3],['id'=>$_POST['id']]);

		if($a){
			echo json_encode(true);
		} else {
			echo json_encode(false);
		}
	}

	function courierDelivery(){
		
		$data = [
			'tanggal_delivery' 	=> date('Y-m-d H:i:s'),
			'status_invoice'	=> 4,
			'resi'				=> $_POST['resi']
		];

		$a = $this->db->update('cp_cart',$data,['id'=>$_POST['id']]);

		if($a){
			echo json_encode(true);
		} else {
			echo json_encode(false);
		}
	}

	// ADD PRODUCT CONFIG

	function addProduct(){

		$activate = getOutletByUser($this->jCfg['client']['id'],'activate');

		if($activate == ''){
			redirect(base_url()."vendors?id=".urlencode(codeEncrypt($this->jCfg['client']['id']))."&&msg=".urlencode('Error Account Not Activated , Check your email to activate account')."#products");
		}

		$data = [
			'cat' 	=> $this->VM->getCat(),
			'title' 	=> "Add New Product",
		];

		$this->_v($this->folder_view.'/addProduct',$data);

	}

	function deleteProduct(){

		$idG = $_POST['id'];

		$cek = $this->db->get_where('cp_cart',['barang_id' => $idG ])->result();

		$a = [
			'msg' 	=> 'Delete data Failed , Barang Sudah ada penjualan',
			'url'	=> base_url()."vendors?id=".urlencode(codeEncrypt($this->jCfg['client']['id']))."#products"
		];

		if(count($cek) > 0){
			$this->db->delete('cp_barang',['id'=>$idG]);
			$this->db->delete('cp_barang_detail',['barang_id'=>$idG]);
			$this->db->delete('cp_barang_varian',['barang_id'=>$idG]);

			$a = [
				'msg' 	=> 'Delete data Success',
				'url'	=> base_url()."vendors?id=".urlencode(codeEncrypt($this->jCfg['client']['id']))."#products"
			];
		}		
		
		echo json_encode($a);

		// redirect($this->own_link."vendor?id=".urlencode(codeEncrypt($this->jCfg['client']['id']))."&msg=".urldecode('Delete data Success')."&type_msg=success#products");

	}

	function productPhoto(){

		$idG = codeDecrypt($_GET['id']);

		$data = [
			// 'img' 		=> $this->db->get_where('cp_barang_gambar',['barang_id' => $idG])->result(),
			'barang_id' => $idG,
		];
		// d($data);
		$this->_v($this->folder_view.'/productPhoto',$data);

	}

	function getPhoto($id){
		
		$img = $this->db->get_where('cp_barang_gambar',['barang_id' => $id])->result();

		echo json_encode($img);

	}

	public function deletePhoto(){

		$msg = '';
		$del = $this->db->delete('cp_barang_gambar',['id' => $_POST['id']]);

		if($del){
			$msg = 'd';
		} else {
			$msg = 'ud';
		}

		echo json_encode($msg);

	}

	function starsPhoto(){

		$del = $this->db->update('cp_barang_gambar',['stars' => 1],['id' => $_POST['id']]);

		if($del){
			$msg = 'd';
		} else {
			$msg = 'ud';
		}

		echo json_encode($msg);
	}

	function productPhotoUpload(){
		// d($_FILES);

		// $this->db->delete('cp_barang_gambar',['barang_id' => $_POST['barang_id']]);

		$data = [
			'barang_id' 	=> $_POST['barang_id'],
		];

		$this->db->insert('cp_barang_gambar',$data);

		$id=$this->db->insert_id();

		$this->DATA->table = 'cp_barang_gambar';
		$this->_uploaded(
		array(
			'id'		=> $id ,
			'input'		=> array_keys($_FILES)[0],
			'param'		=> array(
							'field' => 'gambar_name', 
							'par'	=> array('id' => $id)
						)
		)); 
	}

	function editProduct(){
		
		$this->DATA->table = 'cp_barang';
		$id=codeDecrypt($_GET['id']);

		if(trim($id)!=''){
			$this->data_form = $this->DATA->data_id(array('id'	=> $id));

			$data = [
				'cat' 	=> $this->VM->getCat(),
				'det'	=> $this->db->get_where('cp_barang_detail',['barang_id' => $id])->row(),
				'var'	=> $this->db->get_where('cp_barang_varian',['barang_id' => $id])->result(),
				'title' 	=> "Edit Product",
			];

			$this->_v($this->folder_view."/addProduct",$data);
		}else{
			redirect($this->own_link);
		}
	}

	function catChild(){
		
		$cat = $this->VM->getCat($_POST['id']);
		echo json_encode($cat);
	}

	function getWarna(){

		$wt = '';
		$ut = '';
		$w = $this->VM->getOpt(1);
		$u = isset($_POST['t'])?$this->VM->getOpt(2,$_POST['t']):'';

		$war = isset($_POST['w'])?explode(',',$_POST['w']):'';
		$uks = isset($_POST['u'])?explode(',',$_POST['u']):'';
		// d($u);
		$wr = 0;
		$wt .= '<div class="column">';
		foreach ($w as $k => $v) {
			$wr++;
			$se = isset($_POST['w']) && $war[$k] == $v->id ? 'checked':'';
			$val = isset($_POST['w']) && $war[$k] == $v->id ? $v->id:'0';
			
			$wt .= '<div>';
			$wt .= '<input type="checkbox" class="wp" value="'.$v->id.'" '.$se.'> '.$v->name;
			$wt .= '<input type="hidden" name="warnaPicker[]" class="wph" value="'.$val.'" > ';
			$wt .= '</div>';
			$wt .= ($wr % 3 == 0) ? '</div><div class="column">' : '';
		}
		$wt .= '</div>';
		
		if($u != ''){
			$ur = 0;
			$ut .= '<div class="column">';
			foreach ($u as $k => $v) {
				// $ur++;
				$se = isset($_POST['u']) && isset($uks[$k]) && $uks[$k] == $v->id ? 'checked':'';
				$val = isset($_POST['u']) && isset($uks[$k]) && $uks[$k] == $v->id ? $v->id:'0';

				$ut .= '<div>';
				$ut .= '<input type="checkbox" class="up" value="'.$v->id.'" '.$se.'> '.$v->name;
				$ut .= '<input type="hidden" name="ukuranPicker[]" class="uph" value="'.$val.'" >';
				$ut .= '</div>';
				$ut .= ($ur % 3 == 0) ? '</div><div class="column">' : '';
			}
			$ut .= '</div>';
		}

		$t = [
			'warna' => $wt,
			'ukuran'=> $ut
		];

		echo json_encode($t);
	}

	function saveGood(){

		$is_stok = 0;
		if($_POST['qty'] > 0){
			$is_stok = 1;
		}

		$this->DATA->table = 'cp_barang';

		$data = [
			'nama_barang' 		=> $_POST['nama_barang'],
			'outlet_id' 		=> $_POST['outlet_id'],
			'deskripsi' 		=> $_POST['nama_barang'],
			'kategori_barang' 	=> $_POST['catId'],
			'user_id' 			=> $_POST['user_id'],
			'product_strength'	=> $_POST['product_strength'],
			'is_new'  			=> 1,
			'is_stock'			=> $is_stok,
			'date_publish'		=> date('Y-m-d H:i:s'),	
		];

		$a = $this->_save_master( $data,['id' => ($_POST['id'])],($_POST['id']));

		$dataDetail = [
			'barang_id' 		=> $a['id'],
			'harga_Satuan' 		=> str_replace(".", "", $_POST['harga_satuan']),
			'discount'			=> ($_POST['discount']),
			'stok' 				=> ($_POST['qty']),
			'perkiraan_berat'	=> ($_POST['perkiraan_berat']),
		];
		$this->DATA->table = 'cp_barang_detail';
		$this->_save_master( $dataDetail,['barang_id' => ($a['id'])],($a['id']));


		$this->db->delete('cp_barang_varian',['barang_id'=>$a['id']]);
		$dw = implode(',',$_POST['warnaPicker']);
		$du = isset($_POST['ukuranPicker']) ? implode(',',$_POST['ukuranPicker']) : [] ;		

		$dataWarna = [
			'barang_id' 	=> $a['id'],
			'jenis_varian'	=> 'warna',
			'varian_barang'	=> $dw,
		];

		$this->db->insert('cp_barang_varian',$dataWarna);

		if(!empty($du)){
			$dataUkuran = [
				'barang_id' 	=> $a['id'],
				'jenis_varian'	=> 'ukuran',
				'varian_barang'	=> $du,
			];

			$this->db->insert('cp_barang_varian',$dataUkuran);
		}

		redirect($this->own_link."vendor/pp?id=".urlencode(codeEncrypt($a['id']))."&&msg=".urldecode('Save data Setting Success')."&type_msg=success#products");
	}

	function saveVendorPhoto(){

		$this->DATA->table = 'cp_vendor';
		$this->upload_path="./assets/img/vendor/";
		$this->_uploaded(
		array(
			'id'		=> $_POST['id'] ,
			'input'		=> array_keys($_FILES)[0],
			'param'		=> array(
							'field' => 'photo', 
							'par'	=> array('id' => $_POST['id'])
						)
		)); 
	}

	// SETTING PERSONAL CONFIG

	function shippingMetode(){

		$idV = codeDecrypt($_GET['id']);

		$data = [
			'userId' 	=> $idV,
			'sm'		=> $this->db->select('shipping_metode')->where(['user_id' => $idV])->get('cp_vendor')->row()->shipping_metode,
		];
		// d($data);
		$this->_v($this->folder_view."/shippingMetode",$data);

	}

	function saveShippingMetode(){

		$im = implode(',',$_POST['checksp']);
		$a = $this->db->update('cp_vendor',['shipping_metode' => $im],['user_id' => $_POST['userId']]);


		redirect($this->own_link."vendors?id=".urlencode(codeEncrypt($_POST['userId']))."&&msg=".urldecode('Save data Success')."&type_msg=success#setting");

	}

	function saveSetting(){
		
		foreach ($_POST as $k => $v) {
			$a[$k] = $_POST[$k];
		}
		unset($a['user_id']);
		$a['user_id'] = codeDecrypt($_POST['user_id']);
		$a['register_date'] = date('Y-m-d');

		$this->DATA->table = 'cp_vendor';
		$s = $this->_save_master( 
			$a,
			array(
				'user_id' => dbClean($a['user_id'])
			),
			dbClean($a['user_id'])			
		);

		$cek = getOutletByUser($s['id'],'shipping_metode');

		if($s){

			if($cek != '' && $cek != null){
				redirect($this->own_link."vendor/vd?id=".urlencode(codeEncrypt($a['user_id']))."&&msg=".urldecode('Save data Setting Success')."&type_msg=success#setting");
			} else {
				redirect($this->own_link."vendor/sm?id=".urlencode(codeEncrypt($a['user_id']))."&&msg=".urldecode('Save data Setting Success')."&type_msg=success#setting");
			}

		} else {
			redirect($this->own_link."vendor/vd?id=".urlencode(codeEncrypt($a['user_id']))."&&msg=".urldecode('Save data Setting Failed')."&type_msg=danger#setting");
		}
		
	}

	function getCity(){
		$tmp    = '';
        $data   = $this->_raongProvince('c',$_POST['id'])['rajaongkir']['results'];

        if(!empty($data)) {
            $tmp .= "<option value=''>- Pilih Kota -</option>"; 
			foreach($data as $i){ 
				$se = (isset($_POST['k'])&&$i['city_id'] == $_POST['k'])?'selected':'';
                $tmp .= '<option value="'.$i['city_id'].'" '.$se.'>'.$i['type'].' '.$i['city_name'].'</option>';;
            }
        } else {
            $tmp .= "<option value=''>- Pilih Kota -</option>"; 
        }
        die($tmp);
	}

	function getSubdistrict(){

		$tmp    = '';
        $data   = raongKec($_POST['id'],'c');

        if(!empty($data)) {
            $tmp .= "<option value=''>- Pilih Kecamatan -</option>"; 
			foreach($data as $i){ 
				$se = (isset($_POST['k'])&&$i['subdistrict_id'] == $_POST['k'])?'selected':'';
                $tmp .= '<option value="'.$i['subdistrict_id'].'" '.$se.'>'.$i['subdistrict_name'].'</option>';;
            }
        } else {
            $tmp .= "<option value=''>- Pilih Kecamatan -</option>"; 
        }
        die($tmp);
	}

	function getImage($id){
		$tmp    = '';
        $data   = $this->db->get_where('cp_barang_gambar',['barang_id' => $id])->result();
        $x = [];

        foreach ($data as $k => $v) {

        	$c = str_replace(' ','',FCPATH.'assets\img\goods\thumb\ '.$v->gambar_name);

        	$p['name'] = $v->gambar_name;
        	$p['size'] = filesize($c);
        	$x[] = $p;
        }

        echo json_encode($x);
	}

	function getBuyer(){
		$a = $this->db->get_where('cp_cart',['id'=>$_POST['id']])->row();
		$data = [
			'nama_lengkap' => getUser($a->user_id,'nama_lengkap'),
			'email' => getUser($a->user_id,'email'),
			'telp' => getUser($a->user_id,'telp'),
		];

		echo json_encode($data);

	}

	function createDisbursement(){

		$exid = disburse_code();

		// API key Production xnd_production_NGKe9mpWtDwQLrCPCPwJ0JG1iP887fs5p6kuaZzFtIp8xplT6F8YdWlOTPFL8O
		// API key development xnd_development_IhjXLDUmNHbQco2klFHNLGNDO30o7Av1HqiCY4Hhhffa9UzfSfM3aGg52IPZTQa2

		require 'vendor/autoload.php'; 

	    $options['secret_api_key'] = 'xnd_development_IhjXLDUmNHbQco2klFHNLGNDO30o7Av1HqiCY4Hhhffa9UzfSfM3aGg52IPZTQa2'; 

	    $xenditPHPClient = new XenditClient\XenditPHPClient($options); 

	    $external_id = $exid;
	    $amount = $_POST['amount'];
	    $bank_code = $_POST['bank_code'];
	    $account_holder_name = $_POST['account_holder_name'];
	    $account_number = $_POST['account_number'];

	    $response = $xenditPHPClient->createDisbursement($external_id, $amount, $bank_code, $account_holder_name, $account_number);

	    $res = [];

	    foreach ($response as $k => $v) {
	    	$res[$k] = $v;
	    }

	    $res['user_id'] = $_POST['user_id'];
	    $res['vendor_id'] = $_POST['vendor_id'];
	    $res['fee'] = $_POST['fee'];

	    $ins = $this->db->insert('cp_vendor_disburse',$res);
	  	
	  	if($ins){
	  		$return = [
	  			'msg' 		=> 'Success',
	  			'status'	=> 200,
	  			'data'		=> $res,
	  			'text'		=> 'Success , Status pending.',
	  		];
	  		echo json_encode($return);
	  	} else {
	  		$return = [
	  			'msg' 		=> 'Error',
	  			'status'	=> 500,
	  			'text'		=> 'Something was wrong debug the code',
	  		];
	  		echo json_encode($return);
	  	}

	}

	function disburse_callback(){
		if ($_SERVER["REQUEST_METHOD"] === "POST") {
		    $data = file_get_contents("php://input");
	        
	        $d = json_decode($data);
	        $callback = [];
	        $disbursement_id = $d->id;
	        $external_id = $d->external_id;
	        
	        foreach ($d as $k => $v) {
	        	$callback[$k] = $v;
	        }

	        $res = [
	        	'status' => $d->status,
	        ];

	        $this->db->insert('disburse_callback',$callback);
	        $this->db->update('cp_vendor_disburse',['status' => $res['status']],[ 'external_id' => $external_id ]);

	       	echo json_encode('true');

		} else {
		    $a = Array
				(
				    'id' => '579c8d61f23fa4ca35e52da4',
				    'user_id' => '5781d19b2e2385880609791c',
				    'external_id' => 'invoice_123124123',
				    'amount' => 1500,
				    'account_holder_name' => 'BUSINESS',
				    'bank_code' => 'BCA',
				    'transaction_id' => '57ec8b7e906aa2606ecf8ffc',
				    'transaction_sequence' => '1799',
				    'disbursement_description' => 'This is a description',
				    'disbursement_id' => '57ec8b8130d2d0243f438e11',
				    'failure_code' => 'INVALID_DESTINATION',
				    'is_instant' => 'false',
				    'status' => 'FAILED',
				    'updated' => '2016-10-10T08:15:03.404Z',
				    'created' => '2016-10-10T08:15:03.404Z',
				);

	        $this->db->insert('disburse_callback',$a);
		}
	}

	public function _raongProvince($t,$id=''){

		if($t == 'p'){
			$url = "https://pro.rajaongkir.com/api/province";
		} else if($t == 'c') {
			if($id != ''){
				$url = "https://pro.rajaongkir.com/api/city?province=".$id;
			} else {
				$url = "https://pro.rajaongkir.com/api/city";
			}
		}


		$curl = curl_init();
 
		curl_setopt_array($curl, array(
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
			"key: f5e8ac7da7095787b8c8892cd644e7f1"
		),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		// d(json_decode($response,true));
		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  return json_decode($response,true);
		}

	}
}
