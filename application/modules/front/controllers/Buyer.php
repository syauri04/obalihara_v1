<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH."libraries/FrontController.php");

class Buyer extends FrontController {

	
	function __construct()    
	{
		parent::__construct();
		$this->load->model('buyer_model','BM');
		$this->folder_view = 'buyer';

		$this->upload_resize  = array(
			array('name'	=> 'thumb','width'	=> 100, 'height'	=> 100, 'quality'	=> '75%'),
			array('name'	=> 'small','width'	=> 350, 'height'	=> 185, 'quality'	=> '85%'),
			array('name'	=> 'large','width'	=> 730, 'height'	=> 395, 'quality'	=> '100%')
		);

		$this->upload_path="./assets/img/buyer/";
	}
	
	public function index()
	{

		$idV = codeDecrypt($_GET['id']);
		
		$p = $this->BM->getBuyer($idV);
		
		$data = [
  			'dashboard' => '',
  			'personal'	=> $p,
  			'address'	=> $this->BM->getBuyerAdr($p->id),
  			'order'		=> $this->BM->getBuyerOrder($idV)
  		];

		$this->_v($this->folder_view.'/buyer',$data);
		
	}

	public function personal(){

		$idV = codeDecrypt($_GET['id']);
		$p = $this->BM->getBuyer($idV);

		$data = [
			'personal'  => $p,
			'province'	=> $this->BM->getProvince(),
			'address'	=> $this->BM->getBuyerAdr($p->id),
		];

		$this->_v($this->folder_view.'/bpersonal',$data);
	}

	function shippingAddress(){

		$idV = codeDecrypt($_GET['id']);
		$p = $this->BM->getBuyer($idV);

		$data = [
			'province'	=> $this->_raongProvince('p'),
			'address'	=> $this->BM->getBuyerAdr($p->user_id),
			'buyerId'	=> $idV,
		];
		// d($data);
		$this->_v($this->folder_view.'/bshipping',$data);

	}

	function order(){

		$idV = codeDecrypt($_GET['id']);
		$status = '';
		if(isset($_GET['status'])){
			$status = $_GET['status'];
		}

		$data = [
			'recentOrder' 	=> $this->BM->getBuyerOrder($idV,$status),
			'order' 		=> $this->BM->getBuyerOrder($idV,$status)
		];

		$this->_v($this->folder_view.'/border',$data);

	}

	function deleteAddress(){

		$a = $this->db->delete('cp_buyer_address',['id' => $_POST['id']]);

		$s = 'Delete Address Error';
		if($a){
			$s = 'Delete Address Success';
		}

		echo json_encode($s);

	}

	function changePassword(){

		$idV = codeDecrypt($_GET['id']);

		$data = [
			'user'	=> $this->BM->getUser($idV),
			'userID'=> $_GET['id'],
		];

		$this->_v($this->folder_view.'/bchange_password',$data);
	}

	function saveChangepassword(){
		
		$old 	= md5($_POST['oldPassword']);
		$new 	= md5($_POST['newPassword']);
		$con 	= md5($_POST['conPassword']);
		$userID = $this->BM->getUser(codeDecrypt($_POST['userID']),$old,true);

		if(isset($userID) && !empty($userID)){

			if($new == $con){

				$data = [
					'password' 			=> $new,
					'changePasswordDate'=> date('Y-m-d H:i:s')
				];

				$a = $this->db->update('cp_user',$data,['id' => codeDecrypt($_POST['userID'])]);

				if($a){
					redirect($this->own_link."buyer/change_password?id=".urlencode(codeEncrypt($this->jCfg['client']['id']))."&&msg=".urldecode('Change Password Success')."&type_msg=success");
				} else {
					redirect($this->own_link."buyer/change_password?id=".urlencode(codeEncrypt($this->jCfg['client']['id']))."&&msg=".urldecode('Save data Error')."&type_msg=error");
				}

			} else {

				redirect($this->own_link."buyer/change_password?id=".urlencode(codeEncrypt($this->jCfg['client']['id']))."&&msg=".urldecode('Change Password Error , password Not Match!!')."&type_msg=error");

			}

		} else {

			redirect($this->own_link."buyer/change_password?id=".urlencode(codeEncrypt($this->jCfg['client']['id']))."&&msg=".urldecode('Change Password Error , User Not Found!!')."&type_msg=error");

		}

	}

	function saveAddress(){

		foreach ($_POST['id'] as $k => $v) {
			$data = [
				'buyer_id'		=> $_POST['buyerID'],
				'title' 		=> $_POST['title'][$k],
				'provinsi_id' 	=> $_POST['provinsi_id'][$k],
				'kota_id' 		=> $_POST['kota_id'][$k],
				'kecamatan_id'	=> $_POST['kecamatan_id'][$k],
				'post_code' 	=> $_POST['post_code'][$k],
				'alamat' 		=> $_POST['alamat'][$k],
			];

			$this->DATA->table = 'cp_buyer_address';
			$a = $this->_save_master( $data,['id' => dbClean($_POST['id'][$k])],dbClean($_POST['id'][$k]));
		}

		redirect($this->own_link."buyer/shipping?id=".urlencode(codeEncrypt($this->jCfg['client']['id']))."&&msg=".urldecode('Save data Success')."&type_msg=success");
	}

	function savebPersonal(){
		$data = [
			'user_id' 		=> $_POST['user_id'],
			'nama_lengkap'	=> $_POST['nama_lengkap'],
			'email'			=> $_POST['email'],
			'telp'			=> $_POST['telp'],
			'provinsi_id'	=> $_POST['provinsi_id'],
			'kota_id'		=> $_POST['kota_id'],
			'kecamatan_id'	=> $_POST['kecamatan_id'],
			'post_code'		=> $_POST['post_code'],
			'alamat'		=> $_POST['alamat'],
			'country'		=> 1,//indonesia
 		];

 		$this->DATA->table = 'cp_buyer';
 		$a = $this->_save_master( $data,['id' => dbClean($_POST['id'])],dbClean($_POST['id']));

 		if($a){
 			redirect($this->own_link."buyer/personal?id=".urlencode(codeEncrypt($this->jCfg['client']['id']))."&&msg=".urldecode('Save data Success')."&type_msg=success#products");
 		} else {
 			d('ERROR');
 		}
 		
	}

	function savebPersonalPhoto(){

		$this->DATA->table = 'cp_buyer';
		$a = $this->_uploaded(
		array(
			'id'		=> $_POST['id'] ,
			'input'		=> array_keys($_FILES)[0],
			'param'		=> array(
							'field' => 'photo', 
							'par'	=> array('id' => $_POST['id'])
						)
		)); 

		if($a){
			$this->jCfg['client']['photo'] = $a;
			$this->_releaseSession();
		}

	}

	function getCity(){
		$tmp    = '';
        $data   = $this->_raongProvince('c',$_POST['id']);
        
        if(!empty($data)) {
            $tmp .= "<option value=''>- Pilih Kota -</option>"; 
			foreach($data as $i){ 

				$se = (isset($_POST['k'])&&$i['city_id'] == $_POST['k'])?'selected':'';
                $tmp .= '<option value="'.$i['city_id'].'" '.$se.'>'.$i['type'].' '.$i['city_name'].'</option>';
            }
        } else {
            $tmp .= "<option value=''>- Pilih Kota -</option>"; 
        }
        die($tmp);
	}

	function getSubdistrict(){

		$tmp    = '';
        $data   = raongKec($_POST['id'],'c');

        if(!empty($data)) {
            $tmp .= "<option value=''>- Pilih Kecamatan -</option>"; 
			foreach($data as $i){ 
				$se = (isset($_POST['k'])&&$i['subdistrict_id'] == $_POST['k'])?'selected':'';
                $tmp .= '<option value="'.$i['subdistrict_id'].'" '.$se.'>'.$i['subdistrict_name'].'</option>';;
            }
        } else {
            $tmp .= "<option value=''>- Pilih Kecamatan -</option>"; 
        }
        die($tmp);
	}

	function terimaBarang(){

		$a = $this->db->update('cp_cart',['status_invoice'=>5,'tanggal_shipped'=>date('Y-m-d H:i:s')],['id'=>$_POST['cartid']]);
		$b = $this->db->update('cp_checkout_detail',['status_invoice'=>5],['cart_id'=>$_POST['cartid']]);

		if($a){
			echo json_encode(true);
		} else {
			echo json_encode(false);
		}

	}

	function paymentPage(){

		$userid = codeDecrypt($_GET['id']);
		$code = $_GET['code'];

		$data['url'] = $this->db->get_where('cp_checkout',['user_id' => $userid , 'invoice_code' => $code])->row()->url;

		$this->_v($this->folder_view.'/paymentPage',$data);

	}

	public function _raongProvince($t,$id=''){

		if($t == 'p'){
			$url = "http://api.rajaongkir.com/starter/province";
		} else if($t == 'c') {
			if($id != ''){
				$url = "https://api.rajaongkir.com/starter/city?province=".$id;
			} else {
				$url = "https://api.rajaongkir.com/starter/city";
			}
		}


		$curl = curl_init();
 
		curl_setopt_array($curl, array(
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
			"key: f5e8ac7da7095787b8c8892cd644e7f1"
		),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		$status = json_decode($response,true);

		$a = '';

		if($status['rajaongkir']['status']['code'] == 400){
			$a = $status['rajaongkir']['status']['description'];
		} else {
			$a = json_decode($response,true)['rajaongkir']['results'];
		}

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  return $a;
		}

	}

	function cekResi(){
		
		$courier = strtolower($_POST['courier']);
		$receipt = $_POST['receipt'];

		$curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://pro.rajaongkir.com/api/waybill",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => "waybill=".$receipt."&courier=".$courier,
		CURLOPT_HTTPHEADER => array(
			"key: f5e8ac7da7095787b8c8892cd644e7f1"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);


		curl_close($curl);

		$res = json_encode($response);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			echo json_decode($res);
		}
	}
	
}
