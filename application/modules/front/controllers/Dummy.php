<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH."libraries/FrontController.php");

class Dummy extends FrontController {

	
	function __construct()    
	{
		parent::__construct();
		$this->load->model('home_model','HM');
		$this->folder_view = 'dummy';
		$this->company = $this->HM->companySetting();
	}
	
	public function index()
	{

		$data = [
			'catOM'	  => $this->HM->getCatOM(),
		];

		$this->_v($this->folder_view.'/home',$data);
	}

	function category(){

		// $data = [
		// 	'titleCat'  	=> str_replace("-"," ",$_GET['ct']),
		// 	'bestSeller'	=> $this->HM->getBestSeller($_GET['cid']),
		// 	'getAllProduk'	=> $this->HM->getAllProduk($_GET['cid']),
		// ];

		$data ="";
		
		$this->_v($this->folder_view.'/productCategory',$data);
	}

	function detail(){


		// $idProduk = codeDecrypt($_GET['id']);

		// $data = [
		// 	'titleProd'  => str_replace("-"," ",$_GET['title']),
		// 	'produk'	 => $this->HM->getDetailProduk($idProduk),
		// ];
		$data="";
		// d($data);
		$this->_v($this->folder_view.'/productDetail',$data);
	}
	function cart(){

		$data="";
		// d($data);
		$this->_v($this->folder_view.'/cart',$data);
	}
	function whislist(){

		$data="";
		// d($data);
		$this->_v($this->folder_view.'/whislist',$data);
	}
	function checkout(){

		$data="";
		// d($data);
		$this->_v($this->folder_view.'/checkout',$data);
	}
	function login(){

		$data="";
		// d($data);
		$this->_v($this->folder_view.'/login',$data);
	}
	function vendor(){

		$data="";
		// d($data);
		$this->_v($this->folder_view.'/vendor',$data);
	}
	
	function vorder_detail(){

		$data="";
		// d($data);
		$this->_v($this->folder_view.'/vorder_detail',$data);
	}
	function vadd_product(){

		$data="";
		// d($data);
		$this->_v($this->folder_view.'/vadd_product',$data);
	}
	function vadd_product_photo(){

		$data="";
		// d($data);
		$this->_v($this->folder_view.'/vadd_product_photo',$data);
	}

	function buyer(){

		$data="";
		// d($data);
		$this->_v($this->folder_view.'/buyer',$data);
	}
	function bchange_password(){

		$data="";
		// d($data);
		$this->_v($this->folder_view.'/bchange_password',$data);
	}
	function border(){

		$data="";
		// d($data);
		$this->_v($this->folder_view.'/border',$data);
	}
	function bpersonal(){

		$data="";
		// d($data);
		$this->_v($this->folder_view.'/bpersonal',$data);
	}
	function bshipping(){

		$data="";
		// d($data);
		$this->_v($this->folder_view.'/bshipping',$data);
	}


	
}
