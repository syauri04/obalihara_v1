<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH."libraries/FrontController.php");
include_once(APPPATH."libraries/google-api-php-client/src/Google_Client.php");
include_once(APPPATH."libraries/google-api-php-client/src/contrib/Google_Oauth2Service.php");

class Signin extends FrontController {

	
	function __construct()    
	{
		parent::__construct();
		$this->load->library('facebook');
		$this->load->model('home_model','HM');
		$this->folder_view = 'auth';
	}
	
	public function index()
	{

		$userData = array();

		$data = [
			'msg' => ''
		];
        
        // Check if user is logged in
        if($this->facebook->is_authenticated()){
            // Get user facebook profile details
            $fbUser = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,link,gender,picture');

            // Preparing data for database insertion
            $userData['oauth_provider'] = 'facebook';
            $userData['oauth_uid']    	= !empty($fbUser['id'])?$fbUser['id']:'';;
            $userData['first_name']    	= !empty($fbUser['first_name'])?$fbUser['first_name']:'';
            $userData['last_name']    	= !empty($fbUser['last_name'])?$fbUser['last_name']:'';
            $userData['email']        	= !empty($fbUser['email'])?$fbUser['email']:'';
            $userData['gender']        	= !empty($fbUser['gender'])?$fbUser['gender']:'';
            $userData['picture']    	= !empty($fbUser['picture']['data']['url'])?$fbUser['picture']['data']['url']:'';
            $userData['link']        	= !empty($fbUser['link'])?$fbUser['link']:'';
            
            
            // Insert or update user data
            $userID = $this->HM->getUser($userData);
            // d($userID);
            // Check user data insert or update status
            if(!empty($userID)){

                // $data['userData'] = $userData;
                // $this->session->set_userdata('userData', $userData);
                $this->jCfg['client']['is_login'] 	= 1;
				$this->jCfg['client']['id'] 		= $userID['id'];
				$this->jCfg['client']['name']		= $userID['username'];
				$this->jCfg['client']['fullname'] 	= getUser($userID['id'],'nama_lengkap');
				$this->jCfg['client']['login_tipe']	= "facebook";
				$this->jCfg['client']['photo'] 		= getUser($userID['id'],'photo');
				$this->jCfg['client']['email'] 		= $userID['email'];
				$this->jCfg['client']['logout'] 	= $this->facebook->logout_url();

				$this->_releaseSession();

            }else{
               $data['userData'] = array();
            }
            
            // Get logout URL
            // $data['logoutURL'] = $this->facebook->logout_url();
        	
        	redirect('/home','refresh');

        }elseif($_POST){

			if(in_array("",$_POST)){
				$data = ['msg' => ['m' => 'Password or Username is empty' , 'con' => 'danger' , 't' => 'Failed!'] ];
			} else {

				$this->db->where("(email = '".$_POST['email']."' OR username = '".$_POST['email']."') ");
				$eu = $this->db->get('cp_user')->row();

				if(!isset($eu) && empty($eu)){
					$data = ['msg' => ['m' => 'Email Or Username not registered' , 'con' => 'danger' , 't' => 'Failed!'] ];
				} else {

					$this->db->where("(email = '".$_POST['email']."' OR username = '".$_POST['email']."') AND password = '".md5($_POST['password'])."' ");
					$d = $this->db->get('cp_user')->row();

					if(!isset($d) && empty($d)){
						$data = ['msg' => ['m' => 'Password or Username doesn\'t match' , 'con' => 'danger' , 't' => 'Failed!'] ];
					} else {

						$this->jCfg['client']['is_login'] 	= 1;
						$this->jCfg['client']['id'] 		= $d->id;
						$this->jCfg['client']['name']		= $d->username;
						$this->jCfg['client']['fullname'] 	= getUser($d->id,'nama_lengkap');
						$this->jCfg['client']['login_tipe']	= "";
						$this->jCfg['client']['photo'] 		= getUser($d->id,'photo');
						$this->jCfg['client']['email'] 		= $d->email;
						$this->jCfg['client']['logout']		= '';					
						$this->_releaseSession();
						
						$this->db->update("cp_user",array(
							'logindate' => date("Y-m-d H:i:s")
						),array(
							'id' => $d->id
						));	
						
						if( isset($this->jCfg['client']['referer']) && $this->jCfg['client']['referer'] != '' && $this->jCfg['client']['referer'] != 'chat'){
							redirect($this->jCfg['client']['referer'],'refresh');
						}else{
							redirect('/home','refresh');
						}
						// $data =['msg' => ['m' => 'it\'s a match' , 'con' => 'success' , 't' => 'Yaaay!'] ];
					}

				}
				
			}

        	$data['authURL'] =  $this->facebook->login_url();
		} else {
			$data['authURL'] =  $this->facebook->login_url();
		}
        
        // d($data);
		$this->_v($this->folder_view.'/signin',$data);
	}

	function out(){

		$this->jCfg['client']['id'] 		= '';
		$this->jCfg['client']['name']		= '';
		$this->jCfg['client']['fullname'] 	= '';
		$this->jCfg['client']['login_tipe']	= '';
		$this->jCfg['client']['photo'] 		= '';
		$this->jCfg['client']['email'] 		= '';
		$this->jCfg['client']['is_login']   = 0;
		$this->jCfg['client']['referer']	= "";
		$this->_releaseSession();

		$this->session->unset_userdata(array("jcfg"=>$this->jCfg['client']));
		$this->session->sess_destroy();

		redirect(base_url());
	}

	function logout(){
        // Remove local Facebook session
        $this->facebook->destroy_session();
        // Remove user data from session
        $this->jCfg['client']['id'] 		= '';
		$this->jCfg['client']['name']		= '';
		$this->jCfg['client']['fullname'] 	= '';
		$this->jCfg['client']['login_tipe']	= '';
		$this->jCfg['client']['photo'] 		= '';
		$this->jCfg['client']['email'] 		= '';
		$this->jCfg['client']['is_login']   = 0;
		$this->jCfg['client']['referer']	= "";
		$this->_releaseSession();

		$this->session->unset_userdata(array("jcfg"=>$this->jCfg['client']));
		$this->session->sess_destroy();

		redirect(base_url());
	}

	function register(){


		$data = [
			// 'username' 		=> $_POST['username'],
			'email' 		=> $_POST['email'],
			'password' 		=> md5($_POST['password']),
			'phone_number' 	=> $_POST['phoneNumber'],
			'status' 		=> 1,
			'is_trash'		=> 0,
		];

		$c = $this->db->where("(email = '".$_POST['email']."')")->get('cp_user')->result();
		// $c = $this->db->where("(email = '".$_POST['email']."' OR username = '".$_POST['username']."')")->get('cp_user')->result();

		if(count($c) > 0){
			$datas = ['msg' => ['m' => 'Email sudah terdaftar' , 'con' => 'danger' , 't' => 'Failed!', 'r' => '#register'] ];
		} else {

			$this->db->insert('cp_user',$data);
			$a = $this->db->insert_id();
			
			$buyer = [
				'user_id' 		=> $a,
				'telp' 			=> $_POST['phoneNumber'],
				'nama_lengkap' 	=> $_POST['email'],
				'email' 		=> $_POST['email'],
			];

			$this->db->insert('cp_buyer',$buyer);

			$datas = ['msg' => ['m' => 'Selamat Anda telah terdaftar' , 'con' => 'success' , 't' => 'Success!', 'r' => '#login'] ];
		}

		redirect('/login?msg='.urlencode($datas['msg']['m']).'&&con='.urlencode($datas['msg']['con']).$datas['msg']['r'],'refresh');

	}

	function forgot(){
		
		if($_POST){

			$c = $this->db->where("(email = '".$_POST['email']."')")->get('cp_user')->result();
			$dt['data'] = $c;
			if(count($c) > 0){
				$this->db->where('email', $_POST['email']);
			    $this->db->set('reset_pw',1);
			    $this->db->update('cp_user');
				$message = $this->load->view('email/forgot',$dt,TRUE);
				// d($message);
				$send = $this->sendEmail(array(
									'from' =>  'Obalihara.com', 
									'to' => $_POST['email'], 
									'title' => '', 
									'subject' => 'Konfirmasi Reset Password', 
									'message' => $message 
								));
				redirect('/home');
			} else {
				$datas = ['msg' => ['m' => 'Email belum terdaftar' , 'con' => 'danger' , 't' => 'Failed!', 'r' => '#register'] ];
				redirect('/login?msg='.urlencode($datas['msg']['m']).'&&con='.urlencode($datas['msg']['con']).$datas['msg']['r'],'refresh');
			}
		}
		$data = [
			
		];

		// d($data);
		$this->_v($this->folder_view.'/forgot',$data);
	}

	function reset(){
		
		if($_POST){
			$idu = codeDecrypt($_GET['e']);
			if($_POST['password1'] != "" && $_POST['password2'] != "" ){
				if ($_POST['password1'] != $_POST['password2'] ) {
					$datas = ['msg' => ['m' => 'Password dosent match..' , 'con' => 'danger' , 't' => 'Failed!', 'r' => '#register'] ];
					redirect('/reset?e='.$_GET['e'].'&&'.'msg='.urlencode($datas['msg']['m']).'&&con='.urlencode($datas['msg']['con']).$datas['msg']['r'],'refresh');
			
				}else{
					$this->db->where('id', $idu);
				    $this->db->set('password',md5($_POST['password1']));
				    $this->db->set('reset_pw',0);
				    $this->db->update('cp_user');
				    redirect('/login');
				}
			}else{
				$datas = ['msg' => ['m' => 'Password Belum di Input' , 'con' => 'danger' , 't' => 'Failed!', 'r' => '#register'] ];
				redirect('/reset?e='.$_GET['e'].'&&'.'msg='.urlencode($datas['msg']['m']).'&&con='.urlencode($datas['msg']['con']).$datas['msg']['r'],'refresh');
			
			}
			$c = $this->db->where("(email = '".$_POST['email']."')")->get('cp_user')->result();
			
		}
		$data = [
			
		];

		// d($data);
		$this->_v($this->folder_view.'/reset',$data);
	}

	function loginG() {
		
        /* include_once("src/Google_Client.php");
		include_once("src/contrib/Google_Oauth2Service.php"); */
		######### edit details ##########
		$clientId = '966280773677-sfnqebc1ce47rigsgndnea3lsrt5sh5o.apps.googleusercontent.com'; //Google CLIENT ID
		$clientSecret = 'uHXHGvOgRxjgsMC3V_Ekk_VL'; //Google CLIENT SECRET
		$redirectUrl = 'https://obalihara.com/demo/login-google';  //return url (url to script)
		$homeUrl = 'https://obalihara.com/demo/login-google';  //return to home
		// $redirectUrl = 'https://obalihara.com/obalihara_v1/login-google';  //return url (url to script)
		// $homeUrl = 'https://localhost/obalihara_v1/login-google';  //return to home
		
		##################################

		$gClient = new Google_Client();
		$gClient->setApplicationName('Login to obalihara.com');
		$gClient->setClientId($clientId);
		$gClient->setClientSecret($clientSecret);
		$gClient->setRedirectUri($redirectUrl);
		
		
		$google_oauthV2 = new Google_Oauth2Service($gClient);
		//debugCode($google_oauthV2);
		if(isset($_GET['code']) OR isset($_REQUEST['code'])){
			$gClient->authenticate();
			$_SESSION['token'] = $gClient->getAccessToken();
			//debugCode($_SESSION['token']);
			header('Location: ' . filter_var($redirectUrl, FILTER_SANITIZE_URL));
		}

		if (isset($_SESSION['token'])) {
			$gClient->setAccessToken($_SESSION['token']);
			$accessToken = $gClient->setAccessToken($_SESSION['token']);
		}

		if ($gClient->getAccessToken()) {
			
			$userProfile = $google_oauthV2->userinfo->get();
			
			 // $this->jCfg['client']['is_login'] 	= 1;
				// $this->jCfg['client']['id'] 		= $userID['id'];
				// $this->jCfg['client']['name']		= $userID['username'];
				// $this->jCfg['client']['fullname'] 	= getUser($userID['id'],'nama_lengkap');
				// $this->jCfg['client']['login_tipe']	= "facebook";
				// $this->jCfg['client']['photo'] 		= getUser($userID['id'],'photo');
				// $this->jCfg['client']['email'] 		= $userID['email'];
				// $this->jCfg['client']['logout'] 	= $this->facebook->logout_url();

			 // Preparing data for database insertion
            $userData['oauth_provider'] = 'google';
            $userData['oauth_uid']    	= !empty($userProfile['id'])?$userProfile['id']:'';;
            $userData['first_name']    	= !empty($userProfile['given_name'])?$userProfile['given_name']:'';
            $userData['last_name']    	= !empty($userProfile['family_name'])?$userProfile['family_name']:'';
            $userData['email']        	= !empty($userProfile['email'])?$userProfile['email']:'';
            $userData['gender']        	= !empty($userProfile['gender'])?$userProfile['gender']:'';
            $userData['picture']    	= !empty($userProfile['picture'])?$userProfile['picture']:'';
            $userData['token']        	= (string) $userProfile['id'];
            
            // d($userData);
            // Insert or update user data
            $userID = $this->HM->getUser($userData);

             if(!empty($userID)){

                // $data['userData'] = $userData;
                // $this->session->set_userdata('userData', $userData);
                $this->jCfg['client']['is_login'] 	= 1;
				$this->jCfg['client']['id'] 		= $userID['id'];
				$this->jCfg['client']['name']		= $userID['username'];
				$this->jCfg['client']['fullname'] 	= getUser($userID['id'],'nama_lengkap');
				$this->jCfg['client']['login_tipe']	= "google";
				$this->jCfg['client']['photo'] 		= getUser($userID['id'],'photo');
				$this->jCfg['client']['email'] 		= $userID['email'];
				$this->jCfg['client']['logout'] 	= base_url().'logout-google';

				$this->_releaseSession();

            }else{
               $data['userData'] = array();
            }
		   // d($this->jCfg['client']);
			$this->_releaseSession();
			header("location: ".base_url()."");
			$_SESSION['token'] = $gClient->getAccessToken();
		}else{
			
			$authUrl = $gClient->createAuthUrl();
		}

		
		//debugCode($authUrl);
		if(isset($authUrl)){
			redirect($authUrl);
		}	

	}

	function logoutG(){
		$clientId = '966280773677-sfnqebc1ce47rigsgndnea3lsrt5sh5o.apps.googleusercontent.com'; //Google CLIENT ID
		$clientSecret = 'uHXHGvOgRxjgsMC3V_Ekk_VL'; //Google CLIENT SECRET
		$redirectUrl = 'https://localhost/obalihara_v1/login-google';  //return url (url to script)
		$homeUrl = 'https://localhost/obalihara_v1/login-google';  //return to home
		
		##################################

		$gClient = new Google_Client();
		
		// Remove user data from session
        $this->jCfg['client']['id'] 		= '';
		$this->jCfg['client']['name']		= '';
		$this->jCfg['client']['fullname'] 	= '';
		$this->jCfg['client']['login_tipe']	= '';
		$this->jCfg['client']['photo'] 		= '';
		$this->jCfg['client']['email'] 		= '';
		$this->jCfg['client']['is_login']   = 0;
		$this->jCfg['client']['referer']	= "";
		unset($_SESSION['token']);
		$gClient->revokeToken();
		$this->_releaseSession();

		$this->session->unset_userdata(array("jcfg"=>$this->jCfg['client']));
		$this->session->sess_destroy();

		
		redirect(base_url());
	}

}



		
		