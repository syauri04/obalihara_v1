<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH."libraries/FrontController.php");
require FCPATH . 'vendor/autoload.php';

class Home extends FrontController {

	
	function __construct()    
	{
		parent::__construct();
		$this->load->model('home_model','HM');
		$this->folder_view = 'home';
		$this->menu = $this->HM->getCatPM();
		
	}
	
	public function index()
	{

		$data = [
			'bannerSlide' 	=> $this->HM->getSlide('c'),
			'bannerSide' 	=> $this->HM->getSlide('s'),
			'bannerMid' 	=> $this->HM->getSlide('m'),
			'bannerMidSmall'=> $this->HM->getSlide('ms'),
			'walhiMart'		=> $this->HM->productVendor(2,'DESC'),
			'getProduct' 	=> $this->HM->getProduct('na'),
			'getProductC' 	=> $this->HM->getProduct('naC'),
			'getProductDof' => $this->HM->getProduct('dof'),
			'getProductOr'	=> $this->HM->getProduct('or'),
		];
		// d($data);
		$this->_v($this->folder_view.'/home',$data);
	}

	function vendorDetail(){

		$id = codeDecrypt($_GET['id']);

		$data = [
			'getProduct' 		=> $this->HM->productVendor($id),
			'getMostProduct'	=> $this->HM->mostProduct($id),
			'getVendor'  		=> $this->db->get_where('cp_vendor',['id' => $id,'is_trash' => 0])->row(),
		];
		
		$this->_v($this->folder_view.'/vendorDetail',$data);

	}

	function category(){

		if(isset($_POST['search']) && $_POST['search']){
			// d($_POST);
			$tc = 'Search Result ('.$_POST['search'].' )';
			$bs = [];
			$cs = [];
			$ga = $this->HM->getAllProduk(['search' => $_POST['search']]);
		} else  if(isset($_GET['viewall']) && $_GET['viewall']) {
			$sortp = isset($_GET['sortp'])?$_GET['sortp']:"";
		    $tc = $_GET['viewall'];
			$bs = [];
			$cs = [];
			$ga = $this->HM->getViewAll(['type' => $_GET['viewall'] , 'sortp' => $sortp ]);
		} else  if(isset($_GET['sortp']) && $_GET['sortp']) {
		    $tc = str_replace("-"," ",isset($_GET['ct'])?$_GET['ct']:"");
			$bs = $this->HM->getBestSeller(isset($_GET['ct'])?$_GET['cid']:"");
			$ga = $this->HM->getAllProduk(['id' => isset($_GET['ct'])?$_GET['cid']:"", 'sortp' => isset($_GET['sortp'])?$_GET['sortp']:""]);
			$cs = $this->HM->getSubCat(['id' => isset($_GET['ct'])?$_GET['cid']:""]);
		} else{
			$tc = str_replace("-"," ",isset($_GET['ct'])?$_GET['ct']:"");
			$bs = $this->HM->getBestSeller(isset($_GET['ct'])?$_GET['cid']:"");
			$ga = $this->HM->getAllProduk(['id' => isset($_GET['ct'])?$_GET['cid']:""]);
			$cs = $this->HM->getSubCat(['id' => isset($_GET['ct'])?$_GET['cid']:""]);
		}

		// d($ga);
		$data = [
			'bannerSlide' => $this->HM->getSlide('c'),
			'titleCat'  	=> $tc,
			'bestSeller'	=> $bs,
			'getAllProduk'	=> $ga,
			'subCat'	=> $cs,
		];

		// d($data['bestSeller']);
		
		$this->_v($this->folder_view.'/productCategory',$data);
	}

	function detail(){

		$idProduk = codeDecrypt($_GET['id']);

		$data = [
			'titleProd'  => str_replace("-"," ",$_GET['title']),
			'produk'	 => $this->HM->getDetailProduk($idProduk),
			'wishlist'	 => produk::wishlist($idProduk,$this->jCfg['client']['id'])
		];

		$this->load->helper('cookie');
		$currentURL = current_url(); //http://myhost/main

		$params   = $_SERVER['QUERY_STRING']; //my_id=1,3

		$fullURL = $currentURL . '?' . $params; 
		// d($fullURL);
		
		// $currenturl = base_url().'detail/pd?id='.urlencode(codeEncrypt($value->barang_id)).'&&'.'title='.url_title($_GET['title']);
		// d($currenturl);
		// this line will return the cookie which has slug name
		$check_visitor = $this->input->cookie(urlencode($fullURL), FALSE);
		// this line will return the visitor ip address
		$ip = $this->input->ip_address();

		// d($check_visitor);
		if ($check_visitor == false) {
	        $cookie = array(
	            "name"   => urlencode($fullURL),
	            "value"  => "$ip",
	            "expire" =>  time() + 7200,
	            "secure" => false
	        );
	        // d($cookie);
	        $this->input->set_cookie($cookie);
	        $this->db->where('id', $idProduk);
		    $this->db->select('is_counter');
		    $count = $this->db->get('cp_barang')->row();
		// then increase by one 
		    $this->db->where('id', $idProduk);
		    $this->db->set('is_counter', ($count->is_counter + 1));
		    $this->db->update('cp_barang');
	    }

		$this->_v($this->folder_view.'/productDetail',$data);
	}

	function wishlist(){

		if($this->jCfg['client']['is_login'] != 1){

			// $this->jCfg['client']['referer'] = $this->own_link;
			// $this->_releaseSession();

			redirect('/login','refresh');
		}

		if(isset($_POST['barang_id'])){
			
			$cekWishlist = produk::wishlist($_POST['barang_id'],$this->jCfg['client']['id']);

			if($cekWishlist > 0) {

				$return = [
					'user_id' => urlencode(codeEncrypt($this->jCfg['client']['id'])),
					'status'  => 202,
					'msg'	  => 'Barang Sudah di masukan ke wishlist anda',
				];

			} else {

				$data = [
					'user_id' 		=> $this->jCfg['client']['id'],
					'barang_id'		=> $_POST['barang_id'],
					'date_created'	=> date('Y:m:d H:i:s')
				];

				$a = $this->db->insert('cp_wishlist',$data);

				$return = [
					'status'	  => ($a)?200:500,
				];

			}

			echo json_encode($return);

		} else {
			$idUser = codeDecrypt($_GET['id']);

			$data = [
				'getWishlist' => $this->HM->getWishlist($idUser),
			];

			$this->_v($this->folder_view.'/wishlist',$data);
		}

	}

	function deleteWishlist(){

		$this->db->delete('cp_wishlist',['barang_id' => $_POST['barang_id'] , 'user_id' => $_POST['user_id']]);

		$return = [
			'status' => 200,
			'msg'	 => 'Delete Data Success',
		];

		echo json_encode($return);

	}

	public function checkout()
	{	
		$ci = [];
		$x = explode(',',$_POST['cartId']);
		$q = explode(',',$_POST['qtyId']);
		$t = explode(',',$_POST['ttlItem']);

		foreach ($x as $k => $v) {
			$getCart = $this->HM->getCart($v);
			foreach ($getCart as $y => $a) {
				$ci[$a->vendor_id][$k] 			= $a;
				$ci[$a->vendor_id][$k]->qtyId 	= $q[$k];
				$ci[$a->vendor_id][$k]->ttlItem = $t[$k];
			}
		}

		$data = [
			'shipping' 	=> $this->HM->getShipping($this->jCfg['client']['id']),
			'cartId'   	=> $ci,
			'city'		=> $this->_raongCity(),
		];

		$this->_v($this->folder_view.'/checkout',$data);
	}

	function saveCheckout(){

		$dataHeader = [
			'address_id' 			=> $_POST['addressID'],
			'user_id' 				=> $_POST['userID'],
			'invoice_code' 			=> $_POST['invoiceCode'],
			'total_product_rate' 	=> $_POST['total_product_rate'],
			'total_expedition_rate' => $_POST['total_expedition_rate'],
			'sub_total' 			=> $_POST['sub_total'],
			'disc' 					=> $_POST['discount'],
			'grand_total' 			=> $_POST['grand_total'],
		];

		//Save Header Checkout
		$this->db->insert('cp_checkout',$dataHeader);
		$aID = $this->db->insert_id();

		foreach ($_POST['qty'] as $k => $v) {

			$dataDetail = [
				'header_id'			=> $aID,
				'cart_id' 			=> $_POST['cartID'][$k],
				'vendor_id' 		=> $_POST['vendorID'][$k],
				'total' 			=> $_POST['total'][$k],
				'courier' 			=> $_POST['courier'][$k],
				'courier_service' 	=> $_POST['courierService'][$k],
				'courier_cost' 		=> $_POST['courierPrice'][$k],
				'fee'				=> feePrice($_POST['total'][$k],getOutlet($_POST['vendorID'][$k],'fee'))
			];
			$this->db->insert('cp_checkout_detail',$dataDetail);

            $this->db->update('cp_cart',['tanggal_checkout' => date('Y-m-d H:i:s'),'qty' => $_POST['qty'][$k],'status_invoice' => 1],['id' => $_POST['cartID'][$k] ]);

		}
		
		// API key Production xnd_production_NGKe9mpWtDwQLrCPCPwJ0JG1iP887fs5p6kuaZzFtIp8xplT6F8YdWlOTPFL8O
		// API key development xnd_development_IhjXLDUmNHbQco2klFHNLGNDO30o7Av1HqiCY4Hhhffa9UzfSfM3aGg52IPZTQa2

		$options['secret_api_key'] = 'xnd_development_IhjXLDUmNHbQco2klFHNLGNDO30o7Av1HqiCY4Hhhffa9UzfSfM3aGg52IPZTQa2';
				
		$xenditPHPClient = new XenditClient\XenditPHPClient($options);

		$external_id = $_POST['invoiceCode'];
		$amount = $_POST['grand_total'];
		$payer_email = getUser($_POST['userID'],'email');
		$description = getUser($_POST['userID'],'nama_lengkap').' Nomor Transaksi '.$_POST['invoiceCode'];

		$response = $xenditPHPClient->createInvoice($external_id, $amount, $payer_email, $description);

		$dataUpdate = [
			'xendit_invoice_code' 	=> $response['id'],
			'status' 				=> $response['status'],
			'url' 					=> $response['invoice_url'],
		];

		$this->db->update('cp_checkout',$dataUpdate,['id' => $aID]);

		$htmlRes = [
			'nama_lengkap' 	=> getUser($_POST['userID'],'nama_lengkap'),
			'id_external'  	=> $external_id,
			'amount'	   	=> myNum($amount,'Rp '),
			'date'			=> strftime('%A, %d %B %Y', strtotime($response['expiry_date']))
		];

		$email = [
            'from'      => 'Obalihara.com',
            'title'     => 'Payment Notice',
            'to'        => $payer_email,
            'subject'   => 'Payment Notice',
            'message'   => $this->htmlPayNotice($htmlRes),
        ];

        $this->sendEmail($email);

		$url = base_url().'buyer/payment?id='.urlencode(codeEncrypt($this->jCfg['client']['id'])).'&&code='.urlencode($external_id);

		redirect($url,'refresh');
	}

	function checkOutCallback(){

		if ($_SERVER["REQUEST_METHOD"] === "POST") {
	        $data = file_get_contents("php://input");

	        $d = json_decode($data);
	        $a = [];
	        $xendit_invoice_code = $d->id;
	        $invoice_code = $d->external_id;
	        
	        foreach ($d as $k => $v) {
	        	$a[$k] = $v;
	        }	        

	        $res = [
	        	'status' => $d->status,
	        ];

	        if($d->status == 'PAID'){

	            $w = $this->db->select('id')->where(['xendit_invoice_code'=>$xendit_invoice_code,'invoice_code'=>$invoice_code])->get('cp_checkout')->row();

	            $this->db->select('*')->join('cp_checkout_detail cd','cd.header_id = c.id')->where('cd.header_id',$w->id)->group_by('vendor_id');
	            $result = $this->db->get('cp_checkout c')->result();

	            foreach ($result as $k => $v) {
	                // Email Config
	                $email = [
	                    'from'      => 'Obalihara.com',
	                    'title'     => 'New Order\'s',
	                    'to'        => getOutlet($v->vendor_id,'email'),
	                    'subject'   => 'New Order\'s has been paid',
	                    'message'   => $this->htmlTemplate(getOutlet($v->vendor_id,'nama_lengkap')),
	                ];

	                $this->sendEmail($email);
	            }

	            $htmlRes = [
					'nama_lengkap' 	=> getUser($result[0]->user_id,'nama_lengkap'),
					'id_external'  	=> $invoice_code,
					'amount'	   	=> myNum($d->paid_amount,'Rp '),
					'date'			=> strftime('%A, %d %B %Y', strtotime($d->created))
				];
				
	            // Email1 Config
                $email1 = [
                    'from'      => 'Obalihara.com',
                    'title'     => 'Payment Paid',
                    'to'        => getUser($result[0]->user_id,'email'),
                    'subject'   => 'Payment has been paid',
                    'message'   => $this->htmlPaidNotice($htmlRes),
                ];
                
                $this->sendEmail($email1);

	        }

	        $getCartRow = $this->HM->getCartRow($a,$xendit_invoice_code,$invoice_code,$res);

	        // If getCartRow return true must send alert to vendor to process
	        

	    } else {
	        // print_r("Cannot ".$_SERVER["REQUEST_METHOD"]." ".$_SERVER["SCRIPT_NAME"]);

	    	$a = Array
				(
				    'id' => '579c8d61f23fa4ca35e52da4',
				    'user_id' => '5781d19b2e2385880609791c',
				    'external_id' => 'invoice_123124123',
				    'is_high' => 1,
				    'status' => 'Error',
				    'merchant_name' => 'Xendit',
				    'amount' => '50000',
				    'payer_email' => 'albert@xendit.co',
				    'description' => 'This is a description',
				    'paid_amount' => '50000',
				    'payment_method' => 'POOL',
				    'adjusted_received_amount' => '47500',
				    'updated' => '2016-10-10T08:15:03.404Z',
				    'created' => '2016-10-10T08:15:03.404Z',
				);

	        $this->db->insert('cp_callback',$a);
	    }
	}

	function orderCode(){

		$c = $this->db->query("
					select max(right(invoice_code,6)) as jum from cp_checkout
				")->row();
		$v = $c->jum+1;
		$d = date('Y F H:i:s');
		$prc = "TRX".strtotime($d).str_repeat("0",6-strlen($v)).$v;

		echo json_encode($prc);
	}

	function uniqueNumber($gt){

		$c 	= str_pad(rand(0, pow(10, 3)-1), 3, '0', STR_PAD_LEFT);
		$t 	= ($gt+$c);
		$check = $this->db->get_where('cp_checkout',array('grand_total'=>$t))->result();

		if(count($check) > 0){
			uniqueNumber($gt);
		} else {
			echo json_encode($t);
		}
	}

	function cart(){

		if($this->jCfg['client']['is_login'] == 1){
			if(isset($_POST['barang_id'])){

				$cekCart = produk::getCart($_POST['barang_id'],$this->jCfg['client']['id']);
				$wId = (count($cekCart) > 0) ? $cekCart[0]->warna_id:0;
				$uId = (count($cekCart) > 0) ? $cekCart[0]->ukuran_id:0;

				$cartHeader = [
					'user_id' 			=> $this->jCfg['client']['id'],
					'vendor_id'			=> $_POST['vendor_id'],
					'tanggal_order'		=> date('Y-m-d H:i:s'),
					'barang_id' 		=> $_POST['barang_id'],
					'warna_id'			=> isset($_POST['warna_id']) ? $_POST['warna_id'] : $wId,
					'ukuran_id' 		=> isset($_POST['ukuran_id']) ? $_POST['ukuran_id'] : $uId,
					'qty'				=> 1,
					'status_invoice'	=> 0,//New Order 
					'discount'			=> isset($_POST['discount']) ? $_POST['discount'] : 0,
					'tax'				=> 0
				];
				
				$cc = count($cekCart);

				if($cc > 0){

					$this->DATA->table = 'cp_cart';
					$cartHeader['qty'] = $cekCart[0]->qty + 1;
					$a = $this->_save_master($cartHeader,['id' => $cekCart[0]->id],($cekCart[0]->id));

				} else {
					$a = $this->db->insert('cp_cart',$cartHeader);
				}

				$return = [
					'user_id' => urlencode(codeEncrypt($this->jCfg['client']['id'])),
					'msg'	  => ($a)?200:500,
				];

				echo json_encode($return);

			} else {



				$idUser = codeDecrypt($_GET['id']);
				$c = array();

				$cart = $this->HM->getCartByUser($idUser);

				foreach ($cart as $k => $i) {
				   $c[$i->vendor_id][] = $i;
				}

				$data = [
					'cart' 	=> $c,
					'city'	=> $this->_raongCity(),
				];

				$this->_v($this->folder_view.'/cart',$data);

			}
		} else {
			echo json_encode('500');
		}

		
	}

	function deleteCart(){

		$this->db->delete('cp_cart',['barang_id' => $_POST['barang_id'] , 'user_id' => $_POST['user_id'],'status_invoice' => 0]);

		$return = [
			'status' => 200,
			'msg'	 => 'Delete Data Success',
		];

		echo json_encode($return);

	}

	function getCourierList(){

		$a = getOutlet($_POST['id'],'shipping_metode');
		$tmp = '';

		if(!empty($a)) {
			$x = explode(',',$a);

            $tmp .= "<option value=''>- Pilih Courier -</option>"; 
			foreach($x as $i => $a){ 

                $tmp .= '<option value="'.courierSwitch($a).'" >'.strtoupper($a).'</option>';
            }
        } else {
            $tmp .= "<option value=''>- Pilih Courier -</option>"; 
        }
        die($tmp);

	}

	function checkCoupon(){

		$dn = date('Y-m-d');
		$a = $this->db->get_where('cp_coupon',['coupon_name'=>$_POST['id']])->row();
		$p = 0;
		$m = '';

		if(isset($a)){
			if($dn >= $a->date_from && $dn <= $a->date_to){
				$p = $a->price;
				$m = 'Coupon Check';
				$s = true;
			} else {
				$s = false;
				$m = 'Coupon number expired';
			}
		} else {
			$s = false;
			$m = 'Sorry Coupon number doesn\'t exist';
		}

		$data = [
			'p' => $p,
			'm' => $m,
			's' => $s,
		];

		echo json_encode($data);

	}

	function cartCity(){
		$tmp    = '';
        $data   = $this->_raongCity($_POST['id']);
        
        echo json_encode($data);
	}

	function cartCost(){
		$tmp    = '';
        $data   = $this->_raongCost($_POST['origin'],$_POST['dest'],$_POST['weight'],$_POST['courier']);

        echo json_encode($data[0]);
	}

	public function _raongCity($id=''){

		// if($t == 'p'){
		// 	$url = "http://api.rajaongkir.com/starter/province";
		// } else if($t == 'c') {
		// 	if($id != ''){
		// 		$url = "https://api.rajaongkir.com/starter/city?province=".$id;
		// 	} else {
		// 		$url = "https://api.rajaongkir.com/starter/city";
		// 	}
		// }

		if($id != ''){
			$url = "https://pro.rajaongkir.com/api/city?id=".$id;
		} else {
			$url = "https://pro.rajaongkir.com/api/city";
		}
		

		$curl = curl_init();
 
		curl_setopt_array($curl, array(
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
			"key: f5e8ac7da7095787b8c8892cd644e7f1"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		$status = json_decode($response,true);
		// d($status);
		$a = '';

		if($status['rajaongkir']['status']['code'] == 400){
			$a = $status['rajaongkir']['status']['description'];
		} else {
			$a = json_decode($response,true)['rajaongkir']['results'];
		}

		if ($err) {
		  echo "cURL Error #:" . $err;
		} else {
		  return $a;
		}

	}

	public function _raongCost($origin,$dest,$weight,$courier){

		$curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://pro.rajaongkir.com/api/cost",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => "origin=".$origin."&originType=city&destination=".$dest."&destinationType=city&weight=".$weight."&courier=".$courier,
		CURLOPT_HTTPHEADER => array(
			"key: f5e8ac7da7095787b8c8892cd644e7f1"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		$status = json_decode($response,true);
		$a = '';

		if($status['rajaongkir']['status']['code'] == 400){
			$a = $status['rajaongkir']['status']['description'];
		} else {
			$a = json_decode($response,true)['rajaongkir']['results'];
		}


		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			return $a;
		}
	}

	function privacy(){
		$data = [
			'title' => 'Kebijakan Privasi',
			'dt' => $this->db->get('cp_footer_config')->row(),
			'sideprep' => $this->load->view($this->folder_view.'/sideprep',NULL,true),
		];
		$data['content'] = $data['dt']->tentang_kebijakan_privasi;
		$this->_v($this->folder_view.'/template_footer',$data);
	}

	function terms(){
		$data = [
			'title' => 'Terms And Condition',
			'dt' => $this->db->get('cp_footer_config')->row(),
			'sideprep' => $this->load->view($this->folder_view.'/sideprep',NULL,true),
		];
		$data['content'] = $data['dt']->terms_condition;
		// d($data);
		$this->_v($this->folder_view.'/template_footer',$data);
	}

	function about(){
		$data = [
			'title' => 'Tentang Kami',
			'dt' => $this->db->get('cp_footer_config')->row(),
			'sideprep' => $this->load->view($this->folder_view.'/sideprep',NULL,true),
		];
		$data['content'] = $data['dt']->tentang_kami;
		$this->_v($this->folder_view.'/template_footer',$data);
	}

	function service(){
		$data = [
			'title' => 'Customer Service',
			'dt' => $this->db->get('cp_footer_config')->row(),
			'sideprep' => $this->load->view($this->folder_view.'/sideprep',NULL,true),
		];
		$data['content'] = $data['dt']->customer_service;
		$this->_v($this->folder_view.'/template_footer',$data);
	}

	function faq(){
		$data = [
			'title' => 'Frequenly Asked Questions',
			'dt' => $this->db->get('cp_footer_config')->row(),
			'sideprep' => $this->load->view($this->folder_view.'/sideprep',NULL,true),
		];
		$data['content'] = $data['dt']->faq;
		$this->_v($this->folder_view.'/template_footer',$data);
	}

	function htmlTemplate($vendor){
		$a = '
			<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
			<html>
				<head>
					<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
					<style>
						@import url("https://fonts.googleapis.com/css?family=Heebo:400,500,700");
						@import url("https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css");
						body{
						  font-size: 12px;
						  background: #F5F5F9;
						  font-family: "Heebo" !important;
						  overflow-x: hidden;
						  color: #888888;
						  max-width:600px;
						}
						.red-head{
						    width:100%;
						    background:#8F3248;
						    padding-top:22px;
						    padding-left:14px;
						    padding-right:14px;
						    
						}
						.white-head{
						    border-top-left-radius:10px;
						    border-top-right-radius:10px;
						    text-align:center;
						    background:#fff;
						    padding-top:32px;
						    padding-bottom:10px;
						}
						.content-head{
						    font-size: 14px;
						}
						h1{
						    font-family: Heebo;
						    font-weight:600;
						    font-size: 20px;
						    line-height: 20px;
						    color:#000000;
						}
						.logo{
						    margin-bottom:20px;
						}
						.content-box{
						    padding-top:0px;
						    padding-left:30px;
						    padding-right:30px;
						    background:#fff;
						    width:94.7%;
						}
						.content{
						    padding-top:10px;
						    padding-bottom:10px;
						    padding-left:30px;
						    padding-right:30px;
						}
						a{
						    cursor:pointer;
						    color:#8f3248;
						}
						btn{
						    cursor:pointer;
						}
					</style>
				</head>
				<body>
				    <div class="red-head">
				        <div class="white-head">
				            <img src="<?php echo base_url() ?>assets/img/email/logo.png" alt="" class="logo">
				            <h1>New Order\'s</h1>
				        </div>
				    </div>
				    <div class="content-box">
				        <div class="content">
				            <h1>Hai '.$vendor.'!</h1>
				            <span style="font-size:14px">Ada pesanan baru dari pelanggan setiamu segera check di obalihara.com dan di proses ya</span>
				        </div>
				        <div class="content" style="margin-top:30px;font-size:14px">
				            <span>Kamu sekarang sudah bisa menambahkan produk di toko kamu.<br><br>
				            <span>Silakan cek kembali akun kamu. Selalu jaga keamanan dan kerahasiaan <br>akun Obalihara kamu.</span><br><br>
				            <span>Terima kasih atas perhatiannya.</span>
				        </div>
				        <div style="text-align:center; margin-top:50px">
				            <h1>Ikuti Kami</h1>
				            <span>Jadilah yang pertama tahu promo-promo terbaru Obalihara lewat media <br>sosial kami.</span>
				            <div style="margin-top:10px; text-align:center; width:180px;position:relative; margin:0 auto; padding-top:20px;">
				                <div style="background: #8F3248; width:26px;border-radius:15px;text-align:center;padding-top:7px;padding-bottom:7px; float:left; margin-right:10px;color:#fff;">
				                    <!-- <img src="<?php echo base_url() ?>assets/img/email/ig.png" alt=""> -->
				                    <i class="fa fa-instagram" aria-hidden="true"></i>
				                </div>
				                <div style="background: #8F3248; width:26px;border-radius:15px;text-align:center;padding-top:7px;padding-bottom:7px; float:left; margin-right:10px;color:#fff;">
				                    <!-- <img src="<?php echo base_url() ?>assets/img/email/fb.png" alt=""> -->
				                    <i class="fa fa-facebook" aria-hidden="true"></i>
				                </div>
				                <div style="background: #8F3248; width:26px;border-radius:15px;text-align:center;padding-top:7px;padding-bottom:7px; float:left; margin-right:10px;color:#fff;">
				                    <!-- <img src="<?php echo base_url() ?>assets/img/email/tw.png" alt=""> -->
				                    <i class="fa fa-twitter" aria-hidden="true"></i>
				                </div>
				                <div style="background: #8F3248; width:26px;border-radius:15px;text-align:center;padding-top:7px;padding-bottom:7px; float:left; margin-right:10px;color:#fff;">
				                    <!-- <img src="<?php echo base_url() ?>assets/img/email/in.png" alt=""> -->
				                    <i class="fa fa-linkedin" aria-hidden="true"></i>
				                </div>
				                <div style="background: #8F3248; width:26px;border-radius:15px;text-align:center;padding-top:7px;padding-bottom:7px; float:left;color:#fff;">
				                    <!-- <img src="<?php echo base_url() ?>assets/img/email/yt.png" alt=""> -->
				                    <i class="fa fa-youtube-play" aria-hidden="true"></i>
				                </div>
				                <div style="clear:both"></div>
				            </div>
				            <div style="text-align:center; margin-top:30px; margin-bottom:10px;">
				            Jl. Tegal Parang Utara No.14, RT.5/RW.4, Mampang<br>
				            Prpt., Kec. Mampang Prpt., Kota Jakarta Selatan,<br>
				            Daerah Khusus Ibukota Jakarta 12790<br>
				            </div>
				            <div style="text-align:center; margin-top:30px; margin-bottom:10px;color: #BBBBBB; text-align:center">
				            Copyright © 2019 Obalihara.com. All Rights Reserved
				            </div>
				        </div>
				    </div>
				</body>
			</html>
		';

		return $a;
	}

	function htmlPayNotice($p=[]){
		$a = '
			<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
			<html>
				<head>
					<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
					<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
					<style>
						@import url("https://fonts.googleapis.com/css?family=Heebo:400,500,700");
						@import url("https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css");
						body{
						  font-size: 12px;
						  background: #F5F5F9;
						  font-family: "Heebo" !important;
						  overflow-x: hidden;
						  color: #888888;
						  max-width:600px;
						}
						.red-head{
						    width:100%;
						    background:#8F3248;
						    padding-top:22px;
						    padding-left:14px;
						    padding-right:14px;
						    
						}
						.white-head{
						    border-top-left-radius:10px;
						    border-top-right-radius:10px;
						    text-align:center;
						    background:#fff;
						    padding-top:32px;
						    padding-bottom:10px;
						}
						.content-head{
						    font-size: 14px;
						}
						h1{
						    font-family: Heebo;
						    font-weight:600;
						    font-size: 20px;
						    line-height: 20px;
						    color:#000000;
						}
						.logo{
						    margin-bottom:20px;
						}
						.content-box{
						    padding-top:0px;
						    padding-left:30px;
						    padding-right:30px;
						    background:#fff;
						    width:94.7%;
						}
						.content{
						    padding-top:10px;
						    padding-bottom:10px;
						    padding-left:30px;
						    padding-right:30px;
						}
						a{
						    cursor:pointer;
						    color:#8f3248;
						}
						btn{
						    cursor:pointer;
						}
						.container{
							height: auto;
						    width: auto;
						    border: 1px solid #EBEBEB;
							border-radius: 10px;
						}
						.con-row{
							padding: 18px 30px;
							border-bottom: 1px solid #ebebeb;
						}
						.con-row label{
							display: block;
							font-family: Heebo;
							font-style: normal;
							font-weight: normal;
							font-size: 14px;
							line-height: 21px;
							margin-bottom: 5px;
						}
						.bp{
							font-family: Heebo;
							font-style: normal;
							font-weight: normal;
							font-size: 16px;
							line-height: 23px;
							color: #000;
						}
						.con-row .trans{
							color: #1d1d1d;
							font-family: Heebo;
							font-style: normal;
							font-weight: bold;
							font-size: 20px;
							line-height: 29px;
						}
						.instagram{
							font-family: Font Awesome 5 Brands;
							font-style: normal;
							font-weight: normal;
							font-size: 12px;
							line-height: 14px;
							/* identical to box height */

							text-align: center;

							color: #FFFFFF;
						}
					
					</style>
				</head>
				<body>
				    <div class="red-head">
				        <div class="white-head">
				            <img src="<?php echo base_url() ?>assets/img/email/logo.png" alt="" class="logo">
				            <h1>Saatnya Bayar Pesanan Kamu</h1>
				        </div>
				    </div>
				    <div class="content-box">
				        <div class="content">
				            <h1>Hai '.$p['nama_lengkap'].'</h1>
				            <span style="font-size:14px">Terima kasih sudah berbelanja di Bukalapak. Mohon lakukan pembayaran sebelum tagihan kamu kedaluwarsa.</span>
				        </div>
				        <div class="content" style="margin-top:5px;font-size:14px;padding-left: 0px;padding-right: 0px;">
				            <div class="container">
				            	<div class="con-row">
				            		<label>ID Transaksi</label>
				            		<span class="trans">'.$p['id_external'].'</span>
				            	</div>
				            	<div class="con-row">
				            		<label>Total Pembayaran</label>
				            		<span style="color:#8F3248;" class="trans">'.$p['amount'].'</span>
				            	</div>
				            	<div class="con-row">
				            		<label>Batas Pembayaran</label>
				            		<span class="bp">'.$p['date'].' WIB</span>
				            	</div>
				            	<div class="con-row">
				            		<label>Metode Pembayaran</label>
				            		<span class="bp">Virtual Account BCA</span>
				            	</div>
				            	<div class="con-row">
				            		<label>No. Virtual Account</label>
				            		<span class="trans">80008191390086389</span>
				            	</div>
				            </div>
				        </div>
				        <div style="text-align:center; margin-top:50px">
				            <h1>Ikuti Kami</h1>
				            <span>Jadilah yang pertama tahu promo-promo terbaru Obalihara lewat media <br>sosial kami.</span>
				            <div style="margin-top:10px; text-align:center; width:180px;position:relative; margin:0 auto; padding-top:20px;">
				                <div style="background: #8F3248; width:26px;border-radius:15px;text-align:center;padding-top:7px;padding-bottom:7px; float:left; margin-right:10px;color:#fff;">
				                    <!-- <img src="<?php echo base_url() ?>assets/img/email/ig.png" alt=""> -->
				                    <i class="fa fa-instagram" aria-hidden="true"></i>
				                </div>
				                <div style="background: #8F3248; width:26px;border-radius:15px;text-align:center;padding-top:7px;padding-bottom:7px; float:left; margin-right:10px;color:#fff;">
				                    <!-- <img src="<?php echo base_url() ?>assets/img/email/fb.png" alt=""> -->
				                    <i class="fa fa-facebook" aria-hidden="true"></i>
				                </div>
				                <div style="background: #8F3248; width:26px;border-radius:15px;text-align:center;padding-top:7px;padding-bottom:7px; float:left; margin-right:10px;color:#fff;">
				                    <!-- <img src="<?php echo base_url() ?>assets/img/email/tw.png" alt=""> -->
				                    <i class="fa fa-twitter" aria-hidden="true"></i>
				                </div>
				                <div style="background: #8F3248; width:26px;border-radius:15px;text-align:center;padding-top:7px;padding-bottom:7px; float:left; margin-right:10px;color:#fff;">
				                    <!-- <img src="<?php echo base_url() ?>assets/img/email/in.png" alt=""> -->
				                    <i class="fa fa-linkedin" aria-hidden="true"></i>
				                </div>
				                <div style="background: #8F3248; width:26px;border-radius:15px;text-align:center;padding-top:7px;padding-bottom:7px; float:left;color:#fff;">
				                    <!-- <img src="<?php echo base_url() ?>assets/img/email/yt.png" alt=""> -->
				                    <i class="fa fa-youtube-play" aria-hidden="true"></i>
				                </div>
				                <div style="clear:both"></div>
				            </div>
				            <div style="text-align:center; margin-top:30px; margin-bottom:10px;">
				            Jl. Tegal Parang Utara No.14, RT.5/RW.4, Mampang<br>
				            Prpt., Kec. Mampang Prpt., Kota Jakarta Selatan,<br>
				            Daerah Khusus Ibukota Jakarta 12790<br>
				            </div>
				            <div style="text-align:center; margin-top:30px; margin-bottom:10px;color: #BBBBBB; text-align:center">
				            Copyright © 2019 Obalihara.com. All Rights Reserved
				            </div>
				        </div>
				    </div>
				</body>
			</html>
		';

		return $a;
	}

	function htmlPaidNotice($p=[]){
		$a = '
			<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
			<html>
				<head>
					<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
					<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
					<style>
						@import url("https://fonts.googleapis.com/css?family=Heebo:400,500,700");
						@import url("https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css");
						body{
						  font-size: 12px;
						  background: #F5F5F9;
						  font-family: "Heebo" !important;
						  overflow-x: hidden;
						  color: #888888;
						  max-width:600px;
						}
						.red-head{
						    width:100%;
						    background:#8F3248;
						    padding-top:22px;
						    padding-left:14px;
						    padding-right:14px;
						    
						}
						.white-head{
						    border-top-left-radius:10px;
						    border-top-right-radius:10px;
						    text-align:center;
						    background:#fff;
						    padding-top:32px;
						    padding-bottom:10px;
						}
						.content-head{
						    font-size: 14px;
						}
						h1{
						    font-family: Heebo;
						    font-weight:600;
						    font-size: 20px;
						    line-height: 20px;
						    color:#000000;
						}
						.logo{
						    margin-bottom:20px;
						}
						.content-box{
						    padding-top:0px;
						    padding-left:30px;
						    padding-right:30px;
						    background:#fff;
						    width:94.7%;
						}
						.content{
						    padding-top:10px;
						    padding-bottom:10px;
						    padding-left:30px;
						    padding-right:30px;
						}
						a{
						    cursor:pointer;
						    color:#8f3248;
						}
						btn{
						    cursor:pointer;
						}
						.container{
							height: auto;
						    width: auto;
						    border: 1px solid #EBEBEB;
							border-radius: 10px;
						}
						.con-row{
							padding: 18px 30px;
							border-bottom: 1px solid #ebebeb;
						}
						.con-row label{
							display: block;
							font-family: Heebo;
							font-style: normal;
							font-weight: normal;
							font-size: 14px;
							line-height: 21px;
							margin-bottom: 5px;
						}
						.bp{
							font-family: Heebo;
							font-style: normal;
							font-weight: normal;
							font-size: 16px;
							line-height: 23px;
							color: #000;
						}
						.con-row .trans{
							color: #1d1d1d;
							font-family: Heebo;
							font-style: normal;
							font-weight: bold;
							font-size: 20px;
							line-height: 29px;
						}
						.instagram{
							font-family: Font Awesome 5 Brands;
							font-style: normal;
							font-weight: normal;
							font-size: 12px;
							line-height: 14px;
							/* identical to box height */

							text-align: center;

							color: #FFFFFF;
						}
					
					</style>
				</head>
				<body>
				    <div class="red-head">
				        <div class="white-head">
				            <img src="<?php echo base_url() ?>assets/img/email/logo.png" alt="" class="logo">
				            <h1>Saatnya Bayar Pesanan Kamu</h1>
				        </div>
				    </div>
				    <div class="content-box">
				        <div class="content">
				            <h1>Hai '.$p['nama_lengkap'].'</h1>
				            <span style="font-size:14px">Terimakasih kami telah menerima pembayaran kamu dengan nomor '.$p['id_external'].'</span>
				        </div>
				        <div class="content" style="margin-top:5px;font-size:14px;padding-left: 0px;padding-right: 0px;">
				            <div class="container">
				            	<div class="con-row">
				            		<label>Total Pembayaran</label>
				            		<span style="color:#8F3248;" class="trans">'.$p['amount'].'</span>
				            	</div>
				            	<div class="con-row">
				            		<label>Metode Pembayaran</label>
				            		<span class="bp">Virtual Account BCA</span>
				            	</div>
				            	<div class="con-row">
				            		<label>No. Virtual Account</label>
				            		<span class="trans">80008191390086389</span>
				            	</div>
				            </div>
				        </div>
				        <div style="text-align:center; margin-top:50px">
				            <h1>Ikuti Kami</h1>
				            <span>Jadilah yang pertama tahu promo-promo terbaru Obalihara lewat media <br>sosial kami.</span>
				            <div style="margin-top:10px; text-align:center; width:180px;position:relative; margin:0 auto; padding-top:20px;">
				                <div style="background: #8F3248; width:26px;border-radius:15px;text-align:center;padding-top:7px;padding-bottom:7px; float:left; margin-right:10px;color:#fff;">
				                    <!-- <img src="<?php echo base_url() ?>assets/img/email/ig.png" alt=""> -->
				                    <i class="fa fa-instagram" aria-hidden="true"></i>
				                </div>
				                <div style="background: #8F3248; width:26px;border-radius:15px;text-align:center;padding-top:7px;padding-bottom:7px; float:left; margin-right:10px;color:#fff;">
				                    <!-- <img src="<?php echo base_url() ?>assets/img/email/fb.png" alt=""> -->
				                    <i class="fa fa-facebook" aria-hidden="true"></i>
				                </div>
				                <div style="background: #8F3248; width:26px;border-radius:15px;text-align:center;padding-top:7px;padding-bottom:7px; float:left; margin-right:10px;color:#fff;">
				                    <!-- <img src="<?php echo base_url() ?>assets/img/email/tw.png" alt=""> -->
				                    <i class="fa fa-twitter" aria-hidden="true"></i>
				                </div>
				                <div style="background: #8F3248; width:26px;border-radius:15px;text-align:center;padding-top:7px;padding-bottom:7px; float:left; margin-right:10px;color:#fff;">
				                    <!-- <img src="<?php echo base_url() ?>assets/img/email/in.png" alt=""> -->
				                    <i class="fa fa-linkedin" aria-hidden="true"></i>
				                </div>
				                <div style="background: #8F3248; width:26px;border-radius:15px;text-align:center;padding-top:7px;padding-bottom:7px; float:left;color:#fff;">
				                    <!-- <img src="<?php echo base_url() ?>assets/img/email/yt.png" alt=""> -->
				                    <i class="fa fa-youtube-play" aria-hidden="true"></i>
				                </div>
				                <div style="clear:both"></div>
				            </div>
				            <div style="text-align:center; margin-top:30px; margin-bottom:10px;">
				            Jl. Tegal Parang Utara No.14, RT.5/RW.4, Mampang<br>
				            Prpt., Kec. Mampang Prpt., Kota Jakarta Selatan,<br>
				            Daerah Khusus Ibukota Jakarta 12790<br>
				            </div>
				            <div style="text-align:center; margin-top:30px; margin-bottom:10px;color: #BBBBBB; text-align:center">
				            Copyright © 2019 Obalihara.com. All Rights Reserved
				            </div>
				        </div>
				    </div>
				</body>
			</html>
		';

		return $a;
	}

}
