<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH."libraries/AdminController.php");

class Auth extends FrontController {

	function __construct()    
	{
		parent::__construct();
		$this->load->model('home_model','HM');
	}

	public function index()
	{
		
		$data = [
			'msg' => ''
		];

		if($_POST){
			if(in_array("",$_POST)){
				$data = ['msg' => ['m' => 'Password or Username is empty' , 'con' => 'danger' , 't' => 'Failed!'] ];
			} else {

				$cpUser = $this->db->get_where('cp_app_user',[
					'username' => $_POST['user'],
					'user_password' => md5($_POST['password']),
				]);

				if(isset($d) && $cpUser->num_rows() == 0){
					$data = ['msg' => ['m' => 'Password or Username doesn\'t match' , 'con' => 'danger' , 't' => 'Failed!'] ];
				} else {
					$d = $cpUser->row();
					$group = $this->db->get_where("cp_app_acl_group",array(
							"ag_id" => $d->user_group
						))->row();
					// d($d);
					$this->jCfg['is_login'] 			= 1;
					$this->jCfg['user']['group'] 		= $d->user_group;
					$this->jCfg['user']['id'] 			= $d->user_id;
					$this->jCfg['user']['name']			= $d->username;
					$this->jCfg['user']['fullname'] 	= getUser($d->user_id,'nama_lengkap');
					$this->jCfg['user']['login_tipe']	= "";
					// $this->jCfg['user']['is_all']	= $d->is_show_all;	
					$this->jCfg['user']['photo'] 		= getOutletByUser($d->user_id,'photo');
					$this->jCfg['user']['level'] 		= !empty($group)?$group->ag_group_name:'';
					$this->jCfg['chat_online'] 			= array(
						"userid_chat_online"			=> $d->user_id,
						"username_chat_online"			=> $d->username
					);
											
					$this->_releaseSession();
					
					$this->db->update("cp_app_user",array(
						'user_logindate' => date("Y-m-d H:i:s")
					),array(
						'user_id' => $d->user_id
					));	
					// d($this->jCfg);
					if( $this->jCfg['referer'] != '' && $this->jCfg['referer'] != 'chat'){
						redirect($this->jCfg['referer'],'refresh');
					}else{
						redirect('/dashboard','refresh');
					}
					// $data =['msg' => ['m' => 'it\'s a match' , 'con' => 'success' , 't' => 'Yaaay!'] ];
				}
				
			}
		} 

		$this->load->view('auth/login',$data);
	}


	function out(){
		$this->jCfg['user']['id'] 		= '';
		$this->jCfg['user']['fullname'] = 'Guest';
		$this->jCfg['user']['name'] 	= 'guest';
		$this->jCfg['user']['photo'] 	= '';
		$this->jCfg['user']['level'] 	= '';
		$this->jCfg['user']['ujian_type'] = '';
		$this->jCfg['user']['access'] 	= array();
		$this->jCfg['menu'] 			= array();
		$this->jCfg['is_login'] 		= 0;
		$this->jCfg['user']['is_all']	= 0;
		$this->jCfg['chat_online']		= array();
		$this->jCfg['referer']			= "";
		$this->jCfg['user']['login_tipe'] = "";
		$this->_releaseSession();
		redirect(base_url());
	}
}
