<?= plugin_assets('dropzone/min/dropzone.min.css','b','css') ?>
<?= plugin_assets('dropzone/min/dropzone.min.js','b') ?>
<body>
  <section>
     <div class="container">
       <div class="row">
         <div class="col-md-5 text-left">
           <h3>Vendor Dashboard</h3>
            <div class="">
               <p>Selamat datang, Kelola setiap alur masuk dan keuar barang anda. Dashboard didesain mudah agar anda
                 bisa mentracking barang yang sedang anda jual</p>
            </div>
         </div>
         <div class="col-md-5 offset-md-2">
          <ul class="top-info-vendor clearfix">
            <li>
              <div>
                <i class="text-success fa fa-caret-up"></i> Total Earning
              </div>
              <div class="withdraw-total">
                <h5><?= myNum($earning->total-feePrice($earning->total,getOutlet($userId,'fee')),'Rp. ')?></h5>
              </div>
            </li>
            <li class="border-right">
              <div>
                <i class="text-danger fa fa-caret-down"></i> Total Commision
              </div>
              <div>
                <h5><?= myNum(feePrice($earning->total,getOutlet($userId,'fee')),'Rp. ')?></h5>
              </div>
            </li>
            <!-- <li class="border-right">
              <div>
                <i class="text-success fa fa-caret-up"></i> Total Earning
              </div>
              <div>
               <h5>Rp. 0</h5>
              </div>
            </li> -->
          </ul>
          <div class="text-right mt-1">
            <a href="#" style="font-weight:500" class="witdraw-btn">Withdraw Earning</a>
          </div>
         </div>
       </div>
     </div>
 </section>
 <section class="mt-5">
   <div class="container">
     <div class="head-background">
       <ul class="nav nav-tabs">
         <li class="nav-item">
           <a class="nav-link active" data-toggle="tab" href="#dashboard">
             <h6>Dashboard</h6>
           </a>
         </li>
         <li class="nav-item">
           <a class="nav-link" data-toggle="tab" href="#products">
             <h6>Product</h6>
           </a>
         </li>
         <li class="nav-item">
           <a class="nav-link" data-toggle="tab" href="#orders">
             <h6>Orders</h6>
           </a>
         </li>
         <li class="nav-item">
           <a class="nav-link" data-toggle="tab" href="#setting">
             <h6>Setting</h6>
           </a>
         </li>
       </ul>
     </div>
     <div class="tab-content mt-5">
        <div class="tab-pane active" id="dashboard">
            <h5>New Order-in (<?= count($orderIn)?>)</h5>
            <hr>
            <table class="table">
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Product Detail</th>
                  <th>Price</th>
                  <th>Qnty</th>
                  <th>Total Payment</th>
                  <th>Status</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php
                  foreach ($orderIn as $k => $v) {
                    $dis  = produk::disPrice(produk::detail($v->barang_id,'harga_satuan'),produk::detail($v->barang_id,'discount'));
                ?>
                <tr>
                  <td><?= date('M d, Y',strtotime($v->tanggal_checkout))?></td>
                  <td>
                    <div><a class="title-product" href="<?=base_url().'vendor/order_detail';?>"><?= produk::goods($v->barang_id,'nama_barang')?></a></div>
                    <div>Order Id : <?= getCheckoutbyCart($v->id,'invoice_code'); ?></div>
                    <div>Color : <b class="mr-2"><?= produk::warna($v->warna_id);?></b> Ukuran : <b class="mr-2"><?= ($v->ukuran_id!=0)?produk::ukuran($v->ukuran_id):'-';?></b> </div>
                    <div><b><?= getCheckoutdetailbyCart($v->id,'courier').', '.getCheckoutdetailbyCart($v->id,'courier_service') ?></b></div>
                  </td>
                  <td>
                    <span><?= myNum(produk::detail($v->barang_id,'harga_satuan'),'Rp. ')?></span><br/>
                    <?php if(produk::detail($v->barang_id,'discount') != 0){ ?>
                    <i>(<?= myNum($dis,'Rp. ')?>)</i>
                    <?php } ?>
                  </td>
                  <td><span><?= $v->qty;?></span></td>
                  <td>
                    <i>Discount Coupon : <?= myNum(getCheckoutbyCart($v->id,'disc'),'Rp. '); ?></i><br/>
                    <i>Delivery : <?= myNum(getCheckoutdetailbyCart($v->id,'courier_cost'),'Rp. '); ?></i><br />
                    <hr/>
                    <span><?= myNum(($dis*$v->qty) + getCheckoutdetailbyCart($v->id,'courier_cost'),'Rp. ')?></span>                    
                  </td>
                  <td>
                    <div><span class="text-success"><?= statusInvoice($v->status_invoice)?></span></div>
                    <div>Payment received</div>

                  </td>
                  <td>
                    <?php if($v->status_invoice == 2){ ?>
                    <div>
                      <button class="btn btn-shop processVendor" data-cartid="<?= $v->id; ?>"><i class="fa fa-send"></i> Process</button>
                    </div>
                    <?php } ?>
                    <div class="mt-2">
                      <a href="#" >View Details</a>
                    </div>
                  
                  </td>
                </tr> 
                <?php
                  }
                ?>
              </tbody>
            </table>
            <h5 class="mt-5">Recent Orders</h5>
            <a href="vendor-order.html" class="view-all">View all</a>
            <hr>
            <table class="table">
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Product Detail</th>
                  <th>Price</th>
                  <th>Qnty</th>
                  <th>Total Payment</th>
                  <th>Status</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <?php
                  foreach ($orderRecent as $k => $v) {
                    $dis  = produk::disPrice(produk::detail($v->barang_id,'harga_satuan'),produk::detail($v->barang_id,'discount'));
                ?>
                <tr>
                  <td><?= date('M d, Y',strtotime($v->tanggal_checkout))?></td>
                  <td>
                    <div><a class="title-product" href="<?=base_url().'vendor/order_detail';?>"><?= produk::goods($v->barang_id,'nama_barang')?></a></div>
                    <div>Order Id : <?= getCheckoutbyCart($v->id,'invoice_code'); ?></div>
                    <div>Color : <b class="mr-2"><?= produk::warna($v->warna_id);?></b> Ukuran : <b class="mr-2"><?= ($v->ukuran_id!=0)?produk::ukuran($v->ukuran_id):'-';?></b> </div>
                    <div><b><?= getCheckoutdetailbyCart($v->id,'courier').', '.getCheckoutdetailbyCart($v->id,'courier_service') ?></b></div>
                  </td>
                  <td>
                    <span><?= myNum(produk::detail($v->barang_id,'harga_satuan'),'Rp. ')?></span>
                    <?php if(produk::detail($v->barang_id,'discount') != 0){ ?>
                    <i>(<?= myNum($dis,'Rp. ')?>)</i>
                    <?php } ?>
                  </td>
                  <td><span><?= $v->qty;?></span></td>
                  <td>
                    <i>Discount Coupon : <?= myNum(getCheckoutbyCart($v->id,'disc'),'Rp. '); ?></i><br/>
                    <i>Delivery : <?= myNum(getCheckoutdetailbyCart($v->id,'courier_cost'),'Rp. '); ?></i><br />
                    <hr/>
                    <span><?= myNum(($dis*$v->qty) + getCheckoutdetailbyCart($v->id,'courier_cost'),'Rp. ')?></span>                      
                  </td>
                  <td>
                    <div><span class="text-success"><?= statusInvoice($v->status_invoice)?></span></div>
                    <div>Payment received</div>

                  </td>
                  <td>
                    <?php if($v->status_invoice == 3){ ?>
                    <div>
                      <button class="btn btn-shop btnSend" data-cartid="<?= $v->id; ?>" data-toggle="modal" data-target="#resiModal"><i class="fa fa-send"></i> Send</button>
                    </div>
                    <?php } ?>
                    <div class="mt-2">
                      <a href="#" >View Details</a>
                    </div>
                  
                  </td>
                </tr> 
                <?php
                  }
                ?>
              </tbody>
              
            </table>
            <h5 class="mt-5">Sales Report</h5>
            <hr>
            <form action="">
              <div class="row">
                <div class="col-md-3">
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text input-group-text-date" id="inputGroup-sizing-default">From</span>
                    </div>
                    <input type="text" class="form-control date" aria-label="Default"
                      aria-describedby="inputGroup-sizing-default" data-provide="datepicker">
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text input-group-text-date" id="inputGroup-sizing-default">To</span>
                    </div>
                    <input type="text" class="form-control date" aria-label="Default"
                      aria-describedby="inputGroup-sizing-default" data-provide="datepicker">
                  </div>
                </div>
                <div class="col-md-2">
                   <div class="input-group mb-3">
                     <button class="btn btn-shop"><i class="fa fa-retweet"></i> Update Information</button>
                   </div>
                  
                </div>
              </div>
            </form>
            <table class="data-table table">
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Product Detail</th>
                  <th>Price</th>
                  <th>Commision</th>
                  <th>Rate</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  foreach ($salesReport as $k => $v) {
                    $dis  = produk::disPrice(produk::detail($v->barang_id,'harga_satuan'),produk::detail($v->barang_id,'discount'));
                    $tp   = (getCheckoutdetailbyCart($v->id,'total') ) + getCheckoutdetailbyCart($v->id,'courier_cost');
                    $fee  = feePrice(getCheckoutdetailbyCart($v->id,'total'),getOutlet($v->vendor_id,'fee'));
                ?>
                <tr>
                  <td><?= date('M d, Y',strtotime($v->tanggal_shipped))?><br /><?= date('G : i',strtotime($v->tanggal_shipped))?></td>
                  <td>
                    <div class="row">
                      <div class="col-md-3">
                        <img src="http://placehold.it/70x70" alt="">
                      </div>
                      <div class="col-md-9">
                        <a class="title-product" href="<?=base_url().'vendor/order_detail';?>"><?= produk::goods($v->barang_id,'nama_barang')?></a>
                        <div>Order Id : <?= getCheckoutbyCart($v->id,'invoice_code'); ?></div>
                        <div>Color : <b class="mr-2"><?= produk::warna($v->warna_id);?></b> Ukuran : <b class="mr-2"><?= ($v->ukuran_id!=0)?produk::ukuran($v->ukuran_id):'-';?></b> </div>
                        <div><b><?= getCheckoutdetailbyCart($v->id,'courier').', '.getCheckoutdetailbyCart($v->id,'courier_service') ?></b></div>
                      </div>
                    </div>
                  </td>
                  <td>
                    <span><?= myNum(produk::detail($v->barang_id,'harga_satuan'),'Rp. ')?></span><br />
                    <hr/>
                    <i>Discount By Vendor : <?= myNum((produk::detail($v->barang_id,'harga_satuan') - $dis),'Rp. -')?></i><br/>
                    <i>Delivery Fee : <?= myNum(getCheckoutdetailbyCart($v->id,'courier_cost'),'Rp. '); ?></i><br/>
                    <i>Total Payment : <?= myNum($tp,'Rp. ')?></i>
                    
                  </td>
                  <td><span><?= myNum($fee,'Rp. ');?></span></td>
                  <td><?= getOutlet($v->vendor_id,'fee').'%'?></td>
                </tr>
                <?php
                  }
                ?>
              </tbody>
            </table>
        </div>
        <div class="tab-pane" id="products">
           <div style="display: hidden;" id="errorMsg"><?php (isset($_GET['msg']))?$_GET['msg']:''?></div>
           <h5 class="mt-5"><?= count($product) ?> Products</h5>
           <hr>
           <form action="">
             <div class="row">
               <div class="col-md-2">
                 <div class="input-group mb-3">
                   <a class="btn btn-shop" href="<?=base_url().'vendor/addProduct?id='.urlencode(codeEncrypt($this->jCfg['client']['id']));?>"><i class="fa fa-plus"></i> Add Product</a>
                 </div>

               </div>
             </div>
           </form>
           <table class="data-table table">
             <thead>
               <tr>
                 <th></th>
                 <th>Product Detail</th>
                 <th>Price</th>
                 <th>Categories</th>
                 <th>Date</th>
                 <th>Status</th>
               </tr>
             </thead>
             <tbody>
                <?php
                  foreach ($product as $k => $v) {
                ?>
                <tr>
                  <td><img src="<?= produk::gambar($v->id,'thumb')?>" alt="" width="70" height="70"></td>
                  <td>
                    <div><a class="title-product" href="<?= base_url().'detail/pd?id='.urlencode(codeEncrypt($v->id)).'&&'.'title='.url_title($v->nama_barang); ?>"><?= $v->nama_barang?></a></div>
                    <div class="text-grey">ID : MSDAD132</div>
                    <div><a class="mr-2 text-black" href="<?=base_url().'vendor/ep?id='.urlencode(codeEncrypt($v->id));?>">Edit</a>
                      <a href="#" class=" text-black delProduct" data-id="<?= $v->id ?>" >Delete</a></div>
                  </td>
                  <td><span><?= myNum($v->harga_satuan,'Rp')?></span></td>
                  <td><span><?= produk::catList($v->kategori_barang,true); ?></span></td>
                  <td><span>Published</span><br><span><?= date('F d, Y',strtotime($v->date_publish))?></span></td>
                  <td><?= ($v->is_stock == 1)?'<span class="text-success">In Stock</span>':'<span class="text-danger">Out Of Stock</span>'?></td>
                </tr>
                <?php
                  }
                ?>
              </tbody>
           </table>
        </div>
        <div class="tab-pane" id="orders">
          <h5>New Order-in (<?= count($orderIn)?>)</h5>
          <hr>
          <table class="table">
            <thead>
              <tr>
                <th>Date</th>
                <th>Product Detail</th>
                <th>Price</th>
                <th>Qnty</th>
                <th>Total Payment</th>
                <th>Status</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <?php
                foreach ($orderIn as $k => $v) {
                  $dis  = produk::disPrice(produk::detail($v->barang_id,'harga_satuan'),produk::detail($v->barang_id,'discount'));
              ?>
                <tr>
                  <td><?= date('M d, Y',strtotime($v->tanggal_checkout))?></td>
                  <td>
                    <div><a class="title-product" href="<?=base_url().'vendor/order_detail';?>"><?= produk::goods($v->barang_id,'nama_barang')?></a></div>
                    <div>Order Id : <?= getCheckoutbyCart($v->id,'invoice_code'); ?></div>
                    <div>Color : <b class="mr-2"><?= produk::warna($v->warna_id);?></b> Ukuran : <b class="mr-2"><?= ($v->ukuran_id!=0)?produk::ukuran($v->ukuran_id):'-';?></b> </div>
                    <div><b><?= getCheckoutdetailbyCart($v->id,'courier').', '.getCheckoutdetailbyCart($v->id,'courier_service') ?> Reguler</b></div>
                    </td>
                  <td>
                    <span><?= myNum(produk::detail($v->barang_id,'harga_satuan'),'Rp. ')?></span>
                    <?php if(produk::detail($v->barang_id,'discount') != 0){ ?>
                    <i>(<?= myNum($dis,'Rp. ')?>)</i>
                    <?php } ?>
                  </td>
                  <td><span><?= $v->qty;?></span></td>
                  <td>
                    <i>Discount Coupon : <?= myNum(getCheckoutbyCart($v->id,'disc'),'Rp. '); ?></i><br/>
                    <i>Delivery : <?= myNum(getCheckoutdetailbyCart($v->id,'courier_cost'),'Rp. '); ?></i><br />
                    <hr/>
                    <span><?= myNum(($dis*$v->qty) + getCheckoutdetailbyCart($v->id,'courier_cost'),'Rp. ')?></span>                      
                  </td>
                  <td>
                    <div><span class="text-success"><?= statusInvoice($v->status_invoice)?></span></div>
                    <div>Payment received</div>

                  </td>
                  <td>
                    <?php if($v->status_invoice == 2){ ?>
                    <div>
                      <button class="btn btn-shop processVendor" data-cartid="<?= $v->id; ?>"><i class="fa fa-send"></i> Process</button>
                    </div>
                    <?php } ?>
                    <div class="mt-2">
                      <a href="#" >View Details</a>
                    </div>
                  
                  </td>
                </tr> 
              <?php
                }
              ?>
            </tbody>
          </table>
          <h5 class="mt-5">Recent Orders</h5>
          <hr>
          <table class="table">
            <thead>
              <tr>
              <th>Date</th>
              <th>Product Detail</th>
              <th>Price</th>
              <th>Qnty</th>
              <th>Total Payment</th>
              <th>Status</th>
              <th></th>
            </tr>
            </thead>
            <tbody>
              <?php
                foreach ($orderRecent as $k => $v) {
                  $dis  = produk::disPrice(produk::detail($v->barang_id,'harga_satuan'),produk::detail($v->barang_id,'discount'));
              ?>
              <tr>
                <td><?= date('M d, Y',strtotime($v->tanggal_checkout))?></td>
                <td>
                  <div><a class="title-product" href="<?=base_url().'vendor/order_detail';?>"><?= produk::goods($v->barang_id,'nama_barang')?></a></div>
                  <div>Order Id : <?= getCheckoutbyCart($v->id,'invoice_code'); ?></div>
                  <div>Color : <b class="mr-2"><?= produk::warna($v->warna_id);?></b> Ukuran : <b class="mr-2"><?= ($v->ukuran_id!=0)?produk::ukuran($v->ukuran_id):'-';?></b> </div>
                  <div><b><?= getCheckoutdetailbyCart($v->id,'courier').', '.getCheckoutdetailbyCart($v->id,'courier_service') ?> Reguler</b></div>
                </td>
                <td>
                  <span><?= myNum(produk::detail($v->barang_id,'harga_satuan'),'Rp. ')?></span>
                  <?php if(produk::detail($v->barang_id,'discount') != 0){ ?>
                  <i>(<?= myNum($dis,'Rp. ')?>)</i>
                  <?php } ?>
                </td>
                <td><span><?= $v->qty;?></span></td>
                <td>
                  <i>Discount Coupon : <?= myNum(getCheckoutbyCart($v->id,'disc'),'Rp. '); ?></i><br/>
                  <i>Delivery : <?= myNum(getCheckoutdetailbyCart($v->id,'courier_cost'),'Rp. '); ?></i><br />
                  <hr/>
                  <span><?= myNum(($dis*$v->qty) + getCheckoutdetailbyCart($v->id,'courier_cost'),'Rp. ')?></span>                      
                </td>
                <td>
                  <div><span class="text-success"><?= statusInvoice($v->status_invoice)?></span></div>
                  <div>Payment received</div>

                </td>
                <td>
                <?php if($v->status_invoice == 3){ ?>
                <div>
                  <button class="btn btn-shop btnSend" data-cartid="<?= $v->id; ?>" data-toggle="modal" data-target="#resiModal"><i class="fa fa-send"></i> Send</button>
                </div>
                <?php } ?>
                <div class="mt-2">
                  <a href="#" class="btnTracking" data-status="<?= $v->status_invoice ?>" data-cartid="<?= $v->id ?>" data-courier="<?= getCheckoutdetailbyCart($v->id,'courier')?>" data-receipt="<?= $v->resi;?>">View Details</a>
                </div>

                </td>
              </tr> 
              <?php
                }
              ?>
            </tbody>

          </table>
          <h5 class="mt-5"><?= count($orderShipped)?> Order Shipped</h5>
          <hr>
            <table class="table data-table">
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Product Detail</th>
                  <th>Customer</th>
                  <th>Total Payment</th>
                  <th>Status</th>
                  <th></th>
                </tr>
              </thead>
            <tbody>
            <?php
              foreach ($orderShipped as $k => $v) {
                $dis  = produk::disPrice(produk::detail($v->barang_id,'harga_satuan'),produk::detail($v->barang_id,'discount'));
            ?>
            <tr>
              <td><?= date('M d, Y',strtotime($v->tanggal_checkout))?></td>
              <td>
                <div><a class="title-product" href="<?=base_url().'vendor/order_detail';?>"><?= produk::goods($v->barang_id,'nama_barang')?></a></div>
                  <div>Order Id : <?= getCheckoutbyCart($v->id,'invoice_code'); ?></div>
                  <div>Color : <b class="mr-2"><?= produk::warna($v->warna_id);?></b> Ukuran : <b class="mr-2"><?= ($v->ukuran_id!=0)?produk::ukuran($v->ukuran_id):'-';?></b> </div>
                  <div><b><?= getCheckoutdetailbyCart($v->id,'courier').', '.getCheckoutdetailbyCart($v->id,'courier_service') ?> Reguler</b></div>
              </td>
              <td><span><?= getUser($v->user_id,'nama_lengkap')?></span></td>
              <td>
                <i>Price : <?= myNum(produk::detail($v->barang_id,'harga_satuan'),'Rp. ')?></i><?= (produk::detail($v->barang_id,'discount') != 0)?' <i>('.myNum($dis,'Rp.').')</i><br/>':'<br/>'?>
                <i>Discount : <?= myNum(getCheckoutbyCart($v->id,'disc'),'Rp. '); ?></i><br/>
                <i>Delivery : <?= myNum(getCheckoutdetailbyCart($v->id,'courier_cost'),'Rp. '); ?></i><br />
                <i>Qty : <?= $v->qty; ?> Units</i>
                <hr/>
                <span><?= myNum(($dis*$v->qty) + getCheckoutdetailbyCart($v->id,'courier_cost'),'Rp. ')?></span>
                
              </td>
              <td>
                <div><span class="text-success"><?= statusInvoice($v->status_invoice)?></span></div>
                <?= ($v->status_invoice == 3)? '<div>Order Has Been Shipped</div>':'';?>
                <div>No. Resi : <?= ( $v->resi != '')?$v->resi:'-'; ?></div>
              </td>
              <td>
                <div class="mt-2">
                  <a href="#" class="btnTracking" data-status="<?= $v->status_invoice ?>" data-cartid="<?= $v->id ?>" data-courier="<?= getCheckoutdetailbyCart($v->id,'courier')?>" data-receipt="<?= $v->resi;?>">View Details</a>
                </div>

              </td>
            </tr>
            <?php
              }
            ?>
            </tbody>
          </table>
        </div>
       <div class="tab-pane" id="setting">
        <h5>Setting</h5>
        <hr> 
        <div class="row">
          <div class="col-md-3">
            <div class="card">
              <span style="font-weight:500">MAIN MENU</span>
              <ul class="menu-vendor-setting mt-3">
                <li>
                 <a href="#">Your Shop</a> <i class="fa fa-angle-right"></i>
                </li>
                <li>
                  <a href="<?= base_url().'vendor/sm?id='.urlencode(codeEncrypt(getOutlet($userId,'user_id'))).'#setting'?>">Shipping Methode</a> <i class="fa fa-angle-right"></i>
                </li>
                <!-- <li>
                  <a href="#">Withdraw Payment</a> <i class="fa fa-angle-right"></i>
                </li> -->
              </ul>
            </div>
          </div>
          <div class="col-md-9">
            <div class="card">
              <h5><?= isset($setting)?$setting->nama_lengkap:'';?></h5>
             <!--  <div class="col-md-3">
                <div id="body-overlay">
                  <div><img src="<?= base_url()?>assets/img/loading.gif" width="64px" height="64px" /></div>
                </div>
                <div class="bgColor">
                 
                  <div id="targetOuter">
                    <div id="targetLayer"></div>
                    <i class="fa fa-camera"></i>
                    <div class="icon-choose-image">
                      <input name="userImage" id="userImage" type="file" class="inputFile"
                        onChange="showPreview(this);" />
                    </div>
                  </div>
                  <div>
                    <button class="btn btn-shop w-100">Upload</button>
                  </div>

                </div>
              </div> -->
              <?php 
                if(isset($_GET['msg'])) { 
                    $message = $_GET['msg'];
                    echo "<script type='text/javascript'>alert('$message');</script>";
                } 
              ?>
              <div class="row">
                <div class="col-md-3">
                  <div id="dropzone">
                    <form action="<?= base_url().'front/vendor/saveVendorPhoto'?>" class="dropzone needsclick" id="demoUpload1">
                      <input type="hidden" name="id" value="<?= getOutletByUser($this->jCfg['client']['id'],'id') ?>">
                      <input type="hidden" name="user_id" value="<?= $this->jCfg['client']['id'] ?>">
                      <div class="dz-message needsclick">
                        Drop files <b>here</b> or <b>click</b> to upload.<br />
                        <span class="dz-note needsclick">
                            (This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)
                        </span>
                      </div>
                    </form>
                  </div>
                </div>
                <div class="col-md-3">
                  <?php if(isset($setting->photo)){ ?>
                  <div class="col"><img src="<?= get_image(base_url()."assets/img/vendor/small/".$setting->photo); ?>" width="171.25" height="210" style="border-style: dashed;"/></div>
                  <?php } ?>
                </div>
                <form action="<?= base_url().'/front/vendor/saveSetting'?>" method="POST" enctype="multipart/form-data" id="settingForm">
                  
                  <div class="col-md-12">
                    <h6 class="mt-5 mb-2"><span class="required">*</span> Shop Name</h6>
                    <div class="form-group">
                      <small id="emailHelp" class="form-text text-muted">Your shop name is public and must unique</small>
                      <input type="text" name="nama_lengkap" class="form-control mt-2" id="exampleInputEmail1" aria-describedby="emailHelp" autocomplete="off"
                        placeholder="Shop name" value="<?= isset($setting)?$setting->nama_lengkap:'';?>">
                      <input type="hidden" name="user_id" value="<?= $_GET['id'] ?>">
                    </div>
                    <h6 class=" mb-2"><span class="required">*</span> Bank Account Number</h6>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-6">
                          <select class="form-control" name="bank">
                            <option value="0">- Pilih Bank -</option>
                            <option value="bni" <?= isset($setting) && $setting->bank == 'bni' ? 'selected':''?> >BNI</option>
                            <option value="bca" <?= isset($setting) && $setting->bank == 'bca' ? 'selected':''?> >BCA</option>
                            <option value="man" <?= isset($setting) && $setting->bank == 'man' ? 'selected':''?> >MANDIRI</option>
                          </select>
                        </div>
                        <div class="col-md-6">
                          <input type="text" name="no_rek" class="form-control" id="no_rek"
                            placeholder="Bank Account Number" value="<?= isset($setting)?$setting->no_rek:'';?>" 
                            <?= isset($setting) && $setting->no_rek != ''?'disabled':'';?> autocomplete="off">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <h6 class="mt-5 mb-4"><span class="required">*</span> Shop Address</h6>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Province</label>
                          <select name="provinsi_id" class="form-controll custom-select" id="provinsiID">
                            <?php
                              foreach ($province as $k => $v) {
                              $se = (isset($setting->provinsi_id) && $setting->provinsi_id==$v['province_id'])?'selected':'';
                              echo '<option value="'.$v['province_id'].'" '.$se.' >'.$v['province'].'</option>';
                             } 
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                         <label for="exampleInputEmail1">City</label>
                         <select name="kota_id" class="form-controll custom-select" id="kotaID" data-id="<?= isset($setting)?$setting->kota_id:'';?>"></select>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                         <label for="exampleInputEmail1">Subdistrict</label>
                         <select name="kecamatan_id" class="form-controll custom-select" id="kecID" data-id="<?= isset($setting)?$setting->kecamatan_id:'';?>"></select>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Postal code</label>
                          <input type="text" class="form-control" placeholder="Postal code" name="postcode" value="<?= isset($setting)?$setting->postcode:''; ?>" autocomplete="off">
                        </div>
                      </div>
                      <div class="col-md-9">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Address</label>
                          <input type="text" class="form-control" placeholder="Address" name="alamat" value="<?= isset($setting)?$setting->alamat:''; ?>" autocomplete="off">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <h6 class="mt-3 mb-4"><span class="required">*</span> Shop Information</h6>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Phone Nunmber</label>
                          <input type="text" class="form-control" placeholder="Phone number" name="telp" value="<?= isset($setting)?$setting->telp:''; ?>" autocomplete="off">
                        </div>
                      </div>
                      <div class="col-md-8">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Email Address</label>
                          <input type="email" class="form-control" placeholder="Email Adress" name="email" value="<?= isset($setting)?$setting->email:''; ?>" autocomplete="off">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <h6 class="mt-3 mb-4"><span class="required">*</span> Shop Description</h6>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <textarea  name="deskripsi" id="" cols="100" rows="40" class="form-control" style="height:200px"><?= isset($setting)?$setting->deskripsi:''; ?></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <h6 class="mt-3 mb-4"><span class="required">*</span> Social Media</h6>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Facebook</label>
                          <input type="text" class="form-control" name="facebook" placeholder="Facebook" value="<?= isset($setting)?$setting->facebook:'';?>">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Twitter</label>
                          <input type="text" class="form-control" name="twitter" placeholder="Twitter" value="<?= isset($setting)?$setting->twitter:'';?>">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Instagram</label>
                          <input type="text" class="form-control" name="instagram" placeholder="Instagram" value="<?= isset($setting)?$setting->instagram:'';?>">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12 text-right mt-4">
                    <a href="#" class="btn btn-shop" id="saveSetting">Save changes</a>
                  </div>
                </form>
              </div>              
            </div>
          </div>
        </div> 
       </div>
     </div>
   </div>
 </section>
</body>

<div class="modal fade" id="resiModal" tabindex="-1" role="dialog" aria-labelledby="resiModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="resiModalLabel">Masukan Nomor Resi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
            <div class="form-group">
              <input type="hidden" class="cartid">
              <input type="text" class="form-control resi" placeholder="No. Resi Ex: 350180012251019" >
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-shop saveResi" style="width: 100%">Update Resi</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="trekingModal" tabindex="-1" role="dialog" aria-labelledby="trekingModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content" style="height: 400px;">
      <div class="modal-header">
        <h5 class="modal-title" id="trekingModalLabel">Status Resi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="overflow-y: auto;">
        <input type="hidden" id="cartid">
        <div class="konsumen_kontak">
          <div class="nama_konsumen">Nama : <b></b></div>
          <div class="email_konsumen">Email : <b></b></div>
          <div class="phone_konsumen">Phone : <b></b></div>
        </div>
        <br/>
        <div>
          <b><i class="resiStatus"></i></b>
        </div>
        <br />
        <div class="col-md-12">
          <div class="manifest">
            <ul>
              <li class="title"></li>
              <li class="description"></li>
            </ul>           
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary btnTerima" style="background: #c1355e;">Terima Barang</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="witdrawModal" tabindex="-1" role="dialog" aria-labelledby="witdrawModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content" style="height: auto;">
      <div class="modal-header">
        <h5 class="modal-title" id="witdrawModalLabel">Detail Rekening</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="konsumen_kontak">
          <input type="hidden" class="vendorid" value="<?= getOutletByUser($this->jCfg['client']['id'],'id')?>">
          <input type="hidden" class="userid" value="<?= $this->jCfg['client']['id']?>">
          <div class="nama_vendor">Nama   : <b><?= getOutletByUser($this->jCfg['client']['id'],'nama_lengkap')?></b></div>
          <div class="email_vendor">Email : <b><?= getOutletByUser($this->jCfg['client']['id'],'email')?></b></div>
          <div class="bank_vendor">Bank : <b><?= strtoupper(getOutletByUser($this->jCfg['client']['id'],'bank'));?></b></div>
          <div class="rekening_vendor">Rekening : <b><?= getOutletByUser($this->jCfg['client']['id'],'no_rek')?></b></div>
          <div class="vendor_fee">Vendor Fee (Commision) : <b><?= myNum(feePrice($earning->total,getOutlet($userId,'fee')),'Rp. ')?></b></div>
          <div class="withdraw_fee">Withdraw Fee : <b>Rp. 5.500</b></div>
          <div class="withdraw_total">Total Withdraw : <b></b></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary withdraw-confirm" style="background: #c1355e;">Withdraw</button>
      </div>
    </div>
  </div>
</div>

<?= js_assets('vendor/vendor.setting.js','f'); ?>