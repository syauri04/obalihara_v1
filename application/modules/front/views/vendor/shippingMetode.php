<body>
 <section>
     <div class="container">
       <div class="row">
         <div class="col-md-5 text-left">
           <h3>Vendor Dashboard</h3>
            <div class="">
               <p>Selamat datang, Kelola setiap alur masuk dan keuar barang anda. Dashboard didesain mudah agar anda
                 bisa mentracking barang yang sedang anda jual</p>
            </div>
         </div>
         <div class="col-md-5 offset-md-2">
          <ul class="top-info-vendor clearfix">
            <li>
              <div>
                <i class="text-success fa fa-caret-up"></i> Total Earning
              </div>
              <div>
                <h5>Rp. 1.000.000</h5>
              </div>
            </li>
            <li class="border-right">
              <div>
                <i class="text-danger fa fa-caret-down"></i> This Week Earning
              </div>
              <div>
                <h5>Rp. 1.000.000</h5>
              </div>
            </li>
            <li class="border-right">
              <div>
                <i class="text-success fa fa-caret-up"></i> Total Earning
              </div>
              <div>
               <h5>Rp. 1.000.000</h5>
              </div>
            </li>
          </ul>
          <div class="text-right mt-1">
            <a href="#" style="font-weight:500">Withdraw Earning</a>
          </div>
         </div>
       </div>
     </div>
 </section>
 <section class="mt-5">
   <div class="container">
     <div class="head-background">
       <ul class="nav nav-tabs">
         <li class="nav-item">
           <a class="nav-link" href="<?= base_url().'vendors?id='.urlencode(codeEncrypt($userId)).'#dashboard'?>">
             <h6>Dashboard</h6>
           </a>
         </li>
         <li class="nav-item">
           <a class="nav-link" href="<?= base_url().'vendors?id='.urlencode(codeEncrypt($userId)).'#products'?>">
             <h6>Product</h6>
           </a>
         </li>
         <li class="nav-item">
           <a class="nav-link" href="<?= base_url().'vendors?id='.urlencode(codeEncrypt($userId)).'#orders'?>">
             <h6>Orders</h6>
           </a>
         </li>
         <li class="nav-item">
           <a class="nav-link" data-toggle="tab" href="#setting">
             <h6>Setting</h6>
           </a>
         </li>
       </ul>
     </div>
     <div class="tab-content mt-5">
       <div class="tab-pane active" id="dashboard">
          <h5>New Order-in (3)</h5>
          <hr>
          <table class="table">
            <thead>
              <tr>
                <th>Date</th>
                <th>Product Detail</th>
                <th>Price</th>
                <th>Qnty</th>
                <th>Total Payment</th>
                <th>Status</th>
                <th></th>
              </tr>
            </thead>
            <tr>
              <td>Okt 16, 2019</td>
              <td>
                <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
                <div>Order Id : #271894h</div>
                <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
                </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34</span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                <div><span class="text-success">Order-in</span></div>
                <div>Payment received</div>

              </td>
              <td>
                <div>
                  <a href="cart.html">
                    <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                  </a>
                </div>
                <div class="mt-2">
                  <a href="order-detail.html">View Details</a>
                </div>
              
              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td>
                <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
                <div>Order Id : #271894h</div>
                <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34</span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                <div><span class="text-danger">Pending</span></div>
                <div>Payment not received</div>

              </td>
              <td>
                <div>
                  <a href="cart.html">
                    <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                  </a>
                </div>
                <div class="mt-2">
                  <a href="order-detail.html">View Details</a>
                </div>

              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td>
                <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
                <div>Order Id : #271894h</div>
                <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34</span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                <div><span class="text-danger">Pending</span></div>
                <div>Payment not received</div>

              </td>
              <td>
                <div>
                  <a href="cart.html">
                    <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                  </a>
                </div>
                <div class="mt-2">
                  <a href="order-detail.html">View Details</a>
                </div>

              </td>
            </tr>
          </table>
          <h5 class="mt-5">Recent Orders</h5>
          <a href="vendor-order.html" class="view-all">View all</a>
          <hr>
          <table class="table">
            <thead>
              <tr>
                <th>Date</th>
                <th>Product Detail</th>
                <th>Price</th>
                <th>Qnty</th>
                <th>Total Payment</th>
                <th>Status</th>
                <th></th>
              </tr>
            </thead>
            <tr>
              <td>Okt 16, 2019</td>
              <td>
                <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
                <div>Order Id : #271894h</div>
                <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34</span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                <div><span class="text-success">Order-in</span></div>
                <div>Payment received</div>

              </td>
              <td>
                <div>
                  <a href="cart.html">
                    <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                  </a>
                </div>
                <div class="mt-2">
                  <a href="order-detail.html">View Details</a>
                </div>

              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td>
                <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
                <div>Order Id : #271894h</div>
                <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34</span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                <div><span class="text-danger">Pending</span></div>
                <div>Payment not received</div>

              </td>
              <td>
                <div>
                  <a href="cart.html">
                    <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                  </a>
                </div>
                <div class="mt-2">
                  <a href="order-detail.html">View Details</a>
                </div>

              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td>
                <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
                <div>Order Id : #271894h</div>
                <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34</span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                <div><span class="text-danger">Pending</span></div>
                <div>Payment not received</div>

              </td>
              <td>
                <div>
                  <a href="cart.html">
                    <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                  </a>
                </div>
                <div class="mt-2">
                  <a href="order-detail.html">View Details</a>
                </div>

              </td>
            </tr>
          </table>
          <h5 class="mt-5">Sales Report</h5>
          <hr>
          <form action="">
            <div class="row">
              <div class="col-md-3">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text input-group-text-date" id="inputGroup-sizing-default">From</span>
                  </div>
                  <input type="text" class="form-control date" aria-label="Default"
                    aria-describedby="inputGroup-sizing-default" data-provide="datepicker">
                </div>
              </div>
              <div class="col-md-3">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text input-group-text-date" id="inputGroup-sizing-default">To</span>
                  </div>
                  <input type="text" class="form-control date" aria-label="Default"
                    aria-describedby="inputGroup-sizing-default" data-provide="datepicker">
                </div>
              </div>
              <div class="col-md-2">
                 <div class="input-group mb-3">
                   <button class="btn btn-shop"><i class="fa fa-retweet"></i> Update Information</button>
                 </div>
                
              </div>
            </div>
          </form>
          <table class="data-table table">
            <thead>
              <tr>
                <th>Date</th>
                <th>Product Detail</th>
                <th>Price</th>
                <th>Sold</th>
                <th>Commision</th>
                <th>Rate</th>
              </tr>
            </thead>
            <tr>
              <td>Okt 16, 2019</td>
             <td><img src="http://placehold.it/70x70" alt=""> <a class="title-product" href="">Marshall Lorem Ipsum
                 dolor amet (149077)</a>
                </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34/<b>72</b></span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                23%
              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td><img src="http://placehold.it/70x70" alt=""> <a class="title-product" href="">Marshall Lorem Ipsum
                  dolor amet (149077)</a>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34/<b>72</b></span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                23%
              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td><img src="http://placehold.it/70x70" alt=""> <a class="title-product" href="">Marshall Lorem Ipsum
                  dolor amet (149077)</a>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34/<b>72</b></span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                23%
              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td><img src="http://placehold.it/70x70" alt=""> <a class="title-product" href="">Marshall Lorem Ipsum
                  dolor amet (149077)</a>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34/<b>72</b></span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                23%
              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td><img src="http://placehold.it/70x70" alt=""> <a class="title-product" href="">Marshall Lorem Ipsum
                  dolor amet (149077)</a>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34/<b>72</b></span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                23%
              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td><img src="http://placehold.it/70x70" alt=""> <a class="title-product" href="">Marshall Lorem Ipsum
                  dolor amet (149077)</a>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34/<b>72</b></span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                23%
              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td><img src="http://placehold.it/70x70" alt=""> <a class="title-product" href="">Marshall Lorem Ipsum
                  dolor amet (149077)</a>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34/<b>72</b></span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                23%
              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td><img src="http://placehold.it/70x70" alt=""> <a class="title-product" href="">Marshall Lorem Ipsum
                  dolor amet (149077)</a>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34/<b>72</b></span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                23%
              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td><img src="http://placehold.it/70x70" alt=""> <a class="title-product" href="">Marshall Lorem Ipsum
                  dolor amet (149077)</a>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34/<b>72</b></span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                23%
              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td><img src="http://placehold.it/70x70" alt=""> <a class="title-product" href="">Marshall Lorem Ipsum
                  dolor amet (149077)</a>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34/<b>72</b></span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                23%
              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td><img src="http://placehold.it/70x70" alt=""> <a class="title-product" href="">Marshall Lorem Ipsum
                  dolor amet (149077)</a>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34/<b>72</b></span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                23%
              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td><img src="http://placehold.it/70x70" alt=""> <a class="title-product" href="">Marshall Lorem Ipsum
                  dolor amet (149077)</a>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34/<b>72</b></span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                23%
              </td>
            </tr>
          </table>
       </div>
       <div class="tab-pane" id="products">
         <h5 class="mt-5">193 Products</h5>
         <hr>
         <form action="">
           <div class="row">
             <div class="col-md-2">
               <div class="input-group mb-3">
                 <a href="add-product.html"><button class="btn btn-shop"><i class="fa fa-plus"></i> Add Product</button></a>
               </div>

             </div>
           </div>
         </form>
         <table class="data-table table">
           <thead>
             <tr>
               <th></th>
               <th>Product Detail</th>
               <th>Price</th>
               <th>Categories</th>
               <th>Date</th>
               <th>Status</th>
             </tr>
           </thead>
           <tr>
             <td><img src="http://placehold.it/70x70" alt=""></td>
             <td>
               <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
               <div class="text-grey">ID : MSDAD132</div>
               <div><a class="mr-2 text-black" href="add-product.html">Edit</a><a class=" text-black"
                   href="add-product.html">Delete</a></div>
             </td>
             <td><span>Rp. 720.000</span></td>
             <td><span>Baju, Koko, Muslim</span></td>
             <td><span>Published</span><br><span>Oct 15, 2019</span></td>
             <td>
               <span class="text-success">In Stock</span>
             </td>
           </tr>
           <tr>
             <td><img src="http://placehold.it/70x70" alt=""></td>
             <td>
               <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
               <div class="text-grey">ID : MSDAD132</div>
               <div><a class="mr-2 text-black" href="add-product.html">Edit</a><a class=" text-black"
                   href="add-product.html">Delete</a></div>
             </td>
             <td><span>Rp. 720.000</span></td>
             <td><span>Baju, Koko, Muslim</span></td>
             <td><span>Published</span><br><span>Oct 15, 2019</span></td>
             <td>
               <span class="text-danger">Out of Stock</span>
             </td>
           </tr>
           <tr>
             <td><img src="http://placehold.it/70x70" alt=""></td>
             <td>
               <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
               <div class="text-grey">ID : MSDAD132</div>
               <div><a class="mr-2 text-black" href="add-product.html">Edit</a><a class=" text-black"
                   href="add-product.html">Delete</a></div>
             </td>
             <td><span>Rp. 720.000</span></td>
             <td><span>Baju, Koko, Muslim</span></td>
             <td><span>Published</span><br><span>Oct 15, 2019</span></td>
             <td>
               <span class="text-success">In Stock</span>
             </td>
           </tr>
           <tr>
             <td><img src="http://placehold.it/70x70" alt=""></td>
             <td>
               <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
               <div class="text-grey">ID : MSDAD132</div>
               <div><a class="mr-2 text-black" href="add-product.html">Edit</a><a class=" text-black"
                   href="add-product.html">Delete</a></div>
             </td>
             <td><span>Rp. 720.000</span></td>
             <td><span>Baju, Koko, Muslim</span></td>
             <td><span>Published</span><br><span>Oct 15, 2019</span></td>
             <td>
               <span class="text-danger">Out of Stock</span>
             </td>
           </tr>
           <tr>
             <td><img src="http://placehold.it/70x70" alt=""></td>
             <td>
               <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
               <div class="text-grey">ID : MSDAD132</div>
               <div><a class="mr-2 text-black" href="add-product.html">Edit</a><a class=" text-black"
                   href="add-product.html">Delete</a></div>
             </td>
             <td><span>Rp. 720.000</span></td>
             <td><span>Baju, Koko, Muslim</span></td>
             <td><span>Published</span><br><span>Oct 15, 2019</span></td>
             <td>
               <span class="text-success">In Stock</span>
             </td>
           </tr>
           <tr>
             <td><img src="http://placehold.it/70x70" alt=""></td>
             <td>
               <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
               <div class="text-grey">ID : MSDAD132</div>
               <div><a class="mr-2 text-black" href="add-product.html">Edit</a><a class=" text-black"
                   href="add-product.html">Delete</a></div>
             </td>
             <td><span>Rp. 720.000</span></td>
             <td><span>Baju, Koko, Muslim</span></td>
             <td><span>Published</span><br><span>Oct 15, 2019</span></td>
             <td>
               <span class="text-danger">Out of Stock</span>
             </td>
           </tr>
           <tr>
             <td><img src="http://placehold.it/70x70" alt=""></td>
             <td>
               <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
               <div class="text-grey">ID : MSDAD132</div>
               <div><a class="mr-2 text-black" href="add-product.html">Edit</a><a class=" text-black"
                   href="add-product.html">Delete</a></div>
             </td>
             <td><span>Rp. 720.000</span></td>
             <td><span>Baju, Koko, Muslim</span></td>
             <td><span>Published</span><br><span>Oct 15, 2019</span></td>
             <td>
               <span class="text-success">In Stock</span>
             </td>
           </tr>
           <tr>
             <td><img src="http://placehold.it/70x70" alt=""></td>
             <td>
               <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
               <div class="text-grey">ID : MSDAD132</div>
               <div><a class="mr-2 text-black" href="add-product.html">Edit</a><a class=" text-black"
                   href="add-product.html">Delete</a></div>
             </td>
             <td><span>Rp. 720.000</span></td>
             <td><span>Baju, Koko, Muslim</span></td>
             <td><span>Published</span><br><span>Oct 15, 2019</span></td>
             <td>
               <span class="text-danger">Out of Stock</span>
             </td>
           </tr>
           <tr>
             <td><img src="http://placehold.it/70x70" alt=""></td>
             <td>
               <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
               <div class="text-grey">ID : MSDAD132</div>
               <div><a class="mr-2 text-black" href="add-product.html">Edit</a><a class=" text-black"
                   href="add-product.html">Delete</a></div>
             </td>
             <td><span>Rp. 720.000</span></td>
             <td><span>Baju, Koko, Muslim</span></td>
             <td><span>Published</span><br><span>Oct 15, 2019</span></td>
             <td>
               <span class="text-success">In Stock</span>
             </td>
           </tr>
           <tr>
             <td><img src="http://placehold.it/70x70" alt=""></td>
             <td>
               <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
               <div class="text-grey">ID : MSDAD132</div>
               <div><a class="mr-2 text-black" href="add-product.html">Edit</a><a class=" text-black"
                   href="add-product.html">Delete</a></div>
             </td>
             <td><span>Rp. 720.000</span></td>
             <td><span>Baju, Koko, Muslim</span></td>
             <td><span>Published</span><br><span>Oct 15, 2019</span></td>
             <td>
               <span class="text-danger">Out of Stock</span>
             </td>
           </tr>
           <tr>
             <td><img src="http://placehold.it/70x70" alt=""></td>
             <td>
               <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
               <div class="text-grey">ID : MSDAD132</div>
               <div><a class="mr-2 text-black" href="add-product.html">Edit</a><a class=" text-black"
                   href="add-product.html">Delete</a></div>
             </td>
             <td><span>Rp. 720.000</span></td>
             <td><span>Baju, Koko, Muslim</span></td>
             <td><span>Published</span><br><span>Oct 15, 2019</span></td>
             <td>
               <span class="text-success">In Stock</span>
             </td>
           </tr>
           <tr>
             <td><img src="http://placehold.it/70x70" alt=""></td>
             <td>
               <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
               <div class="text-grey">ID : MSDAD132</div>
               <div><a class="mr-2 text-black" href="add-product.html">Edit</a><a class=" text-black"
                   href="add-product.html">Delete</a></div>
             </td>
             <td><span>Rp. 720.000</span></td>
             <td><span>Baju, Koko, Muslim</span></td>
             <td><span>Published</span><br><span>Oct 15, 2019</span></td>
             <td>
               <span class="text-danger">Out of Stock</span>
             </td>
           </tr>
         </table>
       </div>
       <div class="tab-pane" id="orders">
        <h5>New Order-in (3)</h5>
        <hr>
        <table class="table">
          <thead>
            <tr>
              <th>Date</th>
              <th>Product Detail</th>
              <th>Price</th>
              <th>Qnty</th>
              <th>Total Payment</th>
              <th>Status</th>
              <th></th>
            </tr>
          </thead>
          <tr>
            <td>Okt 16, 2019</td>
            <td>
              <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
              <div>Order Id : #271894h</div>
              <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
            </td>
            <td><span>Rp. 720.000</span></td>
            <td><span>34</span></td>
            <td><span>Rp. 3.000.000</span></td>
            <td>
              <div><span class="text-success">Order-in</span></div>
              <div>Payment received</div>

            </td>
            <td>
              <div>
                <a href="cart.html">
                  <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                </a>
              </div>
              <div class="mt-2">
                <a href="order-detail.html">View Details</a>
              </div>

            </td>
          </tr>
          <tr>
            <td>Okt 16, 2019</td>
            <td>
              <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
              <div>Order Id : #271894h</div>
              <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
            </td>
            <td><span>Rp. 720.000</span></td>
            <td><span>34</span></td>
            <td><span>Rp. 3.000.000</span></td>
            <td>
              <div><span class="text-danger">Pending</span></div>
              <div>Payment not received</div>

            </td>
            <td>
              <div>
                <a href="cart.html">
                  <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                </a>
              </div>
              <div class="mt-2">
                <a href="order-detail.html">View Details</a>
              </div>

            </td>
          </tr>
          <tr>
            <td>Okt 16, 2019</td>
            <td>
              <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
              <div>Order Id : #271894h</div>
              <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
            </td>
            <td><span>Rp. 720.000</span></td>
            <td><span>34</span></td>
            <td><span>Rp. 3.000.000</span></td>
            <td>
              <div><span class="text-danger">Pending</span></div>
              <div>Payment not received</div>

            </td>
            <td>
              <div>
                <a href="cart.html">
                  <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                </a>
              </div>
              <div class="mt-2">
                <a href="order-detail.html">View Details</a>
              </div>

            </td>
          </tr>
        </table>
        <h5 class="mt-5">Recent Orders</h5>
        <hr>
        <table class="table">
          <thead>
            <tr>
              <th>Date</th>
              <th>Product Detail</th>
              <th>Price</th>
              <th>Qnty</th>
              <th>Total Payment</th>
              <th>Status</th>
              <th></th>
            </tr>
          </thead>
          <tr>
            <td>Okt 16, 2019</td>
            <td>
              <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
              <div>Order Id : #271894h</div>
              <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
            </td>
            <td><span>Rp. 720.000</span></td>
            <td><span>34</span></td>
            <td><span>Rp. 3.000.000</span></td>
            <td>
              <div><span class="text-success">Order-in</span></div>
              <div>Payment received</div>

            </td>
            <td>
              <div>
                <a href="cart.html">
                  <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                </a>
              </div>
              <div class="mt-2">
                <a href="order-detail.html">View Details</a>
              </div>

            </td>
          </tr>
          <tr>
            <td>Okt 16, 2019</td>
            <td>
              <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
              <div>Order Id : #271894h</div>
              <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
            </td>
            <td><span>Rp. 720.000</span></td>
            <td><span>34</span></td>
            <td><span>Rp. 3.000.000</span></td>
            <td>
              <div><span class="text-danger">Pending</span></div>
              <div>Payment not received</div>

            </td>
            <td>
              <div>
                <a href="cart.html">
                  <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                </a>
              </div>
              <div class="mt-2">
                <a href="order-detail.html">View Details</a>
              </div>

            </td>
          </tr>
          <tr>
            <td>Okt 16, 2019</td>
            <td>
              <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
              <div>Order Id : #271894h</div>
              <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
            </td>
            <td><span>Rp. 720.000</span></td>
            <td><span>34</span></td>
            <td><span>Rp. 3.000.000</span></td>
            <td>
              <div><span class="text-danger">Pending</span></div>
              <div>Payment not received</div>

            </td>
            <td>
              <div>
                <a href="cart.html">
                  <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                </a>
              </div>
              <div class="mt-2">
                <a href="order-detail.html">View Details</a>
              </div>

            </td>
          </tr>
        </table>
        <h5 class="mt-5">184 Order Shipped</h5>
        <hr>
        <table class="table data-table">
          <thead>
            <tr>
              <th>Date</th>
              <th>Product Detail</th>
              <th>Price</th>
              <th>Qnty</th>
              <th>Total Payment</th>
              <th>Status</th>
              <th></th>
            </tr>
          </thead>
          <tr>
            <td>Okt 16, 2019</td>
            <td>
              <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
              <div>Order Id : #271894h</div>
              <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
            </td>
            <td><span>Rp. 720.000</span></td>
            <td><span>34</span></td>
            <td><span>Rp. 3.000.000</span></td>
            <td>
              <div><span class="text-success">Order-in</span></div>
              <div>Payment received</div>

            </td>
            <td>
              <div>
                <a href="cart.html">
                  <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                </a>
              </div>
              <div class="mt-2">
                <a href="order-detail.html">View Details</a>
              </div>

            </td>
          </tr>
          <tr>
            <td>Okt 16, 2019</td>
            <td>
              <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
              <div>Order Id : #271894h</div>
              <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
            </td>
            <td><span>Rp. 720.000</span></td>
            <td><span>34</span></td>
            <td><span>Rp. 3.000.000</span></td>
            <td>
              <div><span class="text-danger">Pending</span></div>
              <div>Payment not received</div>

            </td>
            <td>
              <div>
                <a href="cart.html">
                  <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                </a>
              </div>
              <div class="mt-2">
                <a href="order-detail.html">View Details</a>
              </div>

            </td>
          </tr>
          <tr>
            <td>Okt 16, 2019</td>
            <td>
              <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
              <div>Order Id : #271894h</div>
              <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
            </td>
            <td><span>Rp. 720.000</span></td>
            <td><span>34</span></td>
            <td><span>Rp. 3.000.000</span></td>
            <td>
              <div><span class="text-danger">Pending</span></div>
              <div>Payment not received</div>

            </td>
            <td>
              <div>
                <a href="cart.html">
                  <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                </a>
              </div>
              <div class="mt-2">
                <a href="order-detail.html">View Details</a>
              </div>

            </td>
          </tr>
        </table>
       </div>
       <div class="tab-pane" id="setting">
        <h5>Setting</h5>
        <hr> 
        <div class="row">
          <div class="col-md-3">
            <div class="card">
              <span style="font-weight:500">MAIN MENU</span>
              <ul class="menu-vendor-setting mt-3">
                <li>
                 <a href="<?= base_url().'vendors?id='.urlencode(codeEncrypt($userId)).'#setting'?>">Your Shop</a> <i class="fa fa-angle-right"></i>
                </li>
                <li>
                  <a href="#">Shipping Method</a> <i class="fa fa-angle-right"></i>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-9">
            <div class="card">
                <h5>Shipping Method</h5>
                <form method="post" action="<?= base_url().'front/vendor/saveShippingMetode'?>" enctype="multipart/form-data">
                    <input type="hidden" name="userId" value="<?= $userId ?>">
                    <div class="row">
                        <div class="col-md-12 mt-3">
                            <div class="box-option">
                                <table>
                                    <tr>
                                        <td width="50px">
                                            <div class="">

                                                <input id="checkbox-1" class="checkbox-custom checksp" type="checkbox" <?= (in_array('lion',explode(',',$sm)))?'checked':'';?> >
                                                <label for="checkbox-1" class="checkbox-custom-label"></label>
                                                <input type="hidden" name="checksp[]" value="lion" <?= (in_array('lion',explode(',',$sm)))?'':'disabled';?>>
                                             </div>
                                        </td>
                                        <td width="100px">
                                          <img src="<?= base_url()?>assets/front/img/lion-parcel-logo.png" alt="" height="30px">
                                        </td>
                                        <td>
                                          <h5 style="margin-bottom: 2px">Lion Air Cargo</h5>
                                          <p style="margin-bottom: 0px;">Lion Express, Lion Basic</p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="box-option">
                                <table>
                                    <tr>
                                        <td width="50px">
                                            <div class="">
                                                <input id="checkbox-2" class="checkbox-custom checksp" type="checkbox" <?= (in_array('tiki',explode(',',$sm)))?'checked':'';?> >
                                                <label for="checkbox-2" class="checkbox-custom-label"></label>
                                                <input type="hidden" name="checksp[]" value="tiki" <?= (in_array('tiki',explode(',',$sm)))?'':'disabled';?> >
                                             </div>
                                        </td>
                                        <td width="100px">
                                          <img src="<?= base_url()?>assets/front/img/Logo-TIKI.png" alt="" height="30px">
                                        </td>
                                        <td>
                                          <h5 style="margin-bottom: 2px">Tiki Expedition</h5>
                                          <p style="margin-bottom: 0px;">Tiki Express, Tiki Basic</p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="box-option">
                                <table>
                                    <tr>
                                        <td width="50px">
                                            <div class="">
                                                <input id="checkbox-3" class="checkbox-custom checksp" type="checkbox" <?= (in_array('j&t',explode(',',$sm)))?'checked':'';?> >
                                                <label for="checkbox-3" class="checkbox-custom-label"></label>
                                                <input type="hidden" name="checksp[]" value="j&t" <?= (in_array('j&t',explode(',',$sm)))?'':'disabled';?> >
                                             </div>
                                        </td>
                                        <td width="100px">
                                          <img src="<?= base_url()?>assets/front/img/j&t.png" alt="" height="20px">
                                        </td>
                                        <td>
                                          <h5 style="margin-bottom: 2px">J&T Expedition</h5>
                                          <p style="margin-bottom: 0px;">J&T Express, J&T Basic</p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="box-option">
                                <table>
                                    <tr>
                                        <td width="50px">
                                            <div class="">
                                                <input id="checkbox-4" class="checkbox-custom checksp" type="checkbox" <?= (in_array('pos',explode(',',$sm)))?'checked':'';?> >
                                                <label for="checkbox-4" class="checkbox-custom-label"></label>
                                                <input type="hidden" name="checksp[]" value="pos" <?= (in_array('pos',explode(',',$sm)))?'':'disabled';?> >
                                             </div>
                                        </td>
                                        <td width="100px">
                                          <img src="<?= base_url()?>assets/front/img/pos-indonesia.png" alt="" height="30px">
                                        </td>
                                        <td>
                                          <h5 style="margin-bottom: 2px">POS Expedition</h5>
                                          <p style="margin-bottom: 0px;">POS Express, POS Basic</p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="box-option">
                                <table>
                                    <tr>
                                        <td width="50px">
                                            <div class="">
                                                <input id="checkbox-5" class="checkbox-custom checksp" type="checkbox" <?= (in_array('jne',explode(',',$sm)))?'checked':'';?> >
                                                <label for="checkbox-5" class="checkbox-custom-label"></label>
                                                <input type="hidden" name="checksp[]" value="jne" <?= (in_array('jne',explode(',',$sm)))?'':'disabled';?> >
                                             </div>
                                        </td>
                                        <td width="100px">
                                          <img src="<?= base_url()?>assets/front/img/jne.png" alt="" height="30px">
                                        </td>
                                        <td>
                                          <h5 style="margin-bottom: 2px">JNE Expedition</h5>
                                          <p style="margin-bottom: 0px;">JNE Express, POS Basic</p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-12 text-right mt-4">
                            <button class="btn btn-shop">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
          </div>
        </div> 
       </div>
     </div>
   </div>
 </section>
</body>

<?= js_assets('vendor/vendor.shippingMetode.js','f'); ?>