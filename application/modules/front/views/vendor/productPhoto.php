<style>
.imageRemove {
    margin-bottom: 5px;
}
.containerP {
  position: relative;
  width: 50%;
  display: inline;
}

.image {
  opacity: 1;
  display: inline-block;
  transition: .5s ease;
  backface-visibility: hidden;
  margin-left: 5px;
  width: 150px;
  height: auto;
}

.middle {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}

.containerP:hover .image {
  opacity: 0.3;
}

.containerP:hover .middle {
  opacity: 1;
}

.text {
  /*background-color: #4CAF50;*/
  /*opacity: 0.3;*/
  color: white;
  font-size: 16px;
  padding: 16px 32px;
}
</style>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-5 text-left">
                <h3>Add New Prodcut's Photo</h3>
                <div class="">
                    <p>Selamat datang, Kelola setiap alur masuk dan keuar barang anda. Dashboard didesain mudah agar
                        anda
                        bisa mentracking barang yang sedang anda jual</p>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <span style="font-weight:500">MAIN MENU</span>
                    <ul class="menu-vendor-setting mt-3">
                        <li>
                            <?php
                                if(isset($val)){
                                    $productInfo    = base_url().'vendor/ep?id='.codeEncrypt($val->id);
                                    $productPhoto   = base_url().'vendor/pp?id='.codeEncrypt($val->id); 
                                } else {
                                    $productInfo    = base_url().'vendor/ep?id='.urlencode(codeEncrypt($barang_id));
                                    $productPhoto   = '#';
                                }
                            ?>
                            <a href="<?= $productInfo; ?>">Product Information</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="<?= $productPhoto; ?>">Product Photo</a> <i class="fa fa-angle-right"></i>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                
                <div class="imageRemove">
                    <?php
                        //foreach ($img as $k => $p) {
                    ?>
                    <!-- <div class="containerP">
                      <img src="<?= get_image(base_url()."assets/img/goods/small/".$p->gambar_name) ?>" alt="Avatar" class="image" >
                      <div class="middle">
                        <div class="text"><a href="#" class="deletePhoto"><i class="fa fa-trash fa-2x"></i></a></div>
                        <div class="text"><a href="#" class="starsPhoto"><i class="fa fa-star fa-2x"></i></a></div>
                      </div>
                    </div> -->
                    <?php //} ?>
                </div>

                <form action="<?= base_url().'front/vendor/productPhotoUpload'?>" class="dropzone needsclick" id="demoUpload1">
                    <input type="hidden" id="barang_id" name="barang_id" value="<?= $barang_id; ?>" >
                    <div class="dz-message needsclick">
                        Drop files <b>here</b> or <b>click</b> to upload.<br />
                        <span class="dz-note needsclick">
                            (This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)
                        </span>
                    </div>
                </form>
            </div>
            <div class="col-md-12 text-right mt-4">
                <a href="<?= base_url().'vendors?id='.urlencode(codeEncrypt($this->jCfg['client']['id'])).'#products'?> " class="btn btn-shop btnBack" style="float:left;">Back</a>
                <a class="btn btn-shop Finish" id="Finish" href="<?= base_url().'vendors?id='.urlencode(codeEncrypt($this->jCfg['client']['id']));?>">Finish</a>
            </div>
        </div>
    </div>
</body>

<link href="<?= base_url()?>assets/def/plugins/dropzone/dropzone.css" rel="stylesheet" />

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?= base_url()?>assets/def/plugins/dropzone/min/dropzone.min.js"></script>

<?= js_assets('vendor/vendor.addProduct.js','f'); ?>