<body>
    <div class="container">
        <div class="row">
            <div class="col-md-5 text-left">
                <h3><?=$title?></h3>
                <div class="">
                    <p>Selamat datang, Kelola setiap alur masuk dan keuar barang anda. Dashboard didesain mudah agar
                        anda
                        bisa mentracking barang yang sedang anda jual</p>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <span style="font-weight:500">MAIN MENU</span>
                    <ul class="menu-vendor-setting mt-3">
                        <li>
                            <?php
                                if(isset($val)){
                                    $productInfo    = base_url().'vendor/ep?id='.codeEncrypt($val->id);
                                    $productPhoto   = base_url().'vendor/pp?id='.codeEncrypt($val->id); 
                                } else {
                                    $productInfo    = base_url().'vendor/addProduct?id='.$_GET['id'];
                                    $productPhoto   = '#';
                                }
                            ?>
                            <a href="<?= $productInfo; ?>">Product Information</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="<?= $productPhoto; ?>">Product Photo</a> <i class="fa fa-angle-right"></i>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <form action="<?= base_url().'front/vendor/saveGood'?>" enctype="multipart/form-data" method="post" id="sgForm">
                    <input type="hidden" name="id" value="<?= isset($val)?$val->id:'';?>">
                    <input type="hidden" name="user_id" value="<?= $this->jCfg['client']['id'] ?>">
                    <input type="hidden" name="outlet_id" value="<?= getOutletByUser($this->jCfg['client']['id'],'id')?>">
                    <input type="hidden" id="barang_id" name="barang_id" value="<?= isset($val)?$val->id:''; ?>" >
                <div class="card">
                    <h5>Product Information</h5>
                     <h6 class="mt-5 mb-2 mb-4"><span class="required">*</span> Your Product Information</h6>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                               
                                 <label for="exampleInputEmail1">Product Name</label>
                                <input type="text" class="form-control" placeholder="Your product name" name="nama_barang" value="<?= isset($val)?$val->nama_barang:'';?>">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Product Category</label>
                                <select name="catId" class="form-controll custom-select" id="catId" data-val="<?= isset($val)?implode(',',produk::catList($val->kategori_barang,false)):'';?>">
                                    <option value="">Select product category</option>
                                    <?php
                                        foreach ($cat as $k => $v) {
                                            $tipe = ($v->tipe != 0 ) ? 'data-tipe='.$v->tipe : ''; 
                                            echo '<option value="'.$v->id.'" '.$tipe.'>'.$v->name.'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4" style="display: none;" id="catId1">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Product SubCategory</label>
                                <select name="catId" class="form-controll custom-select" >
                                   <option value="">Select product subcategory</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4" style="display: none;" id="catId2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Product SubCategory</label>
                                <select name="catId" class="form-controll custom-select" disabled>
                                   <option value="">Select product subcategory</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h6 class="mt-5 mb-4"><span class="required">*</span> Details Product</h6>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Product Price</label>
                                         <span class="input-group-text input-group-text-detail" >Rp.</span>
                                        <input type="text" class="form-control pl-5" placeholder="Product price" name="harga_satuan" value="<?= isset($det)?myNum($det->harga_satuan,''):'';?>">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Units</label>
                                        <span class="input-group-text input-group-text-detail" style="right:10px; width: auto">Units</span>
                                        <input type="text" class="form-control pr-5" placeholder="Product Stocks" name="qty" value="<?= isset($det)?$det->stok:'';?>">
                                        
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Product Weight</label>
                                        <span class="input-group-text input-group-text-detail"
                                            style="right:10px; width: auto">Gram</span>
                                        <input type="text" class="form-control pr-5" placeholder="Product Weight" name="perkiraan_berat" value="<?= isset($det)?$det->perkiraan_berat:'';?>">

                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Product Discount</label>
                                         <span class="input-group-text input-group-text-detail"
                                            style="right:10px; width: auto">%</span>
                                        <input type="text" class="form-control pr-5" placeholder="Product Discount" name="discount" value="<?= isset($det)?$det->discount:'';?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h6 class="mt-3 mb-4"><span class="required">*</span> Product Varians</h6>
                            <div class="row">
                                <?php
                                if(isset($var) && !empty($var)){
                                    foreach ($var as $k => $v) {
                                        if($v->jenis_varian == 'warna'){
                                            echo '<div class="col-md-6" id="warnaPicker" data-val="'.$v->varian_barang.'"></div>';
                                        }

                                        if($v->jenis_varian == 'ukuran'){
                                            echo '<div class="col-md-6" id="ukuranPicker" data-val="'.$v->varian_barang.'"></div>';
                                        }
                                    }
                                } else {
                                    echo '<div class="col-md-6" id="warnaPicker" data-val=""></div>';
                                    echo '<div class="col-md-6" id="ukuranPicker" data-val=""></div>';
                                }
                                ?>
                            </div>
                        </div>
                         <div class="col-md-12">
                             <h6 class="mt-5 mb-4"><span class="required">*</span> Product Strength Point</h6>
                             <div class="form-group">
                                 <label for="exampleInputEmail1">Strength point <small> *Use ( , ) for multiple input</small></label>
                                 <!-- <input type="text" value="Amsterdam,Washington,Sydney,Beijing,Cairo" data-role="tagsinput" /> -->
                                 <input type="text" class="form-control" data-role="tagsinput" placeholder="Strength point" name="product_strength" value="<?= isset($val)?$val->product_strength:''; ?>">
                             </div>
                         </div>
                        <div class="col-md-12">
                            <h6 class="mt-3 mb-4"><span class="required">*</span> Shop Description</h6>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea name="deskripsi" id="" cols="100" rows="40" class="form-control"
                                            style="height:200px"><?= isset($val)?$val->deskripsi:''?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                     
                        <div class="col-md-12 text-right mt-4">
                            <a href="<?= base_url().'vendors?id='.urlencode(codeEncrypt($this->jCfg['client']['id'])).'#products'?> " class="btn btn-shop btnBack" style="float:left;">Back</a>
                            <button class="btn btn-shop saveGoods">Next Step</button>

                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</body>

<?= js_assets('vendor/vendor.addProduct.js','f'); ?>