<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OBALIHARA - HOME</title>
    <link rel="stylesheet" href="<?= base_url()?>assets/front/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/front/css/font-awesome/4.0/css/font-awesome.min.css">
    <?php echo css_assets('custom.css', 'f') ?>
    <?php echo css_assets('bootstrap-tagsinput.css', 'f') ?>
    <?= plugin_assets('chosen/css/chosen.css','d','css')?>
    <script src="<?= base_url() ?>assets/front/js/jquery-3.2.1.min.js" crossorigin="anonymous"></script>
    <div id="ownLink" style="display: none;"><?= base_url(); ?></div>
</head>
<header>
    <section id="top-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    <a href="<?= base_url() ?>"><img class="logo" src="<?= base_url() ?>assets/front/img/Logo.png" alt="Obaihara"></a>
                </div>
                <div class="col-sm-6" style="margin-top: -10px;">
                    <div class="search-top">
                        <form action="<?= base_url().'category/search'?>" method="post" class="searchForm">
                            <!-- <select class="custom-select custom-select-sm">
                                <option selected>All</option>
                                <option value="1">Male</option>
                                <option value="2">Female</option>
                            </select> -->
                            <input id="search" class="form-control" type="text" name="search" value="" placeholder="i'm shopping for...">
                            <button id="b-search" class="btn btn-primary search" name="button" type="submit" name="button">Search</button>
                        </form>
                    </div>
                </div>
                <div class="<?php if (isset($this->jCfg['client']['is_login']) && $this->jCfg['client']['is_login'] == 1) { echo "col-sm-3"; }else{ echo "col-sm-2";} ?>">
                    <div class="list-cart">
                        <?php if (isset($this->jCfg['client']['is_login']) && $this->jCfg['client']['is_login'] == 1) { ?>
                        <ul>
                            <li class="border-tab"><a href="<?= base_url().'wishlist?id='.urlencode(codeEncrypt($this->jCfg['client']['id'])); ?>" id="wishInfo"><i class="fa fa-heart-o" style="font-size:24px"></i><?= ($this->wishInfo > 0 )?'<span>'.$this->wishInfo.'</span>':''; ?></a></li>
                            <li class="border-tab"><a href="<?= base_url().'cart/gc?id='.urlencode(codeEncrypt($this->jCfg['client']['id']));?>" id="cartInfo"><i class="fa fa-shopping-cart" style="font-size:24px"></i><?= ($this->cartInfo > 0 )?'<span>'.$this->cartInfo.'</span>':''; ?></a></li>
                            <li class="border-tab">
                                <a href="cart.html" class="dropdown-toggle" data-toggle="dropdown" id="dropdown-lonceng">
                                    <i class="fa fa-bell" style="font-size:24px"></i><?= ($this->bells > 0 )?'<span>'.$this->bells.'</span>':''; ?>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="dropdown-lonceng">
                                    <div class="dropdown-body">
                                        <div class="dropdown-heading">Notifikasi Pembelian</div>
                                        <a class="dropdown-item" href="<?=base_url().'buyer/order?id='.urlencode(codeEncrypt($this->jCfg['client']['id'])).'&&s=1'?>">Menunggu Pembayaran</a>
                                        <a class="dropdown-item" href="<?=base_url().'buyer/order?id='.urlencode(codeEncrypt($this->jCfg['client']['id'])).'&&s=1'?>">Menunggu Konfirmasi</a>
                                        <a class="dropdown-item" href="<?=base_url().'buyer/order?id='.urlencode(codeEncrypt($this->jCfg['client']['id'])).'&&s=3'?>">Pesanan Diproses</a>
                                        <a class="dropdown-item" href="<?=base_url().'buyer/order?id='.urlencode(codeEncrypt($this->jCfg['client']['id'])).'&&s=4'?>">Sedang Dikirim</a>
                                        <a class="dropdown-item" href="<?=base_url().'buyer/order?id='.urlencode(codeEncrypt($this->jCfg['client']['id'])).'&&s=5'?>">Sampai Tujuan</a>
                                        <a class="dropdown-item-s" href="<?=base_url().'buyer/order?id='.urlencode(codeEncrypt($this->jCfg['client']['id']))?>">Lihat Daftar Pembeliaan</a>
                                    </div>
                                    <?php
                                        $c = header::checkUserVendor($this->jCfg['client']['id']);
                                        if($c){
                                    ?>
                                    <div class="dropdown-body">
                                        <div class="dropdown-heading">Notifikasi Penjualan</div>
                                        <a class="dropdown-item" href="#">Pesanan Baru</a>
                                        <a class="dropdown-item" href="#">Siap Dikirim</a>
                                        <a class="dropdown-item" href="#">Sedang Dikirim</a>
                                        <a class="dropdown-item-s" href="#">Lihat Daftar Penjualan</a>
                                    </div>
                                    <?php } ?>
                                </div>
                            </li>
                        </ul>
                        <?php }else{?>
                        <ul>
                            <li class="border-tab"><a href="<?= base_url().'wishlist'; ?>"><i class="fa fa-heart-o" style="font-size:24px"></i></a></li>
                            <li><a href="<?= base_url().'cart/gc?id='.urlencode(codeEncrypt($this->jCfg['client']['id']));?>"><i class="fa fa-shopping-cart" style="font-size:24px"></i></a></li>
                        </ul>
                        <?php }?>
                    </div>
                </div>
                <div class="<?php if (isset($this->jCfg['client']['is_login']) && $this->jCfg['client']['is_login'] == 1) { echo "col-sm-1";}else{ echo "col-sm-2 text-right li-btn-lgo";} ?> ">
                    <?php if (isset($this->jCfg['client']['is_login']) && $this->jCfg['client']['is_login'] == 1) { ?>
                        <div class="dropdown">
                           
                            <?php if(urlCheck($this->jCfg['client']['photo']) == true) {?>
                            <img class="profile-dropdown" src="<?= $this->jCfg['client']['photo']?>" data-toggle="dropdown" alt=""> <i class="fa fa-caret-down" data-toggle="dropdown"></i>
                            <?php } else {?>
                            <img class="profile-dropdown" src="<?= get_image(base_url().'assets/img/buyer/thumb/'.$this->jCfg['client']['photo'])?>" data-toggle="dropdown" alt=""> <i class="fa fa-caret-down" data-toggle="dropdown"></i>
                            <?php } ?>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="<?= base_url().'buyer?id='.urlencode(codeEncrypt($this->jCfg['client']['id']));?>">My Dashboard</a>
                                <a class="dropdown-item" href="<?= base_url().'vendors?id='.urlencode(codeEncrypt($this->jCfg['client']['id']));?>">My Shop</a>
                                <?php if(isset($this->jCfg['client']['logout']) && $this->jCfg['client']['logout'] != '') {?>
                                <a class="dropdown-item" href="<?= base_url().'front/signin/logout'?>">Logout</a>
                                <?php } else {?>
                                <a class="dropdown-item" href="<?= base_url().'front/signin/out'?>">Logout</a>
                                <?php }?>
                            </div>
                        </div>
                        <!-- <div class="list-cart">
                            <ul>
                                <li style="width: 100%; font-size: 18px;"><a href="<?= base_url().'buyer?id='.urlencode(codeEncrypt($this->jCfg['client']['id']));?>" ><b><?= $this->jCfg['client']['fullname']; ?></b></a></li>
                                <li style="width: 100%; font-size: 18px;"><a href="<?= base_url().'vendors?id='.urlencode(codeEncrypt($this->jCfg['client']['id']));?>" ><b><?= $this->jCfg['client']['fullname']; ?></b> VENDOR</a></li>
                            </ul>
                        </div> -->
                    <?php } else {?>
                        <a href="<?= base_url().'login'?>"><button class="btn btn-login" type="button" name="button">Login</button></a>
                        <a href="<?= base_url().'login'?>#register"><button class="btn btn-daftar" type="button" name="button">Register</button></a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
    <section id="menu-section">
        <div class="container">
            <nav class="navbar navbar-expand-lg">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown" id="bar-department">
                            <a class="nav-link link-department" href="http://example.com" id="dropdown03" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bars"></i></a>
                            <div class="dropdown-menu" aria-labelledby="dropdown03">
                                <?php
                                    foreach ($this->menu as $k => $v) {
                                        echo '<a class="dropdown-item" href="'.base_url().'category/c?cid='.$v->id.'&&ct='.url_title($v->name).'">'.$v->name.'</a>';
                                    }
                                    echo '<a class="dropdown-item" href="'.base_url().'vendorDetail/vd?id='.urlencode(codeEncrypt(2)).'">Walhi Mart</a>';
                                ?>
                                <!-- <a class="dropdown-item" href="product-category.html">Kopi Nusantara</a>
                                <a class="dropdown-item" href="product-category.html">Pangan</a>
                                <a class="dropdown-item" href="product-category.html">Fesyen</a>
                                <a class="dropdown-item" href="product-category.html">Kain Nusantara</a>
                                <a class="dropdown-item" href="product-category.html">Kerajinan</a>
                                <a class="dropdown-item" href="product-category.html">Aksesoris</a>
                                <a class="dropdown-item" href="product-category.html">Merhandise</a> -->
                            </div>
                        </li>
                        <li class="text-department"> <a class="text-department"><span>Shop By Category</span></a></li>
                    </ul>
                </div>
                <div class="" id="navbar-right">
                    <ul class="navbar-nav mr-auto navbar-right">
                         <?php if (isset($this->jCfg['client']['is_login']) && $this->jCfg['client']['is_login'] == 1) { ?>
                            <li class="nav-item">
                            <a class="nav-link become-seller" href="<?= base_url().'vendors?id='.urlencode(codeEncrypt($this->jCfg['client']['id']));?>"><span>Become Seller</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " href="<?=base_url().'buyer/order?id='.urlencode(codeEncrypt($this->jCfg['client']['id']))?>"><span>Track Your Order</span></a>
                            </li>
                         <?php }else{ ?>
                            <li class="nav-item">
                            <a class="nav-link become-seller" href="<?= base_url().'login'?>"><span>Become Seller</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " href="<?= base_url().'login'?>"><span>Track Your Order</span></a>
                            </li>
                         
                         <?php } ?>
                        
                    </ul>
                </div>
            </nav>
        </div>
    </section>
</header>

<script type="text/javascript">
    
    $(document).on('click','.search',function(){
        $('.searchForm').submit();
    });

</script>