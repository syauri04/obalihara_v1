  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-4 ">
          <h5><?= $this->company->nama?></h5>
          <p><?= $this->company->deskripsi?></p>
          <div class="">
            <ul id="address-footer">
              <li><i class="fa fa-building"></i> <?= $this->company->alamat?></li>
              <li><i class="fa fa-phone"></i> <?= $this->company->phone_number?></li>
              <li><i class="fa fa-envelope"></i> <?= $this->company->email?></li>
            </ul>
          </div>
          <div class="copyright">
            Copyright 2019 | Obalihara - Sell Your Products
          </div>
        </div>
        <div class="col-md-4 offset-md-4">
          <div class="link-footer">
            <div class="row">
              <div class="col-md-6">
                <h6>Informasi Bantuan</h6>
                <ul>
                  <li><a href="<?=base_url().'service'?>">Customer Service</li>
                  <li><a href="<?=base_url().'privacy'?>">Layanan Kontak</li>
                  <li><a href="<?=base_url().'faq'?>">FAQ</li>
                  <!-- <li>Cara Membeli</li> -->
                </ul>
              </div>
              <div class="col-md-6">
                <h6>Tentang Kebijakan</h6>
                <ul>
                  <li><a href="<?=base_url().'privacy'?>">Kebijakan Privasi</a></li>
                  <li><a href="<?=base_url().'terms'?>">Terms And Condition</a></li>
                  <li><a href="<?=base_url().'about'?>">Tentang Kami</a></li>
                  <!-- <li>Jadi Team Obalihara</li> -->
                </ul>
              </div>
            </div>
            <!-- <div class="search-top" style="margin-top:30px;">
              <input style="width:70%;border-left:0px; border-top-left-radius:5px;border-bottom-left-radius:5px" id="search" class="form-control" type="text" name="" value="" placeholder="Alamat email kamu">
              <button style="width:30%;" id="b-search" class="btn btn-primary" name="button" type="button" name="button">Berlangganan</button>
            </div> -->
            <div class="text-right"><img class="bank" src="<?= base_url()?>assets/front/img/bank.png" alt=""></div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script> -->
  <script src="<?= base_url()?>assets/front/js/popper.min.js" crossorigin="anonymous"></script>
  <script src="<?= base_url()?>assets/front/js/bootstrap.min.js" crossorigin="anonymous"></script>
  <?= plugin_assets('form-validator/jquery.form-validator.min.js','d')?>
  <?= js_assets('custom.js','f')?>
  <?= js_assets('jquery.blockUI.js','f')?>
  <?= js_assets('bootstrap-tagsinput.js','f')?>
  <?= js_assets('bootstrap-tagsinput-angular.js','f')?>
  <?= js_assets('easyzoom.js','f')?>
  <?= plugin_assets('chosen/js/chosen.jquery.min.js','d')?>
</html>

<script type="text/javascript">
  $.validate({
        modules : 'security'
    });
</script>