<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <title>OBALIHARA - HOME</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/custom.css">
</head>
<header>
    <section id="top-section">
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <a href="index.html"><img class="logo" src="assets/img/Logo.png" alt="Obaihara"></a>
                </div>
                <div class="col-md-6">
                    <div class="search-top">
                        <form action="<?= base_url().'/front/home/category'?>" method="post">
                            <select class="custom-select custom-select-sm">
                                <option selected>All</option>
                                <option value="1">Male</option>
                                <option value="2">Female</option>
                            </select>
                            <input id="search" class="form-control" type="text" name="search" value="" placeholder="i'm shopping for...">
                            <button id="b-search" class="btn btn-primary" name="button" type="submit" name="button">Search</button>
                        </form>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="list-cart">
                        <ul>
                            <li class="border-tab"><a href="whislist.html"><i class="fa fa-heart-o" style="font-size:24px"></i></a></li>
                            <li class="border-tab"><a href="cart.html"><i class="fa fa-shopping-cart" style="font-size:24px"></i></a></li>
                            <li class="border-tab"><a href="cart.html" class="dropdown-toggle" data-toggle="dropdown" id="dropdown-lonceng"><i class="fa fa-bell" style="font-size:24px"></i><span>23</span></a>
                            <div class="dropdown-menu" aria-labelledby="dropdown-lonceng">
                                <div class="dropdown-body">
                                    <div class="dropdown-heading">Notifikasi Pembelian</div>
                                    <a class="dropdown-item" href="#">Menunggu Pembayaran</a>
                                    <a class="dropdown-item" href="#">Menunggu Konfirmasi</a>
                                    <a class="dropdown-item" href="#">Pesanan Diproses</a>
                                    <a class="dropdown-item" href="#">Sedang Dikirim</a>
                                    <a class="dropdown-item" href="#">Sedang Tujuan</a>
                                    <a class="dropdown-item-s" href="#">Lihat Daftar Pembeliaan</a>
                                </div>
                                <div class="dropdown-body">
                                    <div class="dropdown-heading">Notifikasi Penjualan</div>
                                    <a class="dropdown-item" href="#">Pesanan Baru</a>
                                    <a class="dropdown-item" href="#">Siap Dikirim</a>
                                    <a class="dropdown-item" href="#">Sedang Dikirim</a>
                                    <a class="dropdown-item-s" href="#">Lihat Daftar Penjualan</a>
                                </div>
                            </div>
                            <li><a href="cart.html"><i class="fa fa-building" style="font-size:24px"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="dropdown">
                        <img class="profile-dropdown" src="assets/img/profile.jpg" data-toggle="dropdown" alt=""> <i class="fa fa-caret-down" data-toggle="dropdown"></i>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </div>
                </div>
                <!-- <div class="col-md-2">
          <a href="login.html"><button class="btn btn-login" type="button" name="button">Masuk</button></a>
          <a href="login.html#register"><button class="btn btn-daftar" type="button" name="button">Daftar</button></a>
        </div> -->
            </div>
        </div>
    </section>
    <section id="menu-section">
        <div class="container">
            <nav class="navbar navbar-expand-lg">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown" id="bar-department">
                            <a class="nav-link link-department" href="http://example.com" id="dropdown03" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bars"></i></a>
                            <div class="dropdown-menu" aria-labelledby="dropdown03">
                                <?php
                                    foreach ($this->menu as $k => $v) {
                                        echo '<a class="dropdown-item" href="'.base_url().'category/c?cid='.$v->id.'&&ct='.url_title($v->name).'">'.$v->name.'</a>';
                                    }
                                ?>
                            </div>
                        </li>
                        <li class="text-department"> <a class="text-department"><span>Shop By Department</span></a></li>
                    </ul>
                </div>
                <div class="" id="navbar-right">
                    <ul class="navbar-nav mr-auto navbar-right">
                        <li class="nav-item">
                            <a class="nav-link become-seller" href="#"><span>Become Seller</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="#"><span>Track Your Order</span></a>
                        </li>
                        <!-- <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                US Dollar
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">US Dollar</a>
                <a class="dropdown-item" href="#">IDR</a>
              </div>
            </li>
            <li class="nav-item dropdown lang">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <img src="assets/img/ind.jpg" alt="" width="25px"> Indonesia
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#"><img src="assets/img/ind.jpg" alt="" width="25px"> Indonesia</a>
                <a class="dropdown-item" href="#"><img src="assets/img/ind.jpg" alt="" width="25px"> English</a>
              </div>
            </li> -->

                    </ul>
                </div>
            </nav>
        </div>
    </section>
</header>