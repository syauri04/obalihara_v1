<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
<style>
@import url("https://fonts.googleapis.com/css?family=Heebo:400,500,700");
body{
  font-size: 12px;
  background: #F5F5F9;
  font-family: "Heebo" !important;
  overflow-x: hidden;
  color: #888888;
  max-width:600px;
}
.red-head{
    width:100%;
    background:#8F3248;
    padding-top:22px;
    padding-left:14px;
    padding-right:14px;
    
}
.white-head{
    border-top-left-radius:10px;
    border-top-right-radius:10px;
    text-align:center;
    background:#fff;
    padding-top:32px;
    padding-bottom:10px;
}
.content-head{
    font-size: 14px;
}
h1{
    font-family: Heebo;
    font-weight:600;
    font-size: 20px;
    line-height: 20px;
    color:#000000;
}
.logo{
    margin-bottom:20px;
}
.content-box{
    padding-top:0px;
    padding-left:30px;
    padding-right:30px;
    background:#fff;
    width:94.7%;
}
.content{
    padding-top:10px;
    padding-bottom:10px;
    padding-left:30px;
    padding-right:30px;
}
a{
    cursor:pointer;
    color:#8f3248;
}
btn{
    cursor:pointer;
}
</style>
</head>
<body>
    <div class="red-head">
        <div class="white-head">
            <img src="<?php echo base_url() ?>assets/img/email/logo.png" alt="" class="logo">
            <h1>Konfirmasi Akun Kamu</h1>
        </div>
    </div>
    <div class="content-box">
        <div class="content">
            <h1>Hai Oka Kuswandi!</h1>
            <span style="font-size:14px">Konfirmasi akun email kamu untuk verifikasi data, klik tombol di bawah ini:</span>
            <button style="background: #8F3248;
border-radius: 5px; color:#fff;font-size: 16px;border:#8F3248; width:100%; height:45px; margin-top:30px;">Verifikasi Akun</button>
        </div>
        <div class="content" style="margin-top:30px;font-size:14px">
            <span>Jika kamu tidak mengenali aktivitas ini atau tidak merasa melakukan<br>
            pengubahan informasi ini, segera hubungi <a href="<?php echo base_url() ?>">Obalihara</a>.</span><br><br>
            <span>Silakan cek kembali akun kamu. Selalu jaga keamanan dan kerahasiaan <br>akun Obalihara kamu.</span><br><br>
            <span>Terima kasih atas perhatiannya.</span>
        </div>
        <div style="text-align:center; margin-top:50px">
            <h1>Ikuti Kami</h1>
            <span>Jadilah yang pertama tahu promo-promo terbaru Bukalapak lewat media <br>sosial kami.</span>
            <div style="margin-top:10px; text-align:center; width:180px;position:relative; margin:0 auto; padding-top:20px;">
                <div style="background: #8F3248; width:26px;border-radius:15px;text-align:center;padding-top:8px;padding-bottom:8px; float:left; margin-right:10px;">
                    <img src="<?php echo base_url() ?>assets/img/email/ig.png" alt="">
                </div>
                <div style="background: #8F3248; width:26px;border-radius:15px;text-align:center;padding-top:7px;padding-bottom:7px; float:left; margin-right:10px;">
                    <img src="<?php echo base_url() ?>assets/img/email/fb.png" alt="">
                </div>
                <div style="background: #8F3248; width:26px;border-radius:15px;text-align:center;padding-top:8px;padding-bottom:8px; float:left; margin-right:10px;">
                    <img src="<?php echo base_url() ?>assets/img/email/tw.png" alt="">
                </div>
                <div style="background: #8F3248; width:26px;border-radius:15px;text-align:center;padding-top:8px;padding-bottom:8px; float:left; margin-right:10px;">
                    <img src="<?php echo base_url() ?>assets/img/email/in.png" alt="">
                </div>
                <div style="background: #8F3248; width:26px;border-radius:15px;text-align:center;padding-top:9px;padding-bottom:9px; float:left;">
                    <img src="<?php echo base_url() ?>assets/img/email/yt.png" alt="">
                </div>
                <div style="clear:both"></div>
            </div>
            <div style="text-align:center; margin-top:30px; margin-bottom:10px;">
            Jl. Tegal Parang Utara No.14, RT.5/RW.4, Mampang<br>
            Prpt., Kec. Mampang Prpt., Kota Jakarta Selatan,<br>
            Daerah Khusus Ibukota Jakarta 12790<br>
            </div>
            <div style="text-align:center; margin-top:30px; margin-bottom:10px;color: #BBBBBB; text-align:center">
            Copyright © 2019 Obalihara.com. All Rights Reserved
            </div>
        </div>
    </div>
</body>
</html>