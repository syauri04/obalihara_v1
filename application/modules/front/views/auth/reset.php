<body>
   <section>
       <div class="container">
            <div class="row">
                <div class="col-md-6 centerin">
                    <ul class="nav nav-tabs tab-login" id="myTab" role="tablist">
                        <li class="nav-item border-tab">
                            <a class="nav-link active" id="login-tab" data-toggle="tab" href="#login" role="tab"
                                aria-controls="home" aria-selected="true"><h3>Reset Password</h3></a>
                        </li>
                        \
                    </ul>
                    <div class="tab-content" id="content-tab-login">
                        <div class="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="home-tab">
                            <div class="card">
                               <form action="" id="form-login" method="post" >
                                   
                                    <h4 class="form-error"><?= isset($_GET['msg'])?$_GET['msg']:''?></h4>
                                    <h6 class="mt-3 mb-3">Create Password</h6>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" name="password1" class="form-control" placeholder="Password" >
                                    </div> 
                                    <div class="form-group">
                                        <label>Confirm Password</label>
                                        <input type="password" name="password2" class="form-control" placeholder="Confirm Password" >
                                    </div>
                                   
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-login btn-block">Send </button>
                                    </div> <!-- form-group// -->
                               </form>

                            </div>
                        </div>
            
                       
                    </div>
                </div>
            </div>
       </div>
   </section>
</body>

