<body>
   <section>
       <div class="container">
            <div class="row">
                <div class="col-md-6 centerin">
                    <ul class="nav nav-tabs tab-login" id="myTab" role="tablist">
                        <li class="nav-item border-tab">
                            <a class="nav-link active" id="login-tab" data-toggle="tab" href="#login" role="tab"
                                aria-controls="home" aria-selected="true"><h3>Login</h3></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="register-tab" data-toggle="tab" href="#register" role="tab"
                                aria-controls="profile" aria-selected="false"><h3>Register</h3></a>
                        </li>
                    </ul>
                    <div class="tab-content" id="content-tab-login">
                        <div class="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="home-tab">
                            <div class="card">
                               <form action="" id="form-login" method="post" >
                                    <h5 class="mb-0"><b>Sign in to your account</b></h5>
                                    <hr>
                                    <h6 class="mt-3">Sign in with media social</h6>

                                    <div class="mt-4">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <a href="<?=base_url().'login-google';?>" class="btn btn-login btn-google"><i class="fa fa-google"></i>Masuk Dengan Google</a>
                                            </div>
                                            <div class="col-md-2 text-center">
                                                <span class="or">or</span>
                                            </div>
                                            <div class="col-md-5">
                                                <a href="<?= (!empty($authURL))?$authURL:'#'?>" class="btn btn-login btn-fb"><i class="fa fa-facebook"></i>
                                                    Masuk Dengan Facebook</a>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <h4 class="form-error"><?= isset($msg['m'])?$msg['m']:''?></h4>
                                    <h6 class="mt-3 mb-3">Have an Account?</h6>
                                    <div class="form-group">
                                        <label>Username or Email</label>
                                        <input name="email" class="form-control" placeholder="Username" >
                                    </div> <!-- form-group// -->
                                    <div class="form-group" >
                                        <a class="float-right" href="<?=base_url().'login/forgot';?>">Forgot?</a>
                                        <label>Password</label>
                                        <input class="form-control" placeholder="Password" type="password" name="password">
                                    </div> <!-- form-group// -->
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label> <input type="checkbox"> Remember</label>
                                        </div> <!-- checkbox .// -->
                                    </div> <!-- form-group// -->
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-login btn-block"> Sign In </button>
                                    </div> <!-- form-group// -->
                               </form>

                            </div>
                        </div>
                        <div class="tab-pane fade" id="register" role="tabpanel" aria-labelledby="register-tab">

                            <div class="card">
                                <form action="<?= base_url().'front/signin/register'?>" id="form-register" method="POST">
                                    <h5 class="mb-0"><b>Register</b></h5>
                                    <hr>
                                    <h6 class="mt-3">Register with media social</h6>
                                    <div class="mt-4">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <a href="<?=base_url().'login-google';?>" class="btn btn-login btn-google"><i class="fa fa-google"></i>Masuk Dengan Google</a>
                                            </div>
                                            <div class="col-md-2 text-center">
                                                <span class="or">or</span>
                                            </div>
                                            <div class="col-md-5">
                                                <button class="btn btn-login btn-fb"><i class="fa fa-facebook"></i>
                                                    Daftar Dengan Google</button>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <h4 class="form-error"><?= isset($_GET['msg'])?$_GET['msg']:''?></h4>
                                    <h6 class="mt-3 mb-3">Have an Account?</h6>
                                    
                                  <!--   <div class="form-group">
                                        <label>Username</label>
                                        <input name="username" class="form-control" placeholder="Username" data-validation="required">
                                    </div> -->
                                     <div class="form-group">
                                         <label>Email</label>
                                         <input name="email" class="form-control" placeholder="Email" type="email" data-validation="email">
                                     </div>
                                    <div class="form-group">
                                        
                                        <label>Password</label>
                                        <input class="form-control" placeholder="Password" type="password" name="password" data-validation="required length" data-validation-length="min8">
                                    </div>
                                     <div class="form-group">
                                         <label>Phone number</label>
                                         <input  class="form-control" placeholder="Phone Number" name="phoneNumber" data-validation="required">
                                     </div>
                                       <div class="form-group">
                                           <div class="checkbox">
                                               <label> <input type="checkbox" class="termCondition" data-validation="required" data-validation-error-msg="You have to AGREE to the Term and Conditions"> Check if you undestand <a href="">Term and Conditions</a></label>
                                           </div> <!-- checkbox .// -->
                                       </div> <!-- form-group// -->
                                    <div class="form-group">
                                        <button type="button" id="regist" class="btn btn-login btn-block">Register</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        
                        
                       
                    </div>
                </div>
            </div>
       </div>
   </section>
</body>

<script type="text/javascript">
    $("#regist").click(function(){        
       if($('.termCondition').is(':checked') == false){
            event.preventDefault();
            alert("By signing up, you must accept our terms and conditions!");
            return false;
        }else{
            $("#form-register").submit(); // Submit the form
        }
    });
   
</script>