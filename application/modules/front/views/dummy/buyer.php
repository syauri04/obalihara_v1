<body>
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-5">
        <h3>Buyer Dashboard</h3>
        <div class="">
          <p>Selamat datang, Kelola setiap alur masuk dan keuar barang anda. Dashboard didesain mudah agar anda
            bisa mentracking barang yang sedang anda jual</p>
        </div>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-3">
        <div class="card">
          <span style="font-weight:500">MAIN MENU</span>
          <ul class="menu-vendor-setting mt-3">
            <li>
              <a href="<?=base_url().'buyer';?>">Dashboard</a> <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="<?=base_url().'buyer/personal';?>">Personal Information</a> <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="<?=base_url().'buyer/shipping';?>">Shipping Address</a> <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="<?=base_url().'buyer/order';?>">Your Order</a> <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a href="<?=base_url().'buyer/change_password';?>">Change Password</a> <i class="fa fa-angle-right"></i>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-md-9">
        <div class="card">
          <h5 class="mb-5">Dashboard</h5>
          <div class="row">
            <div class="col-md-2">
              <img src="assets/front/img/profile.jpg" class="w-100" alt="">
            </div>
            <div class="col-md-10">
              <h6 class="mb-1 mt-4"></span>Jokowi Prabowo</h6>
              <span>jowokiprabowo@gmail.com</span>
            </div>
            <div class="col-md-6 mt-5" id="order-card">
              <table class="w-100">
                <tr>
                  <td colspan="2"><span class="title-vendor-data">Personal Information</span></td>
                </tr>
                <tr>
                  <td width="100"><span class="text-grey">Full Name : </span></td>
                  <td><span class="text-black">Aris Prabowo</span></td>
                </tr>
                <tr>
                  <td><span class="text-grey">Email : </span></td>
                  <td><span class="text-black">Aris@Prabowo.com</span></td>
                </tr>
                <tr>
                  <td><span class="text-grey">Phone : </span></td>
                  <td><span class="text-black">321321321</span></td>
                </tr>
                <tr>
                  <td><span class="text-grey">Country : </span></td>
                  <td><span class="text-black">Indoneia</span></td>
                </tr>
                <tr>
                  <td><span class="text-grey">Postal code : </span></td>
                  <td><span class="text-black">123</span></td>
                </tr>
              </table>
            </div>
           <div class="col-md-6 mt-5" id="order-card">
              <table class="w-100">
                <tr>
                  <td colspan="2"><span class="title-vendor-data">Shipping Information</span></td>
                </tr>
                <tr>
                  <td width="100"><span class="text-grey">Full Name : </span></td>
                  <td><span class="text-black">Aris Prabowo</span></td>
                </tr>
                <tr>
                  <td><span class="text-grey">Province : </span></td>
                  <td><span class="text-black">Jawa Barat</span></td>
                </tr>
                <tr>
                  <td><span class="text-grey">City : </span></td>
                  <td><span class="text-black">Bogor</span></td>
                </tr>
                <tr>
                  <td><span class="text-grey">Address</span></td>
                  <td><span class="text-black">Sir Matt Busby Way, Old Trafford, Stretford, Manchester M16
                      0RA, Britania Raya</span></td>
                </tr>
                <tr>
                  <td><span class="text-grey">Postal code : </span></td>
                  <td><span class="text-black">123</span></td>
                </tr>
              </table>
            </div>
          </div>
           <span class="title-vendor-data mb-3 mt-3">Recent Orders</span>
            <table class="data-table table">
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Items</th>
                  <th>Status</th>
                  <th></th>
                </tr>
              </thead>
              <tr>
                <td>Okt 16, 2019</td>
                <td>
                  <div class="row">
                    <div class="col-3"><img src="http://placehold.it/70x70" alt=""></div>
                    <div class="col-9">
                      <div><b>Marshall Lorem Ipsum dolor amet (149077)</b></div>
                      <div><span class="mr-3">Color : Black</span> <span>Order ID : #2134124</span></div>
                    </div>
                  </div> 
                </td>
                <td><b class="text-success">Shipping</b></td>
                <td>
                  <a href="#"><button class="btn btn-shop">Tracking</button></a>
                </td>
              </tr>
              <tr>
                <td>Okt 16, 2019</td>
                <td>
                  <div class="row">
                    <div class="col-3"><img src="http://placehold.it/70x70" alt=""></div>
                    <div class="col-9">
                      <div><b>Marshall Lorem Ipsum dolor amet (149077)</b></div>
                      <div><span class="mr-3">Color : Black</span> <span>Order ID : #2134124</span></div>
                    </div>
                  </div>
                </td>
                <td><b class="">Order-in</b></td>
                <td>
                  <a href="#"><button class="btn btn-shop">Tracking</button></a>
                </td>
              </tr>
              <tr>
                <td>Okt 16, 2019</td>
                <td>
                  <div class="row">
                    <div class="col-3"><img src="http://placehold.it/70x70" alt=""></div>
                    <div class="col-9">
                      <div><b>Marshall Lorem Ipsum dolor amet (149077)</b></div>
                      <div><span class="mr-3">Color : Black</span> <span>Order ID : #2134124</span></div>
                    </div>
                  </div>
                </td>
                <td><b class="">Order-in</b></td>
                <td>
                  <a href="#"><button class="btn btn-shop">Tracking</button></a>
                </td>
              </tr>
            </table>
        </div>
      </div>
    </div>
  </div>
</section>
</body>

