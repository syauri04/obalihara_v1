<body>
 <section>
     <div class="container">
         <div id="slide" class="carousel slide" data-ride="carousel">
             <!-- Indicators -->
             <!-- <ul class="carousel-indicators">
                <li data-target="#demo" data-slide-to="0" class="active"></li>
                <li data-target="#demo" data-slide-to="1"></li>
                <li data-target="#demo" data-slide-to="2"></li>
              </ul> -->

             <!-- The slideshow -->
             <div class="carousel-inner">
                 <div class="carousel-item active">
                     <div class="back-slider"
                         style="background:url('<?= base_url()?>assets/front/img/slide1.jpg') no-repeat; background-size: cover;">
                         <div class="row">
                             <div class="col-md-6">
                                 <div class="title-cat">
                                     <span>Limited Edition</span>
                                 </div>
                                 <h2>Korea Long Sofa Fabric In Blue Navy Color</h2>
                                 <h3>$100</h3>
                                 <button type="button" class="btn btn-shop" name="button">Shop Now</button>
                             </div>
                         </div>
                     </div>
                 </div>

                 <div class="carousel-item">
                     <div class="back-slider"
                         style="background:url('<?= base_url()?>assets/front/img/slide2.jpg') no-repeat; background-size: cover;">
                         <div class="row">
                             <div class="col-md-6">
                                 <div class="title-cat">
                                     <span>Limited Edition</span>
                                 </div>
                                 <h2>Korea Long Sofa Fabric In Blue Navy Color</h2>
                                 <h3>$100</h3>
                                 <button type="button" class="btn btn-shop" name="button">Shop Now</button>
                             </div>
                         </div>
                     </div>
                 </div>

                 <div class="carousel-item">
                     <div class="back-slider"
                         style="background:url('<?= base_url()?>assets/front/img/slide3.jpg') no-repeat; background-size: cover;">
                         <div class="row">
                             <div class="col-md-6">
                                 <div class="title-cat">
                                     <span>Limited Edition</span>
                                 </div>
                                 <h2>Korea Long Sofa Fabric In Blue Navy Color</h2>
                                 <h3>$100</h3>
                                 <button type="button" class="btn btn-shop" name="button">Shop Now</button>
                             </div>
                         </div>
                     </div>
                 </div>
                 <!-- Left and right controls -->
                 <a class="carousel-control-prev" href="#slide" data-slide="prev">
                     <div class="control-slide">
                         <i class="fa fa-angle-left"></i>
                     </div>
                 </a>
                 <a class="carousel-control-next" href="#slide" data-slide="next">
                     <div class="control-slide">
                         <i class="fa fa-angle-right"></i>
                     </div>
                 </a>

             </div>
         </div>
     </div>
 </section>
 <section>
     <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                  <div class="row" id="body-row">
                    <div id="sidebar-container" class="sidebar-expanded d-none d-md-block">
                      <h5>CATEGORIES</h5>
                      <ul class="list-group">

                        <?= getcategories();?>

                        <!-- <a href="#submenu1" data-toggle="collapse" aria-expanded="false" class="list-group-item list-group-item-action flex-column align-items-start">
                          <div class="d-flex w-100 justify-content-start align-items-center">
                            <span class="menu-collapsed">Kopi Nusantara</span>
                            <span class="submenu-icon ml-auto"></span>
                          </div>
                        </a>
                        <div id='submenu1' class="collapse sidebar-submenu">
                          <a href="#" class="list-group-item list-group-item-action">
                            <span class="menu-collapsed">Kopi Toraja</span>
                          </a>
                          <a href="#" class="list-group-item list-group-item-action">
                            <span class="menu-collapsed">Kopi Gayo</span>
                          </a>
                          <a href="#" class="list-group-item list-group-item-action">
                            <span class="menu-collapsed">Kopi Sumsel</span>
                          </a>
                        </div> -->

                        <!-- <a href="#submenu2" data-toggle="collapse" aria-expanded="false"
                          class="list-group-item list-group-item-action flex-column align-items-start">
                          <div class="d-flex w-100 justify-content-start align-items-center">
                            <span class="menu-collapsed">Makanan Ringan</span>
                            <span class="submenu-icon ml-auto"></span>
                          </div>
                        </a>
                        <div id='submenu2' class="collapse sidebar-submenu">
                          <a href="#" class="list-group-item list-group-item-action">
                            <span class="menu-collapsed">Taro</span>
                          </a>
                          <a href="#" class="list-group-item list-group-item-action">
                            <span class="menu-collapsed">Potato</span>
                          </a>
                        </div>

                        <a href="#submenu3" data-toggle="collapse" aria-expanded="false"
                          class="list-group-item list-group-item-action flex-column align-items-start">
                          <div class="d-flex w-100 justify-content-start align-items-center">
                            <span class="menu-collapsed">Aneka Minuman</span>
                            <span class="submenu-icon ml-auto"></span>
                          </div>
                        </a>
                        <div id='submenu3' class="collapse sidebar-submenu">
                          <a href="#" class="list-group-item list-group-item-action">
                            <span class="menu-collapsed">Taro</span>
                          </a>
                          <a href="#" class="list-group-item list-group-item-action">
                            <span class="menu-collapsed">Potato</span>
                          </a>
                        </div>

                        <a href="#submenu4" data-toggle="collapse" aria-expanded="false"
                          class="list-group-item list-group-item-action flex-column align-items-start">
                          <div class="d-flex w-100 justify-content-start align-items-center">
                            <span class="menu-collapsed">Clothes</span>
                            <span class="submenu-icon ml-auto"></span>
                          </div>
                        </a>
                        <div id='submenu4' class="collapse sidebar-submenu">
                          <a href="#" class="list-group-item list-group-item-action">
                            <span class="menu-collapsed">Taro</span>
                          </a>
                          <a href="#" class="list-group-item list-group-item-action">
                            <span class="menu-collapsed">Potato</span>
                          </a>
                        </div>

                        <a href="#submenu5" data-toggle="collapse" aria-expanded="false"
                          class="list-group-item list-group-item-action flex-column align-items-start">
                          <div class="d-flex w-100 justify-content-start align-items-center">
                            <span class="menu-collapsed">Bumbu Masak</span>
                            <span class="submenu-icon ml-auto"></span>
                          </div>
                        </a>
                        <div id='submenu5' class="collapse sidebar-submenu">
                          <a href="#" class="list-group-item list-group-item-action">
                            <span class="menu-collapsed">Taro</span>
                          </a>
                          <a href="#" class="list-group-item list-group-item-action">
                            <span class="menu-collapsed">Potato</span>
                          </a>
                        </div> -->

                      </ul>
                    </div>
                  </div>
                </div>
                <div class="card mt-4">
                  <div id="sidebar-container" class="sidebar-expanded d-none d-md-block">
                    <h6 class="text-pink">HIGHLIGHT</h6>
                    <div class="box-side">
                      <div class="">
                         <input id="checkbox-1" class="checkbox-custom" name="checkbox-2" type="checkbox">
                         <label for="checkbox-1" class="checkbox-custom-label">Promotions <span>(800)</span></label>
                      </div>
                      <div class="">
                        <input id="checkbox-2" class="checkbox-custom" name="checkbox-2" type="checkbox">
                        <label for="checkbox-2" class="checkbox-custom-label">New Arrivals <span>(300)</span></label>
                      </div>
                    </div>
                    <h6>BY BRAND</h6>
                    <div class="box-side">
                      <div>
                       <form>
                         <div class="input-group mb-3">
                           <div class="input-group-prepend">
                             <span class="input-group-text"><i class="fa fa-search"></i></span>
                           </div>
                           <input type="text" class="form-control text-custom" placeholder="Cari nama brand">
                         </div>
                       </form>
                      </div>
                      <div class="brand-box">
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Adidas <span>(800)</span></label>
                        </div>
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Nike Sport <span>(800)</span></label>
                        </div>
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Puma <span>(800)</span></label>
                        </div>
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Rolex Swatch <span>(800)</span></label>
                        </div>
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Swiss Millitary <span>(800)</span></label>
                        </div>
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Brodo <span>(800)</span></label>
                        </div>
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Apple Computers <span>(800)</span></label>
                        </div>
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Adidas <span>(800)</span></label>
                        </div>
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Nike Sport <span>(800)</span></label>
                        </div>
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Puma <span>(800)</span></label>
                        </div>
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Rolex Swatch <span>(800)</span></label>
                        </div>
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Swiss Millitary
                            <span>(800)</span></label>
                        </div>
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Brodo <span>(800)</span></label>
                        </div>
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Apple Computers
                            <span>(800)</span></label>
                        </div>
                      </div>
                    </div>
                    <h6>BY PRICE</h6>
                    <div class="box-side">
                      <div class="price-box">
                        <ul>
                          <li>
                            <a href="">> Rp. 50.000</a>
                          </li>
                          <li>
                            <a href="">> Rp. 50.000 - Rp. 300.000</a>
                          </li>
                          <li>
                            <a href="">> Rp. 300.000 - Rp. 1.000.000</a>
                          </li>
                          <li>
                            <a href="">> Rp. 1.000.000 - Rp. 3.000.000</a>
                          </li>
                           <li>
                             <a href="">> Rp. 3.000.000</a>
                           </li>  
                        </ul>
                      </div>
                    </div>
                    <h6>BY MANUFACTURER</h6>
                    <div class="box-side">
                      <div>
                        <form>
                          <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fa fa-search"></i></span>
                            </div>
                            <input type="text" class="form-control text-custom" placeholder="Cari nama manufacturer">
                          </div>
                        </form>
                      </div>
                      <div class="brand-box">
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Adidas <span>(800)</span></label>
                        </div>
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Nike Sport <span>(800)</span></label>
                        </div>
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Puma <span>(800)</span></label>
                        </div>
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Rolex Swatch <span>(800)</span></label>
                        </div>
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Swiss Millitary
                            <span>(800)</span></label>
                        </div>
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Brodo <span>(800)</span></label>
                        </div>
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Apple Computers
                            <span>(800)</span></label>
                        </div>
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Adidas <span>(800)</span></label>
                        </div>
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Nike Sport <span>(800)</span></label>
                        </div>
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Puma <span>(800)</span></label>
                        </div>
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Rolex Swatch <span>(800)</span></label>
                        </div>
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Swiss Millitary
                            <span>(800)</span></label>
                        </div>
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Brodo <span>(800)</span></label>
                        </div>
                        <div class="">
                          <input id="checkbox-3" class="checkbox-custom" name="checkbox-3" type="checkbox">
                          <label for="checkbox-3" class="checkbox-custom-label">Apple Computers
                            <span>(800)</span></label>
                        </div>
                      </div>
                    </div>
                    <h6>DELIVERY AND PAYMENT</h6>
                    <div class="box-side">
                      <div class="">
                        <input id="checkbox-1" class="checkbox-custom" name="checkbox-2" type="checkbox">
                        <label for="checkbox-1" class="checkbox-custom-label">Layanan JNE</label>
                      </div>
                      <div class="">
                        <input id="checkbox-2" class="checkbox-custom" name="checkbox-2" type="checkbox">
                        <label for="checkbox-2" class="checkbox-custom-label">Layanan J-Express</label>
                      </div>
                      <div class="">
                        <input id="checkbox-2" class="checkbox-custom" name="checkbox-2" type="checkbox">
                        <label for="checkbox-2" class="checkbox-custom-label">Post Indonesia</label>
                      </div>
                    </div>
                    <h6>BY RATING</h6>
                    <div class="box-side  side-rating">
                      <div class="">
                        <input id="checkbox-1" class="checkbox-custom" name="rating" type="checkbox">
                        <label for="checkbox-1" class="checkbox-custom-label">
                            <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                              class="fa fa-star"></i> <i class="fa fa-star"></i> <span>(800)</span>
                        </label>
                      </div>
                      <div class="">
                        <input id="checkbox-1" class="checkbox-custom" name="rating" type="checkbox">
                        <label for="checkbox-1" class="checkbox-custom-label">
                          <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                            class="fa fa-star"></i> <i class="fa fa-star null"></i> <span>(800)</span>
                        </label>
                      </div>
                      <div class="">
                        <input id="checkbox-1" class="checkbox-custom" name="rating" type="checkbox">
                        <label for="checkbox-1" class="checkbox-custom-label">
                          <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                            class="fa fa-star null"></i> <i class="fa fa-star null"></i> <span>(800)</span>
                        </label>
                      </div>
                      <div class="">
                        <input id="checkbox-1" class="checkbox-custom" name="rating" type="checkbox">
                        <label for="checkbox-1" class="checkbox-custom-label">
                          <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star null"></i> <i
                            class="fa fa-star null"></i> <i class="fa fa-star null"></i> <span>(800)</span>
                        </label>
                      </div>
                      <div class="">
                        <input id="checkbox-1" class="checkbox-custom" name="rating" type="checkbox">
                        <label for="checkbox-1" class="checkbox-custom-label">
                          <i class="fa fa-star"></i> <i class="fa fa-star null"></i> <i class="fa fa-star null"></i> <i
                            class="fa fa-star null"></i> <i class="fa fa-star null"></i> <span>(800)</span>
                        </label>
                      </div>
                      <div class="">
                        <input id="checkbox-1" class="checkbox-custom" name="rating" type="checkbox">
                        <label for="checkbox-1" class="checkbox-custom-label">
                          <i class="fa fa-star null"></i> <i class="fa fa-star null"></i> <i
                            class="fa fa-star null"></i> <i
                            class="fa fa-star null"></i> <i class="fa fa-star null"></i> <span>(800)</span>
                        </label>
                      </div>
                    </div>
                    <h6>BY COLOR</h6>
                    <div class="box-side colour-prev">
                       <div class="colour-pict" style="background:blue"></div>
                       <div class="colour-pict" style="background:black"></div>
                       <div class="colour-pict" style="background:red"></div>
                       <div class="colour-pict" style="background:pink"></div>
                       <div class="colour-pict" style="background:purple"></div>
                       <div class="colour-pict" style="background:paleturquoise"></div>
                       <div class="colour-pict" style="background:green"></div>
                       <div class="colour-pict" style="background:yellow"></div>
                       <div class="colour-pict" style="background:blue"></div>
                       <div class="colour-pict" style="background:black"></div>
                       <div class="clearfix"></div>
                    </div>
                    <h6>BY SIZE</h6>
                    <div class="box-side">
                      <div class="size">XXL</div>
                      <div class="size">XL</div>
                      <div class="size">L</div>
                      <div class="size">S</div>
                      <div class="cearfix"></div>
                    </div>
                  </div>  
                </div>
            </div>
            <div class="col-md-9">
              <section>
                <div class="head-title">
                  <h5>Subcategory</h5>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="feat-product"
                      style="background:url('assets/front/img/slide2.jpg') no-repeat; height:auto;background-size: cover;">
                      <div class="row">
                        <div class="col-6">
                          <h4>unio leather bags</h4>
                        </div>
                        <div class="col-6">
                          <div class="circle-litle-disc">
                          </div>
                          <div class="circle-big-disc">
                            <span>20%</span>
                            <p>Off</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="feat-product"
                      style="background:url('assets/front/img/slide2.jpg') no-repeat; height:auto;background-size: cover;">
                      <div class="row">
                        <div class="col-6">
                          <h4>unio leather bags</h4>
                        </div>
                        <div class="col-6">
                          <div class="circle-litle-disc">
                          </div>
                          <div class="circle-big-disc">
                            <span>20%</span>
                            <p>Off</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="feat-product"
                      style="background:url('assets/front/img/slide2.jpg') no-repeat; height:auto;background-size: cover;">
                      <div class="row">
                        <div class="col-6">
                          <h4>unio leather bags</h4>
                        </div>
                        <div class="col-6">
                          <div class="circle-litle-disc">
                          </div>
                          <div class="circle-big-disc">
                            <span>20%</span>
                            <p>Off</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>

              <section id="sec-head-category">
                <div class="head-background">
                  <h5>Category : <!-- <?= $titleCat?> --> Kopi Nusantara</h5>
                  <div class="pull-right s-barang-head">
                    <form>
                      <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fa fa-search"></i></span>
                        </div>
                        <input type="text" class="form-control text-custom" placeholder="Cari nama barang">
                      </div>
                    </form>
                  </div>
                </div>
              </section>
              <section>
                <div class="head-title">
                  <h5>Best Seller Product</h5>
                </div>
                <div class="row">
                  <div class="col-sm-3">
                    <div class="">
                      <div class="disc-tem">
                        50%
                      </div>
                      <div class="hovereffect">
                        <a href="<?=base_url().'detail'?>"><img class="img-responsive" src="http://placehold.it/190x200" alt=""></a>
                        <div class="overlay">
                          <p class="icon-links">
                            <a href="">
                              <span class="fa fa-heart-o"></span>
                            </a>
                            <a href="#">
                              <span class="fa fa-shopping-cart"></span>
                            </a>
                          </p>
                        </div>
                      </div>
                      <div class="det-prod det-prod-2">
                        <div class="">
                          <a href="<?=base_url().'detail'?>">Lorem ipsum dolor amet</a>
                        </div>
                        <div class="star">
                          <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                            class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                            style="margin-left:10px">8</span>
                        </div>
                        <div class="">
                          <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="">
                      <div class="disc-tem">
                        50%
                      </div>
                      <div class="hovereffect">
                        <a href="<?=base_url().'detail'?>"><img class="img-responsive" src="http://placehold.it/190x200" alt=""></a>
                        <div class="overlay">
                          <p class="icon-links">
                            <a href="#">
                              <span class="fa fa-heart-o"></span>
                            </a>
                            <a href="#">
                              <span class="fa fa-shopping-cart"></span>
                            </a>
                          </p>
                        </div>
                      </div>
                      <div class="det-prod det-prod-2">
                        <div class="">
                          <a href="#">Lorem ipsum dolor amet</a>
                        </div>
                        <div class="star">
                          <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                            class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                            style="margin-left:10px">8</span>
                        </div>
                        <div class="">
                          <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="">
                      <div class="disc-tem">
                        50%
                      </div>
                      <div class="hovereffect">
                        <a href="<?=base_url().'detail'?>"><img class="img-responsive" src="http://placehold.it/190x200" alt=""></a>
                        <div class="overlay">
                          <p class="icon-links">
                            <a href="#">
                              <span class="fa fa-heart-o"></span>
                            </a>
                            <a href="#">
                              <span class="fa fa-shopping-cart"></span>
                            </a>
                          </p>
                        </div>
                      </div>
                      <div class="det-prod det-prod-2">
                        <div class="">
                          <a href="#">Lorem ipsum dolor amet</a>
                        </div>
                        <div class="star">
                          <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                            class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                            style="margin-left:10px">8</span>
                        </div>
                        <div class="">
                          <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="">
                      <div class="disc-tem">
                        50%
                      </div>
                      <div class="hovereffect">
                        <a href="<?=base_url().'detail'?>"><img class="img-responsive" src="http://placehold.it/190x200" alt=""></a>
                        <div class="overlay">
                          <p class="icon-links">
                            <a href="#">
                              <span class="fa fa-heart-o"></span>
                            </a>
                            <a href="#">
                              <span class="fa fa-shopping-cart"></span>
                            </a>
                          </p>
                        </div>
                      </div>
                      <div class="det-prod det-prod-2">
                        <div class="">
                          <a href="#">Lorem ipsum dolor amet</a>
                        </div>
                        <div class="star">
                          <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                            class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                            style="margin-left:10px">8</span>
                        </div>
                        <div class="">
                          <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                </div>
                </section>
                <section id="product-body">
                  <div class="head-title">
                    <h5>264 Products In Stock</h5>
                    <div class="pull-right sort-order">
                      <ul>
                        <li class="mr-3">
                         <span class="sort-ac"> Sort by:</span> <select class="custom-select custom-select-sm sort-ac">
                            <option selected="">Ascanding</option>
                            <option value="1">Descanding</option>
                          </select>
                          <div class="clearfix"></div>
                        </li>
                        <li>
                          <span class="sort-ac"> View by:</span> <i class="fa fa-th sort-ac"></i> <i
                            class="fa fa-align-justify sort-ac"></i>
                          <div class="clearfix"></div>
                        </li>

                      </ul>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-3">
                      <div class="">
                        <div class="disc-tem">
                          50%
                        </div>
                        <div class="hovereffect">
                          <a href="<?=base_url().'detail'?>"><img class="img-responsive" src="http://placehold.it/190x200" alt=""></a>
                          <div class="overlay">
                            <p class="icon-links">
                              <a href="#">
                                <span class="fa fa-heart-o"></span>
                              </a>
                              <a href="#">
                                <span class="fa fa-shopping-cart"></span>
                              </a>
                            </p>
                          </div>
                        </div>
                        <div class="det-prod det-prod-2">
                          <div class="">
                            <a href="#">Lorem ipsum dolor amet</a>
                          </div>
                          <div class="star">
                            <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                              class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                              style="margin-left:10px">8</span>
                          </div>
                          <div class="">
                            <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="">
                        <div class="disc-tem">
                          50%
                        </div>
                        <div class="hovereffect">
                          <a href="<?=base_url().'detail'?>"><img class="img-responsive" src="http://placehold.it/190x200" alt=""></a>
                          <div class="overlay">
                            <p class="icon-links">
                              <a href="#">
                                <span class="fa fa-heart-o"></span>
                              </a>
                              <a href="#">
                                <span class="fa fa-shopping-cart"></span>
                              </a>
                            </p>
                          </div>
                        </div>
                        <div class="det-prod det-prod-2">
                          <div class="">
                            <a href="#">Lorem ipsum dolor amet</a>
                          </div>
                          <div class="star">
                            <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                              class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                              style="margin-left:10px">8</span>
                          </div>
                          <div class="">
                            <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="">
                        <div class="disc-tem">
                          50%
                        </div>
                        <div class="hovereffect">
                          <a href="<?=base_url().'detail'?>"><img class="img-responsive" src="http://placehold.it/190x200" alt=""></a>
                          <div class="overlay">
                            <p class="icon-links">
                              <a href="#">
                                <span class="fa fa-heart-o"></span>
                              </a>
                              <a href="#">
                                <span class="fa fa-shopping-cart"></span>
                              </a>
                            </p>
                          </div>
                        </div>
                        <div class="det-prod det-prod-2">
                          <div class="">
                            <a href="#">Lorem ipsum dolor amet</a>
                          </div>
                          <div class="star">
                            <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                              class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                              style="margin-left:10px">8</span>
                          </div>
                          <div class="">
                            <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="">
                        <div class="disc-tem">
                          50%
                        </div>
                        <div class="hovereffect">
                          <a href="<?=base_url().'detail'?>"><img class="img-responsive" src="http://placehold.it/190x200" alt=""></a>
                          <div class="overlay">
                            <p class="icon-links">
                              <a href="#">
                                <span class="fa fa-heart-o"></span>
                              </a>
                              <a href="#">
                                <span class="fa fa-shopping-cart"></span>
                              </a>
                            </p>
                          </div>
                        </div>
                        <div class="det-prod det-prod-2">
                          <div class="">
                            <a href="#">Lorem ipsum dolor amet</a>
                          </div>
                          <div class="star">
                            <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                              class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                              style="margin-left:10px">8</span>
                          </div>
                          <div class="">
                            <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="">
                        <div class="disc-tem">
                          50%
                        </div>
                        <div class="hovereffect">
                          <a href="#"><img class="img-responsive" src="http://placehold.it/190x200" alt=""></a>
                          <div class="overlay">
                            <p class="icon-links">
                              <a href="#">
                                <span class="fa fa-heart-o"></span>
                              </a>
                              <a href="#">
                                <span class="fa fa-shopping-cart"></span>
                              </a>
                            </p>
                          </div>
                        </div>
                        <div class="det-prod det-prod-2">
                          <div class="">
                            <a href="#">Lorem ipsum dolor amet</a>
                          </div>
                          <div class="star">
                            <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                              class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                              style="margin-left:10px">8</span>
                          </div>
                          <div class="">
                            <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="">
                        <div class="disc-tem">
                          50%
                        </div>
                        <div class="hovereffect">
                          <a href="#"><img class="img-responsive" src="http://placehold.it/190x200" alt=""></a>
                          <div class="overlay">
                            <p class="icon-links">
                              <a href="#">
                                <span class="fa fa-heart-o"></span>
                              </a>
                              <a href="#">
                                <span class="fa fa-shopping-cart"></span>
                              </a>
                            </p>
                          </div>
                        </div>
                        <div class="det-prod det-prod-2">
                          <div class="">
                            <a href="#">Lorem ipsum dolor amet</a>
                          </div>
                          <div class="star">
                            <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                              class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                              style="margin-left:10px">8</span>
                          </div>
                          <div class="">
                            <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="">
                        <div class="disc-tem">
                          50%
                        </div>
                        <div class="hovereffect">
                          <a href="#"><img class="img-responsive" src="http://placehold.it/190x200" alt=""></a>
                          <div class="overlay">
                            <p class="icon-links">
                              <a href="#">
                                <span class="fa fa-heart-o"></span>
                              </a>
                              <a href="#">
                                <span class="fa fa-shopping-cart"></span>
                              </a>
                            </p>
                          </div>
                        </div>
                        <div class="det-prod det-prod-2">
                          <div class="">
                            <a href="#">Lorem ipsum dolor amet</a>
                          </div>
                          <div class="star">
                            <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                              class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                              style="margin-left:10px">8</span>
                          </div>
                          <div class="">
                            <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="">
                        <div class="disc-tem">
                          50%
                        </div>
                        <div class="hovereffect">
                          <a href="#"><img class="img-responsive" src="http://placehold.it/190x200" alt=""></a>
                          <div class="overlay">
                            <p class="icon-links">
                              <a href="#">
                                <span class="fa fa-heart-o"></span>
                              </a>
                              <a href="#">
                                <span class="fa fa-shopping-cart"></span>
                              </a>
                            </p>
                          </div>
                        </div>
                        <div class="det-prod det-prod-2">
                          <div class="">
                            <a href="#">Lorem ipsum dolor amet</a>
                          </div>
                          <div class="star">
                            <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                              class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                              style="margin-left:10px">8</span>
                          </div>
                          <div class="">
                            <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>

                    <?php
                      // foreach ($getAllProduk as $k => $v) {
                    ?>
                    <!-- <div class="col-sm-3">
                      <div class="">
                        <div class="hovereffect">
                          <a href="#"><img class="img-responsive" src="<?= produk::gambar($v->id,'small')?>" alt="" width="190" height="200"></a>
                          <div class="overlay">
                            <p class="icon-links">
                              <a href="#">
                                <span class="fa fa-heart-o"></span>
                              </a>
                              <a href="#">
                                <span class="fa fa-shopping-cart"></span>
                              </a>
                            </p>
                          </div>
                        </div>
                        <div class="det-prod det-prod-2">
                          <div class="">
                            <a href="<?= base_url().'detail/pd?id='.urlencode(codeEncrypt($v->id)).'&&'.'title='.url_title($v->nama_barang);?>"><?= $v->nama_barang?></a>
                          </div>
                          <div class="star">
                            <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                              class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                              style="margin-left:10px">8</span>
                          </div>
                          <div class="">
                            <span class="true-price"><?= myNum(produk::detail($v->id,'harga_satuan'),'Rp');?></span>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                      </div>
                    </div> -->
                    <?php //} ?>
                  </div>
                </section>
                <section id="sec-pagination" class="text-center">
                  <ul>
                    <li>
                     <a href=""><i class="fa fa-angle-left"></i> Previous page</a>
                    </li>
                    <li class="current">1</li>
                    <li><a href="">2</a></li>
                    <li><a href="">3</a></li>
                    <li><a href="">4</a></li>
                    <li><a href="">5</a></li>
                    <li>...</li>
                    <li><a href="">100</a></li>
                    <li>
                      <a href="">Next page <i class="fa fa-angle-right"></i></a>
                    </li>
                  </ul>
                </section>
              
            </div>
        </div>
     </div>
 </section>
</body>

