<body>
    <div class="container">
        <div class="row">
            <div class="col-md-5 text-left">
                <h3>Add New Product</h3>
                <div class="">
                    <p>Selamat datang, Kelola setiap alur masuk dan keuar barang anda. Dashboard didesain mudah agar
                        anda
                        bisa mentracking barang yang sedang anda jual</p>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <span style="font-weight:500">MAIN MENU</span>
                    <ul class="menu-vendor-setting mt-3">
                        <li>
                            <a href="<?=base_url().'vendor/add_product';?>">Product Information</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="<?=base_url().'vendor/add_product_photo';?>">Product Photo</a> <i class="fa fa-angle-right"></i>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <form action="add-photo-product.html">
                <div class="card">
                    <h5>Product Information</h5>
                     <h6 class="mt-5 mb-2 mb-4"><span class="required">*</span> Your Product Information</h6>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                 <label for="exampleInputEmail1">Product Name</label>
                                <input type="text" class="form-control" placeholder="Your product name">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Product Category</label>
                                <select name="" class="form-controll custom-select" id="">
                                    <option value="">Select product category</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Product SubCategory</label>
                                <select name="" class="form-controll custom-select" id="">
                                   <option value="">Select product subcategory</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h6 class="mt-5 mb-4"><span class="required">*</span> Details Product</h6>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Product Price</label>
                                         <span class="input-group-text input-group-text-detail" >Rp.</span>
                                        <input type="text" class="form-control pl-5" placeholder="Product price">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Units</label>
                                        <span class="input-group-text input-group-text-detail" style="right:10px; width: auto">Units</span>
                                        <input type="text" class="form-control pr-5" placeholder="Product Stocks">
                                        
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Product Weight</label>
                                        <span class="input-group-text input-group-text-detail"
                                            style="right:10px; width: auto">Gram</span>
                                        <input type="text" class="form-control pr-5" placeholder="Product Weight">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h6 class="mt-3 mb-4"><span class="required">*</span> Product Varians</h6>
                            <div class="row">
                                <div class="col-md-6">
                                    <select name="" class="select2 form-controll custom-select" id=""
                                        multiple="multiple">
                                        <option value="">Select product category</option>
                                         <option value="3">Select product category</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <select name="" class="select2 form-controll custom-select" multiple="multiple" id="">
                                        <option value="">Select product category</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                         <div class="col-md-12">
                             <h6 class="mt-5 mb-4"><span class="required">*</span> Product Strength Point</h6>
                             <div class="form-group">
                                 <label for="exampleInputEmail1">Strength point</label>
                                 <input type="text" class="form-control" placeholder="Your product name">
                             </div>
                         </div>
                        <div class="col-md-12">
                            <h6 class="mt-3 mb-4"><span class="required">*</span> Shop Description</h6>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea name="" id="" cols="100" rows="40" class="form-control"
                                            style="height:200px"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 text-right mt-4">
                            <button class="btn btn-shop">Next Step</button>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</body>

