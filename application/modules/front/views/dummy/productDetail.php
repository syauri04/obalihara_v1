<body>
  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-9">
          <div class="card">
            <div class="row">
              <div class="col-md-6">
                <div class="prev-product">
                  <div class="left-prev-product">
                    <ul class="thumbnails">
                      <li>
                        <a href="http://placehold.it/700x900" data-standard="http://placehold.it/350x400">
                          <img src="http://placehold.it/50x55" alt="" />
                        </a>
                      </li>
                      <li>
                        <a href="http://placehold.it/700x900" data-standard="http://placehold.it/350x400">
                          <img src="http://placehold.it/50x55" alt="" />
                        </a>
                      </li>
                      <li>
                        <a href="http://placehold.it/700x900" data-standard="http://placehold.it/350x400">
                          <img src="http://placehold.it/50x55" alt="" />
                        </a>
                      </li>
                      <li>
                        <a href="http://placehold.it/700x900" data-standard="http://placehold.it/350x400">
                          <img src="http://placehold.it/50x55" alt="" />
                        </a>
                      </li> 
                    </ul>
                  </div>
                  <div class="right-prev-product">
                    <div class="easyzoom easyzoom--overlay easyzoom--with-thumbnails">
                      <a href="http://placehold.it/700x900">
                        <img src="http://placehold.it/350x400" alt="" width="330" height="400" />
                      </a>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="col-md-6">
                <h4>Marshall Kilburn Portable Wireless Bluetooth Speaker? Black (4091189)</h4>
                <div class="sub-title">
                  <ul>
                    <li> <span>Vendor : <a href="">Marshall Shop</a></span></li>
                    <li>
                      <div class="star">
                        <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                          class="fa fa-star"></i> <i class="fa fa-star null"></i> <i class="count-start">8</i>
                      </div>
                    </li>
                    <li><span>Status : <span class="text-success">In stock</span></span></li>
                  </ul>
                </div>
                <div id="price-product">
                  <span class="disc-price">Rp. 150.000</span> <span class="true-price">Rp. 75.000</span>
                </div>
                <div id="description-prev">
                  <ul>
                    <li>Lorem ipsum dolor sit amet, consectetur</li>
                    <li>adipiscing elit, sed do eiusmod tempor</li>
                    <li>incididunt ut labore et dolore</li>
                  </ul>
                </div>
                <div id="colour-prev">
                  <div class="colour-pict" style="background:blue"></div>
                  <div class="colour-pict" style="background:black"></div>
                  <div class="colour-pict" style="background:red"></div>
                  <div class="colour-pict" style="background:pink"></div>
                  <div class="clearfix"></div>
                </div>
                <div id="keranjang">
                  <form action="<?=base_url().'cart';?>">
                    <select class="custom-select custom-select-sm" style="width:70px">
                      <option selected="">10</option>
                      <option value="1">5</option>
                      <option value="2">3</option>
                    </select>
                    <button class="btn btn-add"><i class="fa fa-shopping-cart"></i> Tambah ke Keranjang</button>
                    <a href="<?=base_url().'whislist';?>"><i class="fa fa-heart-o pull-right" style="font-size:25px;margin-top:5px;"></i></a>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="card profile-toko">
            <table id="head-prof">
              <tr>
                <td width="50px" rowspan="2">
                  <div class="profile-pic" style="background:#000"></div>
                </td>
                <td><a class="title-profile" href="">Marshall Shop</a></td>
              </tr>
              <tr>
                <td>
                  <span style="color:#9b9db0;">213 product</span>
                </td>
              </tr>
            </table>
            <hr>
            <p class="desc-toko" style="margin-bottom:0px">
              Rating Toko
              <div class="star">
                <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                  class="fa fa-star"></i> <i class="fa fa-star null"></i>
              </div>
              <span style="margin-top:0px" class="text-success">80% positive</span>
              </p>
              <p class="desc-toko">
                Wilayah
                <br>
                <span>Bogor, Jawa Barat</span>
              </p>
              <p class="desc-toko">
                Bergabung Sejak
                <br>
                <span>November 2018</span>
              </p>
              <p class="desc-toko">
                Kurir yang Tersedia
                <br>
               <img style="margin-top:10px;" src="assets/img/courir.png" alt="">
              </p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="sec-desc">
    <div class="container">
      <div class="head-background">
        <ul class="nav nav-tabs">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#descs">
              <h6>Product Description</h6>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#review">
              <h6>Customer Review</h6>
            </a>
          </li>
        </ul>
      </div>
      <div class="tab-content">
        <div class="tab-pane active" id="descs">
          <div class="contain-desc">
            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem
            aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
            Nemo
            enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores
            eos
            qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet,
            consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam
            aliquam
            quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit
            laboriosam,
            nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit
            esse
            quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
          </div>
        </div>
        <div class="tab-pane" id="review">
          <div class="contain-desc">
            
          </div>
        </div>
      </div>
      
        
      
      
    </div>
  </section>
  <section id="makanan-section">
    <div class="container">
      <div class="head-background">
        <h5>Produk Lainnya yang Sejenis</h5> 
      </div>
      <div id="makanan" class="carousel slide" data-ride="carousel" data-interval="0">
        <div class="carousel-inner">
          <div class="item carousel-item active">
            <div class="row">
              <div class="col-sm-2">
                <div class="">
                  <div class="disc-tem">
                    50%
                  </div>
                  <div class="hovereffect">
                    <a href="#"><img class="img-responsive" src="http://placehold.it/160x200" alt=""></a>
                    <div class="overlay">
                      <p class="icon-links">
                        <a href="#">
                          <span class="fa fa-heart-o"></span>
                        </a>
                        <a href="#">
                          <span class="fa fa-shopping-cart"></span>
                        </a>
                      </p>
                    </div>
                  </div>
                  <div class="det-prod">
                    <div class="">
                      <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                    </div>
                    <div class="">
                      <a href="#">Lorem ipsum dolor amet</a>
                    </div>
                    <div class="">
                      <p>Sold by: Game lorem</p>
                    </div>
                    <div class="star">
                      <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                        class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="">
                  <div class="disc-tem">
                    50%
                  </div>
                  <div class="hovereffect">
                    <a href="#"><img class="img-responsive" src="http://placehold.it/160x200" alt=""></a>
                    <div class="overlay">
                      <p class="icon-links">
                        <a href="#">
                          <span class="fa fa-heart-o"></span>
                        </a>
                        <a href="#">
                          <span class="fa fa-shopping-cart"></span>
                        </a>
                      </p>
                    </div>
                  </div>
                  <div class="det-prod">
                    <div class="">
                      <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                    </div>
                    <div class="">
                      <a href="#">Lorem ipsum dolor amet</a>
                    </div>
                    <div class="">
                      <p>Sold by: Game lorem</p>
                    </div>
                    <div class="star">
                      <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                        class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="">
                  <div class="disc-tem">
                    50%
                  </div>
                  <div class="hovereffect">
                    <a href="#"><img class="img-responsive" src="http://placehold.it/160x200" alt=""></a>
                    <div class="overlay">
                      <p class="icon-links">
                        <a href="#">
                          <span class="fa fa-heart-o"></span>
                        </a>
                        <a href="#">
                          <span class="fa fa-shopping-cart"></span>
                        </a>
                      </p>
                    </div>
                  </div>
                  <div class="det-prod">
                    <div class="">
                      <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                    </div>
                    <div class="">
                      <a href="#">Lorem ipsum dolor amet</a>
                    </div>
                    <div class="">
                      <p>Sold by: Game lorem</p>
                    </div>
                    <div class="star">
                      <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                        class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="">
                  <div class="disc-tem">
                    50%
                  </div>
                  <div class="hovereffect">
                    <a href="#"><img class="img-responsive" src="http://placehold.it/160x200" alt=""></a>
                    <div class="overlay">
                      <p class="icon-links">
                        <a href="#">
                          <span class="fa fa-heart-o"></span>
                        </a>
                        <a href="#">
                          <span class="fa fa-shopping-cart"></span>
                        </a>
                      </p>
                    </div>
                  </div>
                  <div class="det-prod">
                    <div class="">
                      <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                    </div>
                    <div class="">
                      <a href="#">Lorem ipsum dolor amet</a>
                    </div>
                    <div class="">
                      <p>Sold by: Game lorem</p>
                    </div>
                    <div class="star">
                      <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                        class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="">
                  <div class="disc-tem">
                    50%
                  </div>
                  <div class="hovereffect">
                    <a href="#"><img class="img-responsive" src="http://placehold.it/160x200" alt=""></a>
                    <div class="overlay">
                      <p class="icon-links">
                        <a href="#">
                          <span class="fa fa-heart-o"></span>
                        </a>
                        <a href="#">
                          <span class="fa fa-shopping-cart"></span>
                        </a>
                      </p>
                    </div>
                  </div>
                  <div class="det-prod">
                    <div class="">
                      <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                    </div>
                    <div class="">
                      <a href="#">Lorem ipsum dolor amet</a>
                    </div>
                    <div class="">
                      <p>Sold by: Game lorem</p>
                    </div>
                    <div class="star">
                      <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                        class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="">
                  <div class="disc-tem">
                    50%
                  </div>
                  <div class="hovereffect">
                    <a href="#"><img class="img-responsive" src="http://placehold.it/160x200" alt=""></a>
                    <div class="overlay">
                      <p class="icon-links">
                        <a href="#">
                          <span class="fa fa-heart-o"></span>
                        </a>
                        <a href="#">
                          <span class="fa fa-shopping-cart"></span>
                        </a>
                      </p>
                    </div>
                  </div>
                  <div class="det-prod">
                    <div class="">
                      <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                    </div>
                    <div class="">
                      <a href="#">Lorem ipsum dolor amet</a>
                    </div>
                    <div class="">
                      <p>Sold by: Game lorem</p>
                    </div>
                    <div class="star">
                      <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                        class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>

          </div>
          <div class="item carousel-item">
            <div class="row">
              <div class="col-sm-2">
                <div class="">
                  <div class="disc-tem">
                    50%
                  </div>
                  <div class="hovereffect">
                    <a href="#"><img class="img-responsive" src="http://placehold.it/160x200" alt=""></a>
                    <div class="overlay">
                      <p class="icon-links">
                        <a href="#">
                          <span class="fa fa-heart-o"></span>
                        </a>
                        <a href="#">
                          <span class="fa fa-shopping-cart"></span>
                        </a>
                      </p>
                    </div>
                  </div>
                  <div class="det-prod">
                    <div class="">
                      <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                    </div>
                    <div class="">
                      <a href="#">Lorem ipsum dolor amet</a>
                    </div>
                    <div class="">
                      <p>Sold by: Game lorem</p>
                    </div>
                    <div class="star">
                      <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                        class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="">
                  <div class="disc-tem">
                    50%
                  </div>
                  <div class="hovereffect">
                    <a href="#"><img class="img-responsive" src="http://placehold.it/160x200" alt=""></a>
                    <div class="overlay">
                      <p class="icon-links">
                        <a href="#">
                          <span class="fa fa-heart-o"></span>
                        </a>
                        <a href="#">
                          <span class="fa fa-shopping-cart"></span>
                        </a>
                      </p>
                    </div>
                  </div>
                  <div class="det-prod">
                    <div class="">
                      <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                    </div>
                    <div class="">
                      <a href="#">Lorem ipsum dolor amet</a>
                    </div>
                    <div class="">
                      <p>Sold by: Game lorem</p>
                    </div>
                    <div class="star">
                      <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                        class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="">
                  <div class="disc-tem">
                    50%
                  </div>
                  <div class="hovereffect">
                    <a href="#"><img class="img-responsive" src="http://placehold.it/160x200" alt=""></a>
                    <div class="overlay">
                      <p class="icon-links">
                        <a href="#">
                          <span class="fa fa-heart-o"></span>
                        </a>
                        <a href="#">
                          <span class="fa fa-shopping-cart"></span>
                        </a>
                      </p>
                    </div>
                  </div>
                  <div class="det-prod">
                    <div class="">
                      <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                    </div>
                    <div class="">
                      <a href="#">Lorem ipsum dolor amet</a>
                    </div>
                    <div class="">
                      <p>Sold by: Game lorem</p>
                    </div>
                    <div class="star">
                      <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                        class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="">
                  <div class="disc-tem">
                    50%
                  </div>
                  <div class="hovereffect">
                    <a href="#"><img class="img-responsive" src="http://placehold.it/160x200" alt=""></a>
                    <div class="overlay">
                      <p class="icon-links">
                        <a href="#">
                          <span class="fa fa-heart-o"></span>
                        </a>
                        <a href="#">
                          <span class="fa fa-shopping-cart"></span>
                        </a>
                      </p>
                    </div>
                  </div>
                  <div class="det-prod">
                    <div class="">
                      <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                    </div>
                    <div class="">
                      <a href="#">Lorem ipsum dolor amet</a>
                    </div>
                    <div class="">
                      <p>Sold by: Game lorem</p>
                    </div>
                    <div class="star">
                      <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                        class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="">
                  <div class="disc-tem">
                    50%
                  </div>
                  <div class="hovereffect">
                    <a href="#"><img class="img-responsive" src="http://placehold.it/160x200" alt=""></a>
                    <div class="overlay">
                      <p class="icon-links">
                        <a href="#">
                          <span class="fa fa-heart-o"></span>
                        </a>
                        <a href="#">
                          <span class="fa fa-shopping-cart"></span>
                        </a>
                      </p>
                    </div>
                  </div>
                  <div class="det-prod">
                    <div class="">
                      <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                    </div>
                    <div class="">
                      <a href="#">Lorem ipsum dolor amet</a>
                    </div>
                    <div class="">
                      <p>Sold by: Game lorem</p>
                    </div>
                    <div class="star">
                      <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                        class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="col-sm-2">
                <div class="">
                  <div class="disc-tem">
                    50%
                  </div>
                  <div class="hovereffect">
                    <a href="#"><img class="img-responsive" src="http://placehold.it/160x200" alt=""></a>
                    <div class="overlay">
                      <p class="icon-links">
                        <a href="#">
                          <span class="fa fa-heart-o"></span>
                        </a>
                        <a href="#">
                          <span class="fa fa-shopping-cart"></span>
                        </a>
                      </p>
                    </div>
                  </div>
                  <div class="det-prod">
                    <div class="">
                      <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                    </div>
                    <div class="">
                      <a href="#">Lorem ipsum dolor amet</a>
                    </div>
                    <div class="">
                      <p>Sold by: Game lorem</p>
                    </div>
                    <div class="star">
                      <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                        class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- Carousel controls -->
        <a class="carousel-control-prev" href="#makanan" data-slide="prev">
          <div class="control-slide">
            <i class="fa fa-angle-left"></i>
          </div>
        </a>
        <a class="carousel-control-next" href="#makanan" data-slide="next">
          <div class="control-slide">
            <i class="fa fa-angle-right"></i>
          </div>
        </a>
      </div>
    </div>
  </section>
</body>