<body>
    <section>
    <div class="container">
      <div id="information" class="text-center centerin">
        <div class="mb-2">
          <h3>Checkout Orders</h3>
        </div>
      </div>
    </section>
    <section class="mt-5">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div class="head-title">
            <h5>Shipping Address</h5>
          </div>
          <div class="card mb-5">
            <div style="width:30%;">
              <h6>Address 1</h6>
              <div>
                <span><b>Michael Panceli</b></span>
              </div>
              <div>
                <span>
                  Jl. Nasional 11
                  Tegallega
                  Bogor Tengah
                  Kota Bogor
                  Jawa Barat 16122 </span>
              </div>
              <div class="mt-2"><span>(0251) 8386658</span></div>
            </div>
            <hr>
            <a href=""><b>Ganti Alamat Pengirim</b></a>
          </div>
          <div class="head-title">
            <h5>Order Details</h5>
          </div>
          <table class="table">
            <tr>
              <td>
                <div class="row">
                  <div class="col-md-3">
                    <img src="http://placehold.it/80x80" alt="">
                  </div>
                  <div class="col-md-9">
                    <div>
                      <a class="title-product" href="">Marshall Lorem Ipsum dolor
                        amet (149077)</a>
                    </div>
                    <div>
                      <span>Color : <b>Black</b></span>
                    </div>
                    <div>
                      <span><b>1 x Rp. 720.000 = Rp. 720.000</b></span>
                    </div>
                  </div>
                </div>
              </td>
              <td>
                <div>
                  <b>Expedition</b>
                </div>
                <div>
                  <select class="custom-select custom-select-sm sort-ac">
                    <option selected="">Pos Indonesia</option>
                    <option value="1">JNE</option>
                  </select>
                </div>
                <div><span class="text-pink"><b>Rp. 20.000</b></span> 3-7 hari</div>
              </td>
              <td>
                <div><b>Total</b></div>
                <div><span class="text-pink"><b>Rp. 72.000</b> </span> </div>
              </td>
            </tr>
            <tr>
              <td>
                <div class="row">
                  <div class="col-md-3">
                    <img src="http://placehold.it/80x80" alt="">
                  </div>
                  <div class="col-md-9">
                    <div>
                      <a class="title-product" href="">Marshall Lorem Ipsum dolor
                        amet (149077)</a>
                    </div>
                    <div>
                      <span>Color : <b>Black</b></span>
                    </div>
                    <div>
                      <span><b>1 x Rp. 720.000 = Rp. 720.000</b></span>
                    </div>
                  </div>
                </div>
              </td>
              <td>
                <div>
                  <b>Expedition</b>
                </div>
                <div>
                  <select class="custom-select custom-select-sm sort-ac">
                    <option selected="">Pos Indonesia</option>
                    <option value="1">JNE</option>
                  </select>
                </div>
                <div><span class="text-pink"><b>Rp. 20.000</b></span> 3-7 hari</div>
              </td>
              <td>
                <div><b>Total</b></div>
                <div><span class="text-pink"><b>Rp. 72.000</b> </span> </div>
              </td>
            </tr>
            <tr>
              <td>
                <div class="row">
                  <div class="col-md-3">
                    <img src="http://placehold.it/80x80" alt="">
                  </div>
                  <div class="col-md-9">
                    <div>
                      <a class="title-product" href="">Marshall Lorem Ipsum dolor
                        amet (149077)</a>
                    </div>
                    <div>
                      <span>Color : <b>Black</b></span>
                    </div>
                    <div>
                      <span><b>1 x Rp. 720.000 = Rp. 720.000</b></span>
                    </div>
                  </div>
                </div>
              </td>
              <td>
                <div>
                  <b>Expedition</b>
                </div>
                <div>
                  <select class="custom-select custom-select-sm sort-ac">
                    <option selected="">Pos Indonesia</option>
                    <option value="1">JNE</option>
                  </select>
                </div>
                <div><span class="text-pink"><b>Rp. 20.000</b></span> 3-7 hari</div>
              </td>
              <td>
                <div><b>Total</b></div>
                <div><span class="text-pink"><b>Rp. 72.000</b> </span> </div>
              </td>
            </tr>
            <tr>
              <td>
                <div class="row">
                  <div class="col-md-3">
                    <img src="http://placehold.it/80x80" alt="">
                  </div>
                  <div class="col-md-9">
                    <div>
                      <a class="title-product" href="">Marshall Lorem Ipsum dolor
                        amet (149077)</a>
                    </div>
                    <div>
                      <span>Color : <b>Black</b></span>
                    </div>
                    <div>
                      <span><b>1 x Rp. 720.000 = Rp. 720.000</b></span>
                    </div>
                  </div>
                </div>
              </td>
              <td>
                <div>
                  <b>Expedition</b>
                </div>
                <div>
                  <select class="custom-select custom-select-sm sort-ac">
                    <option selected="">Pos Indonesia</option>
                    <option value="1">JNE</option>
                  </select>
                </div>
                <div><span class="text-pink"><b>Rp. 20.000</b></span> 3-7 hari</div>
              </td>
              <td>
                <div><b>Total</b></div>
                <div><span class="text-pink"><b>Rp. 72.000</b> </span> </div>
              </td>
            </tr>
          </table>
        </div>
        <div class="col-md-4">
          <div class="card">
            <div class="head-title">
              <h5>Billing Details</h5>
            </div>
            <table class="w-100 bill">
              <tr>
                <td width="50%">Total Product Rate:</td>
                <td width="50%" class="text-right"><b>Rp. 425.000</b></td>
              </tr>
              <tr>
                <td width="50%">Total Expedition Rate:</td>
                <td width="50%" class="text-right"><b>Rp. 25.000</b></td>
              </tr>
            </table>
            <hr>
            <div class="voucher-box">153020</div>
            <hr>
            <table class="w-100 bill">
              <tr>
                <td width="50%"><b>Sub Total</b></td>
                <td width="50%" class="text-right"><b>Rp. 425.000</b></td>
              </tr>
              <tr>
                <td width="50%"><span class="text-success"><b>Discount (15%)</b></span></td>
                <td width="50%" class="text-right"><span class="text-success"><b>-Rp. 45.000</b></span></td>
              </tr>
              <tr>
                <td width="50%"><h6>TOTAL</h6></td>
                <td width="50%" class="text-right">
                  <h6 class="text-pink">Rp.72.000</h6>
                </td>
              </tr>
            </table>
          </div>
           <div>
             <button class="btn btn-shop w-100"><i class="fa fa-credit-card"></i> Payment Method</button>
           </div>
        </div>
      </div>
    </div>
    </section>
    <br>
    <section class="mt-3">
    <div class="container">
      <a class="back" href="product-category.html"><i class="fa fa-angle-left"></i> Back to Shopping</a>
    </div>
    </section>
</body>

