<body>
    <div class="container">
        <div class="row">
            <div class="col-md-5 text-left">
                <h3>Add New Product</h3>
                <div class="">
                    <p>Selamat datang, Kelola setiap alur masuk dan keuar barang anda. Dashboard didesain mudah agar
                        anda
                        bisa mentracking barang yang sedang anda jual</p>
                </div>
            </div>
        </div>
        <hr>
        
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <span style="font-weight:500">MAIN MENU</span>
                    <ul class="menu-vendor-setting mt-3">
                        <li>
                            <a href="<?=base_url().'vendor/add_product';?>">Product Information</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="<?=base_url().'vendor/add_product_photo';?>">Product Photo</a> <i class="fa fa-angle-right"></i>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-3">
                        <div id="body-overlay">
                            <div><img src="assets/img/loading.gif" width="64px" height="64px" /></div>
                        </div>
                        <div class="bgColor">
                            <div id="targetOuter">
                                <div id="targetLayer"></div>
                                <i class="fa fa-camera"></i>
                                <div class="icon-choose-image">
                                    <input name="userImage" id="userImage" type="file" class="inputFile"
                                        onChange="showPreview(this);" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div id="body-overlay">
                            <div><img src="assets/img/loading.gif" width="64px" height="64px" /></div>
                        </div>
                        <div class="bgColor">
                            <div id="targetOuter">
                                <div id="targetLayer"></div>
                                <i class="fa fa-camera"></i>
                                <div class="icon-choose-image">
                                    <input name="userImage" id="userImage" type="file" class="inputFile"
                                        onChange="showPreview(this);" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div id="body-overlay">
                            <div><img src="assets/img/loading.gif" width="64px" height="64px" /></div>
                        </div>
                        <div class="bgColor">
                            <div id="targetOuter">
                                <div id="targetLayer"></div>
                                <i class="fa fa-camera"></i>
                                <div class="icon-choose-image">
                                    <input name="userImage" id="userImage" type="file" class="inputFile"
                                        onChange="showPreview(this);" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div id="body-overlay">
                            <div><img src="assets/img/loading.gif" width="64px" height="64px" /></div>
                        </div>
                        <div class="bgColor">
                            <div id="targetOuter">
                                <div id="targetLayer"></div>
                                <i class="fa fa-camera"></i>
                                <div class="icon-choose-image">
                                    <input name="userImage" id="userImage" type="file" class="inputFile"
                                        onChange="showPreview(this);" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div id="body-overlay">
                            <div><img src="assets/img/loading.gif" width="64px" height="64px" /></div>
                        </div>
                        <div class="bgColor">
                            <div id="targetOuter">
                                <div id="targetLayer"></div>
                                <i class="fa fa-camera"></i>
                                <div class="icon-choose-image">
                                    <input name="userImage" id="userImage" type="file" class="inputFile"
                                        onChange="showPreview(this);" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div id="body-overlay">
                            <div><img src="assets/img/loading.gif" width="64px" height="64px" /></div>
                        </div>
                        <div class="bgColor">
                            <div id="targetOuter">
                                <div id="targetLayer"></div>
                                <i class="fa fa-camera"></i>
                                <div class="icon-choose-image">
                                    <input name="userImage" id="userImage" type="file" class="inputFile"
                                        onChange="showPreview(this);" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div id="body-overlay">
                            <div><img src="assets/img/loading.gif" width="64px" height="64px" /></div>
                        </div>
                        <div class="bgColor">
                            <div id="targetOuter">
                                <div id="targetLayer"></div>
                                <i class="fa fa-camera"></i>
                                <div class="icon-choose-image">
                                    <input name="userImage" id="userImage" type="file" class="inputFile"
                                        onChange="showPreview(this);" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div id="body-overlay">
                            <div><img src="assets/img/loading.gif" width="64px" height="64px" /></div>
                        </div>
                        <div class="bgColor">
                            <div id="targetOuter">
                                <div id="targetLayer"></div>
                                <i class="fa fa-camera"></i>
                                <div class="icon-choose-image">
                                    <input name="userImage" id="userImage" type="file" class="inputFile"
                                        onChange="showPreview(this);" />
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            
            <div class="col-md-12 text-right mt-4">
                <button class="btn btn-shop">Save changes</button>
            </div>
        </div>
    </div>
</body>

