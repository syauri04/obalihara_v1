<body>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h3>Buyer Dashboard</h3>
                <div class="">
                    <p>Selamat datang, Kelola setiap alur masuk dan keuar barang anda. Dashboard didesain mudah agar
                        anda
                        bisa mentracking barang yang sedang anda jual</p>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <span style="font-weight:500">MAIN MENU</span>
                    <ul class="menu-vendor-setting mt-3">
                        <li>
                          <a href="<?=base_url().'buyer';?>">Dashboard</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="<?=base_url().'buyer/personal';?>">Personal Information</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="<?=base_url().'buyer/shipping';?>">Shipping Address</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="<?=base_url().'buyer/order';?>">Your Order</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="<?=base_url().'buyer/change_password';?>">Change Password</a> <i class="fa fa-angle-right"></i>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                    <h5 class="mb-5">Personal Information</h5>
                    <div class="row">
                        <div class="col-md-3">
                            <div id="body-overlay">
                                <div><img src="assets/img/loading.gif" width="64px" height="64px" /></div>
                            </div>
                            <div class="bgColor">
                                <form id="uploadForm" action="upload.php" method="post">
                                    <div id="targetOuter">
                                        <div id="targetLayer"></div>
                                        <i class="fa fa-camera"></i>
                                        <div class="icon-choose-image">
                                            <input name="userImage" id="userImage" type="file" class="inputFile"
                                                onChange="showPreview(this);" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-5">
                                            <button class="btn btn-shop w-100"><i class="fa fa-image"></i></button>
                                        </div>
                                        <div class="col-5">
                                            <button class="btn btn-white w-100"><i class="fa fa-trash"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <h6 class="mt-5 mb-2"><span class="required">*</span> Your Name</h6>
                            <div class="form-group">
                                <small id="emailHelp" class="form-text text-muted">Your name is public</small>
                                <input type="text" class="form-control mt-2" id="exampleInputEmail1"
                                    aria-describedby="emailHelp" placeholder="Your name">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h6 class="mt-5 mb-4"><span class="required">*</span> Your Contact</h6>
                            <div class="row">
                               <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email Address</label>
                                        <input type="email" class="form-control" placeholder="Email Adress">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Phone Number</label>
                                        <input type="text" class="form-control" placeholder="Phone Number">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h6 class="mt-5 mb-4"><span class="required">*</span> Your Address</h6>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Province</label>
                                        <select name="" class="form-controll custom-select" id="">
                                            <option value="d">d</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">City</label>
                                        <select name="" class="form-controll custom-select" id="">
                                            <option value="d">d</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Postal code</label>
                                        <input type="text" class="form-control" placeholder="Postal code">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Address</label>
                                        <input type="text" class="form-control" placeholder="Address">
                                    </div>
                                </div>
                                <div class="col-md-12 text-right mt-4">
                                    <button class="btn btn-shop">Save changes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</body>

