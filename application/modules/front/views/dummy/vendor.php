<body>
  <section>
     <div class="container">
       <div class="row">
         <div class="col-md-5 text-left">
           <h3>Vendor Dashboard</h3>
            <div class="">
               <p>Selamat datang, Kelola setiap alur masuk dan keuar barang anda. Dashboard didesain mudah agar anda
                 bisa mentracking barang yang sedang anda jual</p>
            </div>
         </div>
         <div class="col-md-5 offset-md-2">
          <ul class="top-info-vendor clearfix">
            <li>
              <div>
                <i class="text-success fa fa-caret-up"></i> Total Earning
              </div>
              <div>
                <h5>Rp. 1.000.000</h5>
              </div>
            </li>
            <li class="border-right">
              <div>
                <i class="text-danger fa fa-caret-down"></i> This Week Earning
              </div>
              <div>
                <h5>Rp. 1.000.000</h5>
              </div>
            </li>
            <li class="border-right">
              <div>
                <i class="text-success fa fa-caret-up"></i> Total Earning
              </div>
              <div>
               <h5>Rp. 1.000.000</h5>
              </div>
            </li>
          </ul>
          <div class="text-right mt-1">
            <a href="#" style="font-weight:500">Withdraw Earning</a>
          </div>
         </div>
       </div>
     </div>
 </section>
 <section class="mt-5">
   <div class="container">
     <div class="head-background">
       <ul class="nav nav-tabs">
         <li class="nav-item">
           <a class="nav-link active" data-toggle="tab" href="#dashboard">
             <h6>Dashboard</h6>
           </a>
         </li>
         <li class="nav-item">
           <a class="nav-link" data-toggle="tab" href="#products">
             <h6>Product</h6>
           </a>
         </li>
         <li class="nav-item">
           <a class="nav-link" data-toggle="tab" href="#orders">
             <h6>Orders</h6>
           </a>
         </li>
         <li class="nav-item">
           <a class="nav-link" data-toggle="tab" href="#setting">
             <h6>Setting</h6>
           </a>
         </li>
       </ul>
     </div>
     <div class="tab-content mt-5">
       <div class="tab-pane active" id="dashboard">
          <h5>New Order-in (3)</h5>
          <hr>
          <table class="table">
            <thead>
              <tr>
                <th>Date</th>
                <th>Product Detail</th>
                <th>Price</th>
                <th>Qnty</th>
                <th>Total Payment</th>
                <th>Status</th>
                <th></th>
              </tr>
            </thead>
            <tr>
              <td>Okt 16, 2019</td>
              <td>
                <div><a class="title-product" href="<?=base_url().'vendor/order_detail';?>">Marshall Lorem Ipsum dolor amet (149077)</a></div>
                <div>Order Id : #271894h</div>
                <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
                </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34</span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                <div><span class="text-success">Order-in</span></div>
                <div>Payment received</div>

              </td>
              <td>
                <div>
                  <a href="<?=base_url().'cart';?>">
                    <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                  </a>
                </div>
                <div class="mt-2">
                  <a href="<?=base_url().'vendor/order_detail';?>">View Details</a>
                </div>
              
              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td>
                <div><a class="title-product" href="<?=base_url().'vendor/order_detail';?>">Marshall Lorem Ipsum dolor amet (149077)</a></div>
                <div>Order Id : #271894h</div>
                <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34</span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                <div><span class="text-danger">Pending</span></div>
                <div>Payment not received</div>

              </td>
              <td>
                <div>
                  <a href="<?=base_url().'cart';?>">
                    <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                  </a>
                </div>
                <div class="mt-2">
                  <a href="<?=base_url().'vendor/order_detail';?>">View Details</a>
                </div>

              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td>
                <div><a class="title-product" href="<?=base_url().'vendor/order_detail';?>">Marshall Lorem Ipsum dolor amet (149077)</a></div>
                <div>Order Id : #271894h</div>
                <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34</span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                <div><span class="text-danger">Pending</span></div>
                <div>Payment not received</div>

              </td>
              <td>
                <div>
                  <a href="<?=base_url().'cart';?>">
                    <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                  </a>
                </div>
                <div class="mt-2">
                  <a href="<?=base_url().'vendor/order_detail';?>">View Details</a>
                </div>

              </td>
            </tr>
          </table>
          <h5 class="mt-5">Recent Orders</h5>
          <a href="vendor-order.html" class="view-all">View all</a>
          <hr>
          <table class="table">
            <thead>
              <tr>
                <th>Date</th>
                <th>Product Detail</th>
                <th>Price</th>
                <th>Qnty</th>
                <th>Total Payment</th>
                <th>Status</th>
                <th></th>
              </tr>
            </thead>
            <tr>
              <td>Okt 16, 2019</td>
              <td>
                <div><a class="title-product" href="<?=base_url().'vendor/order_detail';?>">Marshall Lorem Ipsum dolor amet (149077)</a></div>
                <div>Order Id : #271894h</div>
                <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34</span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                <div><span class="text-success">Order-in</span></div>
                <div>Payment received</div>

              </td>
              <td>
                <div>
                  <a href="<?=base_url().'cart';?>">
                    <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                  </a>
                </div>
                <div class="mt-2">
                  <a href="<?=base_url().'vendor/order_detail';?>">View Details</a>
                </div>

              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td>
                <div><a class="title-product" href="<?=base_url().'vendor/order_detail';?>">Marshall Lorem Ipsum dolor amet (149077)</a></div>
                <div>Order Id : #271894h</div>
                <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34</span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                <div><span class="text-danger">Pending</span></div>
                <div>Payment not received</div>

              </td>
              <td>
                <div>
                  <a href="<?=base_url().'cart';?>">
                    <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                  </a>
                </div>
                <div class="mt-2">
                  <a href="<?=base_url().'vendor/order_detail';?>">View Details</a>
                </div>

              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td>
                <div><a class="title-product" href="<?=base_url().'vendor/order_detail';?>">Marshall Lorem Ipsum dolor amet (149077)</a></div>
                <div>Order Id : #271894h</div>
                <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34</span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                <div><span class="text-danger">Pending</span></div>
                <div>Payment not received</div>

              </td>
              <td>
                <div>
                  <a href="<?=base_url().'cart';?>">
                    <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                  </a>
                </div>
                <div class="mt-2">
                  <a href="<?=base_url().'vendor/order_detail';?>">View Details</a>
                </div>

              </td>
            </tr>
          </table>
          <h5 class="mt-5">Sales Report</h5>
          <hr>
          <form action="">
            <div class="row">
              <div class="col-md-3">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text input-group-text-date" id="inputGroup-sizing-default">From</span>
                  </div>
                  <input type="text" class="form-control date" aria-label="Default"
                    aria-describedby="inputGroup-sizing-default" data-provide="datepicker">
                </div>
              </div>
              <div class="col-md-3">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text input-group-text-date" id="inputGroup-sizing-default">To</span>
                  </div>
                  <input type="text" class="form-control date" aria-label="Default"
                    aria-describedby="inputGroup-sizing-default" data-provide="datepicker">
                </div>
              </div>
              <div class="col-md-2">
                 <div class="input-group mb-3">
                   <button class="btn btn-shop"><i class="fa fa-retweet"></i> Update Information</button>
                 </div>
                
              </div>
            </div>
          </form>
          <table class="data-table table">
            <thead>
              <tr>
                <th>Date</th>
                <th>Product Detail</th>
                <th>Price</th>
                <th>Sold</th>
                <th>Commision</th>
                <th>Rate</th>
              </tr>
            </thead>
            <tr>
              <td>Okt 16, 2019</td>
             <td><img src="http://placehold.it/70x70" alt=""> <a class="title-product" href="<?=base_url().'vendor/order_detail';?>">Marshall Lorem Ipsum
                 dolor amet (149077)</a>
                </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34/<b>72</b></span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                23%
              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td><img src="http://placehold.it/70x70" alt=""> <a class="title-product" href="<?=base_url().'vendor/order_detail';?>">Marshall Lorem Ipsum
                  dolor amet (149077)</a>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34/<b>72</b></span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                23%
              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td><img src="http://placehold.it/70x70" alt=""> <a class="title-product" href="<?=base_url().'vendor/order_detail';?>">Marshall Lorem Ipsum
                  dolor amet (149077)</a>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34/<b>72</b></span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                23%
              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td><img src="http://placehold.it/70x70" alt=""> <a class="title-product" href="<?=base_url().'vendor/order_detail';?>">Marshall Lorem Ipsum
                  dolor amet (149077)</a>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34/<b>72</b></span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                23%
              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td><img src="http://placehold.it/70x70" alt=""> <a class="title-product" href="<?=base_url().'vendor/order_detail';?>">Marshall Lorem Ipsum
                  dolor amet (149077)</a>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34/<b>72</b></span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                23%
              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td><img src="http://placehold.it/70x70" alt=""> <a class="title-product" href="<?=base_url().'vendor/order_detail';?>">Marshall Lorem Ipsum
                  dolor amet (149077)</a>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34/<b>72</b></span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                23%
              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td><img src="http://placehold.it/70x70" alt=""> <a class="title-product" href="<?=base_url().'vendor/order_detail';?>">Marshall Lorem Ipsum
                  dolor amet (149077)</a>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34/<b>72</b></span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                23%
              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td><img src="http://placehold.it/70x70" alt=""> <a class="title-product" href="<?=base_url().'vendor/order_detail';?>">Marshall Lorem Ipsum
                  dolor amet (149077)</a>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34/<b>72</b></span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                23%
              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td><img src="http://placehold.it/70x70" alt=""> <a class="title-product" href="<?=base_url().'vendor/order_detail';?>">Marshall Lorem Ipsum
                  dolor amet (149077)</a>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34/<b>72</b></span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                23%
              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td><img src="http://placehold.it/70x70" alt=""> <a class="title-product" href="<?=base_url().'vendor/order_detail';?>">Marshall Lorem Ipsum
                  dolor amet (149077)</a>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34/<b>72</b></span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                23%
              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td><img src="http://placehold.it/70x70" alt=""> <a class="title-product" href="<?=base_url().'vendor/order_detail';?>">Marshall Lorem Ipsum
                  dolor amet (149077)</a>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34/<b>72</b></span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                23%
              </td>
            </tr>
            <tr>
              <td>Okt 16, 2019</td>
              <td><img src="http://placehold.it/70x70" alt=""> <a class="title-product" href="<?=base_url().'vendor/order_detail';?>">Marshall Lorem Ipsum
                  dolor amet (149077)</a>
              </td>
              <td><span>Rp. 720.000</span></td>
              <td><span>34/<b>72</b></span></td>
              <td><span>Rp. 3.000.000</span></td>
              <td>
                23%
              </td>
            </tr>
          </table>
       </div>
       <div class="tab-pane" id="products">
         <h5 class="mt-5">193 Products</h5>
         <hr>
         <form action="">
           <div class="row">
             <div class="col-md-2">
               <div class="input-group mb-3">
                 <a class="btn btn-shop" href="<?=base_url().'vendor/add_product';?>"><i class="fa fa-plus"></i> Add Product</a>
               </div>

             </div>
           </div>
         </form>
         <table class="data-table table">
           <thead>
             <tr>
               <th></th>
               <th>Product Detail</th>
               <th>Price</th>
               <th>Categories</th>
               <th>Date</th>
               <th>Status</th>
             </tr>
           </thead>
           <tr>
             <td><img src="http://placehold.it/70x70" alt=""></td>
             <td>
               <div><a class="title-product" href="<?=base_url().'vendor/order_detail';?>">Marshall Lorem Ipsum dolor amet (149077)</a></div>
               <div class="text-grey">ID : MSDAD132</div>
               <div><a class="mr-2 text-black" href="<?=base_url().'vendor/add_product';?>">Edit</a><a class=" text-black"
                   href="add-product.html">Delete</a></div>
             </td>
             <td><span>Rp. 720.000</span></td>
             <td><span>Baju, Koko, Muslim</span></td>
             <td><span>Published</span><br><span>Oct 15, 2019</span></td>
             <td>
               <span class="text-success">In Stock</span>
             </td>
           </tr>
           <tr>
             <td><img src="http://placehold.it/70x70" alt=""></td>
             <td>
               <div><a class="title-product" href="<?=base_url().'vendor/order_detail';?>">Marshall Lorem Ipsum dolor amet (149077)</a></div>
               <div class="text-grey">ID : MSDAD132</div>
               <div><a class="mr-2 text-black" href="add-product.html">Edit</a><a class=" text-black"
                   href="add-product.html">Delete</a></div>
             </td>
             <td><span>Rp. 720.000</span></td>
             <td><span>Baju, Koko, Muslim</span></td>
             <td><span>Published</span><br><span>Oct 15, 2019</span></td>
             <td>
               <span class="text-danger">Out of Stock</span>
             </td>
           </tr>
           <tr>
             <td><img src="http://placehold.it/70x70" alt=""></td>
             <td>
               <div><a class="title-product" href="<?=base_url().'vendor/order_detail';?>">Marshall Lorem Ipsum dolor amet (149077)</a></div>
               <div class="text-grey">ID : MSDAD132</div>
               <div><a class="mr-2 text-black" href="add-product.html">Edit</a><a class=" text-black"
                   href="add-product.html">Delete</a></div>
             </td>
             <td><span>Rp. 720.000</span></td>
             <td><span>Baju, Koko, Muslim</span></td>
             <td><span>Published</span><br><span>Oct 15, 2019</span></td>
             <td>
               <span class="text-success">In Stock</span>
             </td>
           </tr>
           <tr>
             <td><img src="http://placehold.it/70x70" alt=""></td>
             <td>
               <div><a class="title-product" href="<?=base_url().'vendor/order_detail';?>">Marshall Lorem Ipsum dolor amet (149077)</a></div>
               <div class="text-grey">ID : MSDAD132</div>
               <div><a class="mr-2 text-black" href="add-product.html">Edit</a><a class=" text-black"
                   href="add-product.html">Delete</a></div>
             </td>
             <td><span>Rp. 720.000</span></td>
             <td><span>Baju, Koko, Muslim</span></td>
             <td><span>Published</span><br><span>Oct 15, 2019</span></td>
             <td>
               <span class="text-danger">Out of Stock</span>
             </td>
           </tr>
           <tr>
             <td><img src="http://placehold.it/70x70" alt=""></td>
             <td>
               <div><a class="title-product" href="<?=base_url().'vendor/order_detail';?>">Marshall Lorem Ipsum dolor amet (149077)</a></div>
               <div class="text-grey">ID : MSDAD132</div>
               <div><a class="mr-2 text-black" href="add-product.html">Edit</a><a class=" text-black"
                   href="add-product.html">Delete</a></div>
             </td>
             <td><span>Rp. 720.000</span></td>
             <td><span>Baju, Koko, Muslim</span></td>
             <td><span>Published</span><br><span>Oct 15, 2019</span></td>
             <td>
               <span class="text-success">In Stock</span>
             </td>
           </tr>
           <tr>
             <td><img src="http://placehold.it/70x70" alt=""></td>
             <td>
               <div><a class="title-product" href="<?=base_url().'vendor/order_detail';?>">Marshall Lorem Ipsum dolor amet (149077)</a></div>
               <div class="text-grey">ID : MSDAD132</div>
               <div><a class="mr-2 text-black" href="add-product.html">Edit</a><a class=" text-black"
                   href="add-product.html">Delete</a></div>
             </td>
             <td><span>Rp. 720.000</span></td>
             <td><span>Baju, Koko, Muslim</span></td>
             <td><span>Published</span><br><span>Oct 15, 2019</span></td>
             <td>
               <span class="text-danger">Out of Stock</span>
             </td>
           </tr>
           <tr>
             <td><img src="http://placehold.it/70x70" alt=""></td>
             <td>
               <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
               <div class="text-grey">ID : MSDAD132</div>
               <div><a class="mr-2 text-black" href="add-product.html">Edit</a><a class=" text-black"
                   href="add-product.html">Delete</a></div>
             </td>
             <td><span>Rp. 720.000</span></td>
             <td><span>Baju, Koko, Muslim</span></td>
             <td><span>Published</span><br><span>Oct 15, 2019</span></td>
             <td>
               <span class="text-success">In Stock</span>
             </td>
           </tr>
           <tr>
             <td><img src="http://placehold.it/70x70" alt=""></td>
             <td>
               <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
               <div class="text-grey">ID : MSDAD132</div>
               <div><a class="mr-2 text-black" href="add-product.html">Edit</a><a class=" text-black"
                   href="add-product.html">Delete</a></div>
             </td>
             <td><span>Rp. 720.000</span></td>
             <td><span>Baju, Koko, Muslim</span></td>
             <td><span>Published</span><br><span>Oct 15, 2019</span></td>
             <td>
               <span class="text-danger">Out of Stock</span>
             </td>
           </tr>
           <tr>
             <td><img src="http://placehold.it/70x70" alt=""></td>
             <td>
               <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
               <div class="text-grey">ID : MSDAD132</div>
               <div><a class="mr-2 text-black" href="add-product.html">Edit</a><a class=" text-black"
                   href="add-product.html">Delete</a></div>
             </td>
             <td><span>Rp. 720.000</span></td>
             <td><span>Baju, Koko, Muslim</span></td>
             <td><span>Published</span><br><span>Oct 15, 2019</span></td>
             <td>
               <span class="text-success">In Stock</span>
             </td>
           </tr>
           <tr>
             <td><img src="http://placehold.it/70x70" alt=""></td>
             <td>
               <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
               <div class="text-grey">ID : MSDAD132</div>
               <div><a class="mr-2 text-black" href="add-product.html">Edit</a><a class=" text-black"
                   href="add-product.html">Delete</a></div>
             </td>
             <td><span>Rp. 720.000</span></td>
             <td><span>Baju, Koko, Muslim</span></td>
             <td><span>Published</span><br><span>Oct 15, 2019</span></td>
             <td>
               <span class="text-danger">Out of Stock</span>
             </td>
           </tr>
           <tr>
             <td><img src="http://placehold.it/70x70" alt=""></td>
             <td>
               <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
               <div class="text-grey">ID : MSDAD132</div>
               <div><a class="mr-2 text-black" href="add-product.html">Edit</a><a class=" text-black"
                   href="add-product.html">Delete</a></div>
             </td>
             <td><span>Rp. 720.000</span></td>
             <td><span>Baju, Koko, Muslim</span></td>
             <td><span>Published</span><br><span>Oct 15, 2019</span></td>
             <td>
               <span class="text-success">In Stock</span>
             </td>
           </tr>
           <tr>
             <td><img src="http://placehold.it/70x70" alt=""></td>
             <td>
               <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
               <div class="text-grey">ID : MSDAD132</div>
               <div><a class="mr-2 text-black" href="add-product.html">Edit</a><a class=" text-black"
                   href="add-product.html">Delete</a></div>
             </td>
             <td><span>Rp. 720.000</span></td>
             <td><span>Baju, Koko, Muslim</span></td>
             <td><span>Published</span><br><span>Oct 15, 2019</span></td>
             <td>
               <span class="text-danger">Out of Stock</span>
             </td>
           </tr>
         </table>
       </div>
       <div class="tab-pane" id="orders">
        <h5>New Order-in (3)</h5>
        <hr>
        <table class="table">
          <thead>
            <tr>
              <th>Date</th>
              <th>Product Detail</th>
              <th>Price</th>
              <th>Qnty</th>
              <th>Total Payment</th>
              <th>Status</th>
              <th></th>
            </tr>
          </thead>
          <tr>
            <td>Okt 16, 2019</td>
            <td>
              <div><a class="title-product" href="<?=base_url().'vendor/order_detail';?>">Marshall Lorem Ipsum dolor amet (149077)</a></div>
              <div>Order Id : #271894h</div>
              <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
            </td>
            <td><span>Rp. 720.000</span></td>
            <td><span>34</span></td>
            <td><span>Rp. 3.000.000</span></td>
            <td>
              <div><span class="text-success">Order-in</span></div>
              <div>Payment received</div>

            </td>
            <td>
              <div>
                <a href="<?=base_url().'cart';?>">
                  <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                </a>
              </div>
              <div class="mt-2">
                <a href="<?=base_url().'vendor/order_detail';?>">View Details</a>
              </div>

            </td>
          </tr>
          <tr>
            <td>Okt 16, 2019</td>
            <td>
              <div><a class="title-product" href="<?=base_url().'vendor/order_detail';?>">Marshall Lorem Ipsum dolor amet (149077)</a></div>
              <div>Order Id : #271894h</div>
              <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
            </td>
            <td><span>Rp. 720.000</span></td>
            <td><span>34</span></td>
            <td><span>Rp. 3.000.000</span></td>
            <td>
              <div><span class="text-danger">Pending</span></div>
              <div>Payment not received</div>

            </td>
            <td>
              <div>
                <a href="<?=base_url().'cart';?>">
                  <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                </a>
              </div>
              <div class="mt-2">
                <a href="<?=base_url().'vendor/order_detail';?>">View Details</a>
              </div>

            </td>
          </tr>
          <tr>
            <td>Okt 16, 2019</td>
            <td>
              <div><a class="title-product" href="<?=base_url().'vendor/order_detail';?>">Marshall Lorem Ipsum dolor amet (149077)</a></div>
              <div>Order Id : #271894h</div>
              <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
            </td>
            <td><span>Rp. 720.000</span></td>
            <td><span>34</span></td>
            <td><span>Rp. 3.000.000</span></td>
            <td>
              <div><span class="text-danger">Pending</span></div>
              <div>Payment not received</div>

            </td>
            <td>
              <div>
                <a href="<?=base_url().'cart';?>">
                  <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                </a>
              </div>
              <div class="mt-2">
                <a href="<?=base_url().'vendor/order_detail';?>">View Details</a>
              </div>

            </td>
          </tr>
        </table>
        <h5 class="mt-5">Recent Orders</h5>
        <hr>
        <table class="table">
          <thead>
            <tr>
              <th>Date</th>
              <th>Product Detail</th>
              <th>Price</th>
              <th>Qnty</th>
              <th>Total Payment</th>
              <th>Status</th>
              <th></th>
            </tr>
          </thead>
          <tr>
            <td>Okt 16, 2019</td>
            <td>
              <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
              <div>Order Id : #271894h</div>
              <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
            </td>
            <td><span>Rp. 720.000</span></td>
            <td><span>34</span></td>
            <td><span>Rp. 3.000.000</span></td>
            <td>
              <div><span class="text-success">Order-in</span></div>
              <div>Payment received</div>

            </td>
            <td>
              <div>
                <a href="<?=base_url().'cart';?>">
                  <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                </a>
              </div>
              <div class="mt-2">
                <a href="order-detail.html">View Details</a>
              </div>

            </td>
          </tr>
          <tr>
            <td>Okt 16, 2019</td>
            <td>
              <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
              <div>Order Id : #271894h</div>
              <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
            </td>
            <td><span>Rp. 720.000</span></td>
            <td><span>34</span></td>
            <td><span>Rp. 3.000.000</span></td>
            <td>
              <div><span class="text-danger">Pending</span></div>
              <div>Payment not received</div>

            </td>
            <td>
              <div>
                <a href="<?=base_url().'cart';?>">
                  <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                </a>
              </div>
              <div class="mt-2">
                <a href="order-detail.html">View Details</a>
              </div>

            </td>
          </tr>
          <tr>
            <td>Okt 16, 2019</td>
            <td>
              <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
              <div>Order Id : #271894h</div>
              <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
            </td>
            <td><span>Rp. 720.000</span></td>
            <td><span>34</span></td>
            <td><span>Rp. 3.000.000</span></td>
            <td>
              <div><span class="text-danger">Pending</span></div>
              <div>Payment not received</div>

            </td>
            <td>
              <div>
                <a href="<?=base_url().'cart';?>">
                  <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                </a>
              </div>
              <div class="mt-2">
                <a href="order-detail.html">View Details</a>
              </div>

            </td>
          </tr>
        </table>
        <h5 class="mt-5">184 Order Shipped</h5>
        <hr>
        <table class="table data-table">
          <thead>
            <tr>
              <th>Date</th>
              <th>Product Detail</th>
              <th>Price</th>
              <th>Qnty</th>
              <th>Total Payment</th>
              <th>Status</th>
              <th></th>
            </tr>
          </thead>
          <tr>
            <td>Okt 16, 2019</td>
            <td>
              <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
              <div>Order Id : #271894h</div>
              <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
            </td>
            <td><span>Rp. 720.000</span></td>
            <td><span>34</span></td>
            <td><span>Rp. 3.000.000</span></td>
            <td>
              <div><span class="text-success">Order-in</span></div>
              <div>Payment received</div>

            </td>
            <td>
              <div>
                <a href="<?=base_url().'cart';?>">
                  <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                </a>
              </div>
              <div class="mt-2">
                <a href="order-detail.html">View Details</a>
              </div>

            </td>
          </tr>
          <tr>
            <td>Okt 16, 2019</td>
            <td>
              <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
              <div>Order Id : #271894h</div>
              <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
            </td>
            <td><span>Rp. 720.000</span></td>
            <td><span>34</span></td>
            <td><span>Rp. 3.000.000</span></td>
            <td>
              <div><span class="text-danger">Pending</span></div>
              <div>Payment not received</div>

            </td>
            <td>
              <div>
                <a href="<?=base_url().'cart';?>">
                  <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                </a>
              </div>
              <div class="mt-2">
                <a href="order-detail.html">View Details</a>
              </div>

            </td>
          </tr>
          <tr>
            <td>Okt 16, 2019</td>
            <td>
              <div><a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></div>
              <div>Order Id : #271894h</div>
              <div>Color : <b class="mr-2">Black</b> <b>JNE Reguler</b></div>
            </td>
            <td><span>Rp. 720.000</span></td>
            <td><span>34</span></td>
            <td><span>Rp. 3.000.000</span></td>
            <td>
              <div><span class="text-danger">Pending</span></div>
              <div>Payment not received</div>

            </td>
            <td>
              <div>
                <a href="<?=base_url().'cart';?>">
                  <button class="btn btn-shop"><i class="fa fa-send"></i> Process</button>
                </a>
              </div>
              <div class="mt-2">
                <a href="order-detail.html">View Details</a>
              </div>

            </td>
          </tr>
        </table>
       </div>
       <div class="tab-pane" id="setting">
        <h5>Setting</h5>
        <hr> 
        <div class="row">
          <div class="col-md-3">
            <div class="card">
              <span style="font-weight:500">MAIN MENU</span>
              <ul class="menu-vendor-setting mt-3">
                <li>
                 <a href="#">Your Shop</a> <i class="fa fa-angle-right"></i>
                </li>
                <li>
                  <a href="#">Shipping Methode</a> <i class="fa fa-angle-right"></i>
                </li>
                <li>
                  <a href="#">Withdraw Payment</a> <i class="fa fa-angle-right"></i>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-9">
            <div class="card">
              <h5>Your Shop</h5>
              <div class="row">
                <div class="col-md-3">
                  <div id="body-overlay">
                    <div><img src="assets/img/loading.gif" width="64px" height="64px" /></div>
                  </div>
                  <div class="bgColor">
                    <form id="uploadForm" action="upload.php" method="post">
                      <div id="targetOuter">
                        <div id="targetLayer"></div>
                        <i class="fa fa-camera"></i>
                        <div class="icon-choose-image">
                          <input name="userImage" id="userImage" type="file" class="inputFile"
                            onChange="showPreview(this);" />
                        </div>
                      </div>
                      <div>
                        <button class="btn btn-shop w-100">Upload</button>
                      </div>
                    </form>
                  </div>
                </div>
                <div class="col-md-9">
                  <h6 class="mt-5 mb-2"><span class="required">*</span> Shop Name</h6>
                    <div class="form-group">
                      <small id="emailHelp" class="form-text text-muted">Your shop name is public and must unique</small>
                      <input type="text" class="form-control mt-2" id="exampleInputEmail1" aria-describedby="emailHelp"
                        placeholder="Shop name">
                    </div>
                </div>
                <div class="col-md-12">
                  <h6 class="mt-5 mb-4"><span class="required">*</span> Shop Address</h6>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Province</label>
                        <select name="" class="form-controll custom-select" id="">
                          <option value="d">d</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                       <label for="exampleInputEmail1">City</label>
                       <select name="" class="form-controll custom-select" id="">
                         <option value="d">d</option>
                       </select>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Postal code</label>
                        <input type="text" class="form-control" placeholder="Postal code">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Address</label>
                        <input type="text" class="form-control" placeholder="Adress">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <h6 class="mt-3 mb-4"><span class="required">*</span> Shop Information</h6>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Phone Nunmber</label>
                        <input type="text" class="form-control" placeholder="Phone number">
                      </div>
                    </div>
                    <div class="col-md-8">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email Address</label>
                        <input type="email" class="form-control" placeholder="Email Adress">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <h6 class="mt-3 mb-4"><span class="required">*</span> Shop Description</h6>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <textarea  name="" id="" cols="100" rows="40" class="form-control" style="height:200px"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <h6 class="mt-3 mb-4"><span class="required">*</span> Sociaa Media</h6>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Facebook</label>
                        <input type="text" class="form-control" placeholder="Facebook">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Twitter</label>
                        <input type="text" class="form-control" placeholder="Twitter">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Instagram</label>
                        <input type="text" class="form-control" placeholder="Instagram">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12 text-right mt-4">
                  <button class="btn btn-shop">Save changes</button>
                </div>
              </div>
            </div>
          </div>
        </div> 
       </div>
     </div>
   </div>
 </section>
</body>

