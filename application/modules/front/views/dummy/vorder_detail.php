<body>
  <div class="container">
        <div class="row">
            <div class="col-md-2">
                <a href="#">Kembali</a>
            </div>
            <div class="col-md-4 offset-md-6 text-right">
                <button class="btn btn-shop mr-2"><i class="fa fa-send"></i> Process</button> <button class="btn btn-white"><i
                        class="fa fa-print"></i> Print out</button>
            </div>
        </div>
        <div class="card mt-5" id="order-card">
            <div class="row">
                <div class="col-md-8">
                    <h3>Lorem Isum dolo Amet das 214 Bank (314)</h3>
                </div>
                <div class="col-md-12 mt-2">
                    <div class="row">
                        <div class="col-md-3">
                            <h6>Payment/Order ID : #42142</h6>
                        </div>
                        <div class="col-md-3">
                            <h6>Product ID : #42142</h6>
                        </div>
                        <div class="col-md-6 text-right">
                            <h6 class="text-pink">Rp. 3.21.321.312</h6>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-5 offset-md-1">
                    <table class="w-100">
                        <tr>
                            <td colspan="2"><span class="title-vendor-data">Vendor Information</span></td>
                        </tr>
                        <tr>
                            <td width="100"><span class="text-grey">Full Name : </span></td>
                            <td><span class="text-black">Aris Prabowo</span></td>
                        </tr>
                         <tr>
                             <td><span class="text-grey">Email : </span></td>
                             <td><span class="text-black">Aris@Prabowo.com</span></td>
                         </tr>
                          <tr>
                              <td><span class="text-grey">Phone : </span></td>
                              <td><span class="text-black">321321321</span></td>
                          </tr>
                          <tr>
                              <td><span class="text-grey">Country : </span></td>
                              <td><span class="text-black">Indoneia</span></td>
                          </tr>
                          <tr>
                              <td><span class="text-grey">Postal code : </span></td>
                              <td><span class="text-black">123</span></td>
                          </tr>
                          <tr>
                              <td><span class="text-grey">Address</span></td>
                              <td><span class="text-black">Sir Matt Busby Way, Old Trafford, Stretford, Manchester M16
                                      0RA, Britania Raya</span></td>
                          </tr>
                    </table>
                </div>
                <div class="col-md-5">
                    <table class="w-100">
                        <tr>
                            <td colspan="2"><span class="title-vendor-data">Shipping Information</span></td>
                        </tr>
                        <tr>
                            <td width="100"><span class="text-grey">Full Name : </span></td>
                            <td><span class="text-black">Aris Prabowo</span></td>
                        </tr>
                        <tr>
                            <td><span class="text-grey">Email : </span></td>
                            <td><span class="text-black">Aris@Prabowo.com</span></td>
                        </tr>
                        <tr>
                            <td><span class="text-grey">Phone : </span></td>
                            <td><span class="text-black">321321321</span></td>
                        </tr>
                        <tr>
                            <td><span class="text-grey">Address</span></td>
                            <td><span class="text-black">Sir Matt Busby Way, Old Trafford, Stretford, Manchester M16
                                    0RA, Britania Raya</span></td>
                        </tr>
                        <tr>
                            <td><span class="text-grey">Postal code : </span></td>
                            <td><span class="text-black">123</span></td>
                        </tr>
                        <tr>
                            <td colspan="2"><span class="title-vendor-data">(+62) 8787-033-8115</span></td>
                        </tr>
                    </table>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-5 offset-md-1">
                    <table class="w-100">
                         <tr>
                             <td width="100"><span class="text-black"><b>Status : </b> </span></td>
                             <td><span class="text-success"><b>Order-in</b></span></td>
                         </tr>
                         <tr>
                             <td><span class="text-black"><b>Order-in : </b></span></td>
                             <td><span class="text-black"><b>Senin, 17 April 2019</b></span></td>
                         </tr>
                         <tr>
                             <td><span class="text-black"><b>Payment/Order ID : </b></span></td>
                             <td><span class="text-black"><b>#1241421</b></span></td>
                         </tr>
                    </table>
                </div>
                <div class="col-md-5">
                    <table class="w-100">
                        <tr>
                            <td width="100"><span class="text-black"><b>Status : </b> </span></td>
                            <td><span class="text-success">JNE Reguler (3-4 hari kerja)</span></td>
                        </tr>
                        <tr>
                            <td><span class="text-black"><b>Expedition Service Class : </b></span></td>
                            <td><span class="text-black"><b>REG</b></span></td>
                        </tr>
                        <tr>
                            <td><span class="text-black"><b>Tracking Number : </b></span></td>
                            <td><span class="text-black"><b>#1241421</b></span></td>
                        </tr>
                    </table>
                </div>
            </div>
            <hr>
             <table class="table">
                 <thead>
                     <tr>
                         <th>Order Date</th>
                         <th>Description</th>
                         <th>Price</th>
                         <th>Qnty</th>
                         <th>Total</th>
                     </tr>
                 </thead>
                 <tr>
                     <td>16 Okt, 2019</td>
                     <td><img src="http://placehold.it/50x50" alt=""> <a class="title-product" href="">Marshall Lorem
                             Ipsum dolor amet (149077)</a></td>
                     <td><span>Rp. 720.000</span></td>
                     <td><span class="text-success">72</span></td>
                      <td><span>Rp. 720.000</span></td>
                 </tr>
                 <tr>
                     <td>16 Okt, 2019</td>
                     <td><img src="http://placehold.it/50x50" alt=""> <a class="title-product" href="">Marshall Lorem
                             Ipsum dolor amet (149077)</a></td>
                     <td><span>Rp. 720.000</span></td>
                     <td><span class="text-success">72</span></td>
                     <td><span>Rp. 720.000</span></td>
                 </tr>
                 <tr>
                     <td>16 Okt, 2019</td>
                     <td><img src="http://placehold.it/50x50" alt=""> <a class="title-product" href="">Marshall Lorem
                             Ipsum dolor amet (149077)</a></td>
                     <td><span>Rp. 720.000</span></td>
                     <td><span class="text-success">72</span></td>
                     <td><span>Rp. 720.000</span></td>
                 </tr>
                 <tfoot>
                     <tr>
                         <td colspan="6" class="text-right"><span><b>Total </b></span><span class="text-pink">Rp.
                                 720.000</span></td>
                     </tr>
                 </tfoot>

             </table>
             <div class="ceramah-detail">
                 Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam
                 rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt
                 explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia
                 consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui
                 dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora
                 incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum
                 exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis
                 autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel
                 illum qui dolorem eum fugiat quo voluptas nulla pariatur
             </div>
        </div>
  </div>
</body>

