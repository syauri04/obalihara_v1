<body>
  <section>
       <div class="container">
            <div class="row">
                <div class="col-md-6 centerin">
                    <ul class="nav nav-tabs tab-login" id="myTab" role="tablist">
                        <li class="nav-item border-tab">
                            <a class="nav-link active" id="login-tab" data-toggle="tab" href="#login" role="tab"
                                aria-controls="home" aria-selected="true"><h3>Login</h3></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="register-tab" data-toggle="tab" href="#register" role="tab"
                                aria-controls="profile" aria-selected="false"><h3>Register</h3></a>
                        </li>
                    </ul>
                    <div class="tab-content" id="content-tab-login">
                        <div class="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="home-tab">
                            <div class="card">
                               <form action="" id="form-login">
                                    <h5 class="mb-0"><b>Sign in to your account</b></h5>
                                    <hr>
                                    <h6 class="mt-3">Sign in with media social</h6>
                                    <div class="mt-4">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <button class="btn btn-login btn-google"><i class="fa fa-google"></i>Masuk Dengan Google</button>
                                            </div>
                                            <div class="col-md-2 text-center">
                                                <span class="or">or</span>
                                            </div>
                                            <div class="col-md-5">
                                                <button class="btn btn-login btn-fb"><i class="fa fa-facebook"></i>
                                                    Masuk Dengan Google</button>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <h6 class="mt-3 mb-3">Have an Account?</h6>
                                    <div class="form-group">
                                        <label>Username or Email</label>
                                        <input name="" class="form-control" placeholder="Username" type="email">
                                    </div> <!-- form-group// -->
                                    <div class="form-group">
                                        <a class="float-right" href="#">Forgot?</a>
                                        <label>Password</label>
                                        <input class="form-control" placeholder="Password" type="password">
                                    </div> <!-- form-group// -->
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label> <input type="checkbox"> Remember</label>
                                        </div> <!-- checkbox .// -->
                                    </div> <!-- form-group// -->
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-login btn-block"> Sign In </button>
                                    </div> <!-- form-group// -->
                               </form>

                            </div>
                        </div>
                        <div class="tab-pane fade" id="register" role="tabpanel" aria-labelledby="register-tab">
                            <div class="card">
                                <form action="" id="form-login">
                                    <h5 class="mb-0"><b>Register</b></h5>
                                    <hr>
                                    <h6 class="mt-3">Register with media social</h6>
                                    <div class="mt-4">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <button class="btn btn-login btn-google"><i
                                                        class="fa fa-google"></i>Daftar Dengan Google</button>
                                            </div>
                                            <div class="col-md-2 text-center">
                                                <span class="or">or</span>
                                            </div>
                                            <div class="col-md-5">
                                                <button class="btn btn-login btn-fb"><i class="fa fa-facebook"></i>
                                                    Daftar Dengan Google</button>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <h6 class="mt-3 mb-3">Have an Account?</h6>
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input name="" class="form-control" placeholder="Username" type="email">
                                    </div>
                                     <div class="form-group">
                                         <label>Email</label>
                                         <input name="" class="form-control" placeholder="Username" type="email">
                                     </div>
                                    <div class="form-group">
                                        <a class="float-right" href="#">Forgot?</a>
                                        <label>Password</label>
                                        <input class="form-control" placeholder="Password" type="password">
                                    </div>
                                     <div class="form-group">
                                         <label>Phone number</label>
                                         <input name="" class="form-control" placeholder="Username" type="email">
                                     </div>
                                       <div class="form-group">
                                           <div class="checkbox">
                                               <label> <input type="checkbox"> Check if you undestand <a href="">Term and Conditions</a></label>
                                           </div> <!-- checkbox .// -->
                                       </div> <!-- form-group// -->
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-login btn-block">Register</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
       </div>
   </section>
</body>

