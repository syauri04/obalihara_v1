<body>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h3>Buyer Dashboard</h3>
                <div class="">
                    <p>Selamat datang, Kelola setiap alur masuk dan keuar barang anda. Dashboard didesain mudah agar
                        anda
                        bisa mentracking barang yang sedang anda jual</p>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <span style="font-weight:500">MAIN MENU</span>
                    <ul class="menu-vendor-setting mt-3">
                        <li>
                          <a href="<?=base_url().'buyer';?>">Dashboard</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="<?=base_url().'buyer/personal';?>">Personal Information</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="<?=base_url().'buyer/shipping';?>">Shipping Address</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="<?=base_url().'buyer/order';?>">Your Order</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="<?=base_url().'buyer/change_password';?>">Change Password</a> <i class="fa fa-angle-right"></i>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                    <h5 class="mb-5">Your Order</h5>
                    <h6 class="mb-2">Recent Order</h6>
                    <table class="data-table table">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Items</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tr>
                            <td>Okt 16, 2019</td>
                            <td>
                                <div class="row">
                                    <div class="col-3"><img src="http://placehold.it/70x70" alt=""></div>
                                    <div class="col-9">
                                        <div><b>Marshall Lorem Ipsum dolor amet (149077)</b></div>
                                        <div><span class="mr-3">Color : Black</span> <span>Order ID :
                                                #2134124</span></div>
                                    </div>
                                </div>
                            </td>
                            <td><b class="text-success">Shipping</b></td>
                            <td>
                                <a href="tracking.html"><button class="btn btn-shop">Tracking</button></a>
                            </td>
                        </tr>
                        <tr>
                            <td>Okt 16, 2019</td>
                            <td>
                                <div class="row">
                                    <div class="col-3"><img src="http://placehold.it/70x70" alt=""></div>
                                    <div class="col-9">
                                        <div><b>Marshall Lorem Ipsum dolor amet (149077)</b></div>
                                        <div><span class="mr-3">Color : Black</span> <span>Order ID :
                                                #2134124</span></div>
                                    </div>
                                </div>
                            </td>
                            <td><b class="">Order-in</b></td>
                            <td>
                                <a href="tracking.html"><button class="btn btn-shop">Tracking</button></a>
                            </td>
                        </tr>
                        <tr>
                            <td>Okt 16, 2019</td>
                            <td>
                                <div class="row">
                                    <div class="col-3"><img src="http://placehold.it/70x70" alt=""></div>
                                    <div class="col-9">
                                        <div><b>Marshall Lorem Ipsum dolor amet (149077)</b></div>
                                        <div><span class="mr-3">Color : Black</span> <span>Order ID :
                                                #2134124</span></div>
                                    </div>
                                </div>
                            </td>
                            <td><b class="">Order-in</b></td>
                            <td>
                                <a href="tracking.html"><button class="btn btn-shop">Tracking</button></a>
                            </td>
                        </tr>
                    </table>
                    <h6 class="mt-3 mb-2">Last Order</h6>
                    <table class="data-table table">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Items</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tr>
                            <td>Okt 16, 2019</td>
                            <td>
                                <div class="row">
                                    <div class="col-3"><img src="http://placehold.it/70x70" alt=""></div>
                                    <div class="col-9">
                                        <div><b>Marshall Lorem Ipsum dolor amet (149077)</b></div>
                                        <div><span class="mr-3">Color : Black</span> <span>Order ID :
                                                #2134124</span></div>
                                    </div>
                                </div>
                            </td>
                            <td><b class="text-success">Received</b></td>
                            <td>
                                <a href="order-detail.html"><button class="btn btn-shop">Tracking</button></a>
                            </td>
                        </tr>
                        <tr>
                            <td>Okt 16, 2019</td>
                            <td>
                                <div class="row">
                                    <div class="col-3"><img src="http://placehold.it/70x70" alt=""></div>
                                    <div class="col-9">
                                        <div><b>Marshall Lorem Ipsum dolor amet (149077)</b></div>
                                        <div><span class="mr-3">Color : Black</span> <span>Order ID :
                                                #2134124</span></div>
                                    </div>
                                </div>
                            </td>
                            <td><b class="text-success">Received</b></td>
                            <td>
                                <a href="order-detail.html"><button class="btn btn-shop">Tracking</button></a>
                            </td>
                        </tr>
                        <tr>
                            <td>Okt 16, 2019</td>
                            <td>
                                <div class="row">
                                    <div class="col-3"><img src="http://placehold.it/70x70" alt=""></div>
                                    <div class="col-9">
                                        <div><b>Marshall Lorem Ipsum dolor amet (149077)</b></div>
                                        <div><span class="mr-3">Color : Black</span> <span>Order ID :
                                                #2134124</span></div>
                                    </div>
                                </div>
                            </td>
                            <td><b class="text-success">Received</b></td>
                            <td>
                                <a href="order-detail.html"><button class="btn btn-shop">Ivoice</button></a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
</body>

