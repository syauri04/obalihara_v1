
  <body>
    <section>
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <div id="slide" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              <!-- <ul class="carousel-indicators">
                <li data-target="#demo" data-slide-to="0" class="active"></li>
                <li data-target="#demo" data-slide-to="1"></li>
                <li data-target="#demo" data-slide-to="2"></li>
              </ul> -->

              <!-- The slideshow -->
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <div class="back-slider"
                      style="background:url('assets/front/img/banner1.png') no-repeat; background-size: cover;">
                      <div class="title-cat">
                        <span>Limited Edition</span>
                      </div>
                      <h2>Korea Long Sofa Fabric In Blue Navy Color</h2>
                      <h3>$100</h3>
                      <a href="detail-product.html">
                        <button type="button" class="btn btn-shop" name="button">Shop Now</button>
                      </a>
                    </div>
                  </div>

                  <div class="carousel-item">
                    <div class="back-slider"
                      style="background:url('assets/front/img/banner2.png') no-repeat; background-size: cover;">
                      <div class="title-cat">
                        <span>Limited Edition</span>
                      </div>
                      <h2>Korea Long Sofa Fabric In Blue Navy Color</h2>
                      <h3>$100</h3>
                      <a href="detail-product.html">
                        <button type="button" class="btn btn-shop" name="button">Shop Now</button>
                      </a>
                    </div>
                  </div>

                  <div class="carousel-item">
                    <div class="back-slider"
                      style="background:url('assets/front/img/banner3.png') no-repeat; background-size: cover;">
                      <div class="title-cat">
                        <span>Limited Edition</span>
                      </div>
                      <h2>Korea Long Sofa Fabric In Blue Navy Color</h2>
                      <h3>$100</h3>
                      <a href="detail-product.html">
                        <button type="button" class="btn btn-shop" name="button">Shop Now</button>
                      </a>
                    </div>
                  </div>

                  <div class="carousel-item">
                    <div class="back-slider"
                      style="background:url('assets/front/img/banner4.png') no-repeat; background-size: cover;">
                      <div class="title-cat">
                        <span>Limited Edition</span>
                      </div>
                      <h2>Korea Long Sofa Fabric In Blue Navy Color</h2>
                      <h3>$100</h3>
                      <a href="detail-product.html">
                        <button type="button" class="btn btn-shop" name="button">Shop Now</button>
                      </a>
                    </div>
                  </div>
                  
                  <!-- Left and right controls -->
                  <a class="carousel-control-prev" href="#slide" data-slide="prev">
                    <div class="control-slide">
                      <i class="fa fa-angle-left"></i>
                    </div>
                  </a>
                  <a class="carousel-control-next" href="#slide" data-slide="next">
                    <div class="control-slide">
                      <i class="fa fa-angle-right"></i>
                    </div>
                  </a>
                </div>
              </div>
          </div>
          <div class="col-md-4">
            <div class="feat-product" style="background:url('assets/front/img/banner2.png') no-repeat; background-position:top center;background-size: cover;">
            <div class="row">
              <div class="col-6">
                  <h4>unio leather bags</h4>
                  <p>100% lorem ipsum</p>
              </div>
              <div class="col-6">
                <div class="circle-litle-disc">
                </div>
                <div class="circle-big-disc">
                  <span>20%</span>
                  <p>Off</p>
                </div>
              </div>
            </div>
            </div>
            <div class="feat-product" style="background:url('assets/front/img/banner3.png') no-repeat; background-position:top center;background-size: cover;">
            <div class="row">
              <div class="col-6">
                  <h4>unio leather bags</h4>
                  <p>100% lorem ipsum</p>
              </div>
              <div class="col-6">
                <div class="circle-litle-disc">
                </div>
                <div class="circle-big-disc">
                  <span>20%</span>
                  <p>Off</p>
                </div>
              </div>
            </div>
            </div>
          </div>
        </div>
    </section>
    <section id="information">
      <div class="container">
        <ul>
          <li>
            <table>
              <tr>
                <td rowspan="2"><i class="fa fa-rocket"></i></td>
                <td><span>Free Delivery</span></td>
              </tr>
              <tr>
                <td><p>For all order from $99</p></td>
              </tr>
            </table>
          </li>
          <li>
            <table>
              <tr>
                <td rowspan="2"><i class="fa fa-retweet"></i></td>
                <td><span>Free Delivery</span></td>
              </tr>
              <tr>
                <td><p>Free return in 90 days</p></td>
              </tr>
            </table>
          </li>
          <li>
            <table>
              <tr>
                <td rowspan="2"><i class="fa fa-credit-card"></i></td>
                <td><span>Secure Payments</span></td>
              </tr>
              <tr>
                <td><p>100% Secure payments</p></td>
              </tr>
            </table>
          </li>
          <li>
            <table>
              <tr>
                <td rowspan="2"><i class="fa fa-headphones"></i></td>
                <td><span>24/7 Support</span></td>
              </tr>
              <tr>
                <td><p>Dedicated support in 24h</p></td>
              </tr>
            </table>
          </li>
          <li class="last">
            <table>
              <tr>
                <td rowspan="2"><i class="fa fa-gift"></i></td>
                <td><span>Special gift</span></td>
              </tr>
              <tr>
                <td><p>Free special gift</p></td>
              </tr>
            </table>
          </li>
        </ul>
        <div class="clearfix">

        </div>
      </div>
    </section>

    <section id="deals-section">
      <div class="container">
        <div class="head-title">
          <h5>Deals of The Day</h5> <span class="btn btn-sale" name="button">Sales up to 70%</span>
        </div>
        <div class="">
          <div id="deal" class="carousel slide" data-ride="carousel" data-interval="0">
            <div class="carousel-inner">
              <div class="item carousel-item active">
                <div class="row">
                  <div class="col-sm-2">
                    <div class="">
                      <div class="disc-tem">
                        50%
                      </div>
                      <div class="hovereffect">
                        <a href="<?=base_url().'detail';?>"><img class="img-responsive w-100" src="assets/front/img/Product1@2.png" alt=""></a>
                        <div class="overlay">
                          <p class="icon-links">
                            <a href="whislist.html">
                              <span class="fa fa-heart-o"></span>
                            </a>
                            <a href="cart.html">
                              <span class="fa fa-shopping-cart"></span>
                            </a>
                          </p>
                        </div>
                      </div>
                      <div class="det-prod">
                        <div class="">
                          <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                        </div>
                        <div class="">
                          <a href="detail-product.html">Lorem ipsum dolor amet</a>
                        </div>
                        <div class="">
                          <a href="vendor.html">
                            <p>Sold by: Game lorem</p>
                          </a>
                        </div>
                        <div class="star">
                          <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                            class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                            style="margin-left:10px">8</span>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="">
                      <div class="disc-tem">
                        50%
                      </div>
                      <div class="hovereffect">
                        <a href="<?=base_url().'detail';?>"><img class="img-responsive w-100" src="assets/front/img/Product2@2.png"
                            alt=""></a>
                        <div class="overlay">
                          <p class="icon-links">
                            <a href="whislist.html">
                              <span class="fa fa-heart-o"></span>
                            </a>
                            <a href="cart.html">
                              <span class="fa fa-shopping-cart"></span>
                            </a>
                          </p>
                        </div>
                      </div>
                      <div class="det-prod">
                        <div class="">
                          <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                        </div>
                        <div class="">
                          <a href="<?=base_url().'detail';?>">Lorem ipsum dolor amet</a>
                        </div>
                        <div class="">
                          <a href="vendor.html">
                            <p>Sold by: Game lorem</p>
                          </a>
                        </div>
                        <div class="star">
                          <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                            class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                            style="margin-left:10px">8</span>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="">
                      <div class="disc-tem">
                        50%
                      </div>
                      <div class="hovereffect">
                        <a href="<?=base_url().'detail';?>"><img class="img-responsive w-100" src="assets/front/img/Product3@2.png"
                            alt=""></a>
                        <div class="overlay">
                          <p class="icon-links">
                            <a href="whislist.html">
                              <span class="fa fa-heart-o"></span>
                            </a>
                            <a href="cart.html">
                              <span class="fa fa-shopping-cart"></span>
                            </a>
                          </p>
                        </div>
                      </div>
                      <div class="det-prod">
                        <div class="">
                          <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                        </div>
                        <div class="">
                          <a href="<?=base_url().'detail';?>">Lorem ipsum dolor amet</a>
                        </div>
                        <div class="">
                          <a href="vendor.html">
                            <p>Sold by: Game lorem</p>
                          </a>
                        </div>
                        <div class="star">
                          <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                            class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                            style="margin-left:10px">8</span>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="">
                      <div class="disc-tem">
                        50%
                      </div>
                      <div class="hovereffect">
                       <a href="<?=base_url().'detail';?>"><img class="img-responsive w-100" src="assets/front/img/Product4@2.png"
                           alt=""></a>
                        <div class="overlay">
                          <p class="icon-links">
                            <a href="whislist.html">
                              <span class="fa fa-heart-o"></span>
                            </a>
                            <a href="cart.html">
                              <span class="fa fa-shopping-cart"></span>
                            </a>
                          </p>
                        </div>
                      </div>
                      <div class="det-prod">
                        <div class="">
                          <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                        </div>
                        <div class="">
                          <a href="<?=base_url().'detail';?>">Lorem ipsum dolor amet</a>
                        </div>
                        <div class="">
                          <a href="vendor.html">
                            <p>Sold by: Game lorem</p>
                          </a>
                        </div>
                        <div class="star">
                          <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                            class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                            style="margin-left:10px">8</span>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="">
                      <div class="disc-tem">
                        50%
                      </div>
                      <div class="hovereffect">
                       <a href="<?=base_url().'detail';?>"><img class="img-responsive w-100" src="assets/front/img/Product5@2.png"
                           alt=""></a>
                        <div class="overlay">
                          <p class="icon-links">
                            <a href="whislist.html">
                              <span class="fa fa-heart-o"></span>
                            </a>
                            <a href="cart.html">
                              <span class="fa fa-shopping-cart"></span>
                            </a>
                          </p>
                        </div>
                      </div>
                      <div class="det-prod">
                        <div class="">
                          <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                        </div>
                        <div class="">
                          <a href="detail-product.html">Lorem ipsum dolor amet</a>
                        </div>
                        <div class="">
                          <a href="vendor.html">
                            <p>Sold by: Game lorem</p>
                          </a>
                        </div>
                        <div class="star">
                          <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                            class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                            style="margin-left:10px">8</span>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="">
                      <div class="disc-tem">
                        50%
                      </div>
                      <div class="hovereffect">
                        <a href="<?=base_url().'detail';?>"><img class="img-responsive w-100" src="assets/front/img/Product6@2.png"
                            alt=""></a>
                        <div class="overlay">
                          <p class="icon-links">
                            <a href="whislist.html">
                              <span class="fa fa-heart-o"></span>
                            </a>
                            <a href="cart.html">
                              <span class="fa fa-shopping-cart"></span>
                            </a>
                          </p>
                        </div>
                      </div>
                      <div class="det-prod">
                        <div class="">
                          <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                        </div>
                        <div class="">
                          <a href="<?=base_url().'detail';?>">Lorem ipsum dolor amet</a>
                        </div>
                        <div class="">
                          <a href="vendor.html">
                            <p>Sold by: Game lorem</p>
                          </a>
                        </div>
                        <div class="star">
                          <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                            class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                            style="margin-left:10px">8</span>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                </div>

              </div>
              <div class="item carousel-item">
                <div class="row">
                  <div class="col-sm-2">
                    <div class="">
                      <div class="disc-tem">
                        50%
                      </div>
                      <div class="hovereffect">
                       <a href="<?=base_url().'detail';?>"><img class="img-responsive w-100" src="assets/front/img/Product7@2.png"
                           alt=""></a>
                        <div class="overlay">
                          <p class="icon-links">
                            <a href="whislist.html">
                              <span class="fa fa-heart-o"></span>
                            </a>
                            <a href="cart.html">
                              <span class="fa fa-shopping-cart"></span>
                            </a>
                          </p>
                        </div>
                      </div>
                      <div class="det-prod">
                        <div class="">
                          <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                        </div>
                        <div class="">
                          <a href="<?=base_url().'detail';?>">Lorem ipsum dolor amet</a>
                        </div>
                        <div class="">
                          <a href="vendor.html">
                            <p>Sold by: Game lorem</p>
                          </a>
                        </div>
                        <div class="star">
                          <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                            class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                            style="margin-left:10px">8</span>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="">
                      <div class="disc-tem">
                        50%
                      </div>
                      <div class="hovereffect">
                       <a href="<?=base_url().'detail';?>"><img class="img-responsive w-100" src="assets/front/img/Product8@2.png"
                           alt=""></a>
                        <div class="overlay">
                          <p class="icon-links">
                            <a href="whislist.html">
                              <span class="fa fa-heart-o"></span>
                            </a>
                            <a href="cart.html">
                              <span class="fa fa-shopping-cart"></span>
                            </a>
                          </p>
                        </div>
                      </div>
                      <div class="det-prod">
                        <div class="">
                          <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                        </div>
                        <div class="">
                          <a href="detail-product.html">Lorem ipsum dolor amet</a>
                        </div>
                        <div class="">
                          <a href="vendor.html">
                            <p>Sold by: Game lorem</p>
                          </a>
                        </div>
                        <div class="star">
                          <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                            class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                            style="margin-left:10px">8</span>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="">
                      <div class="disc-tem">
                        50%
                      </div>
                      <div class="hovereffect">
                        <a href="detail-product.html"><img class="img-responsive w-100" src="assets/front/img/Product1@2.png"
                            alt=""></a>
                        <div class="overlay">
                          <p class="icon-links">
                            <a href="whislist.html">
                              <span class="fa fa-heart-o"></span>
                            </a>
                            <a href="cart.html">
                              <span class="fa fa-shopping-cart"></span>
                            </a>
                          </p>
                        </div>
                      </div>
                      <div class="det-prod">
                        <div class="">
                          <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                        </div>
                        <div class="">
                          <a href="d<?=base_url().'detail';?>">Lorem ipsum dolor amet</a>
                        </div>
                        <div class="">
                          <a href="vendor.html">
                            <p>Sold by: Game lorem</p>
                          </a>
                        </div>
                        <div class="star">
                          <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                            class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                            style="margin-left:10px">8</span>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="">
                      <div class="disc-tem">
                        50%
                      </div>
                      <div class="hovereffect">
                       <a href="<?=base_url().'detail';?>"><img class="img-responsive w-100" src="assets/front/img/Product2@2.png"
                           alt=""></a>
                        <div class="overlay">
                          <p class="icon-links">
                            <a href="whislist.html">
                              <span class="fa fa-heart-o"></span>
                            </a>
                            <a href="cart.html">
                              <span class="fa fa-shopping-cart"></span>
                            </a>
                          </p>
                        </div>
                      </div>
                      <div class="det-prod">
                        <div class="">
                          <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                        </div>
                        <div class="">
                          <a href="detail-product.html">Lorem ipsum dolor amet</a>
                        </div>
                        <div class="">
                          <a href="vendor.html">
                            <p>Sold by: Game lorem</p>
                          </a>
                        </div>
                        <div class="star">
                          <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                            class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                            style="margin-left:10px">8</span>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="">
                      <div class="disc-tem">
                        50%
                      </div>
                      <div class="hovereffect">
                        <a href="detail-product.html"><img class="img-responsive w-100" src="assets/front/img/Product3@2.png"
                            alt=""></a>
                        <div class="overlay">
                          <p class="icon-links">
                            <a href="whislist.html">
                              <span class="fa fa-heart-o"></span>
                            </a>
                            <a href="cart.html">
                              <span class="fa fa-shopping-cart"></span>
                            </a>
                          </p>
                        </div>
                      </div>
                      <div class="det-prod">
                        <div class="">
                          <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                        </div>
                        <div class="">
                          <a href="detail-product.html">Lorem ipsum dolor amet</a>
                        </div>
                        <div class="">
                          <a href="vendor.html">
                            <p>Sold by: Game lorem</p>
                          </a>
                        </div>
                        <div class="star">
                          <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                            class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                            style="margin-left:10px">8</span>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="">
                      <div class="disc-tem">
                        50%
                      </div>
                      <div class="hovereffect">
                        <a href="<?=base_url().'detail';?>"><img class="img-responsive w-100" src="assets/front/img/Product4@2.png"
                            alt=""></a>
                        <div class="overlay">
                          <p class="icon-links">
                            <a href="whislist.html">
                              <span class="fa fa-heart-o"></span>
                            </a>
                            <a href="cart.html">
                              <span class="fa fa-shopping-cart"></span>
                            </a>
                          </p>
                        </div>
                      </div>
                      <div class="det-prod">
                        <div class="">
                          <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                        </div>
                        <div class="">
                          <a href="<?=base_url().'detail';?>">Lorem ipsum dolor amet</a>
                        </div>
                        <div class="">
                          <a href="vendor.html">
                            <p>Sold by: Game lorem</p>
                          </a>
                        </div>
                        <div class="star">
                          <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                            class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                            style="margin-left:10px">8</span>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
            <!-- Carousel controls -->
            <a class="carousel-control-prev" href="#deal" data-slide="prev">
              <div class="control-slide">
                <i class="fa fa-angle-left"></i>
              </div>
            </a>
            <a class="carousel-control-next" href="#deal" data-slide="next">
              <div class="control-slide">
                <i class="fa fa-angle-right"></i>
              </div>
            </a>
          </div>
        </div>
      </div>
    </section>
    <section id="a-thousand">
      <div class="container">
        <a href="#"><img src="assets/front/img/a-thousand.png" width="100%"/></a>
      </div>
    </section>
    <section id="uno">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="feat-product" style="background:url('assets/front/img/slide2.jpg') no-repeat; background-position:top center;background-size: cover;">
            <div class="row">
              <div class="col-6">
                  <h4>unio leather bags</h4>
                  <p>100% lorem ipsum</p>
              </div>
              <div class="col-6">
                <div class="circle-litle-disc">
                </div>
                <div class="circle-big-disc">
                  <span>20%</span>
                  <p>Off</p>
                </div>
              </div>
            </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="feat-product" style="background:url('assets/front/img/slide2.jpg') no-repeat; background-position:top center;background-size: cover;">
            <div class="row">
              <div class="col-6">
                  <h4>unio leather bags</h4>
                  <p>100% lorem ipsum</p>
              </div>
              <div class="col-6">
                <div class="circle-litle-disc">
                </div>
                <div class="circle-big-disc">
                  <span>20%</span>
                  <p>Off</p>
                </div>
              </div>
            </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="feat-product" style="background:url('assets/front/img/slide2.jpg') no-repeat; background-position:top center;background-size: cover;">
            <div class="row">
              <div class="col-6">
                  <h4>unio leather bags</h4>
                  <p>100% lorem ipsum</p>
              </div>
              <div class="col-6">
                <div class="circle-litle-disc">
                </div>
                <div class="circle-big-disc">
                  <span>20%</span>
                  <p>Off</p>
                </div>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section>
      <div class="container">
        <div class="head-title">
          <h5>Categories of The Month</h5>
        </div>
        <div class="">
          <div class="row">
            <div class="col-md-2">
              <a href="<?=base_url().'category';?>"><img class="img-responsive w-100" src="assets/front/img/Product8@2.png" alt=""></a>
              <div class="title-categories">
                <a href="<?=base_url().'category';?>">Lorem ipsum</a>
              </div>
            </div>
            <div class="col-md-2">
              <a href="<?=base_url().'category';?>"><img class="img-responsive w-100" src="assets/front/img/Product7@2.png" alt=""></a>
              <div class="title-categories">
                <a href="<?=base_url().'category';?>">Lorem ipsum</a>
              </div>
            </div>
            <div class="col-md-2">
              <a href="<?=base_url().'category';?>"><img class="img-responsive w-100" src="assets/front/img/Product6@2.png" alt=""></a>
              <div class="title-categories">
                <a href="<?=base_url().'category';?>">Lorem ipsum</a>
              </div>
            </div>
            <div class="col-md-2">
              <a href="<?=base_url().'category';?>"><img class="img-responsive w-100" src="assets/front/img/Product5@2.png" alt=""></a>
              <div class="title-categories">
                <a href="<?=base_url().'category';?>">Lorem ipsum</a>
              </div>
            </div>
            <div class="col-md-2">
              <a href="<?=base_url().'category';?>"><img class="img-responsive w-100" src="assets/front/img/Product4@2.png" alt=""></a>
              <div class="title-categories">
                <a href="<?=base_url().'category';?>">Lorem ipsum</a>
              </div>
            </div>
            <div class="col-md-2">
              <a href="<?=base_url().'category';?>"><img class="img-responsive w-100" src="assets/front/img/Product3@2.png" alt=""></a>
              <div class="title-categories">
                <a href="<?=base_url().'category';?>">Lorem ipsum</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="new-arival">
      <div class="container">
        <div class="head-background">
          <h5>New Arrivals</h5> <a class="view-all" href="product-category.html">View all</a>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="row">
              <div class="col-md-4">
                <a href="<?=base_url().'detail';?>"><img class="img-responsive w-100" src="assets/front/img/Product2@2.png" alt=""></a>
              </div>
              <div class="col-md-8">
                <div class="det-prod">
                  <div class="">
                    <a href="<?=base_url().'detail';?>">Lorem ipsum dolor amet</a>
                  </div>
                  <div class="star">
                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                      class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                  </div>
                  <div class="">
                    <!-- <span class="disc-price">Rp. 75.000</span>--> <span class="true-price">Rp. 75.000</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="row">
              <div class="col-md-4">
                <a href="<?=base_url().'detail';?>"><img class="img-responsive w-100" src="assets/front/img/Product3@2.png" alt=""></a>
              </div>
              <div class="col-md-8">
                <div class="det-prod">
                  <div class="">
                    <a href="<?=base_url().'detail';?>">Lorem ipsum dolor amet</a>
                  </div>
                  <div class="star">
                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                      class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                  </div>
                  <div class="">
                    <!-- <span class="disc-price">Rp. 75.000</span>--> <span class="true-price">Rp. 75.000</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="row">
              <div class="col-md-4">
                <a href="<?=base_url().'detail';?>"><img class="img-responsive w-100" src="assets/front/img/Product5@2.png" alt=""></a>
              </div>
              <div class="col-md-8">
                <div class="det-prod">
                  <div class="">
                    <a href="<?=base_url().'detail';?>">Lorem ipsum dolor amet</a>
                  </div>
                  <div class="star">
                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                      class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                  </div>
                  <div class="">
                    <!-- <span class="disc-price">Rp. 75.000</span>--> <span class="true-price">Rp. 75.000</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-md-4">
            <div class="row">
              <div class="col-md-4">
                <a href="<?=base_url().'detail';?>"><img class="img-responsive w-100" src="assets/front/img/Product7@2.png" alt=""></a>
              </div>
              <div class="col-md-8">
                <div class="det-prod">
                  <div class="">
                    <a href="<?=base_url().'detail';?>">Lorem ipsum dolor amet</a>
                  </div>
                  <div class="star">
                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                      class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                  </div>
                  <div class="">
                    <!-- <span class="disc-price">Rp. 75.000</span>--> <span class="true-price">Rp. 75.000</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="row">
              <div class="col-md-4">
                <a href="<?=base_url().'detail';?>"><img class="img-responsive w-100" src="assets/front/img/Product7@2.png" alt=""></a>
              </div>
              <div class="col-md-8">
                <div class="det-prod">
                  <div class="">
                    <a href="<?=base_url().'detail';?>">Lorem ipsum dolor amet</a>
                  </div>
                  <div class="star">
                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                      class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                  </div>
                  <div class="">
                    <!-- <span class="disc-price">Rp. 75.000</span>--> <span class="true-price">Rp. 75.000</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="row">
              <div class="col-md-4">
                <a href="<?=base_url().'detail';?>"><img class="img-responsive w-100" src="assets/front/img/Product8@2.png" alt=""></a>
              </div>
              <div class="col-md-8">
                <div class="det-prod">
                  <div class="">
                    <a href="<?=base_url().'detail';?>">Lorem ipsum dolor amet</a>
                  </div>
                  <div class="star">
                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                      class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                  </div>
                  <div class="">
                    <!-- <span class="disc-price">Rp. 75.000</span>--> <span class="true-price">Rp. 75.000</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="makanan-section">
      <div class="container">
        <div class="head-background">
          <h5>Rekomendasi Kami</h5> 
        </div>
        <div id="makanan" class="carousel slide" data-ride="carousel" data-interval="0">
          <div class="carousel-inner">
            <div class="item carousel-item active">
              <div class="row">
                <div class="col-sm-2">
                  <div class="">
                    <div class="disc-tem">
                      50%
                    </div>
                    <div class="hovereffect">
                     <a href="<?=base_url().'detail';?>"><img class="img-responsive w-100" src="assets/front/img/Product1@2.png"
                         alt=""></a>
                      <div class="overlay">
                        <p class="icon-links">
                          <a href="#">
                            <span class="fa fa-heart-o"></span>
                          </a>
                          <a href="#">
                            <span class="fa fa-shopping-cart"></span>
                          </a>
                        </p>
                      </div>
                    </div>
                    <div class="det-prod">
                      <div class="">
                        <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                      </div>
                      <div class="">
                        <a href="#">Lorem ipsum dolor amet</a>
                      </div>
                      <div class="">
                        <p>Sold by: Game lorem</p>
                      </div>
                      <div class="star">
                        <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                          class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="">
                    <div class="disc-tem">
                      50%
                    </div>
                    <div class="hovereffect">
                      <a href="<?=base_url().'detail';?>"><img class="img-responsive w-100" src="assets/front/img/Product2@2.png"
                          alt=""></a>
                      <div class="overlay">
                        <p class="icon-links">
                          <a href="#">
                            <span class="fa fa-heart-o"></span>
                          </a>
                          <a href="#">
                            <span class="fa fa-shopping-cart"></span>
                          </a>
                        </p>
                      </div>
                    </div>
                    <div class="det-prod">
                      <div class="">
                        <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                      </div>
                      <div class="">
                        <a href="#">Lorem ipsum dolor amet</a>
                      </div>
                      <div class="">
                        <p>Sold by: Game lorem</p>
                      </div>
                      <div class="star">
                        <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                          class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="">
                    <div class="disc-tem">
                      50%
                    </div>
                    <div class="hovereffect">
                     <a href="<?=base_url().'detail';?>"><img class="img-responsive w-100" src="assets/front/img/Product3@2.png"
                         alt=""></a>
                      <div class="overlay">
                        <p class="icon-links">
                          <a href="#">
                            <span class="fa fa-heart-o"></span>
                          </a>
                          <a href="#">
                            <span class="fa fa-shopping-cart"></span>
                          </a>
                        </p>
                      </div>
                    </div>
                    <div class="det-prod">
                      <div class="">
                        <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                      </div>
                      <div class="">
                        <a href="#">Lorem ipsum dolor amet</a>
                      </div>
                      <div class="">
                        <p>Sold by: Game lorem</p>
                      </div>
                      <div class="star">
                        <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                          class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="">
                    <div class="disc-tem">
                      50%
                    </div>
                    <div class="hovereffect">
                      <a href="<?=base_url().'detail';?>"><img class="img-responsive w-100" src="assets/front/img/Product4@2.png"
                          alt=""></a>
                      <div class="overlay">
                        <p class="icon-links">
                          <a href="#">
                            <span class="fa fa-heart-o"></span>
                          </a>
                          <a href="#">
                            <span class="fa fa-shopping-cart"></span>
                          </a>
                        </p>
                      </div>
                    </div>
                    <div class="det-prod">
                      <div class="">
                        <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                      </div>
                      <div class="">
                        <a href="#">Lorem ipsum dolor amet</a>
                      </div>
                      <div class="">
                        <p>Sold by: Game lorem</p>
                      </div>
                      <div class="star">
                        <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                          class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="">
                    <div class="disc-tem">
                      50%
                    </div>
                    <div class="hovereffect">
                      <a href="detail-product.html"><img class="img-responsive w-100" src="assets/front/img/Product5@2.png"
                          alt=""></a>
                      <div class="overlay">
                        <p class="icon-links">
                          <a href="#">
                            <span class="fa fa-heart-o"></span>
                          </a>
                          <a href="#">
                            <span class="fa fa-shopping-cart"></span>
                          </a>
                        </p>
                      </div>
                    </div>
                    <div class="det-prod">
                      <div class="">
                        <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                      </div>
                      <div class="">
                        <a href="#">Lorem ipsum dolor amet</a>
                      </div>
                      <div class="">
                        <p>Sold by: Game lorem</p>
                      </div>
                      <div class="star">
                        <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                          class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="">
                    <div class="disc-tem">
                      50%
                    </div>
                    <div class="hovereffect">
                     <a href="detail-product.html"><img class="img-responsive w-100" src="assets/front/img/Product6@2.png"
                         alt=""></a>
                      <div class="overlay">
                        <p class="icon-links">
                          <a href="#">
                            <span class="fa fa-heart-o"></span>
                          </a>
                          <a href="#">
                            <span class="fa fa-shopping-cart"></span>
                          </a>
                        </p>
                      </div>
                    </div>
                    <div class="det-prod">
                      <div class="">
                        <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                      </div>
                      <div class="">
                        <a href="#">Lorem ipsum dolor amet</a>
                      </div>
                      <div class="">
                        <p>Sold by: Game lorem</p>
                      </div>
                      <div class="star">
                        <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                          class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>

            </div>
            <div class="item carousel-item">
              <div class="row">
                <div class="col-sm-2">
                  <div class="">
                    <div class="disc-tem">
                      50%
                    </div>
                    <div class="hovereffect">
                      <a href="detail-product.html"><img class="img-responsive w-100" src="assets/front/img/Product7@2.png"
                          alt=""></a>
                      <div class="overlay">
                        <p class="icon-links">
                          <a href="#">
                            <span class="fa fa-heart-o"></span>
                          </a>
                          <a href="#">
                            <span class="fa fa-shopping-cart"></span>
                          </a>
                        </p>
                      </div>
                    </div>
                    <div class="det-prod">
                      <div class="">
                        <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                      </div>
                      <div class="">
                        <a href="#">Lorem ipsum dolor amet</a>
                      </div>
                      <div class="">
                        <p>Sold by: Game lorem</p>
                      </div>
                      <div class="star">
                        <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                          class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="">
                    <div class="disc-tem">
                      50%
                    </div>
                    <div class="hovereffect">
                      <a href="detail-product.html"><img class="img-responsive w-100" src="assets/front/img/Product8@2.png"
                          alt=""></a>
                      <div class="overlay">
                        <p class="icon-links">
                          <a href="#">
                            <span class="fa fa-heart-o"></span>
                          </a>
                          <a href="#">
                            <span class="fa fa-shopping-cart"></span>
                          </a>
                        </p>
                      </div>
                    </div>
                    <div class="det-prod">
                      <div class="">
                        <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                      </div>
                      <div class="">
                        <a href="#">Lorem ipsum dolor amet</a>
                      </div>
                      <div class="">
                        <p>Sold by: Game lorem</p>
                      </div>
                      <div class="star">
                        <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                          class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="">
                    <div class="disc-tem">
                      50%
                    </div>
                    <div class="hovereffect">
                      <a href="detail-product.html"><img class="img-responsive w-100" src="assets/front/img/Product9@2.png"
                          alt=""></a>
                      <div class="overlay">
                        <p class="icon-links">
                          <a href="#">
                            <span class="fa fa-heart-o"></span>
                          </a>
                          <a href="#">
                            <span class="fa fa-shopping-cart"></span>
                          </a>
                        </p>
                      </div>
                    </div>
                    <div class="det-prod">
                      <div class="">
                        <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                      </div>
                      <div class="">
                        <a href="#">Lorem ipsum dolor amet</a>
                      </div>
                      <div class="">
                        <p>Sold by: Game lorem</p>
                      </div>
                      <div class="star">
                        <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                          class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="">
                    <div class="disc-tem">
                      50%
                    </div>
                    <div class="hovereffect">
                      <a href="detail-product.html"><img class="img-responsive w-100" src="assets/front/img/Product1@2.png"
                          alt=""></a>
                      <div class="overlay">
                        <p class="icon-links">
                          <a href="#">
                            <span class="fa fa-heart-o"></span>
                          </a>
                          <a href="#">
                            <span class="fa fa-shopping-cart"></span>
                          </a>
                        </p>
                      </div>
                    </div>
                    <div class="det-prod">
                      <div class="">
                        <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                      </div>
                      <div class="">
                        <a href="#">Lorem ipsum dolor amet</a>
                      </div>
                      <div class="">
                        <p>Sold by: Game lorem</p>
                      </div>
                      <div class="star">
                        <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                          class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="">
                    <div class="disc-tem">
                      50%
                    </div>
                    <div class="hovereffect">
                     <a href="detail-product.html"><img class="img-responsive w-100" src="assets/front/img/Product2@2.png"
                         alt=""></a>
                      <div class="overlay">
                        <p class="icon-links">
                          <a href="#">
                            <span class="fa fa-heart-o"></span>
                          </a>
                          <a href="#">
                            <span class="fa fa-shopping-cart"></span>
                          </a>
                        </p>
                      </div>
                    </div>
                    <div class="det-prod">
                      <div class="">
                        <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                      </div>
                      <div class="">
                        <a href="#">Lorem ipsum dolor amet</a>
                      </div>
                      <div class="">
                        <p>Sold by: Game lorem</p>
                      </div>
                      <div class="star">
                        <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                          class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="">
                    <div class="disc-tem">
                      50%
                    </div>
                    <div class="hovereffect">
                      <a href="detail-product.html"><img class="img-responsive w-100" src="assets/front/img/Product3@2.png"
                          alt=""></a>
                      <div class="overlay">
                        <p class="icon-links">
                          <a href="#">
                            <span class="fa fa-heart-o"></span>
                          </a>
                          <a href="#">
                            <span class="fa fa-shopping-cart"></span>
                          </a>
                        </p>
                      </div>
                    </div>
                    <div class="det-prod">
                      <div class="">
                        <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span>
                      </div>
                      <div class="">
                        <a href="#">Lorem ipsum dolor amet</a>
                      </div>
                      <div class="">
                        <p>Sold by: Game lorem</p>
                      </div>
                      <div class="star">
                        <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                          class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>

            </div>
          </div>
          <!-- Carousel controls -->
 <!--          <a class="carousel-control-prev" href="#makanan" data-slide="prev">
            <div class="control-slide">
              <i class="fa fa-angle-left"></i>
            </div>
          </a>
          <a class="carousel-control-next" href="#makanan" data-slide="next">
            <div class="control-slide">
              <i class="fa fa-angle-right"></i>
            </div>
          </a> -->
        </div>
      </div>
    </section>
  </body>

