<body>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h3>Buyer Dashboard</h3>
                <div class="">
                    <p>Selamat datang, Kelola setiap alur masuk dan keuar barang anda. Dashboard didesain mudah agar
                        anda
                        bisa mentracking barang yang sedang anda jual</p>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <span style="font-weight:500">MAIN MENU</span>
                    <ul class="menu-vendor-setting mt-3">
                        <li>
                          <a href="<?=base_url().'buyer';?>">Dashboard</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="<?=base_url().'buyer/personal';?>">Personal Information</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="<?=base_url().'buyer/shipping';?>">Shipping Address</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="<?=base_url().'buyer/order';?>">Your Order</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="<?=base_url().'buyer/change_password';?>">Change Password</a> <i class="fa fa-angle-right"></i>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                    <h5 class="mb-0">Change Password</h5>
                    <input type="hidden" value="1" id="count_address">
                    <div class="row" id="box-shipping">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12 mt-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Password</label>
                                        <input type="password" class="form-control" placeholder="Password">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Confirm Password</label>
                                        <input type="v" class="form-control" placeholder="Confirm Password">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right mt-4">
                            <button class="btn btn-shop">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</body>

