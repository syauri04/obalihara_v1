<body>
  <section>
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center centerin">
                <h3>Wishlist</h3>
                <p>Selamat datang, Kelola setiap alur masuk dan keuar barang anda. Dashboard didesain mudah agar anda
                    bisa mentracking barang yang sedang anda jual</p>
            </div>
        </div>
    </div>
 </section>
 <section class="mt-5">
     <div class="container">
         <div class="head-title">
             <h5>Your Whislist Items</h5>
         </div>
         <table class="table">
             <thead>
                 <tr>
                     <th></th>
                     <th>Product Name</th>
                     <th>Price</th>
                     <th>Status</th>
                     <th></th>
                 </tr>
             </thead>
             <tr>
                 <td><i class="fa fa-trash delete"></i></td>
                 <td><img src="http://placehold.it/50x50" alt=""> <a class="title-product" href="">Marshall Lorem Ipsum dolor amet (149077)</a></td>
                 <td><span>Rp. 720.000</span></td>
                 <td><span class="text-success">In stock</span></td>
                 <td><a href="<?=base_url().'cart';?>"><button class="btn btn-shop"><i class="fa fa-shopping-cart"></i> Checkout</button></a>
                 </td>
             </tr>
             <tr>
                 <td><i class="fa fa-trash delete"></i></td>
                 <td><img src="http://placehold.it/50x50" alt=""> <a class="title-product" href="">Marshall Lorem Ipsum
                         dolor amet (149077)</a></td>
                 <td><span>Rp. 720.000</span></td>
                 <td><span class="text-success">In stock</span></td>
                 <td><a href="<?=base_url().'cart';?>"><button class="btn btn-shop"><i class="fa fa-shopping-cart"></i>
                       Checkout</button></a></td>
             </tr>
             <tr>
                 <td><i class="fa fa-trash delete"></i></td>
                 <td><img src="http://placehold.it/50x50" alt=""> <a class="title-product" href="">Marshall Lorem Ipsum
                         dolor amet (149077)</a></td>
                 <td><span>Rp. 720.000</span></td>
                 <td><span class="text-danger">Out of stock</span></td>
                 <td><a href="<?=base_url().'cart';?>"><button class="btn btn-shop"><i class="fa fa-shopping-cart"></i>
                       Checkout</button></a></td>
             </tr>
         </table>
     </div>
 </section>
 <br>
 <section class="mt-3">
     <div class="container">
         <a class="back" href=""><i class="fa fa-angle-left"></i> Back to Shopping</a>
     </div>
 </section>
</body>

