<body>
    <section>
    <div class="container">
        <div id="information" class="text-center centerin">
          <div class="mb-2">
            <h3 >Shopping Cart</h3>
          </div>
          <div class="card mb-0 pb-0 mt-4">
            <ul class="mb-0">
              <li>
                <table>
                  <tr>
                    <td rowspan="2"><i class="fa fa-rocket"></i></td>
                    <td><span>Free Delivery</span></td>
                  </tr>
                  <tr>
                    <td>
                      <p>For all order from $99</p>
                    </td>
                  </tr>
                </table>
              </li>
              <li>
                <table>
                  <tr>
                    <td rowspan="2"><i class="fa fa-retweet"></i></td>
                    <td><span>Free Delivery</span></td>
                  </tr>
                  <tr>
                    <td>
                      <p>Free return in 90 days</p>
                    </td>
                  </tr>
                </table>
              </li>
              <li>
                <table>
                  <tr>
                    <td rowspan="2"><i class="fa fa-credit-card"></i></td>
                    <td><span>Secure Payments</span></td>
                  </tr>
                  <tr>
                    <td>
                      <p>100% Secure payments</p>
                    </td>
                  </tr>
                </table>
              </li>
              <li>
                <table>
                  <tr>
                    <td rowspan="2"><i class="fa fa-headphones"></i></td>
                    <td><span>24/7 Support</span></td>
                  </tr>
                  <tr>
                    <td>
                      <p>Dedicated support in 24h</p>
                    </td>
                  </tr>
                </table>
              </li>
              <li class="last">
                <table>
                  <tr>
                    <td rowspan="2"><i class="fa fa-gift"></i></td>
                    <td><span>Special Gift</span></td>
                  </tr>
                  <tr>
                    <td>
                      <p>Free special gift</p>
                    </td>
                  </tr>
                </table>
              </li>
            </ul>
            <div class="clearfix">
          </div>
        </div>
    </div>
  </section>
  <section class="mt-5">
    <div class="container">
      <div class="head-title">
        <h5>Your Cart Items</h5>
      </div>
      <table class="table">
        <tr>
          <td><i class="fa fa-trash delete"></i></td>
          <td width="70px"><b>QTY</b> <input type="text" class="form-control" /> </td>
          <td ><div class="row">
            <div class="col-md-2">
              <img src="http://placehold.it/80x80" alt=""> 
            </div>
            <div class="col-md-10">
              <div>
                <a class="title-product" href="">Marshall Lorem Ipsum dolor
                  amet (149077)</a>
              </div>
              <div>
                <span>Color : <b>Black</b></span>
              </div>
               <div>
                 <span><b>1 x Rp. 720.000 = Rp. 720.000</b></span>
               </div>
            </div>
          </div></td>
          <td>
            <div>
              <b>Expedition Service:</b>
            </div>
            <div>
              From : Semarang To: <a href="#">Bandung</a>
            </div>
            <div>
              <select class="custom-select custom-select-sm sort-ac">
                <option selected="">Pos Indonesia</option>
                <option value="1">JNE</option>
              </select>
            </div>
          </td>
          <td>
            <div>
              <b>Expedition Rate: </b>
            </div>
            <div>
             <span class="text-pink"><b>Rp. 20.000</b></span>
            </div>
            <div>
              <span>3-7 hari sampai</span>
            </div>
          </td>
          <td>
            <div><b>Total</b></div>
            <div><span class="text-pink"><b>Rp. 72.000</b> </span> </div>
          </td>
        </tr>
        <tr>
          <td><i class="fa fa-trash delete"></i></td>
          <td width="70px"><b>QTY</b> <input type="text" class="form-control" /> </td>
          <td>
            <div class="row">
              <div class="col-md-2">
                <img src="http://placehold.it/80x80" alt="">
              </div>
              <div class="col-md-10">
                <div>
                  <a class="title-product" href="">Marshall Lorem Ipsum dolor
                    amet (149077)</a>
                </div>
                <div>
                  <span>Color : <b>Black</b></span>
                </div>
                <div>
                  <span><b>1 x Rp. 720.000 = Rp. 720.000</b></span>
                </div>
              </div>
            </div>
          </td>
          <td>
            <div>
              <b>Expedition Service:</b>
            </div>
            <div>
              From : Semarang To: <a href="#">Bandung</a>
            </div>
            <div>
              <select class="custom-select custom-select-sm sort-ac">
                <option selected="">Pos Indonesia</option>
                <option value="1">JNE</option>
              </select>
            </div>
          </td>
          <td>
            <div>
              <b>Expedition Rate: </b>
            </div>
            <div>
              <span class="text-pink"><b>Rp. 20.000</b></span>
            </div>
            <div>
              <span>3-7 hari sampai</span>
            </div>
          </td>
          <td>
            <div><b>Total</b></div>
            <div><span class="text-pink"><b>Rp. 72.000</b> </span> </div>
          </td>
        </tr>
        <tr>
          <td><i class="fa fa-trash delete"></i></td>
          <td width="70px"><b>QTY</b> <input type="text" class="form-control" /> </td>
          <td>
            <div class="row">
              <div class="col-md-2">
                <img src="http://placehold.it/80x80" alt="">
              </div>
              <div class="col-md-10">
                <div>
                  <a class="title-product" href="">Marshall Lorem Ipsum dolor
                    amet (149077)</a>
                </div>
                <div>
                  <span>Color : <b>Black</b></span>
                </div>
                <div>
                  <span><b>1 x Rp. 720.000 = Rp. 720.000</b></span>
                </div>
              </div>
            </div>
          </td>
          <td>
            <div>
              <b>Expedition Service:</b>
            </div>
            <div>
              From : Semarang To: <a href="#">Bandung</a>
            </div>
            <div>
              <select class="custom-select custom-select-sm sort-ac">
                <option selected="">Pos Indonesia</option>
                <option value="1">JNE</option>
              </select>
            </div>
          </td>
          <td>
            <div>
              <b>Expedition Rate: </b>
            </div>
            <div>
              <span class="text-pink"><b>Rp. 20.000</b></span>
            </div>
            <div>
              <span>3-7 hari sampai</span>
            </div>
          </td>
          <td>
            <div><b>Total</b></div>
            <div><span class="text-pink"><b>Rp. 72.000</b> </span> </div>
          </td>
        </tr>
        <tr>
          <td><i class="fa fa-trash delete"></i></td>
          <td width="70px"><b>QTY</b> <input type="text" class="form-control" /> </td>
          <td>
            <div class="row">
              <div class="col-md-2">
                <img src="http://placehold.it/80x80" alt="">
              </div>
              <div class="col-md-10">
                <div>
                  <a class="title-product" href="">Marshall Lorem Ipsum dolor
                    amet (149077)</a>
                </div>
                <div>
                  <span>Color : <b>Black</b></span>
                </div>
                <div>
                  <span><b>1 x Rp. 720.000 = Rp. 720.000</b></span>
                </div>
              </div>
            </div>
          </td>
          <td>
            <div>
              <b>Expedition Service:</b>
            </div>
            <div>
              From : Semarang To: <a href="#">Bandung</a>
            </div>
            <div>
              <select class="custom-select custom-select-sm sort-ac">
                <option selected="">Pos Indonesia</option>
                <option value="1">JNE</option>
              </select>
            </div>
          </td>
          <td>
            <div>
              <b>Expedition Rate: </b>
            </div>
            <div>
              <span class="text-pink"><b>Rp. 20.000</b></span>
            </div>
            <div>
              <span>3-7 hari sampai</span>
            </div>
          </td>
          <td>
            <div><b>Total</b></div>
            <div><span class="text-pink"><b>Rp. 72.000</b> </span> </div>
          </td>
        </tr>
       <tfoot>
         <tr>
           <td colspan="6" class="text-right"><span><b>Total </b></span><span class="text-pink">Rp. 720.000</span></td>
         </tr>
       </tfoot>
       
      </table>
    </div>
  </section>
  <br>
  <section class="mt-3">
    <div class="container">
      <a class="back" href="<?=base_url().'category';?>"><i class="fa fa-angle-left"></i> Back to Shopping</a>
      <a href="<?=base_url().'checkout';?>"> <button class="btn btn-shop pull-right"><i class="fa fa-shopping-cart"></i>
          Checkout</button></a>
    </div>
  </section>
</body>

