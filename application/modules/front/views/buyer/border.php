<body>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h3>Buyer Dashboard</h3>
                <div class="">
                    <p>Selamat datang, Kelola setiap alur masuk dan keuar barang anda. Dashboard didesain mudah agar
                        anda
                        bisa mentracking barang yang sedang anda jual</p>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <span style="font-weight:500">MAIN MENU</span>
                    <ul class="menu-vendor-setting mt-3">
                        <li>
                          <a href="<?=base_url().'buyer?id='.urlencode(codeEncrypt($this->jCfg['client']['id']));?>">Dashboard</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="<?=base_url().'buyer/personal?id='.urlencode(codeEncrypt($this->jCfg['client']['id']));?>">Personal Information</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="<?=base_url().'buyer/shipping?id='.urlencode(codeEncrypt($this->jCfg['client']['id']))?>">Shipping Address</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="#">Your Order</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="<?=base_url().'buyer/change_password?id='.urlencode(codeEncrypt($this->jCfg['client']['id']))?>">Change Password</a> <i class="fa fa-angle-right"></i>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                    <h5 class="mb-5">Your Order</h5>
                    <h6 class="mb-2">Recent Order</h6>
                    <table class="data-table table">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th width="400">Items</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach ($order as $k => $v) {
                            ?>
                            <tr>
                                <td><?= date('F d, Y',strtotime($v->tanggal_order))?></td>
                                <td>
                                    <div class="row">
                                        <div class="col-3"><img src="<?= produk::gambar($v->barang_id,'thumb')?>" alt="" width="70" height="70" ></div>
                                        <div class="col-9">
                                            <div><b><?= produk::goods($v->barang_id,'nama_barang') ?></b></div>
                                            <div>
                                                <span class="mr-3">Color : <?= produk::warna($v->warna_id) ?></span><br/>
                                                <span>Order ID : <a href="<?= base_url().'buyer/payment?id='.urlencode(codeEncrypt($this->jCfg['client']['id'])).'&&code='.urlencode(getCheckoutbyCart($v->id,'invoice_code'));?>"><?= getCheckoutbyCart($v->id,'invoice_code'); ?></a></span>
                                            </div>
                                        </div>
                                    </div> 
                                </td>
                                <td><b class=""><?= statusInvoice($v->status_invoice)?></b></td>
                                <td>
                                    <a href="#" class="btnTracking" data-status="<?= $v->status_invoice ?>" data-cartid="<?= $v->id ?>" data-courier="<?= courierSwitch(getCheckoutdetailbyCart($v->id,'courier'))?>" data-receipt="<?= $v->resi;?>"><button class="btn btn-shop">Tracking</button></a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <h6 class="mt-3 mb-2">Last Order</h6>
                    <table class="data-table table">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th width="400">Items</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach ($order as $k => $v) {
                            ?>
                            <tr>
                                <td><?= date('F d, Y',strtotime($v->tanggal_order))?></td>
                                <td>
                                    <div class="row">
                                        <div class="col-3"><img src="<?= produk::gambar($v->barang_id,'thumb')?>" alt="" width="70" height="70" ></div>
                                        <div class="col-9">
                                            <div><b><?= produk::goods($v->barang_id,'nama_barang') ?></b></div>
                                            <div>
                                                <span class="mr-3">Color : <?= produk::warna($v->warna_id) ?></span><br/>
                                                <span>Order ID : <a href="<?= base_url().'buyer/payment?id='.urlencode(codeEncrypt($this->jCfg['client']['id'])).'&&code='.urlencode(getCheckoutbyCart($v->id,'invoice_code'));?>"><?= getCheckoutbyCart($v->id,'invoice_code'); ?></a></span></div>
                                        </div>
                                    </div> 
                                </td>
                                <td><b class=""><?= statusInvoice($v->status_invoice)?></b></td>
                                <td>
                                    <a href="#" class="btnTracking" data-status="<?= $v->status_invoice ?>" data-cartid="<?= $v->id ?>" data-courier="<?= getCheckoutdetailbyCart($v->id,'courier')?>" data-receipt="<?= $v->resi;?>"><button class="btn btn-shop">Tracking</button></a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
</body>

<div class="modal fade" id="trekingModal" tabindex="-1" role="dialog" aria-labelledby="trekingModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content" style="height: 400px;">
            <div class="modal-header">
                <h5 class="modal-title" id="trekingModalLabel">Status Resi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="overflow-y: auto;">
                <input type="hidden" id="cartid">
                <div>
                    <b><i class="resiStatus"></i></b>
                </div>
                <br />
                <div class="col-md-12">
                    <div class="manifest">
                        <ul>
                            <li class="title"></li>
                            <li class="description"></li>
                        </ul>           
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btnTerima" style="background: #c1355e;">Terima Barang</button>
            </div>
        </div>
    </div>
</div>

<?= js_assets('buyer/buyer.index.js','f'); ?>