<?= plugin_assets('dropzone/min/dropzone.min.css','b','css') ?>
<?= plugin_assets('dropzone/min/dropzone.min.js','b') ?>
<body>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h3>Buyer Dashboard</h3>
                <div class="">
                    <p>Selamat datang, Kelola setiap alur masuk dan keuar barang anda. Dashboard didesain mudah agar
                        anda
                        bisa mentracking barang yang sedang anda jual</p>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <span style="font-weight:500">MAIN MENU</span>
                    <ul class="menu-vendor-setting mt-3">
                        <li>
                          <a href="<?=base_url().'buyer?id='.urlencode(codeEncrypt($this->jCfg['client']['id']));?>">Dashboard</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="#">Personal Information</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="<?=base_url().'buyer/shipping?id='.urlencode(codeEncrypt($this->jCfg['client']['id']))?>">Shipping Address</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="<?=base_url().'buyer/order?id='.urlencode(codeEncrypt($this->jCfg['client']['id']))?>">Your Order</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="<?=base_url().'buyer/change_password?id='.urlencode(codeEncrypt($this->jCfg['client']['id']))?>">Change Password</a> <i class="fa fa-angle-right"></i>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                    <h5 class="mb-5">Personal Information</h5>
                        <div class="row">
                            <div class="col-md-3">
                                <div id="body-overlay">
                                    <div><img src="assets/img/loading.gif" width="64px" height="64px" /></div>
                                </div>
                                <div id="dropzone">
                                    <form action="<?= base_url().'front/buyer/savebPersonalPhoto'?>" class="dropzone needsclick" id="demoUpload1">
                                        <input type="hidden" name="id" value="<?= getUser($this->jCfg['client']['id'],'id') ?>">
                                        <input type="hidden" name="user_id" value="<?= $this->jCfg['client']['id'] ?>">
                                        <div class="dz-message needsclick">
                                            Drop files <b>here</b> or <b>click</b> to upload.<br />
                                            <span class="dz-note needsclick">
                                                (This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)
                                            </span>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <form method="post" action="<?= base_url().'front/buyer/savebPersonal'?>" enctype="multipart/form-data" id="formInput">
                                <input type="hidden" name="id" value="<?= getUser($this->jCfg['client']['id'],'id') ?>">
                                <input type="hidden" name="user_id" value="<?= $this->jCfg['client']['id'] ?>">
                                <div class="col-md-9">
                                    <h6 class="mt-5 mb-2"><span class="required">*</span> Your Name</h6>
                                    <div class="form-group">
                                        <small id="emailHelp" class="form-text text-muted">Your name is public</small>
                                        <input type="text" name="nama_lengkap" class="form-control mt-2" id="exampleInputEmail1" value="<?= isset($personal)?$personal->nama_lengkap:'';?>" 
                                            aria-describedby="emailHelp" placeholder="Your name">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h6 class="mt-5 mb-4"><span class="required">*</span> Your Contact</h6>
                                    <div class="row">
                                       <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email Address</label>
                                                <input type="email" class="form-control" name="email" placeholder="Email Adress" value="<?= isset($personal)?$personal->email:'';?>">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Phone Number</label>
                                                <input type="text" class="form-control" name="telp" placeholder="Phone Number" value="<?= isset($personal)?$personal->telp:'';?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h6 class="mt-5 mb-4"><span class="required">*</span> Your Address</h6>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Province</label>
                                                <select name="provinsi_id" class="form-controll custom-select" id="provinsiID">
                                                    <option >- Pilih -</option>
                                                    <?php
                                                        foreach ($province as $k => $v) {
                                                            $se = isset($personal)&&$personal->provinsi_id == $v->id?'selected':'';
                                                            echo '<option value="'.$v->id.'" '.$se.'>'.$v->nama_provinsi.'</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">City</label>
                                                <select name="kota_id" class="form-controll custom-select" id="kotaID" data-id="<?= isset($personal)?$personal->kota_id:'';?>">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Subdistrict</label>
                                                <select name="kecamatan_id" class="form-controll custom-select" id="kecID" data-id="<?= isset($personal)?$personal->kecamatan_id:'';?>"></select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Postal code</label>
                                                <input type="text" name="post_code" class="form-control" placeholder="Postal code" value="<?= isset($personal)?$personal->post_code:''; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Address</label>
                                                <input type="text" name="alamat" class="form-control" placeholder="Address" value="<?= isset($personal)?$personal->alamat:''; ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-12 text-right mt-4">
                                            <button class="btn btn-shop">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
</body>

<?= js_assets('buyer/buyer.setting.js','f'); ?>