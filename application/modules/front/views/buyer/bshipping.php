<body>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h3>Buyer Dashboard</h3>
                <div class="">
                    <p>Selamat datang, Kelola setiap alur masuk dan keuar barang anda. Dashboard didesain mudah agar
                        anda
                        bisa mentracking barang yang sedang anda jual</p>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <span style="font-weight:500">MAIN MENU</span>
                    <ul class="menu-vendor-setting mt-3">
                        <li>
                          <a href="<?=base_url().'buyer?id='.urlencode(codeEncrypt($this->jCfg['client']['id']));?>">Dashboard</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="<?=base_url().'buyer/personal?id='.urlencode(codeEncrypt($this->jCfg['client']['id']));?>">Personal Information</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="#">Shipping Address</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="<?=base_url().'buyer/order?id='.urlencode(codeEncrypt($this->jCfg['client']['id']))?>">Your Order</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="<?=base_url().'buyer/change_password?id='.urlencode(codeEncrypt($this->jCfg['client']['id']))?>">Change Password</a> <i class="fa fa-angle-right"></i>
                        </li>
                    </ul>
                </div>
            </div>
            
            <div class="col-md-9">
                <form method="post" action="<?= base_url().'front/buyer/saveAddress'?>" enctype="multipart/form-data" >
                    <input type="hidden" name="buyerID" value="<?= $buyerId; ?>">
                    <div class="card">
                        <h5 class="mb-0">Shipping Address</h5>
                        <?php 
                            if(isset($_GET['msg'])) { 
                                $message = $_GET['msg'];
                                echo "<script type='text/javascript'>alert('$message');</script>";
                            } 
                        ?>
                        <?php
                            if(!empty($address)){
                                $no = 1;
                                foreach ($address as $k => $v) {
                        ?>
                        <div class="row addId<?= $v->id ?>" id="box-shipping-<?=$no++?>">
                            <div class="col-md-12">
                                <h6 class="mt-5"><span class="required">*</span> Shipping Address</h6><a href="#" class="btn btn-danger deleteAddress" data-id="<?= $v->id?>"> Hapus Alamat</a>
                                <input type="hidden" name="id[]" value="<?= $v->id; ?>">
                                <div class="row" >
                                    <div class="col-md-12 mt-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Full Name</label>
                                            <input name="title[]" type="text" class="form-control" placeholder="Full Name" value="<?= $v->title; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Province</label>
                                            <select name="provinsi_id[]" class="form-controll custom-select provinsiID" >
                                                <option >- Pilih -</option>
                                                <?php
                                                    foreach ($province as $p => $c) {

                                                        $se = $v->provinsi_id == $c['province_id'] ? 'selected':'';
                                                        echo '<option value="'.$c['province_id'].'" '.$se.'>'.$c['province'].'</option>';
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">City</label>
                                            <select name="kota_id[]" class="form-controll custom-select kotaID"  data-id="<?= $v->kota_id;?>"></select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Subdistrict</label>
                                            <select name="kecamatan_id[]" class="form-controll custom-select kecID" data-id="<?= isset($v)?$v->kecamatan_id:'';?>"></select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Postal code</label>
                                            <input type="text" name="post_code[]" class="form-control" placeholder="Postal code" value="<?= $v->post_code; ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Address</label>
                                            <input type="text" name="alamat[]" class="form-control" placeholder="Address" value="<?= $v->alamat; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                                }
                            } else {
                        ?>
                        <div class="row" id="box-shipping-1">
                            <div class="col-md-12">
                                <h6 class="mt-5 titleAdd"><span class="required">*</span> Shipping Address</h6>
                                <input type="hidden" name="id[]" >
                                <div class="row" >
                                    <div class="col-md-12 mt-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Full Name</label>
                                            <input type="text" name="title[]" class="form-control" placeholder="Full Name" >
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Province</label>
                                            <select name="provinsi_id[]" class="form-controll custom-select provinsiID" id="provinsiID">
                                                <option >- Pilih -</option>
                                                <?php
                                                    foreach ($province as $p => $c) {
                                                        echo '<option value="'.$c['province_id'].'" >'.$c['province'].'</option>';
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">City</label>
                                            <select name="kota_id[]" class="form-controll custom-select kotaID" id="kotaID" ></select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Postal code</label>
                                            <input type="text" name="post_code[]" class="form-control" placeholder="Postal code" >
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Address</label>
                                            <input type="text" name="alamat[]" class="form-control" placeholder="Address" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                            }
                        ?>
                        <div class="row">
                            <div class="col-md-12 mt-4">
                                <a href="#" class="cloneAdd"><b>+ Add Another Shipping Address</b></a>
                            </div>
                            <div class="col-md-12 text-right mt-4">
                                <button class="btn btn-shop">Save changes</button>
                                <!-- <a class="btn btn-shop raong" >raong</a> -->
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            
        </div>
    </div>
</section>
</body>

<?= js_assets('buyer/buyer.shipping.js','f'); ?>