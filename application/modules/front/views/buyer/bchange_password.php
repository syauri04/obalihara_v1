<body>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h3>Buyer Dashboard</h3>
                <div class="">
                    <p>Selamat datang, Kelola setiap alur masuk dan keuar barang anda. Dashboard didesain mudah agar
                        anda
                        bisa mentracking barang yang sedang anda jual</p>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <span style="font-weight:500">MAIN MENU</span>
                    <ul class="menu-vendor-setting mt-3">
                        <li>
                          <a href="<?=base_url().'buyer?id='.urlencode(codeEncrypt($this->jCfg['client']['id']));?>">Dashboard</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="<?=base_url().'buyer/personal?id='.urlencode(codeEncrypt($this->jCfg['client']['id']));?>">Personal Information</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="<?=base_url().'buyer/shipping?id='.urlencode(codeEncrypt($this->jCfg['client']['id']))?>">Shipping Address</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="<?=base_url().'buyer/order?id='.urlencode(codeEncrypt($this->jCfg['client']['id']))?>">Your Order</a> <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                          <a href="#">Change Password</a> <i class="fa fa-angle-right"></i>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <form action="<?= base_url().'front/buyer/saveChangepassword'?>" method="POST" enctype="multipart/form-data">
                    <div class="card">
                        <h5 class="mb-4">Change Password</h5>
                        <?php 
                            if(isset($_GET['msg'])) { 
                                $message = $_GET['msg'];
                                echo "<script type='text/javascript'>alert('$message');</script>";
                            } 
                        ?>
                        <input type="hidden" value="<?= $userID ?>" id="userID" name="userID">
                        <div class="row" id="box-shipping">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12 mt-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Old Password</label>
                                            <input type="password" class="form-control" placeholder="Old Password" name="oldPassword">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Password</label>
                                            <input type="password" class="form-control" placeholder="New Password" name="newPassword">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Confirm Password</label>
                                            <input type="password" class="form-control" placeholder="Confirm Password" name="conPassword">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right mt-4">
                                <button class="btn btn-shop">Save changes</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
</body>

