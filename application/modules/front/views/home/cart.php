<body>
  <section>
    <div class="container">
        <div id="information" class="text-center centerin">
          <div class="mb-2">
            <h3 >Shopping Cart</h3>
          </div>
          <div class="card mb-0 pb-0 mt-4">
            <ul class="mb-0">
              <li>
                <table>
                  <tr>
                    <td rowspan="2"><i class="fa fa-rocket"></i></td>
                    <td><span>Free Delivery</span></td>
                  </tr>
                  <tr>
                    <td>
                      <p>For all order from $99</p>
                    </td>
                  </tr>
                </table>
              </li>
              <li>
                <table>
                  <tr>
                    <td rowspan="2"><i class="fa fa-retweet"></i></td>
                    <td><span>Free Delivery</span></td>
                  </tr>
                  <tr>
                    <td>
                      <p>Free return in 90 days</p>
                    </td>
                  </tr>
                </table>
              </li>
              <li>
                <table>
                  <tr>
                    <td rowspan="2"><i class="fa fa-credit-card"></i></td>
                    <td><span>Secure Payments</span></td>
                  </tr>
                  <tr>
                    <td>
                      <p>100% Secure payments</p>
                    </td>
                  </tr>
                </table>
              </li>
              <li>
                <table>
                  <tr>
                    <td rowspan="2"><i class="fa fa-headphones"></i></td>
                    <td><span>24/7 Support</span></td>
                  </tr>
                  <tr>
                    <td>
                      <p>Dedicated support in 24h</p>
                    </td>
                  </tr>
                </table>
              </li>
              <li class="last">
                <table>
                  <tr>
                    <td rowspan="2"><i class="fa fa-gift"></i></td>
                    <td><span>Special Gift</span></td>
                  </tr>
                  <tr>
                    <td>
                      <p>Free special gift</p>
                    </td>
                  </tr>
                </table>
              </li>
            </ul>
            <div class="clearfix">
          </div>
        </div>
    </div>
  </section>
  <section class="mt-5">
    <div class="container">
      <div class="head-title">
        <h5>Your Cart Items</h5>
      </div>
      <input id="checkAll" class="checkbox-custom" type="checkbox" /><label for="checkAll" class="checkbox-custom-label"><b>Check All</b></label>
      <table class="table">
        <?php
          $no = 1;
          foreach ($cart as $k => $v) {
            echo '<tr><td colspan="6" class="text-left"><input id="checkVendor'.$k.'" class="checkbox-custom checkVendor" type="checkbox" data-vendor="'.$k.'" /><label for="checkVendor'.$k.'" class="checkbox-custom-label"><b>Check '.getOutlet($k,'nama_lengkap').'</b></label></td></tr>';
            foreach ($v as $cItem => $vItem) {
        ?>
        <tr id="rowNum_<?=$no?>" data-vendor="<?= $k; ?>" data-cart="<?= $vItem->id?>">
          <input type="hidden" class="weight" value="<?= produk::detail($vItem->barang_id,'perkiraan_berat')?>">
          <!-- <input type="hidden" name=""> -->
          <td>
            <input id="checkbox<?=$no?>" class="checkbox-custom checksp" type="checkbox" />
            <label for="checkbox-<?=$no?>" class="checkbox-custom-label"></label>
            <br/>
            <br/>
            <a href="#" class="delCart" data-user="<?= $vItem->user_id ?>" data-val="<?= $vItem->barang_id;?>"><i class="fa fa-trash delete"></i></a>
          </td>
          <td width="70px"><b>QTY</b> <input type="text" class="form-control qtyText" value="<?= $vItem->qty;?>" /> </td>
          <td >
            <div class="row">
              <div class="col-md-3">
                <img src="<?= produk::gambar($vItem->barang_id,'small')?>" alt="" width="80" height="80"> 
              </div>
              <div class="col-md-9">
                <div>
                  <a class="title-product" href="<?= base_url().'detail/pd?id='.urlencode(codeEncrypt($vItem->barang_id)).'&&'.'title='.url_title(produk::goods($vItem->barang_id,'nama_barang'));?>"><?= produk::goods($vItem->barang_id,'nama_barang')?></a>
                </div>
                <div>
                  <span>Color : <b><?= produk::warna($vItem->warna_id,'value')?></b></span>
                  <?php if($vItem->ukuran_id > 0){?>
                    <span>Ukuran : <b><?= produk::ukuran($vItem->ukuran_id,'value')?></b></span>
                  <?php } ?>
                </div>
                 <div class="priceDet">
                  <?php if($vItem->discount > 0 ){?>
                    <span><b class="qty"><?= $vItem->qty ?></b> x <b class="hargaSatuan"><?= produk::disPrice(produk::detail($vItem->barang_id,'harga_satuan'),$vItem->discount)?></b> = <b class="ttl_item"></b></span>
                  <?php } else { ?>
                    <span><b class="qty"><?= $vItem->qty ?></b> x <b class="hargaSatuan"><?= produk::detail($vItem->barang_id,'harga_satuan')?></b> = <b class="ttl_item"></b></span>
                  <?php }?>                  
                 </div>
              </div>
            </div>
          </td>
          <td>
            <div>
              <b>Expedition From:</b>
            </div>
            <div class="originDest" data-val="<?= getOutlet($vItem->vendor_id,'kota_id');?>">
              <span >Semarang</span> <!-- To: <a href="#" class="cityDest" data-toggle="modal" data-target="#exampleModalCenter" data-val="rowNum_<?=$no?>">Pilih Kota</a> -->
            </div>
            <div class="courierDesc"><span></span></div>            
          </td>
          <td>
            <!-- <div>
              <b>Expedition Rate: </b>
            </div>
            <div class="courierPrice">
             <span class="text-pink"><b></b></span>
            </div>
            <div class="courierEtd">
              <span></span>
            </div> -->
          </td>
          <td class="tdSubttl">
            <div><b>Total</b></div>
            <div><span class="text-pink subTtl_item"><b>Rp. 0</b> </span> </div>
          </td>
        </tr>
        <?php $no++; } } ?>
       <tfoot>
         <tr>
           <td colspan="6" class="text-right"><span><b>Total </b></span><span class="text-pink totalCart">Rp. 0</span></td>
         </tr>
       </tfoot>
       
      </table>
    </div>
  </section>
  <br>
  <section class="mt-3">
    <div class="container">
      <a class="back" href="<?=base_url()?>"><i class="fa fa-angle-left"></i> Back to Shopping</a>
      <a href="#" class="btn btn-shop pull-right checkOut"><i class="fa fa-shopping-cart"></i> Checkout</a>
    </div>
  </section>
  <form action="<?= base_url().'front/home/checkout';?>" method="post" id="formCartId">
    <input type="hidden" name="cartId" id="cartId">
    <input type="hidden" name="qtyId" id="qtyId">
    <input type="hidden" name="ttlItem" id="ttlItem">
  </form>
</body>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Choose Expedition Service</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" id="colVal" >
        Destination City :<br/>
        <select class="form-control destination chosen" >
          <option value="0">- Choose City -</option>
          <?php
            foreach ($city as $c => $i) {
              echo '<option value="'.$i['city_id'].'">'.$i['type'].' '.$i['city_name'].'</option>';
            }
          ?>
          
        </select>
        Courier :<br/>
        <select class="custom-select custom-select-sm sort-ac courier" disabled>
          <option value="0">- Choose Courier -</option>
          <option value="pos">Pos Indonesia</option>
          <option value="jne">JNE</option>
        </select>
        Service :<br/>
        <select class="custom-select custom-select-sm sort-ac service" disabled>
          <option value="">- Choose Service -</option>
        </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary chooseDest" >Choose</button>
      </div>
    </div>
  </div>
</div>

<?= js_assets('home/cart.js','f'); ?>