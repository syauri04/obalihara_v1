<body>
  <section>
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center centerin">
                <h3>Wishlist</h3>
            </div>
        </div>
    </div>
 </section>
<section class="mt-5">
    <div class="container">
        <div class="head-title">
            <h5>Your Whislist Items</h5>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th></th>
                    <th>Product Name</th>
                    <th>Price</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    foreach ($getWishlist as $k => $v) {
                ?>
                <tr>
                    <td><a href="#" class="delWish" data-id="<?= $v->user_id?>" data-val="<?= $v->barang_id?>"><i class="fa fa-trash delete"></i></a></td>
                    <td><img src="<?= produk::gambar($v->barang_id,'thumb');?>" width="50" height="50"> <a class="title-product" href="<?= base_url().'detail/pd?id='.urlencode(codeEncrypt($v->barang_id)).'&&'.'title='.url_title($v->nama_barang);?>"><?= $v->nama_barang?></a></td>
                    <td><span><?= myNum($v->harga_satuan,'Rp.') ?></span></td>
                    <td><span class="text-success"><?= ($v->stok > 0)?'In Stock':'Out of Stock';?></span></td>
                    <!-- <td><a href="<?=base_url().'cart';?>"><button class="btn btn-shop"><i class="fa fa-shopping-cart"></i> Checkout</button></a></td> -->
                </tr>
                <?php
                    }
                ?>
            </tbody>
        </table>
    </div>
</section>
 <br>
 <section class="mt-3">
     <div class="container">
         <a class="back" href="<?= base_url()?>"><i class="fa fa-angle-left"></i> Back to Shopping</a>
     </div>
 </section>
</body>

<?= js_assets('home/wishlist.js','f'); ?>