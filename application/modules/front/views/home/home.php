
  <body>
    <section>
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <div id="slide" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              <!-- <ul class="carousel-indicators">
                <li data-target="#demo" data-slide-to="0" class="active"></li>
                <li data-target="#demo" data-slide-to="1"></li>
                <li data-target="#demo" data-slide-to="2"></li>
              </ul> -->

              <!-- The slideshow -->
                <div class="carousel-inner">
                  <?php
                    foreach ($bannerSlide as $k => $v) {
                  ?>
                  <div class="carousel-item <?= ($k == 0)? 'active':'' ?>">
                    <a href='<?=($v->url != null)?$v->url:'#'; ?>'>
                    <div class="back-slider" style="background:url('assets/img/banner/large/<?= $v->image?>') no-repeat;  background-position:top center">
                      <!--<div class="title-cat">-->
                      <!--  <span>Limited Edition</span>-->
                      <!--</div>-->
                      <!--<h2>Korea Long Sofa Fabric In Blue Navy Color</h2>-->
                      <!--<h3>$100</h3>-->
                      <!--<button type="button" class="btn btn-shop" name="button">Shop Now</button>-->
                    </div>
                    </a>
                  </div>
                <?php } ?>
                <!-- Left and right controls -->
                <a class="carousel-control-prev" href="#slide" data-slide="prev">
                  <div class="control-slide">
                    <i class="fa fa-angle-left"></i>
                  </div>
                </a>
                <a class="carousel-control-next" href="#slide" data-slide="next">
                  <div class="control-slide">
                    <i class="fa fa-angle-right"></i>
                  </div>
                </a>

              </div>
            </div>
          </div>
          <div class="col-md-4">
            <?php
              if(count($bannerSide)){
                foreach ($bannerSide as $k => $v) {

            ?>
            <a href='<?=($v->url != null)?$v->url:'#'; ?>'>
              <div class="feat-product" style="background:url('assets/img/banner/small/<?= $v->image ?>') no-repeat; background-position:top center;background-size: cover;">

              </div>
            </a>
            <?php 
                }
              }else{
            ?>
               <div class="feat-product" style="background:url('assets/noimage.png') no-repeat; background-position:top center;background-size: cover;">

               </div>
                <div class="feat-product" style="background:url('assets/noimage.png') no-repeat; background-position:top center;background-size: cover;">

               </div>
            <?php
              }
            ?>
          </div>
        </div>
    </section>
    <section id="information">
      <div class="container">
        <ul>
          <li>
            <table>
              <tr>
                <td rowspan="2"><i class="fa fa-rocket"></i></td>
                <td><span>Free Delivery</span></td>
              </tr>
              <tr>
                <td><p>For all order from $99</p></td>
              </tr>
            </table>
          </li>
          <li>
            <table>
              <tr>
                <td rowspan="2"><i class="fa fa-retweet"></i></td>
                <td><span>Free Delivery</span></td>
              </tr>
              <tr>
                <td><p>Free return in 90 days</p></td>
              </tr>
            </table>
          </li>
          <li>
            <table>
              <tr>
                <td rowspan="2"><i class="fa fa-credit-card"></i></td>
                <td><span>Secure Payments</span></td>
              </tr>
              <tr>
                <td><p>100% Secure payments</p></td>
              </tr>
            </table>
          </li>
          <li>
            <table>
              <tr>
                <td rowspan="2"><i class="fa fa-headphones"></i></td>
                <td><span>24/7 Support</span></td>
              </tr>
              <tr>
                <td><p>Dedicated support in 24h</p></td>
              </tr>
            </table>
          </li>
          <li class="last">
            <table>
              <tr>
                <td rowspan="2"><i class="fa fa-gift"></i></td>
                <td><span>Special gift</span></td>
              </tr>
              <tr>
                <td><p>Free special gift</p></td>
              </tr>
            </table>
          </li>
        </ul>
        <div class="clearfix">

        </div>
      </div>
    </section>
    <?php
      if (count($getProductDof) > 0 ) {
    ?>
    <section id="deals-section">
      <div class="container">
        <div class="head-title">
          <h5>Deals of The Day</h5> <span class="btn btn-sale" name="button">Sales up to <?= produk::maxDiscount().'%'?></span>
        </div>
        <div class="">
            <div id="deal" class="carousel slide" data-ride="carousel" data-interval="0">
                <div class="carousel-inner">
                    <div class="item carousel-item active">
                        <div class="row">
                          <?php
                            foreach ($getProductDof as $key => $value) {

                              if($value->gambar_name != ""){
                                $getimg = "assets/img/goods/small/".$value->gambar_name;
                              }else{
                                $getimg = "assets/img/no_image.jpg";
                              }

                              $w = produk::wishlist($value->barang_id,$this->jCfg['client']['id']);
                              $c = produk::cart($value->barang_id,$this->jCfg['client']['id']);
                              // d($c);
                          ?>

                            <div class="col-sm-2">
                                <div class="">
                                 <!--  <div class="disc-tem">
                                    50%
                                  </div> -->
                                  <div class="hovereffect">
                                      <a href="<?= base_url().'detail/pd?id='.urlencode(codeEncrypt($value->barang_id)).'&&'.'title='.url_title($value->nama_barang);?>"><div class="img-responsive small-b" style="background:url('<?=base_url().$getimg?>')center center / cover no-repeat "  alt=""></div></a>
                                          <div class="overlay">
                                              <p class="icon-links">
                                                  <?php 
                                                    if (isset($this->jCfg['client']['is_login']) && $this->jCfg['client']['is_login'] == 1) { 
                                                      $login = '#';
                                                    } else {
                                                      $login = base_url().'login';
                                                    }

                                                    if($w > 0){
                                                      echo '<a href="'.$login.'" class="wishlistButton" data-id="'.$value->barang_id.'" ><span class="fa fa-heart" style="color:#c1355e"></span></a>';
                                                    } else {
                                                      echo '<a href="'.$login.'" class="wishlistButton" data-id="'.$value->barang_id.'" ><span class="fa fa-heart-o"></span></a>';
                                                    }

                                                    // if($c > 0){
                                                    //   echo '<a href="'.$login.'" class="cartButton" data-id="'.$value->barang_id.'"><span class="fa fa-shopping-cart" style="color:#c1355e"></span></a>';
                                                    // } else {
                                                    //   echo '<a href="'.$login.'" class="cartButton" data-id="'.$value->barang_id.'" data-vendor="'.$this->jCfg['client']['id'].'"><span class="fa fa-shopping-cart"></span></a>';
                                                    // }

                                                  ?>
                                                  
                                              </p>
                                          </div>
                                  </div>
                                  <div class="det-prod">
                                    <div class="">
                                      <!-- <span class="disc-price">Rp. 75.000</span> --> 
                                      <span class="true-price"><?= myNum($value->harga_satuan,'Rp');?></span>
                                    </div>
                                    <div class="">
                                       <a href="<?= base_url().'detail/pd?id='.urlencode(codeEncrypt($value->barang_id)).'&&'.'title='.url_title($value->nama_barang);?>"><?=$value->nama_barang?></a>
                                    </div>
                                    <div class="">
                                      <!-- <p>Sold by: Game lorem</p> -->
                                    </div>
                                    <div class="star">
                                      <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                                    </div>
                                  </div>
                                  <div class="clearfix"></div>
                                </div>
                            </div>
                          <?php
                            }
                          ?>
                        </div>

                    </div>
                    <div class="item carousel-item">
                        <div class="row">
                          <?php
                          if (count($getProductC) > 0 ) {
                            foreach ($getProductC as $key => $value) {
                              if($value->gambar_name != ""){
                                $getimg = "assets/img/goods/small/".$value->gambar_name;
                              }else{
                                $getimg = "assets/img/no_image.jpg";
                              }

                              $w = produk::wishlist($value->barang_id,$this->jCfg['client']['id']);

                              $c = produk::cart($value->barang_id,$this->jCfg['client']['id']);

                          ?>
                              <div class="col-sm-2">
                                <div class="">
                                 <!--  <div class="disc-tem">
                                    50%
                                  </div> -->
                                  <div class="hovereffect">
                                      <a href="<?= base_url().'detail/pd?id='.urlencode(codeEncrypt($value->barang_id)).'&&'.'title='.url_title($value->nama_barang);?>"><div class="img-responsive small-b" style="background:url('<?=base_url().$getimg?>')center center / cover no-repeat "  alt=""></div></a>
                                          <div class="overlay">
                                              <p class="icon-links">
                                                  <?php 
                                                    if (isset($this->jCfg['client']['is_login']) && $this->jCfg['client']['is_login'] == 1) { 
                                                      $login = '#';
                                                    } else {
                                                      $login = base_url().'login';
                                                    }

                                                    if($w > 0){
                                                      echo '<a href="'.$login.'"><span class="fa fa-heart" style="color:#c1355e"></span></a>';
                                                    } else {
                                                      echo '<a href="'.$login.'"><span class="fa fa-heart-o"></span></a>';
                                                    }

                                                    // if($c > 0){
                                                    //   echo '<a href="'.$login.'"><span class="fa fa-shopping-cart" style="color:#c1355e"></span></a>';
                                                    // } else {
                                                    //   echo '<a href="'.$login.'"><span class="fa fa-shopping-cart"></span></a>';
                                                    // }

                                                  ?>
                                                  
                                              </p>
                                          </div>
                                  </div>
                                  <div class="det-prod">
                                    <div class="">
                                      <!-- <span class="disc-price">Rp. 75.000</span>  -->
                                      <span class="true-price"><?= myNum($value->harga_satuan,'Rp');?></span>
                                    </div>
                                    <div class="">
                                       <a href="<?= base_url().'detail/pd?id='.urlencode(codeEncrypt($value->barang_id)).'&&'.'title='.url_title($value->nama_barang);?>"><?=$value->nama_barang?></a>
                                    </div>
                                    <div class="">
                                      <!-- <p>Sold by: Game lorem</p> -->
                                    </div>
                                    <div class="star">
                                      <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                                    </div>
                                  </div>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                          <?php
                            }
                          }
                          ?> 
                           
                        </div>

                    </div>
                </div>
                <!-- Carousel controls -->
                <a class="carousel-control-prev" href="#deal" data-slide="prev">
                  <div class="control-slide">
                    <i class="fa fa-angle-left"></i>
                  </div>
                </a>
                <a class="carousel-control-next" href="#deal" data-slide="next">
                  <div class="control-slide">
                    <i class="fa fa-angle-right"></i>
                  </div>
                </a>
            </div>
        </div>
      </div>
    </section>
    <?php
      }
    ?>
    <section id="a-thousand">
      <div class="container">
        <?php

          if(!empty($bannerMid)){

            echo '<a href="'.$bannerMid[0]->url.'"><img src="assets/img/banner/large/'.$bannerMid[0]->image.'" width="100%"/></a>';

          }

        ?>        
      </div>
    </section>
    <section id="uno">
      <div class="container">
        <div class="row">
          <?php

            if(!empty($bannerMidSmall)){
              foreach ($bannerMidSmall as $b => $s) {

          ?>
          <div class="col-md-4">
            <a href="<?= $s->url?>" target="blank_">
              <div class="feat-product" style="background:url('assets/img/banner/large/<?= $s->image?>') no-repeat; background-position:top center;background-size: cover;">
              <!-- <div class="row">
                <div class="col-6">
                    <h4>unio leather bags</h4>
                    <p>100% lorem ipsum</p>
                </div>
                <div class="col-6">
                  <div class="circle-litle-disc">
                  </div>
                  <div class="circle-big-disc">
                    <span>20%</span>
                    <p>Off</p>
                  </div>
                </div>
              </div> -->
              </div>
            </a>
          </div>

          <?php
              }

            }

          ?>
          
        </div>
      </div>
    </section>
    <?php
      if(count($walhiMart) > 0){
    ?>
    <section>
      <div class="container">
        <div class="head-title">
          <h5>Walhi Mart</h5>
        </div>
        <div class="">
          <div class="row">

            <?php
              foreach ($walhiMart as $w => $m) {
                
                if(isset($value) && $value->gambar_name != ""){
                  $getimg = "assets/img/goods/thumb/".$value->gambar_name;
                }else{
                  $getimg = "assets/img/no_image.jpg";
                }

            ?>

            <div class="col-md-2">
              <a href="<?= base_url().'detail/pd?id='.urlencode(codeEncrypt($m->id)).'&&'.'title='.url_title($m->nama_barang);?>"> <div class="img-responsive small-b" style="background:url('<?= produk::gambar($m->id,'small')?>')center center / cover no-repeat "  alt=""></div>
              <div class="title-categories">
                <a href="<?= base_url().'detail/pd?id='.urlencode(codeEncrypt($m->id)).'&&'.'title='.url_title($m->nama_barang);?>"><?= $m->nama_barang ?></a>
              </div>
            </div>

            <!-- <div class="col-md-2">
              <a href="<?= base_url().'category/c?cid='.$v->id.'&&ct='.url_title($v->name)?>"> <div class="img-responsive small-b" style="background:url('<?=produk::gambarCOM($v->id,'small')?>')center center / cover no-repeat "  alt=""></div>
              <div class="title-categories">
                <a href="<?= base_url().'category/c?cid='.$v->id.'&&ct='.url_title($v->name)?>"><?= $v->name ?></a>
              </div>
            </div> -->
            
            <?php
              }
            ?>

          </div>
        </div>
      </div>
    </section>
    <?php } ?>
    <?php
      if(count($getProduct) > 0){
    ?>
    <section id="new-arival">
      <div class="container">
        <div class="head-background">
          <h5>New Arrivals</h5> <a class="view-all" href="<?=base_url().'category/c?viewall=New Arrivals'?>">View all</a>
        </div>
        <div class="row">
          <?php
          if (count($getProduct) > 0 ) {
            // d($getProduct);
            foreach ($getProduct as $key => $value) {

              if($value->gambar_name != ""){
                $getimg = "assets/img/goods/thumb/".$value->gambar_name;
              }else{
                $getimg = "assets/img/no_image.jpg";
              }
          ?>
            <div class="col-md-4 pbNA">
              <div class="row">
                <div class="col-md-4">
                    <a href="<?= base_url().'detail/pd?id='.urlencode(codeEncrypt($value->barang_id)).'&&'.'title='.url_title($value->nama_barang);?>"><img class="img-responsive" src="<?=base_url().$getimg?>" width="100" alt=""></a>
                </div>
                <div class="col-md-8">
                  <div class="det-prod">
                    <div class="">
                      <a href="<?= base_url().'detail/pd?id='.urlencode(codeEncrypt($value->barang_id)).'&&'.'title='.url_title($value->nama_barang);?>"><?=$value->nama_barang?></a>
                    </div>
                    <div class="star">
                      <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                    </div>
                    <div class="">
                      <!-- <span class="disc-price">Rp. 75.000</span>--> 
                      <span class="true-price"><?= myNum($value->harga_satuan,'Rp');?></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php
            }
          }
          ?>
          
          
        </div>
        
      </div>
    </section>
    <?php } ?>
    <?php
      if(count($getProductOr) > 0){
    ?>
    <section id="makanan-section">
      <div class="container">
        <div class="head-background">
          <h5>Our Recomendation</h5> <a class="view-all" href="<?=base_url().'category/c?viewall=Rekomendasi Kami'?>">View all</a>
        </div>
        <div id="makanan" class="carousel slide" data-ride="carousel" data-interval="0">
          <div class="carousel-inner">
            <div class="row">
              <?php
              if (count($getProductOr) > 0 ) {

                foreach ($getProductOr as $key => $value) {

                  if($value->gambar_name != ""){
                    $getimg = "assets/img/goods/small/".$value->gambar_name;
                  }else{
                    $getimg = "assets/img/no_image.jpg";
                  }

                  $w = produk::wishlist($value->barang_id,$this->jCfg['client']['id']);
                  $c = produk::cart($value->barang_id,$this->jCfg['client']['id']);
                  
              ?>

                <div class="col-sm-2">
                    <div class="">
                     <!--  <div class="disc-tem">
                        50%
                      </div> -->
                      <div class="hovereffect">
                          <a href="<?= base_url().'detail/pd?id='.urlencode(codeEncrypt($value->barang_id)).'&&'.'title='.url_title($value->nama_barang);?>"><div class="img-responsive small-b" style="background:url('<?=base_url().$getimg?>')center center / cover no-repeat "  alt=""></div></a>
                              <div class="overlay">
                                  <p class="icon-links">
                                    <?php

                                      if (isset($this->jCfg['client']['is_login']) && $this->jCfg['client']['is_login'] == 1) { 
                                        $login = '#';
                                      } else {
                                        $login = base_url().'login';
                                      }

                                      if($w > 0){
                                        echo '<a href="'.$login.'" class="wishlistButton" data-id="'.$value->barang_id.'" ><span class="fa fa-heart" style="color:#c1355e"></span></a>';
                                      } else {
                                        echo '<a href="'.$login.'" class="wishlistButton" data-id="'.$value->barang_id.'" ><span class="fa fa-heart-o"></span></a>';
                                      }

                                      // if($c > 0){
                                      //   echo '<a href="'.$login.'" class="cartButton" data-id="'.$value->barang_id.'"><span class="fa fa-shopping-cart" style="color:#c1355e"></span></a>';
                                      // } else {
                                      //   echo '<a href="'.$login.'" class="cartButton" data-id="'.$value->barang_id.'" data-vendor="'.$this->jCfg['client']['id'].'"><span class="fa fa-shopping-cart"></span></a>';
                                      // }
                                      
                                    ?>
                                  </p>
                              </div>
                      </div>
                      <div class="det-prod">
                        <div class="">
                          <!-- <span class="disc-price">Rp. 75.000</span> --> 
                          <span class="true-price"><?= myNum($value->harga_satuan,'Rp');?></span>
                        </div>
                        <div class="">
                           <a href="<?= base_url().'detail/pd?id='.urlencode(codeEncrypt($value->barang_id)).'&&'.'title='.url_title($value->nama_barang);?>"><?=$value->nama_barang?></a>
                        </div>
                        <div class="">
                          <!-- <p>Sold by: Game lorem</p> -->
                        </div>
                        <div class="star">
                          <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                </div>
              <?php
                }
              }
              ?>
            </div>
            
          </div>

        </div>
      </div>
    </section>
    <?php } ?>
  </body>

<?= js_assets('home/home.js','f'); ?>