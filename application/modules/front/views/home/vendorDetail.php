<body>
 <section>
     <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?php //d($getVendor);?>
                <img class="img-responsive w-100" src="<?= get_image(base_url()."assets/img/vendor/small/".$getVendor->photo)?>" alt="">
                <div class="card card-2">
                    <h5><?= $getVendor->nama_lengkap?></h5>
                    <div class="star">
                        <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                            class="fa fa-star"></i> <i class="fa fa-star null"></i>
                    </div>
                     <span style="margin-top:0px;font-size: 11px;margin-top:5px;" class="text-success">80% Positive Feedback</span> <span style="font-size:11px;" class="text-grey">(278 Reviews)</span>
                <hr>
                <p><?= $getVendor->deskripsi;?></p>
                <!-- <p>Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                    culpa qui officia deserunt mollit anim id est laborum.</p> -->
                    <hr>
                    <p><b>Address</b>: <?= $getVendor->alamat?></p>
                    <p><b>Contact</b>: <?= $getVendor->instagram?></p>
                    <p style="font-size:15px; font-weight: 500"><?= $getVendor->telp?></p>
                    <!-- <img src="assets/img/social.png" width="70%" alt=""> -->
                    <hr>
                    <button class="btn btn-contact"> <i class="fa fa-headphones"></i> contact Vendor</button>
                </div>
            </div>
            <div class="col-md-9">
              <div class="head-background">
                <ul class="nav nav-tabs">
                  <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#product">
                      <h6>Product</h6>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#review">
                      <h6>Review</h6>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#about">
                      <h6>Orders</h6>
                    </a>
                  </li>
                </ul>
              </div>
              <div class="tab-content mt-5">
                <div class="tab-pane active" id="product">
                  <section id="">
                    <div>
                      <div class="pull-right ">
                        <form>
                          <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text"><i class="fa fa-search"></i></span>
                            </div>
                            <input type="text" class="form-control text-custom" placeholder="Cari nama barang">
                          </div>
                        </form>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </section>
                  <section>
                    <div class="head-title">
                      <h5>Best Seller Product</h5>
                    </div>
                    <div class="row">
                      <?php foreach($getMostProduct as $m => $p){?>
                      <div class="col-sm-3">
                        <div class="">
                          <?= ($p->disc == 0)?'': '<div class="disc-tem">'.$p->disc."%".'</div>';?>
                          <div class="hovereffect">
                            <a href="<?= base_url().'detail/pd?id='.urlencode(codeEncrypt($p->id)).'&&'.'title='.url_title($p->nama_barang);?>  "><div class="img-responsive small-b" style="background:url('<?= produk::gambar($p->id,'small')?>')center center / cover no-repeat "  alt=""></div></a>
                            <div class="overlay">
                              <p class="icon-links">
                                <a href="<?= (isset($this->jCfg['client']['is_login']) && $this->jCfg['client']['is_login'] == 1)?'#':base_url().'login'?>">
                                  <span class="fa fa-heart-o"></span>
                                </a>
                              </p>
                            </div>
                          </div>
                          <div class="det-prod det-prod-2">
                            <div class="">
                              <a href="<?= base_url().'detail/pd?id='.urlencode(codeEncrypt($p->id)).'&&'.'title='.url_title($p->nama_barang);?>"><?= $p->nama_barang?></a>
                            </div>
                            <div class="star">
                              <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                                style="margin-left:10px">8</span>
                            </div>
                            <div class="">
                              <!-- <span class="disc-price">Rp. 75.000</span> --> <span class="true-price"><?= myNum(produk::detail($p->id,'harga_satuan'),'Rp. ')?></span>
                            </div>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      <?php }?>
                    </div>
                  </section>
                  <section id="product-body">
                    <div class="head-title">
                      <h5><?= count($getProduct)?> Products In Stock</h5>
                      <!-- <div class="pull-right sort-order">
                        <ul>
                          <li class="mr-3">
                            <span class="sort-ac"> Sort by:</span> <select
                              class="custom-select custom-select-sm sort-ac">
                              <option selected="">Ascanding</option>
                              <option value="1">Descanding</option>
                            </select>
                            <div class="clearfix"></div>
                          </li>
                          <li>
                            <span class="sort-ac"> View by:</span> <i class="fa fa-th sort-ac"></i> <i
                              class="fa fa-align-justify sort-ac"></i>
                            <div class="clearfix"></div>
                          </li>

                        </ul>
                      </div> -->
                    </div>
                    <div class="row">
                      <?php foreach($getProduct as $g => $p){?>
                      <div class="col-sm-3">
                        <div class="">
                          <?= ($p->disc == 0)?'': '<div class="disc-tem">'.$p->disc."%".'</div>';?>
                          <div class="hovereffect">
                           <a href="<?= base_url().'detail/pd?id='.urlencode(codeEncrypt($p->id)).'&&'.'title='.url_title($p->nama_barang);?>  "><div class="img-responsive small-b" style="background:url('<?= produk::gambar($p->id,'small')?>')center center / cover no-repeat "  alt=""></div></a>
                            <div class="overlay">
                              <p class="icon-links">
                                <a href="<?= (isset($this->jCfg['client']['is_login']) && $this->jCfg['client']['is_login'] == 1)?'#':base_url().'login'?>">
                                  <span class="fa fa-heart-o"></span>
                                </a>
                                <!-- <a href="#">
                                  <span class="fa fa-shopping-cart"></span>
                                </a> -->
                              </p>
                            </div>
                          </div>
                          <div class="det-prod det-prod-2">
                            <div class="">
                              <a href="<?= base_url().'detail/pd?id='.urlencode(codeEncrypt($p->id)).'&&'.'title='.url_title($p->nama_barang);?>"><?= $p->nama_barang?></a>
                            </div>
                            <div class="star">
                              <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                                style="margin-left:10px">8</span>
                            </div>
                            <div class="">
                              <!-- <span class="disc-price">Rp. 75.000</span> --> <span class="true-price"><?= myNum(produk::detail($p->id,'harga_satuan'),'Rp. ')?></span>
                            </div>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      <?php }?>
                    </div>
                  </section>
                  <!-- <section id="sec-pagination" class="text-center">
                    <ul>
                      <li>
                        <a href=""><i class="fa fa-angle-left"></i> Previous page</a>
                      </li>
                      <li class="current">1</li>
                      <li><a href="">2</a></li>
                      <li><a href="">3</a></li>
                      <li><a href="">4</a></li>
                      <li><a href="">5</a></li>
                      <li>...</li>
                      <li><a href="">100</a></li>
                      <li>
                        <a href="">Next page <i class="fa fa-angle-right"></i></a>
                      </li>
                    </ul>
                  </section> -->
                </div>
                <div class="tab-pane" id="review">
                </div>
                <div class="tab-pane" id="about">
                </div>
              </div>
            </div>
        </div>
     </div>
 </section>
</body>