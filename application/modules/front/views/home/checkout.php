<body>
    <section>
    <div class="container">
      <div id="information" class="text-center centerin">
        <div class="mb-2">
          <h3>Checkout Orders</h3>
        </div>
      </div>
    </section>
    <section class="mt-5">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div class="head-title">
            <h5>Shipping Address</h5>
          </div>
          <div class="card mb-5 cardAddress">
            <?php

              if(!empty($shipping)){
                $raongDet = raongCity($shipping[0]->kota_id);
            ?>
            <div style="width:30%;">
              <h6><?= $shipping[0]->title?></h6>
              <div><span><b><?= $shipping[0]->nama_lengkap?></b></span></div>
              <div><span><?= $shipping[0]->alamat?>.</span></div>
              <div><span><?= $raongDet['province'];?></span> <br/> <span><?= $raongDet['type'].' '.$raongDet['city_name']?></span> , <span><?= $raongDet['postal_code'];?></span> </div>
              <div class="mt-2"><span><?= $shipping[0]->telp;?></span></div>
            </div>
            <hr>
            <input type="hidden" class="userId" value="<?= $shipping[0]->buyer_id?>"/>
            <input type="hidden" class="addressId" value="<?= $shipping[0]->id?>"/>
            <a href="#" data-toggle="modal" data-target="#addressModal" data-val="rowVendor'.$k.'"><b>Ganti Alamat Pengirim</b></a>
          <?php 
              } else {
                echo 'Tidak Ada Alamat Pengiriman , Silahkan isi alamat terlebih dahulu di menu dashboard';
              }
              
          ?>
          </div>
          <div class="head-title">
            <h5>Order Details</h5>
          </div>
          <form action="<?= base_url().'front/home/saveCheckout'?>" enctype="multipart/form-data" method="post" id="coForm">
            <table class="table">
              <?php
             
                $no = 1;
                foreach ($cartId as $k => $v) {
                  echo '<tr><td colspan="6" class="text-left"><i class="fa fa-shopping-cart"></i><b> '.getOutlet($k,'nama_lengkap').'</b></td></tr>';
                  
                  foreach ($v as $cItem => $vItem) {
                    // d($vItem);
              ?>
              <tr id="rowVendor<?= $k; ?>" class="rowVendor<?= $k; ?>">
                <input type="hidden" class="weight" value="<?= (produk::detail($vItem->barang_id,'perkiraan_berat') * $vItem->qtyId)?>">
                <input type="hidden" class="originDest" value="<?= getOutlet($vItem->vendor_id,'kota_id')?>">
                <input type="hidden" name="qty[]" class="qty" value="<?= $vItem->qtyId?>">
                <!-- <input type="hidden" class="cartid[]" value="<?= $vItem->id; ?>"> -->
                <input type="hidden" name="cartID[]" value="<?= $vItem->id; ?>" >
                <input type="hidden" name="vendorID[]" value="<?= $vItem->vendor_id; ?>" >
                <input type="hidden" name="courier[]" id="courierCO" >
                <input type="hidden" name="courierService[]" id="courierServiceCO" >
                <input type="hidden" name="courierPrice[]" id="courierPriceCO" >
                <td>
                  <div class="row">
                    <div class="col-md-3">
                      <img src="<?= produk::gambar($vItem->barang_id,'small')?>" width="80" height="80" alt="">
                    </div>
                    <div class="col-md-7">
                      <div>
                        <a class="title-product" href="<?= base_url().'detail/pd?id='.urlencode(codeEncrypt($vItem->barang_id)).'&&'.'title='.url_title(produk::goods($vItem->barang_id,'nama_barang'));?>"><?= produk::goods($vItem->barang_id,'nama_barang')?></a>
                      </div>
                      <div>
                        <span>Color : <b><?= produk::warna($vItem->warna_id,'value')?></b></span>
                      </div>
                      <div>
                        <span><b><?= $vItem->qtyId ?> x <?= $vItem->ttlItem ?> = <?= myNum($vItem->ttlItem,'Rp. ')?></b></span>
                      </div>
                    </div>
                  </div>
                </td>
                <td>
                  <div>
                    <b>Expedition</b>
                  </div>
                  <div class="courierCity">-</div>
                  <div class="courierName">-</div>
                  <div class="courierDesc">-</div>
                  <div><span class="text-pink"><b class="courierCost">-</b></span> <span class="courierEtd">-</span></div>
                </td>
                <td>
                  <div><b>Total</b></div>
                  <div><span class="text-pink subTtl_item"><b><?= myNum($vItem->ttlItem,'Rp. ')?></b> </span> </div>
                  <input type="hidden" name="total[]" value="<?= $vItem->ttlItem;?>" >
                </td>
              </tr>
              <?php 
                  }
                ?>
                  
                <?php
                  echo '<tr id="rowVendor'.$k.'">';
                  // echo '<input type="hidden" name="cartID[]" id="cartID" >';
                  // echo '<input type="hidden" name="vendorID[]" id="vendorID" value="'.$k.'" >';
                  // echo '<input type="hidden" name="total[]" id="totalCO" >';
                  // echo '<input type="hidden" name="courier[]" id="courierCO" >';
                  // echo '<input type="hidden" name="courierService[]" id="courierServiceCO" >';
                  // echo '<input type="hidden" name="courierPrice[]" id="courierPriceCO" >';
                  echo '<td colspan="2" class="text-left"><a href="#" data-toggle="modal" data-target="#exampleModalCenter" data-val="rowVendor'.$k.'" data-cost="0" class="btn btn-shop ces">Choose Expedition Service</a> </td><td>Sub Total : <i class="text-pink subTotalVendor">Rp. 0</i></td>';
                  echo '</tr>';
                } 
              ?>
            </table>
            <input type="hidden" name="invoiceCode" id="invoiceCode" >
            <input type="hidden" name="addressID" id="addressID" >
            <input type="hidden" name="userID" id="userID" >
            <input type="hidden" name="total_product_rate" id="total_product_rateCO" >
            <input type="hidden" name="total_expedition_rate" id="total_expedition_rateCO" >
            <input type="hidden" name="sub_total" id="sub_totalCO" >
            <input type="hidden" name="discount" id="discountCO" >
            <input type="hidden" name="grand_total" id="grand_totalCO" >
          </form>
        </div>
        <div class="col-md-4">
          <div class="card">
            <div class="head-title">
              <h5>Billing Details</h5>
            </div>
            <table class="w-100 bill">
              <tr>
                <td width="50%">Total Product Rate:</td>
                <td width="50%" class="text-right" id="totalProductPrice"><b>Rp. 0</b></td>
              </tr>
              <tr>
                <td width="50%">Total Expedition Rate:</td>
                <td width="50%" class="text-right" id="totalExpeditionPrice"><b>Rp. 0</b></td>
              </tr>
            </table>
            <hr>
            <!-- <div class="voucher-box"></div> -->
            <!-- <input type="text" name="coupon" class="form-control coupon"> -->
            <div class="input-group mb-3">
              <input type="text" class="form-control coupon-text" placeholder="Coupon Code" aria-label="Coupon Code" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-outline-secondary coupon-btn" type="button">Check</button>
              </div>
            </div>
            <hr>
            <table class="w-100 bill">
              <tr>
                <td width="50%"><b>SUB TOTAL</b></td>
                <td width="50%" class="text-right" id="subGrandTotal"><b>Rp. 0</b></td>
              </tr>
              <tr style="display: none;" class="couponel">
                <td width="50%"><span class="text-success discVoucherP"><b>Discount</b></span></td>
                <td width="50%" class="text-right"><span class="text-success discVoucherV"><b>- Rp. 0</b></span></td>
              </tr>
              <tr>
                <td width="50%"><b>TOTAL</b></td>
                <td width="50%" class="text-right">
                  <h6 class="text-pink grandTotal">Rp. 0</h6>
                </td>
              </tr>
            </table>
          </div>
           <div>
             <button class="btn btn-shop w-100 paymentMethod"><i class="fa fa-credit-card"></i> Payment Method</button>
           </div>
        </div>
      </div>
    </div>
    </section>
    <br>
    <section class="mt-3">
    <div class="container">
      <a class="back" href="<?=base_url()?>"><i class="fa fa-angle-left"></i> Back to Shopping</a>
    </div>
    </section>
</body>

<!-- <div style="display: none;">
  <form>
    <input type="hidden" name="total[]" id="total">
    <input type="hidden" name="courier[]" id="courier">
    <input type="hidden" name="courierService[]" id="courierService">
    <input type="hidden" name="courierPrice[]" id="courierPrice">
  </form>
</div> -->

<!-- Modal -->
<div class="modal fade" id="addressModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Choose Address</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php
          foreach ($shipping as $s => $g) {
            $raongDetM = raongCity($g->kota_id);
        ?>
        <div class="card">
          <!-- <input type="hidden" id="addressId" value="<?= $g->id;?>" > -->
          <div>
            <h6><?= $g->title?></h6>
            <div>
              <span><b><?= $g->nama_lengkap?></b></span>
            </div>
            <div>
              <span><?= $g->alamat?></span>
            </div>
            <div class="mt-2"><span><?= $g->telp;?></span> <span><?= $raongDetM['province'];?></span> , <span><?= $raongDetM['type'].' '.$raongDetM['city_name'];?></span> , <span><?= $raongDetM['postal_code'];?></span> </div>
          </div>
          <hr>
          <a href="#" 
          data-userid="<?=$this->jCfg['client']['id'];?>"
          data-addressId="<?= $g->id;?>" 
          data-title="<?= $g->title;?>"
          data-nama_lengkap="<?= $g->nama_lengkap;?>"
          data-alamat="<?= $g->alamat;?>"
          data-telp="<?= $g->telp;?>"
          data-province="<?= $raongDetM['province'];?>"
          data-city="<?= $raongDetM['type'].' '.$raongDetM['city_name'];?>"
          data-postal_code="<?= $raongDetM['postal_code'];?>"
          class="btn btn-shop cAddress"><b>Pilih Alamat</b></a>
        </div>
        <?php
          }
        ?>
        <!-- <input type="hidden" id="colVal" >
        Destination City :<br/>
        <select class="form-control destination chosen" >
          <option value="0">- Choose City -</option>
          <?php
            foreach ($city as $c => $i) {
              echo '<option value="'.$i['city_id'].'">'.$i['type'].' '.$i['city_name'].'</option>';
            }
          ?>
          
        </select>
        Courier :<br/>
        <select class="custom-select custom-select-sm sort-ac courier" disabled>
          <option value="0">- Choose Courier -</option>
          <option value="pos">Pos Indonesia</option>
          <option value="jne">JNE</option>
        </select>
        Service :<br/>
        <select class="custom-select custom-select-sm sort-ac service" disabled>
          <option value="0">- Choose Service -</option>
        </select> -->
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary chooseDest" >Choose</button>
      </div> -->
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Choose Expedition Service</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" id="colVal" >
        Destination City :<br/>
        <select class="form-control destination chosen" >
          <option value="0">- Choose City -</option>
          <?php
            foreach ($city as $c => $i) {
              echo '<option value="'.$i['city_id'].'">'.$i['type'].' '.$i['city_name'].'</option>';
            }
          ?>
          
        </select>
        Courier :<br/>
        <select class="custom-select custom-select-sm sort-ac courier" disabled>
          <option value="0">- Choose Courier -</option>
          <option value="pos">Pos Indonesia</option>
          <option value="jne">JNE</option>
        </select>
        Service :<br/>
        <select class="custom-select custom-select-sm sort-ac service" disabled>
          <option value="0">- Choose Service -</option>
        </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary chooseDest" >Choose</button>
      </div>
    </div>
  </div>
</div>



<?= js_assets('home/checkout.js','f'); ?>