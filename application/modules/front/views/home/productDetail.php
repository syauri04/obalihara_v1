<body>
  <section>
    <div class="container">
      <div class="row">
        <div class="col-md-9">
          <div class="card">
            <div class="row">
              <div class="col-md-6">
                <div class="prev-product">

                  <div class="left-prev-product">
                    <ul class="thumbnails">
                      <?php
                        foreach ($produk['gambar'] as $g => $p) {
                          if($g < 7){
                      ?>
                      <li>
                        <a href="<?= get_image(base_url()."assets/img/goods/large/".$p->gambar_name) ?>" data-standard="<?= get_image(base_url()."assets/img/goods/large/".$p->gambar_name) ?>">
                          <img src="<?= get_image(base_url()."assets/img/goods/thumb/".$p->gambar_name) ?>" alt="" width="50" height="50"/>
                        </a>
                      </li>
                      <?php } }?>
                    </ul>
                  </div>
                  <div class="right-prev-product">

                    <div class="easyzoom easyzoom--overlay easyzoom--with-thumbnails">
                      <a href="<?= !empty($produk['gambar']) ? get_image(base_url()."assets/img/goods/large/".$produk['gambar'][0]->gambar_name) : get_image(base_url()."assets/img/no_image.jpg") ?>">
                        <img src="<?= !empty($produk['gambar']) ? get_image(base_url()."assets/img/goods/large/".$produk['gambar'][0]->gambar_name) : get_image(base_url()."assets/img/no_image.jpg") ?>" alt="" width="330" height="400" />
                      </a>
                    </div>
                  </div>

                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="col-md-6">
                <h4><?= $produk['nama_barang']?></h4>
                <div class="sub-title">
                  <ul>
                    <li> <span>Vendor : <a href="<?= base_url().'vendorDetail/vd?id='.urlencode(codeEncrypt($produk['outlet_id']));?>"><?= getOutletByUser($produk['user_id'],'nama_lengkap')?></a></span></li>
                    <li>
                      <div class="star">
                        <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                          class="fa fa-star"></i> <i class="fa fa-star null"></i> <i class="count-start">8</i>
                      </div>
                    </li>
                    <li><span>Status : <span class="text-success"><?=$produk['stok']?> In stock</span></span></li>
                  </ul>
                </div>
                <div id="price-product">
                  <?php if($produk['discount'] != 0 ){?>
                  <span class="disc-price"><?= myNum($produk['harga_satuan'],'Rp')?></span> 
                  <span class="true-price"><?= myNum(produk::disPrice($produk['harga_satuan'],$produk['discount']),'Rp')?></span>
                  <?php } else { ?>
                  <span class="true-price"><?= myNum($produk['harga_satuan'],'Rp')?></span> 
                  <?php }?>
                </div>
                <div id="description-prev">
                  <ul>
                    <?php 
                      $ps = explode(",", $produk['product_strength']);
                      foreach ($ps as $key => $value) {
                    ?>
                        <li><?=$value?></li>
                    <?php
                      }

                    ?>
                    
                    
                  </ul>
                </div>
                <div id="colour-prev">
                  <?php
                    $x = explode(',',$produk['variant']['warna']->varian_barang);
                    $c = count($x);
                    foreach ($x as $k => $v) {
                      if($v!=0){
                        echo '<div class="colour-pict" style="background:'.produk::warna($v).'" id="warnaPicker" data-id="'.$v.'"></div>';
                      }
                    }
                  ?>
                  <div class="clearfix"></div>
                </div>
                <div id="keranjang">
                  <!-- <form action="cart.html"> -->
                    <input type="hidden" id="discount" value="<?= $produk['discount']; ?>">
                    <input type="hidden" id="barang_id" value="<?= $produk['id']; ?>">
                    <input type="hidden" id="vendor_id" value="<?= $produk['outlet_id']; ?>">
                    <input type="hidden" id="warna_id">          
                    <?php

                      if (isset($produk['variant']['ukuran']->varian_barang)) {
                    ?>
                      <select class="custom-select custom-select-sm" id="ukuran_id" style="width:70px" name="ukuran">
                        <?php

                          $x  = explode(',',$produk['variant']['ukuran']->varian_barang);
                          $c  = count($x);
                          if(!empty($x)){
                            
                            foreach ($x as $k => $v) {
                              if($v!=0){
                               echo '<option value="'.$v.'">'.produk::ukuran($v).'</option>';
                              }
                            }
                          }
                        ?>
                      </select>
                    <?php
                      }
                    ?>          
                    
                    <button class="btn btn-add" id="cartButton"><i class="fa fa-shopping-cart"></i> Tambah ke Keranjang</button>
                    <button class="btn btn-add" id="buyButton"><!-- <i class="fa fa-shopping-cart"></i> --> Buy Now</button>
                    <a href="<?php echo (isset($this->jCfg['client']['is_login']) && $this->jCfg['client']['is_login'] == 1)?'#':base_url().'login'?>" id="wishlistButton">
                      <?php 
                        if($wishlist > 0){
                          echo '<i class="fa fa-heart pull-right" style="color:#c1355e;font-size:25px;margin-top:5px;"></i>';
                        } else {
                          echo '<i class="fa fa-heart-o pull-right" style="font-size:25px;margin-top:5px;"></i>';
                        }
                      ?>
                      
                    </a>
                  <!-- </form> -->
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="card profile-toko">
            <table id="head-prof">
              <tr>
                <td  rowspan="2">
                  <div class="profile-pic" ><img src="<?= get_image(base_url()."assets/img/vendor/thumb/".getOutletByUser($produk['user_id'],'photo'))?>" width="50"></div>
                </td>
                <td><a class="title-profile" href="<?= base_url().'vendorDetail/vd?id='.urlencode(codeEncrypt($produk['outlet_id']));?>"><?= getOutletByUser($produk['user_id'],'nama_lengkap')?></a></td>
              </tr>
              <tr>
                <td>
                  <span style="color:#9b9db0;"><?= $produk['produkCount']?> product(s)</span>
                </td>
              </tr>
            </table>
            <hr>
            <p class="desc-toko" style="margin-bottom:0px">
              Rating Toko
              <div class="star">
                <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                  class="fa fa-star"></i> <i class="fa fa-star null"></i>
              </div>
              <span style="margin-top:0px" class="text-success">80% positive</span>
              </p>
              <p class="desc-toko">
                Wilayah
                <br>

                <span><?= regional::kota(getOutletByUser($produk['user_id'],'kota_id')) ?>, <?= regional::province(getOutletByUser($produk['user_id'],'provinsi_id'))?></span>
              </p>
              <p class="desc-toko">
                Bergabung Sejak
                <br>
                <span><?= date('d F Y',strtotime(getOutletByUser($produk['user_id'],'register_date')))?></span>
              </p>
              <p class="desc-toko">
                Kurir yang Tersedia
                <br>
                <?=image::shipping(getOutletByUser($produk['user_id'],'shipping_metode'))?>
               <!-- <img style="margin-top:10px;" src="<?=image::shipping(getOutletByUser($produk['user_id'],'shipping_metode'))?>" alt=""> -->
              </p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="sec-desc">
    <div class="container">
      <div class="head-background">
        <ul class="nav nav-tabs">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#descs">
              <h6>Product Description</h6>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#review">
              <h6>Customer Review</h6>
            </a>
          </li>
        </ul>
      </div>
      <div class="tab-content">
        <div class="tab-pane active" id="descs">
          <div class="contain-desc"><?= $produk['deskripsi']?></div>
        </div>
        <div class="tab-pane" id="review">
          <div class="contain-desc">
            
          </div>
        </div>
      </div>
      
        
      
      
    </div>
  </section>
  <section id="makanan-section">
    <div class="container">
      <div class="head-background">
        <h5>Produk Lainnya yang Sejenis</h5> 
      </div>
      <div id="makanan" class="carousel slide" data-ride="carousel" data-interval="0">
        <div class="carousel-inner">
          <?php 
            $pc = '';
            $countChain = count($produk['produkChain']);
            if($countChain > 6){
              
              for($i = 1;$i <= 2; $i++){
          ?>

          <div class="item carousel-item <?= $i == 1 ? 'active' : '';?>">

            <div class="row">
              <?php
                for ($c=0; $c < 6; $c++) { 
                  $pc = $produk['produkChain'][$c];
                  d($pc);
              ?>
              <div class="col-sm-2">
                <div class="">
                  <!-- <div class="disc-tem">
                    50%
                  </div> -->
                  <div class="hovereffect">
                    <a href="<?= base_url().'detail/pd?id='.urlencode(codeEncrypt($pc->id)).'&&'.'title='.url_title($pc->nama_barang)?>"> <img class="img-responsive" src="<?= produk::gambar($pc->id,'small')?>" alt="" width="160" height="200"></a>
                    <div class="overlay">
                      <p class="icon-links">
                        <a href="#">
                          <span class="fa fa-heart-o"></span>
                        </a>
                        <a href="#">
                          <span class="fa fa-shopping-cart"></span>
                        </a>
                      </p>
                    </div>
                  </div>
                  <div class="det-prod">
                    <div class="">
                      <!-- <span class="disc-price">Rp. 75.000</span> --> <span class="true-price"><?= myNum(produk::detail($pc->id,'harga_satuan'),'Rp')?></span>
                    </div>
                    <div class="">
                      <a href="<?= base_url().'detail/pd?id='.urlencode(codeEncrypt($pc->id)).'&&'.'title='.url_title($pc->nama_barang);?>"><?= $pc->nama_barang?></a>
                    </div>
                    <div class="">
                      <p>Sold by: <?= getOutletByUser($pc->user_id,'nama_lengkap')?></p>
                    </div>
                    <div class="star">
                      <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                        class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <?php
                }
              ?>
            </div>

          </div>

          <?php
              }
            } else {

          ?>
          <div class="item carousel-item active">
            <div class="row">
              <?php
                for ($c=0; $c < $countChain; $c++) { 
                  $pc = $produk['produkChain'][$c];
              ?>
              <div class="col-sm-2">
                <div class="">
                  <!-- <div class="disc-tem">
                    50%
                  </div> -->
                  <div class="hovereffect">
                    <a href="<?= base_url().'detail/pd?id='.urlencode(codeEncrypt($pc->id)).'&&'.'title='.url_title($pc->nama_barang);?>"><div class="img-responsive small-b" style="background:url('<?= produk::gambar($pc->id,'small')?>')center center / cover no-repeat "  alt=""></div></a>
                    <div class="overlay">
                      <p class="icon-links">
                        <a href="#">
                          <span class="fa fa-heart-o"></span>
                        </a>
                        <a href="#">
                          <span class="fa fa-shopping-cart"></span>
                        </a>
                      </p>
                    </div>
                  </div>
                  <div class="det-prod">
                    <div class="">
                      <!-- <span class="disc-price">Rp. 75.000</span> --> <span class="true-price"><?= myNum(produk::detail($pc->id,'harga_satuan'),'Rp')?></span>
                    </div>
                    <div class="">
                      <a href="<?= base_url().'detail/pd?id='.urlencode(codeEncrypt($pc->id)).'&&'.'title='.url_title($pc->nama_barang);?>"><?= $pc->nama_barang?></a>
                    </div>
                    <div class="">
                      <p>Sold by: <?= getOutletByUser($pc->user_id,'nama_lengkap')?></p>
                    </div>
                    <div class="star">
                      <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                        class="fa fa-star"></i> <i class="fa fa-star null"></i> <span style="margin-left:10px">8</span>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <?php
                }
              ?>
            </div>
          </div>
          <?php
            }
          ?>

        </div>
        <!-- Carousel controls -->
        <a class="carousel-control-prev" href="#makanan" data-slide="prev">
          <div class="control-slide">
            <i class="fa fa-angle-left"></i>
          </div>
        </a>
        <a class="carousel-control-next" href="#makanan" data-slide="next">
          <div class="control-slide">
            <i class="fa fa-angle-right"></i>
          </div>
        </a>
      </div>
    </div>
  </section>
</body>

<?= js_assets('product/productDetail.js','f'); ?>