<body>

 <section>
     <div class="container">
        <div class="row">
            <?=$sideprep?>
            <div class="col-md-9">
             
                
              <section class="section-prep">
                <div class="head-prep">
                  <h4><?=$title?></h4>
                </div>
                <div class="body-prep">
                  <?=$content  ?>
                </div>
              </section>
             
            </div>
        </div>
     </div>
 </section>
</body>


<script type="text/javascript">
  $('select#sortp').on('change', function(event) {
    event.preventDefault();
      $("#fsp").submit(); // Submit the form
  });
</script>