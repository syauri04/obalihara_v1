<div class="col-md-3">
        <div class="card cprep">
          <div class="row" id="body-row">
            <div id="sidebar-container" class="sidebar-expanded d-none d-md-block">
              <h6>TABLE OF CONTENT</h6>
              <ul class="list-group">

                <a href="<?=base_url().'privacy'?>" class="list-group-item-prep list-group-item-action">
                  <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="menu-collapsed">Kebijakan Privasi</span>
                    <span class="submenu-icon ml-auto"></span>
                  </div>
                </a>
                <a href="<?=base_url().'terms'?>" class="list-group-item-prep list-group-item-action">
                  <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="menu-collapsed">Terms And Condition</span>
                    <span class="submenu-icon ml-auto"></span>
                  </div>
                </a>
                <a href="<?=base_url().'about'?>" class="list-group-item-prep list-group-item-action">
                  <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="menu-collapsed">Tentang Kami</span>
                    <span class="submenu-icon ml-auto"></span>
                  </div>
                </a> 
                <a href="<?=base_url().'service'?>" class="list-group-item-prep list-group-item-action">
                  <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="menu-collapsed">Customer Service</span>
                    <span class="submenu-icon ml-auto"></span>
                  </div>
                </a>
                <a href="<?=base_url().'faq'?>" class="list-group-item-prep list-group-item-action">
                  <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="menu-collapsed">Frequenly Asked Questions</span>
                    <span class="submenu-icon ml-auto"></span>
                  </div>
                </a>
              </ul>
            </div>
          </div>
        </div>
        
    </div>