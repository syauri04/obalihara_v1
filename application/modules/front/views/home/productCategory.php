<body>

 <section>
     <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                  <div class="row" id="body-row">
                    <div id="sidebar-container" class="sidebar-expanded d-none d-md-block">
                      <h5>CATEGORIES</h5>
                      <ul class="list-group">

                        <?= getcategories();?>

                      </ul>
                    </div>
                  </div>
                </div>
                <div class="card mt-4">
                  <div id="sidebar-container" class="sidebar-expanded d-none d-md-block">
                    <h6 class="text-pink">HIGHLIGHT</h6>
                    <div class="box-side">
                      <div class="">
                         <input id="checkbox-1" class="checkbox-custom" name="checkbox-2" type="checkbox">
                         <label for="checkbox-1" class="checkbox-custom-label">Promotions <span>(800)</span></label>
                      </div>
                      <div class="">
                        <input id="checkbox-2" class="checkbox-custom" name="checkbox-2" type="checkbox">
                        <label for="checkbox-2" class="checkbox-custom-label">New Arrivals <span>(300)</span></label>
                      </div>
                    </div>
                    
                    
                    <h6>BY PRICE</h6>
                    <div class="box-side">
                      <div class="price-box">
                        <ul>
                          <li>
                            <a href="">> Rp. 50.000</a>
                          </li>
                          <li>
                            <a href="">> Rp. 50.000 - Rp. 300.000</a>
                          </li>
                          <li>
                            <a href="">> Rp. 300.000 - Rp. 1.000.000</a>
                          </li>
                          <li>
                            <a href="">> Rp. 1.000.000 - Rp. 3.000.000</a>
                          </li>
                           <li>
                             <a href="">> Rp. 3.000.000</a>
                           </li>  
                        </ul>
                      </div>
                    </div>
                    
                    
                    <!-- <h6>DELIVERY AND PAYMENT</h6>
                    <div class="box-side">
                      <div class="">
                        <input id="checkbox-1" class="checkbox-custom" name="checkbox-2" type="checkbox">
                        <label for="checkbox-1" class="checkbox-custom-label">Layanan JNE</label>
                      </div>
                      <div class="">
                        <input id="checkbox-2" class="checkbox-custom" name="checkbox-2" type="checkbox">
                        <label for="checkbox-2" class="checkbox-custom-label">Layanan J-Express</label>
                      </div>
                      <div class="">
                        <input id="checkbox-2" class="checkbox-custom" name="checkbox-2" type="checkbox">
                        <label for="checkbox-2" class="checkbox-custom-label">Post Indonesia</label>
                      </div>
                    </div> -->
                    <h6>BY RATING</h6>
                    <div class="box-side  side-rating">
                      <div class="">
                        <input id="checkbox-1" class="checkbox-custom" name="rating" type="checkbox">
                        <label for="checkbox-1" class="checkbox-custom-label">
                            <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                              class="fa fa-star"></i> <i class="fa fa-star"></i> <span>(800)</span>
                        </label>
                      </div>
                      <div class="">
                        <input id="checkbox-1" class="checkbox-custom" name="rating" type="checkbox">
                        <label for="checkbox-1" class="checkbox-custom-label">
                          <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                            class="fa fa-star"></i> <i class="fa fa-star null"></i> <span>(800)</span>
                        </label>
                      </div>
                      <div class="">
                        <input id="checkbox-1" class="checkbox-custom" name="rating" type="checkbox">
                        <label for="checkbox-1" class="checkbox-custom-label">
                          <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                            class="fa fa-star null"></i> <i class="fa fa-star null"></i> <span>(800)</span>
                        </label>
                      </div>
                      <div class="">
                        <input id="checkbox-1" class="checkbox-custom" name="rating" type="checkbox">
                        <label for="checkbox-1" class="checkbox-custom-label">
                          <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star null"></i> <i
                            class="fa fa-star null"></i> <i class="fa fa-star null"></i> <span>(800)</span>
                        </label>
                      </div>
                      <div class="">
                        <input id="checkbox-1" class="checkbox-custom" name="rating" type="checkbox">
                        <label for="checkbox-1" class="checkbox-custom-label">
                          <i class="fa fa-star"></i> <i class="fa fa-star null"></i> <i class="fa fa-star null"></i> <i
                            class="fa fa-star null"></i> <i class="fa fa-star null"></i> <span>(800)</span>
                        </label>
                      </div>
                      <div class="">
                        <input id="checkbox-1" class="checkbox-custom" name="rating" type="checkbox">
                        <label for="checkbox-1" class="checkbox-custom-label">
                          <i class="fa fa-star null"></i> <i class="fa fa-star null"></i> <i
                            class="fa fa-star null"></i> <i
                            class="fa fa-star null"></i> <i class="fa fa-star null"></i> <span>(800)</span>
                        </label>
                      </div>
                    </div>
                   
                  </div>  
                </div>
            </div>
            <div class="col-md-9">
              <?php
               
                if(!empty($subCat) && empty($_POST['search']) && empty($_GET['viewall'])){
              ?>
                <section>
                  <div class="head-title">

                    <h5>Subcategory</h5>
                  </div>
                  <div class="row">
                    <?php
                      foreach ($subCat as $c => $v) {
                    ?>
                      <div class="col-md-4">
                        <a href="<?= base_url().'category/c?cid='.$v->id.'&&ct='.url_title($v->name)?>">
                          <div class="img-responsive small-sc" style="background:url('<?=produk::gambarCOM($v->id,'large')?>')center center / cover no-repeat "  alt=""></div>
                         </a>
                        <div class="title-categories">
                          <a href="<?= base_url().'category/c?cid='.$v->id.'&&ct='.url_title($v->name)?>"><?= $v->name ?></a>
                        </div>
                      </div>
                    <?php
                      }
                    ?>
                  </div>
                </section>
              <?php
                }
              ?>
              

              <section id="sec-head-category">
                <div class="head-background">
                  <h5>Category : <?= $titleCat?></h5>
                  <div class="pull-right s-barang-head">
                    <form action="<?= base_url().'/front/home/category'?>" method="post" class="searchForm">
                      <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fa fa-search"></i></span>
                        </div>
                        
                        <input type="text" name="search" class="form-control text-custom" placeholder="Cari nama barang">
                        
                      </div>
                    </form>
                  </div>
                </div>
              </section>
              <?php
                if(count($bestSeller) > 0){
              ?>
              <section>
                <div class="head-title">
                  <h5>Best Seller Product</h5>
                </div>
                <div class="row">
                   <?php
                    // d($bestSeller);
                      foreach ($bestSeller as $k => $v) {

                    ?>
                        <div class="col-sm-3">
                          <div class="">
                            <!-- <div class="disc-tem">
                              50%
                            </div> -->
                            <div class="hovereffect">
                              <a href="#"><div class="img-responsive small-b" style="background:url('<?= produk::gambar($v->id,'small')?>')center center / cover no-repeat "  alt=""></div></a>
                              <div class="overlay">
                                <p class="icon-links">
                                  <a href="<?php echo (isset($this->jCfg['client']['is_login']) && $this->jCfg['client']['is_login'] == 1)?'#':base_url().'login'?>">
                                    <span class="fa fa-heart-o"></span>
                                  </a>
                                  <!-- <a href="#">
                                    <span class="fa fa-shopping-cart"></span>
                                  </a> -->
                                </p>
                              </div>
                            </div>
                            <div class="det-prod det-prod-2">
                              <div class="">
                               <a href="<?= base_url().'detail/pd?id='.urlencode(codeEncrypt($v->id)).'&&'.'title='.url_title($v->nama_barang);?>"><?= $v->nama_barang?></a>
                          
                              </div>
                              <!-- <div class="star">
                                <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                                  class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                                  style="margin-left:10px">8</span>
                              </div> -->
                              <div class="">
                                <span class="true-price"><?= myNum(produk::detail($v->id,'harga_satuan'),'Rp');?></span>
                                <!-- <span class="disc-price">Rp. 75.000</span> <span class="true-price">Rp. 75.000</span> -->
                              </div>
                            </div>
                            <div class="clearfix"></div>
                          </div>
                        </div>
                    <?php
                      }
                    ?>

                
                </div>

              </section>
              <?php } ?>
                <section id="product-body">
                  <div class="head-title">
                    <h5><?=count($getAllProduk);?> Products In Stock</h5>
                    <div class="pull-right sort-order">
                      <ul>
                        <li class="mr-3">
                          <form action="" method="GET" id="fsp">

                            <span class="sort-ac"> Sort by:</span> 
                            <input type="hidden" name="cid" value="12">
                            <input type="hidden" name="ct" value="Fashion-Pria">
                              <select name="sortp" class="custom-select custom-select-sm sort-ac" id="sortp">
                                <option <?=isset($_GET['sortp']) && $_GET['sortp'] == 'ASC'? 'selected':''?> value="ASC">Lower Price</option>
                                <option <?=isset($_GET['sortp']) && $_GET['sortp'] == 'DESC'? 'selected':''?> value="DESC">Higher Price</option>
                              </select>
                          </form>
                         
                          <div class="clearfix"></div>
                        </li>
                       <!--  <li>
                          <span class="sort-ac"> View by:</span> <i class="fa fa-th sort-ac"></i> <i
                            class="fa fa-align-justify sort-ac"></i>
                          <div class="clearfix"></div>
                        </li> -->

                    </ul>
                  </div>
                </div>
                <div class="row">
                  <?php

                    foreach ($getAllProduk as $k => $v) {

                  ?>
                  <div class="col-sm-3">
                    <div class="">
                      <!-- <div class="disc-tem">
                        50%
                      </div> -->
                      <div class="hovereffect">
                        <a href="<?= base_url().'detail/pd?id='.urlencode(codeEncrypt($v->id)).'&&'.'title='.url_title($v->nama_barang);?>  "><div class="img-responsive small-b" style="background:url('<?= produk::gambar($v->id,'small')?>')center center / cover no-repeat "  alt=""></div></a>
                        <div class="overlay">
                          <p class="icon-links">
                            <a href="<?php echo (isset($this->jCfg['client']['is_login']) && $this->jCfg['client']['is_login'] == 1)?'#':base_url().'login'?>">
                              <span class="fa fa-heart-o"></span>
                            </a>
                           <!--  <a href="<?php echo (isset($this->jCfg['client']['is_login']) && $this->jCfg['client']['is_login'] == 1)?'#':base_url().'login'?>">
                              <span class="fa fa-shopping-cart"></span>
                            </a> -->
                          </p>
                        </div>
                      </div>
                      <div class="det-prod det-prod-2">
                        <div class="">
                          <a href="<?= base_url().'detail/pd?id='.urlencode(codeEncrypt($v->id)).'&&'.'title='.url_title($v->nama_barang);?>"><?= $v->nama_barang?></a>
                        
                        </div>
                        <div class="star">
                          <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i
                            class="fa fa-star"></i> <i class="fa fa-star null"></i> <span
                            style="margin-left:10px">8</span>
                        </div>
                        <div class="">
                          <!--<span class="disc-price">Rp. 75.000</span>--><span class="true-price"><?= myNum(produk::detail($v->id,'harga_satuan'),'Rp');?></span>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <?php } ?>
                </div>
              </section>
                <!-- <section id="sec-pagination" class="text-center">
                  <ul>
                    <li>
                     <a href=""><i class="fa fa-angle-left"></i> Previous page</a>
                    </li>
                    <li class="current">1</li>
                    <li><a href="">2</a></li>
                    <li><a href="">3</a></li>
                    <li><a href="">4</a></li>
                    <li><a href="">5</a></li>
                    <li>...</li>
                    <li><a href="">100</a></li>
                    <li>
                      <a href="">Next page <i class="fa fa-angle-right"></i></a>
                    </li>
                  </ul>
                </section> -->
              
            </div>
        </div>
     </div>
 </section>
</body>


<script type="text/javascript">
  $('select#sortp').on('change', function(event) {
    event.preventDefault();
      $("#fsp").submit(); // Submit the form
  });
</script>