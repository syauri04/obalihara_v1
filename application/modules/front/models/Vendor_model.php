<?php

class Vendor_model extends CI_Model{

  function __construct(){
  	parent::__construct();
	}

	function getProduct($id){
    $this->db->select('b.*,bd.harga_satuan')
    ->join('cp_barang_detail bd','bd.barang_id = b.id')
    ->where('b.user_id',$id);
    $a = $this->db->get('cp_barang b')->result();
    return $a;
	}

	function getOrders($id,$type=''){
    $this->db->select('*')
    ->where('vendor_id',$id);

    if($type == 'orderIn'){
      $this->db->where('status_invoice < ',3);
      $this->db->where('status_invoice > ',0);
    } else if($type == 'orderShipped'){
      $this->db->where('status_invoice > ',3);
    } else if($type == 'orderRecent'){
      $this->db->where('status_invoice >',2);
      $this->db->order_by('id','DESC');
    } else {
      $this->db->where('status_invoice > ',0);
    }

    $a = $this->db->get('cp_cart')->result();
    
		return $a;
	}

  function getSales($id){

    $this->db->select('*')
    ->where('vendor_id',$id)
    ->where('status_invoice >',4);
    $a = $this->db->get('cp_cart')->result();
    
    return $a;

  }

  function getEarning($id){

    $this->db->select('sum(total) as total')
    ->where('vendor_id',$id)
    ->where('status_invoice >=',5);
    $a = $this->db->get('cp_checkout_detail')->row();

    $this->db->select('sum(amount) as amount,sum(fee) as fee')
    ->where('vendor_id',$id)
    ->where('status','COMPLETED');
    $b = $this->db->get('cp_vendor_disburse')->row();

    $r = new stdClass;
    if(isset($b)){
      $r->total = $a->total-$b->amount-$b->fee;
    } else {
      $r->total = $a->total;
    }
    
    return $r;

  }

	function getSetting($id){

		$this->db->select('*')
		->where('user_id',$id);
		$a = $this->db->get('cp_vendor')->row();

		return $a;

	}

	function getProvince() {

      $this->db->select('id,nama_provinsi')
      ->where('is_trash',0)
      ->order_by('id','ASC');
      return $this->db->get('cp_app_province')->result();

  }

  function getCity($id) {

      $this->db->select('id,nama_kota,kota_kab')
      ->where('provinsi_id',$id)
      ->where('is_trash',0)
      ->order_by('id','ASC');
      return $this->db->get('cp_app_city')->result();

  }

  function getCat($id=''){
      
    if($id != ''){
      $a = $this->db->where('parent_id',$id)->get('cp_kategori_barang')->result();
    } else {
      $a = $this->db->where('parent_id',0)->get('cp_kategori_barang')->result();
    }
    
    return $a;

  }

    function getOpt($id,$t=''){
      
        if($id == 1){
            $a = $this->db->get('cp_warna')->result();
        } else {
            $a = $this->db->get_where('cp_ukuran',['kategori_barang_tipe' => $t])->result();
        }
      
      
      return $a;

    }
}