<?php

class Home_model extends CI_Model{

    function __construct(){
    	parent::__construct();
  	}

    function getCatOM(){
        $this->db->select('id,name,parent_id')
        ->where('parent_id !=',0)
        ->where('tipe !=',0)
        ->where('is_trash',0)
        ->limit(6,0)
        ->order_by('clicked','DESC');
        $a = $this->db->get('cp_kategori_barang')->result();
        return $a;
    }

    function productVendor($id,$order=''){
        $this->db->select('*')
        ->where('outlet_id',$id)
        ->where('is_trash',0);
        if($order != ''){
            $this->db->order_by('id',$order);
        }
        $a = $this->db->get('cp_barang')->result();
        return $a;
    }

    function mostProduct($id){
        $this->db->select('*')
        ->where('outlet_id',$id)
        ->where('is_trash',0)
        ->limit(4)
        ->order_by('is_counter','DESC');
        $a = $this->db->get('cp_barang')->result();
        return $a;
    }

    function getSlide($type=''){
        $this->db->select('*');
        if($type == 'c'){
            $this->db->where('carousel',1);
            $this->db->where('is_trash',0)->limit(5,0)->order_by('id','DESC');
        } else if($type == 's'){
            $this->db->where('side',1);
            $this->db->where('is_trash',0)->limit(2,0)->order_by('id','DESC');
        } else if($type == 'm') {
            $this->db->where('mid',1);
            $this->db->where('is_trash',0)->limit(1,0)->order_by('id','DESC');
        } else if($type == 'ms') {
            $this->db->where('mid_small',1);
            $this->db->where('is_trash',0)->limit(3,0)->order_by('id','DESC');
        }

        $a = $this->db->get('cp_banner')->result();
        return $a;
    }

    function getCatPM(){
        $this->db->select('id,name,parent_id')
        ->where('parent_id',0)
        ->where('tipe',0)
        ->where('is_trash',0)
        ->order_by('clicked','DESC');
        $a = $this->db->get('cp_kategori_barang')->result();
        return $a;
    }

    function companySetting(){
        return $this->db->get('cp_company_setting')->row();
    }

    function getBestSeller($id){
        $c = getChild($id,'cp_kategori_barang');
       // d($id);
        $this->db->select('*')->where('is_trash',0);
        if(count($c) > 0){
            $this->db->where_in('kategori_barang',$c);
        } else {
            $this->db->where('kategori_barang',$id);
        }

        $this->db->order_by("is_counter", "DESC");
        $this->db->limit(4);
        $a = $this->db->get('cp_barang')->result();
        return $a;
    }

    function getAllProduk($p=[]){
        // d($p);
        if(isset($p['id']) && $p['id'] != ''){
            $c = getChild($p['id'],'cp_kategori_barang');
        }


        if(isset($p['sortp']) && $p['sortp']){
            $this->db->select('b.*, bd.harga_satuan');
            $this->db->from('cp_barang as b');
            $this->db->join('cp_barang_detail as bd', 'bd.barang_id = b.id');
            if(isset($c) && count($c) > 0){
                $this->db->where_in('b.kategori_barang',$c);
            } else if(isset($c)){
                $this->db->where('b.kategori_barang',$p['id']);
            }
            // d($p['so']);
            $this->db->order_by('bd.harga_satuan', $p['sortp']);
            $a = $this->db->get()->result();

        }else{
            
        
            $this->db->select('*')->where('is_trash',0);
            if(isset($p['search']) && $p['search']){
                $this->db->like('nama_barang', $p['search']);
            }
            if(isset($c) && count($c) > 0){
                $this->db->where_in('kategori_barang',$c);
            } else if(isset($c)){
                $this->db->where('kategori_barang',$p['id']);
            }

            $a = $this->db->get('cp_barang')->result();
        }
       

        
        // d($this->db->last_query());
       
        // d($this->db->last_query());
        return $a;
    }

    function getDetailProduk($id){
        $this->db->select('*')
        ->join('cp_barang_detail bd','bd.barang_id = b.id')
        ->where('b.id',$id)
        ->where('b.is_trash',0);
        $a = $this->db->get('cp_barang b')->row_array();

        $a['gambar']            = $this->db->get_where('cp_barang_gambar',['barang_id' => $id])->result();
        $a['produkCount']       = $this->db->get_where('cp_barang',['user_id' => $a['user_id']])->num_rows();
        $a['produkChain']       = $this->db->where(['kategori_barang' => $a['kategori_barang'], 'id !=' => $id])->limit(12)->get('cp_barang')->result();
        $a['variant']['warna']  = $this->db->get_where('cp_barang_varian',['barang_id' => $id,'jenis_varian' => 'warna'])->row();
        $a['variant']['ukuran'] = $this->db->get_where('cp_barang_varian',['barang_id' => $id,'jenis_varian' => 'ukuran'])->row();
        // d($a);
        return $a;
    }

    function getCart($id){

        $this->db->select('*')
        ->where('id',$id);
        $a = $this->db->get('cp_cart')->result();

        return $a;
    }

    function getCartByUser($id){

        $this->db->select('*')
        ->where('user_id',$id)
        ->where('status_invoice',0);
        $a = $this->db->get('cp_cart')->result();

        return $a;
    }

    function getUser($p=[]){

        if(!empty($p)){
            $this->db->select('*');
            $this->db->from('cp_user');
            $this->db->where(array('oauth_provider'=>$p['oauth_provider'], 'oauth_uid'=>$p['oauth_uid']));

            $prevQuery = $this->db->get();
            $prevCheck = $prevQuery->num_rows();

            if($prevCheck > 0){
                $prevResult = $prevQuery->row_array();
                
                //update user data
                $userData['logindate'] = date("Y-m-d H:i:s");
                $update = $this->db->update('cp_user', $userData, array('id' => $prevResult['id']));
                
                //get user ID
                $userID = $prevResult['id'];
                $r = [
                    'id'        => $userID,
                    'username'  => $p['email'],
                    'email'     => $p['email']
                ];
            }else{
                //insert user data
                $userData = [
                    'oauth_provider' => $p['oauth_provider'],
                    'oauth_uid'      => $p['oauth_uid'],
                    'email'          => $p['email'],
                    'createdDate'    => date("Y-m-d H:i:s"),
                    'logindate'      => date("Y-m-d H:i:s"),
                    'status'         => 1,
                    'is_trash'       => 0,
                ];

                $c = $this->db->where("(email = '".$p['email']."')")->get('cp_user')->result();

                if(count($c) > 0){
                    $userID = ['msg' => ['m' => 'Email atau Username Sudah terdaftar' , 'con' => 'danger' , 't' => 'Failed!'] ];
                } else {
                    $insert = $this->db->insert('cp_user', $userData);
                
                    //get user ID
                    $userID = $this->db->insert_id();

                    $buyer = [
                        'user_id'       => $userID,
                        'nama_lengkap'  => $p['first_name'].' '.$p['last_name'],
                        'email'         => $p['email'],
                        'photo'         => $p['picture'],
                        'is_trash'      => 0,
                    ];

                    $this->db->insert('cp_buyer',$buyer);

                    $r = [
                        'id'        => $userID,
                        'username'  => $p['email'],
                        'email'     => $p['email']
                    ];
                }

                
            }
        }

        //return user ID
        return $r?$r:FALSE;

    }

    function getProduct($type=''){
        $this->db->select('*');
        $this->db->from('cp_barang AS b');
        $this->db->join('cp_barang_detail AS d', 'b.id = d.barang_id');
        $this->db->join('cp_barang_gambar AS g', 'b.id = g.barang_id');
        if($type == 'na'){
            $this->db->where('b.is_new',1);
        } 
        if($type == 'dof'){
            $this->db->where('d.discount !=',0)
            ->limit(12,0);
        }

        if($type == 'or'){
            $this->db->where('b.is_trash',0)->order_by('b.is_counter','DESC')->group_by('g.barang_id')->limit(6,0);
        } else {
            $this->db->where('b.is_trash',0)->order_by('b.id','DESC')->group_by('g.barang_id');
        }

        if($type == 'na'){
            $this->db->limit(6,0);
        }else if($type == 'naC'){
             $this->db->limit(12,6);
        }
        
        $a = $this->db->get()->result();
        // dq();
        return $a;
    }
    
    function getViewAll($p=[]){
        
        if(isset($p['type']) && $p['type'] != ''){
             $type = $p['type'];
        }
        // d($p);
        $this->db->select('*');
        $this->db->from('cp_barang AS b');
        $this->db->join('cp_barang_detail as bd', 'bd.barang_id = b.id');
        $this->db->where('b.is_new',1);
        $this->db->where('b.is_trash',0);
        
        if($type == 'New Arrivals'){
            if(isset($p['sortp']) && $p['sortp'] != ''){
                $this->db->order_by('bd.harga_satuan', $p['sortp']);
            } else {
                $this->db->order_by('b.id','DESC');
            }
        } else if($type == 'Rekomendasi Kami'){
            if(isset($p['sortp']) && $p['sortp'] != ''){
                $this->db->order_by('bd.harga_satuan', $p['sortp']);
            } else {
                $this->db->order_by('b.is_counter','DESC');
            }
        }

        $a = $this->db->get()->result();
        
        return $a;
    }

    function getSubCat($id=''){
    
        $this->db->select('id,name,parent_id')
        ->where('parent_id',$id['id'])
        // ->where('parent_id !=',0)
        ->where('tipe !=',0)
        ->where('is_trash',0)
        ->limit(3,0)
        ->order_by('clicked','DESC');
        $a = $this->db->get('cp_kategori_barang')->result();
        // d($a);
        return $a;
    }

    function getWishlist($id) {

        $this->db->select('w.*,b.nama_barang,bd.harga_satuan,bd.stok')
        ->join('cp_barang b','b.id = w.barang_id')
        ->join('cp_barang_detail bd','bd.barang_id = w.barang_id')
        ->where('w.user_id',$id);
        
        $a = $this->db->get('cp_wishlist w')->result();

        return $a;
    }

    function getShipping($id){
        $this->db->select('*')
        ->join('cp_user u','u.id = b.user_id')
        ->join('cp_buyer_address ba','ba.buyer_id = u.id')

        ->where('b.user_id',$id);
        $a = $this->db->get('cp_buyer b')->result();
        return $a;
    }

    function getCartRow($callback,$xic,$ic,$res=[]){

        $this->db->insert('cp_callback',$callback);

        $a = $this->db->update('cp_checkout',['status' => $res['status']],[ 'invoice_code' => $ic , 'xendit_invoice_code' => $xic]);

        $w = $this->db->select('id')->where(['xendit_invoice_code'=>$xic,'invoice_code'=>$ic])->get('cp_checkout')->row();

        $this->db->select('*')->join('cp_checkout_detail cd','cd.header_id = c.id')->where('header_id',$w->id);
        $result = $this->db->get('cp_checkout c')->result();
        
        if(count($result) > 0){
            foreach ($result as $k => $v) {
                $this->db->update('cp_cart',['status_invoice' => 2],['id' => $v->cart_id]);                
            }
        } else {
            return false; 
        }

        return true;
                  
    }

    // function cek_email($p=[]){

    //     $a = $this->db->get_where('cp_user')->result();

    // }
}
