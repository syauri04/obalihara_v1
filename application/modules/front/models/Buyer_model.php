<?php

class Buyer_model extends CI_Model{

  function __construct(){
  	parent::__construct();
	}

	function getBuyer($id){

    $this->db->select('*')
    ->where('user_id',$id)
    ->where('is_trash',0);
    $a = $this->db->get('cp_buyer')->row();
    
    return $a;
  }

  function getUser($id,$p='',$pass = false){

    $this->db->select('*');
    $this->db->where('id',$id);
    if($pass == true){
      $this->db->where('password',$p);
    }
    $this->db->where('is_trash',0);
    $a = $this->db->get('cp_user')->row();

    return $a;
  }

  function getBuyerAdr($id){

    $this->db->select('*')
    ->where('buyer_id',$id);
    $a = $this->db->get('cp_buyer_address')->result();

    return $a;
  }

  function getBuyerOrder($id,$s=''){
    $this->db->select('*')
    ->where('user_id',$id);
    if($s != ''){
      $this->db->where('status_invoice',$s);
    } else {
      $this->db->where('status_invoice > ',0);
    }
    $a = $this->db->get('cp_cart')->result();

    return $a;
  }

  function getProvince() {
    $this->db->select('id,nama_provinsi')
    ->where('is_trash',0)
    ->order_by('id','ASC');
    return $this->db->get('cp_app_province')->result();
  }

  function getCity($id) {
    $this->db->select('id,nama_kota,kota_kab')
    ->where('provinsi_id',$id)
    ->where('is_trash',0)
    ->order_by('id','ASC');
    return $this->db->get('cp_app_city')->result();
  }

}