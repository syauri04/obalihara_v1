<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
	    <div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="form-stuff-1">
				<!-- begin panel-heading -->
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Form</h4>
                </div>
                <!-- end panel-heading -->
                <?php //d($val);?>
                <!-- begin panel-body -->
                <div class="panel-body">
                	<form enctype="multipar/form-data" action="<?=$this->own_link.'save'?>" method="POST">
                		<input type="hidden" name="id" value="<?= isset($val)?$val->id:'';?>">

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Coupon Name</label>
							<div class="col-md-9">
								<input type="text" name="coupon_name" class="form-control m-b-5" autocomplete="off" value="<?= isset($val)?$val->coupon_name:'';?>" />
							</div>
						</div>
						
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Date range</label>
							<div class="col-md-9">
							    <!-- <input type="text" class="daterange" name="start" /> -->
							    <input type="text" id="demo" class="form-control">
							</div>
							<input type="hidden" name="date_from" class="dateFrom" value="<?= isset($val)?$val->date_from:'';?>">
							<input type="hidden" name="date_to" class="dateTo" value="<?= isset($val)?$val->date_to:'';?>">
						</div>

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">price</label>
							<div class="col-md-9">
								<input type="number" name="price" class="form-control m-b-5" autocomplete="off" value="<?= isset($val)?$val->price:'';?>" />
							</div>
						</div>

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Status</label>
							<div class="col-md-9">
								<div class="form-check form-check-inline">
								  <input class="form-check-input" type="radio" name="status" id="inlineRadio1" value="1" <?= (isset($val) && $val->status == 1)?'checked':'';?>>
								  <label class="form-check-label" for="inlineRadio1">Active</label>
								</div>
								<div class="form-check form-check-inline">
								  <input class="form-check-input" type="radio" name="status" id="inlineRadio2" value="0" <?= (isset($val) && $val->status == 1)?'checked':'';?>>
								  <label class="form-check-label" for="inlineRadio2">Non Active</label>
								</div>
							</div>
						</div>

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3"></label>
							<div class="col-md-9">
								<a href="<?= $this->own_link?>" class="btn btn-white cancel">Cancel</a>
								<button type="button" class="btn btn-primary simpan">Simpan</button>
							</div>
						</div>
					</form>
                </div>
			</div>
            <!-- end panel -->
	    </div>
	    <!-- Edn col-6 -->
	</div>
<!-- End Row -->

<!-- <link href="<?= base_url()?>assets/def/plugins/select2/dist/css/select2.min.css" rel="stylesheet" /> -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<!-- <script src="<?= base_url()?>assets/def/plugins/select2/dist/js/select2.min.js"></script> -->
<!-- ================== END PAGE LEVEL JS ================== -->

<script type="text/javascript">
	$('.simpan').on('click',function(){
		$('form').submit();
	});

	$('#demo').daterangepicker({
		locale: {
            format: 'DD/MM/YYYY'
        },
        
	},function(start, end, label) {
		$('.dateFrom').val(start.format('YYYY-MM-DD'));
		$('.dateTo').val(end.format('YYYY-MM-DD'));
	});

	function formatDate(date) {
	  if (date != undefined) {

	    str = date.replace(/\/|-/g,'/')
	    var date_split = str.split('/');
	    var d = date_split[2];
	    var m = date_split[1];
	    var y = date_split[0];
	    var yyyymmdd = y + m + d;

	    return [d, m, y].join('-');
	  }
	}
</script>