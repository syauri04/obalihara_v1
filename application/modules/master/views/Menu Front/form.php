<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
	    <div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="form-stuff-1">
				<!-- begin panel-heading -->
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Form</h4>
                </div>
                <!-- end panel-heading -->
                <!-- begin panel-body -->
                <div class="panel-body">
                	<form enctype="multipar/form-data" action="<?=$this->own_link.'save'?>" method="POST">
                		<input type="hidden" name="id" value="<?= isset($val)?$val->id:'';?>">
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Parent</label>
							<div class="col-md-9">
								<select class="form-control m-b-5" name="parent" >
									<option value="0" <?= isset($val) && $val->parent == 0 ?'selected':'';?> >- Pilih -</option>
									<?php
										if(!empty($parent)){
											foreach($parent as $k => $v){
												$se = isset($val) && $val->parent == $v->id ?'selected':'';
												echo "<option value='".$v->id."' $se >".$v->title."</option>";
											}
										}										
									?>
								</select>
							</div>
						</div>

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Menu Name</label>
							<div class="col-md-9">
								<input type="text" name="title" class="form-control m-b-5" autocomplete="off" value="<?= isset($val)?$val->title:'';?>" />
							</div>
						</div>
						
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Menu Description</label>
							<div class="col-md-9">
								<textarea class="form-control" name="desc" rows="3"><?= isset($val)?$val->desc:'';?></textarea>
							</div>
						</div>

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Menu Order</label>
							<div class="col-md-9">
								<input type="text" name="order" class="form-control m-b-5" autocomplete="off" value="<?= isset($val)?$val->order:'';?>" />
							</div>
						</div>

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Menu Status</label>
							<div class="col-md-9">
								<div class="radio radio-css radio-inline">
									<input type="radio" name="status" value="1" id="inlineCssRadio1" <?= isset($val) && $val->status == 1 ?'checked':'';?>/>
									<label for="inlineCssRadio1">Aktif</label>
								</div>
								<div class="radio radio-css radio-inline">
									<input type="radio" name="status" value="0" id="inlineCssRadio2" <?= isset($val) && $val->status == 2 ?'checked':'';?>/>
									<label for="inlineCssRadio2">Non Aktif</label>
								</div>
							</div>
						</div>
						

						<!-- <div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Tipe Ukuran</label>
							<div class="col-md-9">
								<select class="form-control m-b-5" name="tipe" >
									<option value="0" <?= isset($val)&&$val->tipe == 0 ? 'selected':'';?> >- Pilih -</option>
									<option value="1" <?= isset($val)&&$val->tipe == 1 ? 'selected':'';?> >Ukuran Baju & Warna</option>
									<option value="2" <?= isset($val)&&$val->tipe == 2 ? 'selected':'';?> >Ukuran Sepatu & Warna</option>
									<option value="3" <?= isset($val)&&$val->tipe == 3 ? 'selected':'';?> >Warna Saja</option>
								</select>
							</div>
						</div> -->

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3"></label>
							<div class="col-md-9">
								<a href="<?= $this->own_link?>" class="btn btn-white cancel">Cancel</a>
								<button type="button" class="btn btn-primary simpan">Simpan</button>
							</div>
						</div>
					</form>
                </div>
			</div>
            <!-- end panel -->
	    </div>
	    <!-- Edn col-6 -->
	</div>
<!-- End Row -->

<!-- <link href="<?= base_url()?>assets/def/plugins/select2/dist/css/select2.min.css" rel="stylesheet" /> -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<!-- <script src="<?= base_url()?>assets/def/plugins/select2/dist/js/select2.min.js"></script> -->
<!-- ================== END PAGE LEVEL JS ================== -->

<script type="text/javascript">
	$('.simpan').on('click',function(){
		$('form').submit();
	});
</script>