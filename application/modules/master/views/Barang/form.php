<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
	    <div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="form-stuff-1">
				<!-- begin panel-heading -->
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title f-s-16">Data Barang</h4>
                </div>
                <!-- end panel-heading -->
                <!-- begin panel-body -->
                <div class="panel-body">
                	<form>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Nama Barang</label>
							<div class="col-md-9">
								<input type="text" class="form-control m-b-5"  />
							</div>
						</div>
						
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Kategori Barang</label>
							<div class="col-md-3">
								<select class="form-control" id="catId">
									<option>- Pilih Kategori -</option>
									<?php
										foreach ($cat as $k => $v) {
											echo '<option value="'.$v->id.'">'.$v->name.'</option>';
										}
									?>
								</select>
							</div>
							<div class="col-md-3" style="display: none;" id="catId1">
								<select class="form-control" name="catId"></select>
							</div>
							<div class="col-md-3" style="display: none;" id="catId2">
								<select class="form-control" name="catId" disabled></select>
							</div>
						</div>
					</form>
                </div>
			</div>
            <!-- end panel -->
	    </div>
	    <!-- Edn col-12 -->
	    <!-- begin col-12 -->
	    <div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="form-stuff-2">
				<!-- begin panel-heading -->
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title f-s-16">Detail Barang</h4>
                </div>
                <!-- end panel-heading -->
                <!-- begin panel-body -->
            	<div class="panel-body">
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Harga</label>
						<div class="col-md-9">
							<input type="text" class="form-control m-b-5" placeholder="" />
						</div>
					</div>

					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Perkiraan Berat</label>
						<div class="col-md-9">
							<input type="text" class="form-control m-b-5" placeholder="" />
						</div>
					</div>

					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Stok</label>
						<div class="col-md-9">
							<input type="text" class="form-control m-b-5" placeholder="" />
						</div>
					</div>

					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Pembelian Minimum</label>
						<div class="col-md-9">
							<input type="text" class="form-control m-b-5" placeholder="" />
						</div>
					</div>

					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Kondisi Barang</label>
						<div class="col-md-9">
							<div class="radio radio-css radio-inline">
								<input type="radio" name="kondisi" id="konBaru" value="0" checked>
								<label for="konBaru">Baru</label>
							</div>
							<div class="radio radio-css radio-inline">
								<input type="radio" name="kondisi" id="konBekas" value="1">
								<label for="konBekas">Bekas</label>
							</div>
						</div>
					</div>

					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Barang Impor(Opsional)</label>
						<div class="col-md-9">
							<div class="radio radio-css radio-inline">
								<input type="radio" name="imporStat" id="konImpor" value="0" checked>
								<label for="konImpor">Impor</label>
							</div>
							<div class="radio radio-css radio-inline">
								<input type="radio" name="imporStat" id="kontImpor" value="1">
								<label for="kontImpor">Tidak</label>
							</div>
						</div>
					</div>
                </div>
			</div>
            <!-- end panel -->
	    </div>
	    <!-- Edn col-12 -->
	    <!-- begin col-12 -->
	    <div class="col-lg-12" id="varianPanel">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="form-stuff-3" >
				<!-- begin panel-heading -->
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title f-s-16" style="display: inline-block;">Varian Barang</h4>
                    <!-- <div >
                    </div>
                    <div style="text-align:right;">
                    	<a href="#" class="btn btn-primary btn-lg ">Atur Varian Barang</a>
                    </div> -->
                    
                </div>
                <!-- end panel-heading -->
                <!-- begin panel-body -->
            	<div class="panel-body" id="varian-panel">
            		<div class="panel-body1">
            			<h3 class="panel-title f-s-16 m-b-15">1. Tentukan varian yang ingin dibuat</h3>

            			<div class="form-group row  m-b-5">
							<label class="col-form-label col-md-3 f-s-14">Jenis Varian</label>
							<label class="col-form-label col-md-3 f-s-14">Pilihan Varian Barang</label>
						</div>

						<div class="form-group row" id="varianEle" class="varianEle">
							<label class="col-form-label col-md-3">
								<select class="form-control" name="varianType[]">
									<option value="1">Warna Varian</option>
									<option value="2">Ukuran Varian</option>
								</select>
							</label>
							<label class="col-form-label col-md-8">
								<select  name="warna[]" multiple class="form-control chs" placeholder="Select a state...">
									<?php
										// foreach ($warna as $w => $a) {
										// 	echo '<option value="'.$a->name.'" data-id="'.$a->id.'">'.$a->name.'</option>';
										// }
									?>
								</select>
							</label>
						</div>

						<button class="btn btn-primary addVarian">Tambah Jenis Varian</button>					

            		</div>
            		<hr />
            		<div class="panel-body2" id="panel-varianDet" style="display: none;">
            			<h3 class="panel-title f-s-16 m-b-15">2. Pilih Varian Barang</h3>

            			<div class="form-group row  m-b-5">
							<label class="col-form-label col-md-2 f-s-15">Ukuran</label>
							<label class="col-form-label col-md-2 f-s-15">Warna</label>
							<label class="col-form-label col-md-2 f-s-15">Harga</label>
							<label class="col-form-label col-md-1 f-s-15">Stok</label>
							<label class="col-form-label col-md-2 f-s-15">SKU</label>
							<label class="col-form-label col-md-2 f-s-15">Aksi</label>
						</div>

						<!-- <div class="form-group row" id="varianEleDet" style="display: none;">
							<label class="col-form-label col-md-2 f-s-13 m-t-5">34</label>
							<label class="col-form-label col-md-2 f-s-13 m-t-5">Hitam</label>
							<label class="col-form-label col-md-2"><input type="text" class="form-control" name="harga_varian[]"></label>
							<label class="col-form-label col-md-1"><input type="text" class="form-control" name="stok_varian[]"></label>
							<label class="col-form-label col-md-2"><input type="text" class="form-control" name="sku_varian[]"></label>
							<label class="col-form-label col-md-2"><button class="btn btn-danger btn-icon btn-circle cancel" ><i class="fa fa-trash "></i></button></label>
						</div>	 -->			

            		</div>
                </div>
			</div>
            <!-- end panel -->
	    </div>
	    <!-- Edn col-12 -->
	    <!-- begin col-12 -->
	    <div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="form-stuff-4">
				<!-- begin panel-heading -->
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title f-s-16">Spesifikasi Barang</h4>
                </div>
                <!-- end panel-heading -->
                <!-- begin panel-body -->
            	<div class="panel-body">
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Mekr (Opsional)</label>
						<div class="col-md-9">
							<input type="text" class="form-control m-b-5" placeholder="" />
						</div>
					</div>

					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Tipe</label>
						<div class="col-md-9">
							<div class="radio radio-css radio-inline">
								<input type="radio" name="kondisi" id="konBaru" value="0" checked>
								<label for="konBaru">XS</label>
							</div>
							<div class="radio radio-css radio-inline">
								<input type="radio" name="kondisi" id="konBekas" value="1">
								<label for="konBekas">S</label>
							</div>
							<div class="radio radio-css radio-inline">
								<input type="radio" name="kondisi" id="konBekas" value="1">
								<label for="konBekas">M</label>
							</div>
							<div class="radio radio-css radio-inline">
								<input type="radio" name="kondisi" id="konBekas" value="1">
								<label for="konBekas">L</label>
							</div>
							<div></div>
							<div class="radio radio-css radio-inline">
								<input type="radio" name="kondisi" id="konBekas" value="1">
								<label for="konBekas">XL</label>
							</div>
							<div class="radio radio-css radio-inline">
								<input type="radio" name="kondisi" id="konBekas" value="1">
								<label for="konBekas">XXL</label>
							</div>
							<div class="radio radio-css radio-inline">
								<input type="radio" name="kondisi" id="konBekas" value="1">
								<label for="konBekas">All Size</label>
							</div>
						</div>
					</div>

                </div>
			</div>
            <!-- end panel -->
	    </div>
	    <!-- Edn col-12 -->
	    <div class="col-lg-12">
		    <!-- begin panel -->
			<div class="panel panel-inverse">
				<div class="panel-heading">
					<div class="panel-heading-btn">
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
						<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
					</div>
					<h4 class="panel-title">Deskripsi Barang</h4>
				</div>
				<div class="panel-body">
					<textarea class="textarea form-control" id="wysihtml5" placeholder="Enter text ..." rows="12"></textarea>			
				</div>
			</div>
			<!-- end panel -->
	    </div>
	</div>
<!-- End Row -->

<link href="<?= base_url()?>assets/def/plugins/bootstrap-wysihtml5/dist/bootstrap3-wysihtml5.min.css" rel="stylesheet" />
<link href="<?= base_url()?>assets/def/plugins/chosen/css/chosen.min.css" rel="stylesheet" />

<script src="<?= base_url()?>assets/def/plugins/chosen/js/chosen.jquery.min.js"></script>
<script src="<?= base_url()?>assets/def/plugins/ckeditor/ckeditor.js"></script>
<script src="<?= base_url()?>assets/def/plugins/bootstrap-wysihtml5/dist/bootstrap3-wysihtml5.all.min.js"></script>
<script src="<?= base_url()?>assets/cust/js/Barang/barang-form-plugin.js"></script>

