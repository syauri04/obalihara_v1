<!-- begin row -->
<div class="row">
	<!-- begin col-12 -->
    <div class="col-lg-12">
		<!-- begin panel -->
		<div class="panel panel-inverse" data-sortable-id="form-stuff-1">
			<!-- begin panel-heading -->
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title f-s-16">Data Barang</h4>
            </div>
            <!-- end panel-heading -->
            <!-- begin panel-body -->
            <div class="panel-body">
            	<form>
					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Nama Barang</label>
						<div class="col-md-9">
							<input type="text" class="form-control m-b-5" value="<?= $barang->nama_barang ?>" disabled />
						</div>
					</div>
					
					<!-- <div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Kategori Barang</label>
						<div class="col-md-3"><input type="text" class="form-control m-b-5" value="" disabled /></div>
						<div class="col-md-3"><input type="text" class="form-control m-b-5"  /></div>
						<div class="col-md-3"><input type="text" class="form-control m-b-5"  /></div>
					</div> -->
				</form>
            </div>
		</div>
        <!-- end panel -->
    </div>
    <!-- End col-12 -->
    <!-- begin col-12 -->
    <div class="col-lg-12">
		<!-- begin panel -->
		<div class="panel panel-inverse" data-sortable-id="form-stuff-3">
			<!-- begin panel-heading -->
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title f-s-16">Detail Barang</h4>
            </div>
            <!-- end panel-heading -->
            <!-- begin panel-body -->
        	<div class="panel-body">

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Harga</label>
					<div class="col-md-9">
						<input type="text" class="form-control m-b-5" value="<?= $barangDetail->harga_satuan ?>" disabled />
					</div>
				</div>

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Perkiraan Berat</label>
					<div class="col-md-9">
						<input type="text" class="form-control m-b-5" value="<?= $barangDetail->perkiraan_berat ?>" disabled />
					</div>
				</div>

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Stok</label>
					<div class="col-md-9">
						<input type="text" class="form-control m-b-5" value="<?= $barangDetail->stok ?>" disabled />
					</div>
				</div>

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Pembelian Minimum</label>
					<div class="col-md-9">
						<input type="text" class="form-control m-b-5" value="<?= $barangDetail->pembeliaan_min ?>" disabled />
					</div>
				</div>

				<div class="form-group row m-b-15">
					<label class="col-form-label col-md-3">Kondisi Barang</label>
					<div class="col-md-9">
						<input type="text" class="form-control m-b-5" value="<?= ($barangDetail->kondisi_barang != 0)?'Bekas' : 'Baru'; ?>" disabled />
					</div>
				</div>

            </div>
            <!-- end panel-body -->
		</div>
        <!-- end panel -->
    </div>
    <!-- End col-12 -->

    <!-- begin col-12 -->
	<div class="col-lg-12">
		<!-- begin panel -->
		<div class="panel panel-inverse" data-sortable-id="form-stuff-3" >
			<!-- begin panel-heading -->
	        <div class="panel-heading">
	            <div class="panel-heading-btn">
	                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
	                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
	                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
	                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
	            </div>
	            <h4 class="panel-title f-s-16" style="display: inline-block;">Varian Barang</h4>
	            <!-- <div >
	            </div>
	            <div style="text-align:right;">
	            	<a href="#" class="btn btn-primary btn-lg ">Atur Varian Barang</a>
	            </div> -->
	            
	        </div>
	        <!-- end panel-heading -->
	        <!-- begin panel-body -->
	    	<div class="panel-body" id="varian-panel">
	    		<div class="panel-body1">

	    			<div class="form-group row  m-b-5">
						<label class="col-form-label col-md-3 f-s-14">Jenis Varian</label>
						<label class="col-form-label col-md-3 f-s-14">Pilihan Varian Barang</label>
					</div>

					<div class="form-group row" >
						<label class="col-form-label col-md-3">Warna</label>
						<div class="col-md-9">
							<?php
								echo isset($barangVarian['warna'])?implode($barangVarian['warna'],' , '):'-';
							?>
						</div>
						<label class="col-form-label col-md-3">Ukuran</label>
						<div class="col-md-9">
							<?php
								echo isset($barangVarian['ukuran'])?implode($barangVarian['ukuran'],' , '):'-';
							?>
						</div>
					</div>				

	    		</div>
	    		
	        </div>
		</div>
	    <!-- end panel -->
	</div>
	<!-- End col-12 -->


	<!-- begin col-12 -->
	<div class="col-lg-12">
	    <!-- begin panel -->
		<div class="panel panel-inverse">
			<div class="panel-heading">
				<div class="panel-heading-btn">
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
					<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
				</div>
				<h4 class="panel-title">Deskripsi Barang</h4>
			</div>
			<div class="panel-body">
				<textarea class="textarea form-control" id="wysihtml5" placeholder="Enter text ..." rows="12" disabled><?= $barang->deskripsi;?></textarea>			
			</div>
		</div>
		<!-- end panel -->
	</div>
    
</div>
<!-- End Row -->

<link href="<?= base_url()?>assets/def/plugins/bootstrap-wysihtml5/dist/bootstrap3-wysihtml5.min.css" rel="stylesheet" />
<link href="<?= base_url()?>assets/def/plugins/chosen/css/chosen.min.css" rel="stylesheet" />

<script src="<?= base_url()?>assets/def/plugins/chosen/js/chosen.jquery.min.js"></script>
<script src="<?= base_url()?>assets/def/plugins/ckeditor/ckeditor.js"></script>
<script src="<?= base_url()?>assets/def/plugins/bootstrap-wysihtml5/dist/bootstrap3-wysihtml5.all.min.js"></script>
<script src="<?= base_url()?>assets/cust/js/Barang/barang-form-plugin.js"></script>

