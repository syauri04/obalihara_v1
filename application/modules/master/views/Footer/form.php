<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
	    <div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="form-stuff-1">
				<!-- begin panel-heading -->
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Form</h4>
                </div>
                <!-- end panel-heading -->
                <?php //d($val);?>
                <!-- begin panel-body -->
                <div class="panel-body">
                	<form enctype="multipar/form-data" action="<?=$this->own_link.'save'?>" method="POST">
                		<input type="hidden" name="id" value="<?= isset($val)?$val->id:'';?>">
                		<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Tentang Kami</label>
							<div class="col-md-9">
								<!-- <input type="text" name="tentang_kami" class="form-control m-b-5" autocomplete="off" value="<?= isset($val)?$val->tentang_kami:'';?>" /> -->
								<textarea class="form-control" id="about" height="10" rows="10" cols="100" name="tentang_kami"><?= isset($val)?$val->tentang_kami:'';?></textarea>
							</div>
						</div>	

                		<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Kebijakan Privasi</label>
							<div class="col-md-9">
								<!-- <input type="text" name="tentang_kebijakan_privasi" class="form-control m-b-5" autocomplete="off" value="<?= isset($val)?$val->tentang_kebijakan_privasi:'';?>" /> -->
								<textarea class="form-control" id="privacy" rows="10" cols="100" name="tentang_kebijakan_privasi"><?= isset($val)?$val->tentang_kebijakan_privasi:'';?></textarea>
							</div>
						</div>	
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Terms And Condition</label>
							<div class="col-md-9">
								<!-- <input type="text" name="terms_condition" class="form-control m-b-5" autocomplete="off" value="<?= isset($val)?$val->terms_condition:'';?>" /> -->
								<textarea class="form-control" id="terms" height="10" rows="10" cols="100" name="terms_condition"><?= isset($val)?$val->terms_condition:'';?></textarea>
							</div>
						</div>	
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Costumer Service</label>
							<div class="col-md-9">
								<!-- <input type="text" name="customer_service" class="form-control m-b-5" autocomplete="off" value="<?= isset($val)?$val->customer_service:'';?>" /> -->
								<textarea class="textarea form-control"  id="wysihtml5" rows="10" cols="100" name="customer_service"><?= isset($val)?$val->customer_service:'';?></textarea>
							</div>
						</div>

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Frequenly Asked Questions</label>
							<div class="col-md-9">
								<!-- <input type="text" name="customer_service" class="form-control m-b-5" autocomplete="off" value="<?= isset($val)?$val->customer_service:'';?>" /> -->
								<textarea class="textarea form-control"  id="faq" rows="10" cols="100" name="faq"><?= isset($val)?$val->faq:'';?></textarea>
							</div>
						</div>

						<!-- <div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Layanan Kontak</label>
							<div class="col-md-9">
								
								<textarea class="form-control" height="10" rows="10" cols="100" name="layanan_kontak"><?= isset($val)?$val->layanan_kontak:'';?></textarea>
							</div>
						</div>	

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Jadi Penjual</label>
							<div class="col-md-9">
								
								<textarea class="form-control" height="10" rows="10" cols="100" name="jadi_penjual"><?= isset($val)?$val->jadi_penjual:'';?></textarea>
							</div>
						</div>	
						
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Jadi Team Obalihara</label>
							<div class="col-md-9">
								
								<textarea class="form-control" height="10" rows="10" cols="100"name="jadi_team"><?= isset($val)?$val->jadi_team:'';?></textarea>
							</div>
						</div> -->

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3"></label>
							<div class="col-md-9">
								<a href="<?= $this->own_link?>" class="btn btn-white cancel">Cancel</a>
								<button type="button" class="btn btn-primary simpan">Simpan</button>
							</div>
						</div>
					</form>
                </div>
			</div>
            <!-- end panel -->
	    </div>
	    <!-- Edn col-6 -->
	</div>
<!-- End Row -->

<link href="<?= base_url()?>assets/def/plugins/bootstrap-wysihtml5/dist/bootstrap3-wysihtml5.min.css" rel="stylesheet" />
<link href="<?= base_url()?>assets/def/plugins/chosen/css/chosen.min.css" rel="stylesheet" />

<script src="<?= base_url()?>assets/def/plugins/chosen/js/chosen.jquery.min.js"></script>
<script src="<?= base_url()?>assets/def/plugins/ckeditor/ckeditor.js"></script>
<script src="<?= base_url()?>assets/def/plugins/bootstrap-wysihtml5/dist/bootstrap3-wysihtml5.all.min.js"></script>

<script type="text/javascript">
	$('#wysihtml5').wysihtml5();
	$('#privacy').wysihtml5();
	$('#terms').wysihtml5();
	$('#about').wysihtml5();
	$('#faq').wysihtml5();

	$('.simpan').on('click',function(){
		$('form').submit();
	});

	$('#demo').daterangepicker({
		locale: {
            format: 'DD/MM/YYYY'
        },
        startDate: formatDate($('.dateFrom').val()),
    	endDate: formatDate($('.dateTo').val()),
	},function(start, end, label) {
		$('.dateFrom').val(start.format('YYYY-MM-DD'));
		$('.dateTo').val(end.format('YYYY-MM-DD'));
	});

	function formatDate(date) {
	  if (date != undefined) {

	    str = date.replace(/\/|-/g,'/')
	    var date_split = str.split('/');
	    var d = date_split[2];
	    var m = date_split[1];
	    var y = date_split[0];
	    var yyyymmdd = y + m + d;

	    return [d, m, y].join('-');
	  }
	}
</script>