

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.2/d3.min.js"></script>
<script src="<?= base_url()?>assets/def/plugins/nvd3/build/nv.d3.js"></script>
<script src="<?= base_url()?>assets/def/plugins/jquery-jvectormap/jquery-jvectormap.min.js"></script>
<script src="<?= base_url()?>assets/def/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js"></script>
<script src="<?= base_url()?>assets/def/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js"></script>
<script src="<?= base_url()?>assets/def/plugins/gritter/js/jquery.gritter.js"></script>
<script src="<?= base_url()?>assets/def/js/demo/dashboard-v2.min.js"></script> -->
<!-- ================== END PAGE LEVEL JS ================== -->


<div class="panel panel-inverse" data-sortable-id="table-basic-1">
	<!-- begin panel-heading -->
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
        <h4 class="panel-title"><a href="<?= $this->own_link.'add'?>"><button type="button" class="btn btn-lime btn-sm">Add</button></a></h4>
    </div>
    <!-- end panel-heading -->
    <!-- begin panel-body -->
    <div class="panel-body">
    	<!-- begin table-responsive -->
		<table id="data-table-default" class="table table-striped ">
			<thead>
				<tr>
					<th>Costumer Service</th>
					<th>Layanan Kontak</th>
					<th>Jadi Penjual</th>
					<th>Kebijakan Privasi</th>
					<th>Terms And Condition</th>
					<th>Tentang Kami</th>
					<th>Jadi Team Obalihara</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					foreach ($d as $k => $v) {
				?>
				<tr>
					<td><?= $v->customer_service;?></td>
					<td><?= $v->layanan_kontak;?></td>
					<td><?= $v->jadi_penjual;?></td>
					<td><?= $v->tentang_kebijakan_privasi;?></td>
					<td><?= $v->terms_condition;?></td>
					<td><?= $v->tentang_kami;?></td>
					<td><?= $v->jadi_team;?></td>
					<td><a href="#" class="btn btn-primary btn-icon btn-circle edit" data-id="<?= $v->id;?>" ><i class="fa fa-edit "></i></a> &nbsp; &nbsp; &nbsp; <a href="#" class="btn btn-danger btn-icon btn-circle trash" data-id="<?= $v->id;?>" ><i class="fa fa-trash "></i></a></td>
				</tr>
				<?php
					}
				?>
			</tbody>
		</table>
		<!-- end table-responsive -->
    </div>
    <!-- end panel-body -->
</div>

<link href="<?= base_url()?>assets/def/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="<?= base_url()?>assets/def/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?= base_url()?>assets/def/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?= base_url()?>assets/def/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
<script src="<?= base_url()?>assets/def/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="<?= base_url()?>assets/def/js/demo/table-manage-default.demo.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script type="text/javascript">
	TableManageDefault.init();

	
</script>