<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
	    <div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="form-stuff-1">
				<!-- begin panel-heading -->
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Form</h4>
                </div>
                <!-- end panel-heading -->
                <!-- begin panel-body -->
                <div class="panel-body">
                	<form>
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Merk Name</label>
							<div class="col-md-9">
								<input type="email" class="form-control m-b-5"  />
							</div>
						</div>
						
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Merk Description</label>
							<div class="col-md-9">
								<textarea class="form-control" rows="3"></textarea>
							</div>
						</div>

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3"></label>
							<div class="col-md-9">
								<button type="button" class="btn btn-white">Cancel</button>
								<button type="button" class="btn btn-primary">Simpan</button>
							</div>
						</div>
					</form>
                </div>
			</div>
            <!-- end panel -->
	    </div>
	    <!-- Edn col-6 -->
	</div>
<!-- End Row -->