<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
	    <div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="form-stuff-1">
				<!-- begin panel-heading -->
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Form</h4>
                </div>
                <!-- end panel-heading -->
                <!-- begin panel-body -->
                <div class="panel-body">
                	<form enctype="multipar/form-data" action="<?=$this->own_link.'save'?>" method="POST">
                		<input type="hidden" name="id" value="<?= isset($val)?$val->id:'';?>">
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Parent Kategori</label>
							<div class="col-md-9">
								<select class="form-control m-b-5" name="parent_id" >
									<option value="0" <?= isset($val) && $val->parent_id == 0 ?'selected':'';?> >- Pilih -</option>
									<?php
										if(!empty($parent)){
											foreach($parent as $k => $v){
												$se = isset($val) && $val->parent_id == $v->id ?'selected':'';
												echo "<option value='".$v->id."' $se >".$v->name."</option>";
											}
										}										
									?>
								</select>
							</div>
						</div>

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Kategori Name</label>
							<div class="col-md-9">
								<input type="text" name="name" class="form-control m-b-5" autocomplete="off" value="<?= isset($val)?$val->name:'';?>" />
							</div>
						</div>
						
<!-- 						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Kategori Description</label>
							<div class="col-md-9">
								<textarea class="form-control" name="cat_description" rows="3"></textarea>
							</div>
						</div> -->

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Tipe Ukuran</label>
							<div class="col-md-9">
								<select class="form-control m-b-5" name="tipe" >
									<option value="0" <?= isset($val)&&$val->tipe == 0 ? 'selected':'';?> >- Pilih -</option>
									<option value="1" <?= isset($val)&&$val->tipe == 1 ? 'selected':'';?> >Ukuran Baju & Warna</option>
									<option value="2" <?= isset($val)&&$val->tipe == 2 ? 'selected':'';?> >Ukuran Sepatu & Warna</option>
									<option value="3" <?= isset($val)&&$val->tipe == 3 ? 'selected':'';?> >Warna Saja</option>
								</select>
							</div>
						</div>

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3"></label>
							<div class="col-md-9">
								<a href="<?= $this->own_link?>" class="btn btn-white cancel">Cancel</a>
								<button type="button" class="btn btn-primary simpan">Simpan</button>
							</div>
						</div>
					</form>
                </div>
			</div>
            <!-- end panel -->
	    </div>
	    <!-- Edn col-6 -->
	</div>
<!-- End Row -->

<!-- <link href="<?= base_url()?>assets/def/plugins/select2/dist/css/select2.min.css" rel="stylesheet" /> -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<!-- <script src="<?= base_url()?>assets/def/plugins/select2/dist/js/select2.min.js"></script> -->
<!-- ================== END PAGE LEVEL JS ================== -->

<script type="text/javascript">
	$('.simpan').on('click',function(){
		$('form').submit();
	});
</script>