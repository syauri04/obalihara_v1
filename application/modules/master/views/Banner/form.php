<!-- begin row -->
	<div class="row">
		<!-- begin col-6 -->
	    <div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="form-stuff-1">
				<!-- begin panel-heading -->
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Form</h4>
                </div>
                <!-- end panel-heading -->
                <!-- begin panel-body -->
                <div class="panel-body">
                		<input type="hidden" name="id" value="<?= isset($val)?$val->id:'';?>">
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Banner Carousel Max(5)</label>
							<div class="col-md-10">
								<form action="<?=$this->own_link.'uploadCarousel'?>" class="dropzone needsclick" id="demoUpload1">
									<input type="hidden" name="status" id="status" value="1">
									<input type="hidden" name="bannerCarrousel" id="status" value="1">
									<input type="hidden" name="bannerSide" id="status" value="0">
									<input type="hidden" name="bannerMid" id="status" value="0">
									<input type="hidden" name="bannerMidSmall" id="status" value="0">
									<input type="hidden" name="id" >
		                            <div class="dz-message needsclick">
		                            	Drop files <b>here</b> or <b>click</b> to upload.<br />
		                                <span class="dz-note needsclick">
		                                    (This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)
		                                </span>
		                            </div>
		                        </form>
							</div>
						</div>

						<form action="#" method="post" id="urlCarouselForm" enctype="multipart/form-data">
							<div id="inputCarousel"></div>
							
							<div class="form-group row m-b-15">
								<label class="col-form-label col-md-2"></label>
								<div class="col-md-10">
									<a href="#" class="btn btn-success btnCarousel" style="width: 100%;">UPDATE</a>
		                        </div>
							</div>
						</form>

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Banner Left Max(2)</label>
							<div class="col-md-10">
								<form action="<?=$this->own_link.'uploadCarousel'?>" class="dropzone needsclick" id="demoUpload2">
									<input type="hidden" name="status" id="status" value="1">
									<input type="hidden" name="bannerCarrousel" id="status" value="0">
									<input type="hidden" name="bannerSide" id="status" value="1">
									<input type="hidden" name="bannerMid" id="status" value="0">
									<input type="hidden" name="bannerMidSmall" id="status" value="0">
									<input type="hidden" name="id" >
		                            <div class="dz-message needsclick">
		                            	Drop files <b>here</b> or <b>click</b> to upload.<br />
		                                <span class="dz-note needsclick">
		                                    (This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)
		                                </span>
		                            </div>
		                        </form>
							</div>
							
						</div>

						

						<form action="#" method="post" enctype="multipart/form-data" id="urlSideForm">
							<div id="inputSide"></div>
							
							<div class="form-group row m-b-15">
								<label class="col-form-label col-md-2"></label>
								<div class="col-md-10">
									<a href="#" class="btn btn-success btnSide" style="width: 100%;">UPDATE</a>
		                        </div>
							</div>
						</form>

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Banner Mid Max(1)</label>
							<div class="col-md-10">
								<form action="<?=$this->own_link.'uploadCarousel'?>" class="dropzone needsclick" id="demoUpload3">
									<input type="hidden" name="status" id="status" value="1">
									<input type="hidden" name="bannerCarrousel" id="status" value="0">
									<input type="hidden" name="bannerSide" id="status" value="0">
									<input type="hidden" name="bannerMid" id="status" value="1">
									<input type="hidden" name="bannerMidSmall" id="status" value="0">
									<input type="hidden" name="id" >
		                            <div class="dz-message needsclick">
		                            	Drop files <b>here</b> or <b>click</b> to upload.<br />
		                                <span class="dz-note needsclick">
		                                    ( This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded. )
		                                </span>
		                            </div>
		                        </form>
							</div>
							
						</div>

						<form action="#" method="post" enctype="multipart/form-data" id="urlMidForm">
							<div id="inputMid"></div>
							
							<div class="form-group row m-b-15">
								<label class="col-form-label col-md-2"></label>
								<div class="col-md-10">
									<a href="#" class="btn btn-success btnMid" style="width: 100%;">UPDATE</a>
		                        </div>
							</div>
						</form>

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Banner Mid Small Max(3)</label>
							<div class="col-md-10">
								<form action="<?=$this->own_link.'uploadCarousel'?>" class="dropzone needsclick" id="demoUpload4">
									<input type="hidden" name="status" id="status" value="1">
									<input type="hidden" name="bannerCarrousel" id="status" value="0">
									<input type="hidden" name="bannerSide" id="status" value="0">
									<input type="hidden" name="bannerMid" id="status" value="0">
									<input type="hidden" name="bannerMidSmall" id="status" value="1">
									<input type="hidden" name="id" >
		                            <div class="dz-message needsclick">
		                            	Drop files <b>here</b> or <b>click</b> to upload.<br />
		                                <span class="dz-note needsclick">
		                                    ( This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded. )
		                                </span>
		                            </div>
		                        </form>
							</div>
							
						</div>

						<form action="#" method="post" enctype="multipart/form-data" id="urlMidSmallForm">
							<div id="inputMidSmall"></div>
							
							<div class="form-group row m-b-15">
								<label class="col-form-label col-md-2"></label>
								<div class="col-md-10">
									<a href="#" class="btn btn-success btnMidSmall" style="width: 100%;">UPDATE</a>
		                        </div>
							</div>
						</form>

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2"></label>
							<div class="col-md-9">
								<a href="<?= $this->own_link?>" class="btn btn-primary cancel">Back</a>
							</div>
						</div>
                </div>
			</div>
            <!-- end panel -->
	    </div>
	    <!-- Edn col-6 -->
	</div>
<!-- End Row -->
<div class="ownlink" style="display: hidden;"><?= $this->own_link; ?></div>

<link href="<?= base_url()?>assets/def/plugins/dropzone/dropzone.css" rel="stylesheet" />

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?= base_url()?>assets/def/plugins/dropzone/min/dropzone.min.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<style>
  .dz-max-files-reached {background-color: red};
</style>

<script type="text/javascript">
	var ownLink = $('.ownlink').text();
	$('.btnCarousel').on('click',function(){
		$('.urlCarousel').each(function(){
			var val = { value : $(this).val() , id : $(this).data('id') };

			$.post(ownLink+'updateUrl',val,function(res){
				// console.log(res);
				if(res == 200){
					alert('SUCCESS');
				} else {
					alert('ERROR');
				}
			});

		});
	});

	$('.btnSide').on('click',function(){
		$('.urlSide').each(function(){
			var val = { value : $(this).val() , id : $(this).data('id') };

			$.post(ownLink+'updateUrl',val,function(res){
				if(res == 200){
					alert('SUCCESS');
				} else {
					alert('ERROR');
				}
			});

		});
	});

	$('.btnMid').on('click',function(){
		$('.urlMid').each(function(){
			var val = { value : $(this).val() , id : $(this).data('id') };

			$.post(ownLink+'updateUrl',val,function(res){
				if(res == 200){
					alert('SUCCESS');
				} else {
					alert('ERROR');
				}
			});

		});
	});

	$('.btnMidSmall').on('click',function(){
		$('.urlMidSmall').each(function(){
			var val = { value : $(this).val() , id : $(this).data('id') };

			$.post(ownLink+'updateUrl',val,function(res){
				if(res == 200){
					alert('SUCCESS');
				} else {
					alert('ERROR');
				}
			});

		});
	});


	Dropzone.options.demoUpload1 = {
	  paramName: "file", // The name that will be used to transfer the file
	  maxFiles: 5,
	  maxFilesize: 5, // MB
	  paramName: 'userImage',
	  addRemoveLinks: true,
	  init: function(){

	  	this.on("maxfilesexceeded", function(file){
	        alert("No more files please!");
	    });

	  	this.on("sending", function(file, xhr, formData) {
	       var value = $('form#demoUpload1 #key').val();
	       formData.append("key", value); // Append all the additional input data of your form here!
	    });

	  	this.on("success", function (file, response) {

            var t = '<div class="form-group row m-b-15">';
				t +='<label class="col-form-label col-md-2">URL Banner Carousel</label>';
				t +='<div class="col-md-10">';
				t +='<input type="text" name="urlGambar[]" class="form-control urlCarousel" data-id="'+response+'" placeholder="URL GAMBAR">';
				t +='</div>';
				t +='</div>';

            $('#urlCarouselForm #inputCarousel').append(t);
        });

	  }
	};

	Dropzone.options.demoUpload2 = {
		paramName: "file", // The name that will be used to transfer the file
		maxFiles: 2,
		maxFilesize: 2, // MB
		thumbnailWidth: 350,
		thumbnailHeight: 185,
		// resizeWidth: 350,
		// resizeHeight: 185,
		addRemoveLinks: true,
		paramName: 'userImage',
		init: function(){

		this.on("maxfilesexceeded", function(file){
		    alert("No more files please!");
		});

		this.on("sending", function(file, xhr, formData) {
		   var value = $('form#demoUpload1 #key').val();
		   formData.append("key", value); // Append all the additional input data of your form here!
		});

		this.on("success", function (file, response) {
			console.log(response);
		    var t = '<div class="form-group row m-b-15">';
				t +='<label class="col-form-label col-md-2">URL Banner Side</label>';
				t +='<div class="col-md-10">';
				t +='<input type="text" name="urlSide[]" class="form-control urlSide" data-id="'+response+'" placeholder="URL GAMBAR">';
				t +='</div>';
				t +='</div>';

		    $('#urlSideForm #inputSide').append(t);
		});

		}
	};

	Dropzone.options.demoUpload3 = {
		paramName: "file", // The name that will be used to transfer the file
		maxFiles: 1,
		maxFilesize: 2, // MB
		thumbnailWidth: 350,
		thumbnailHeight: 185,
		// resizeWidth: 350,
		// resizeHeight: 185,
		addRemoveLinks: true,
		paramName: 'userImage',
		init: function(){

		this.on("maxfilesexceeded", function(file){
		    alert("No more files please!");
		});

		this.on("sending", function(file, xhr, formData) {
		   var value = $('form#demoUpload1 #key').val();
		   formData.append("key", value); // Append all the additional input data of your form here!
		});

		this.on("success", function (file, response) {
		    var t = '<div class="form-group row m-b-15">';
				t +='<label class="col-form-label col-md-2">URL Banner Side</label>';
				t +='<div class="col-md-10">';
				t +='<input type="text" name="urlMid[]" class="form-control urlMid" data-id="'+response+'" placeholder="URL GAMBAR">';
				t +='</div>';
				t +='</div>';

		    $('#urlMidForm #inputMid').append(t);
		});

		}
	};

	Dropzone.options.demoUpload4 = {
		paramName: "file", // The name that will be used to transfer the file
		maxFiles: 3,
		maxFilesize: 2, // MB
		thumbnailWidth: 350,
		thumbnailHeight: 185,
		// resizeWidth: 350,
		// resizeHeight: 185,
		addRemoveLinks: true,
		paramName: 'userImage',
		init: function(){

		this.on("maxfilesexceeded", function(file){
		    alert("No more files please!");
		});

		this.on("sending", function(file, xhr, formData) {
		   var value = $('form#demoUpload1 #key').val();
		   formData.append("key", value); // Append all the additional input data of your form here!
		});

		this.on("success", function (file, response) {
		    var t = '<div class="form-group row m-b-15">';
				t +='<label class="col-form-label col-md-2">URL Banner Mid Small</label>';
				t +='<div class="col-md-10">';
				t +='<input type="text" name="urlMidSmall[]" class="form-control urlMidSmall" data-id="'+response+'" placeholder="URL GAMBAR">';
				t +='</div>';
				t +='</div>';

		    $('#urlMidSmallForm #inputMidSmall').append(t);
		});

		}
	};

</script>