<?php

class Merek_model extends CI_Model{

    function __construct(){
    	parent::__construct();
  	}

    function getAll(){
        $this->db->where('is_trash <>',1);
        return $this->db->get('barang_kategori')->result();
    }

}
