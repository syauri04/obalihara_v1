<?php

class Goods_model extends CI_Model{

    function __construct(){
    	parent::__construct();
  	}

    // function getAll(){
    //     $this->db->where('is_trash <>',1);
    //     return $this->db->get('barang_kategori')->result();
    // }

    function getCat($id=''){
    	
    	if($id != ''){
    		$a = $this->db->where('parent_id',$id)->get('cp_kategori_barang')->result();
    	} else {
    		$a = $this->db->where('parent_id',0)->get('cp_kategori_barang')->result();
    	}
    	
    	return $a;

    }

    function getOpt($id,$t=''){
    	
        if($id == 1){
            $a = $this->db->get('cp_warna')->result();
        } else {
            $a = $this->db->get_where('cp_ukuran',['kategori_barang_tipe' => $t])->result();
        }
    	
    	
    	return $a;

    }

    function getDetail($id){

        $barang       = $this->db->get_where('cp_barang',['id'=>$id])->row();
        $barangDetail = $this->db->get_where('cp_barang_detail',['barang_id'=>$id])->row();
        $barangGambar = $this->db->get_where('cp_barang_gambar',['barang_id'=>$id])->result();
        $barangVarian = $this->db->get_where('cp_barang_varian',['barang_id'=>$id])->result();

        $bv = [];
        $bc = [];

        foreach ($barangVarian as $b => $v) {

            $a = explode(',',$v->varian_barang);

            if($v->jenis_varian == 'warna'){
                
                foreach ($a as $k => $w) {
                    if($w != 0){
                        $bc['warna'][] = produk::warna($w);
                    }
                }   
                
            } else {

                foreach ($a as $k => $u) {
                    if($u != 0){
                        $bc['ukuran'][] = produk::ukuran($u);
                    }
                }

            }

        }

        $bv = [
            'barang'        => $barang,
            'barangDetail'  => $barangDetail,
            'barangGambar'  => $barangGambar,
            'barangVarian'  => $bc
        ];

        return $bv;

    }

}
