<?php

class Coupon_model extends CI_Model{

    function __construct(){
    	parent::__construct();
  	}

    function getCoupon(){
    	$this->db->where('is_trash <>',1)
    	->order_by('id','ASC');
        return $this->db->get('cp_coupon')->result();
    }

    function deleteCheck($id){
    	$this->db->where('parent_id',$id);
    	$a = $this->db->get('cp_kategori_barang')->result();

    	if(count($a) > 0){
    		return false;
    	} else {
    		return true;
    	}
    }

}
