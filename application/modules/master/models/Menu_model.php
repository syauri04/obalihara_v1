<?php

class Menu_model extends CI_Model{

    function __construct(){
    	parent::__construct();
  	}

    function getMenu(){
    	$this->db->where('is_trash <>',1)
    	->order_by('id','ASC');
        return $this->db->get('cp_kategori')->result();
    }

    function deleteCheck($id){
    	$this->db->where('parent',$id);
    	$a = $this->db->get('cp_kategori')->result();

    	if(count($a) > 0){
    		return false;
    	} else {
    		return true;
    	}
    }

}
