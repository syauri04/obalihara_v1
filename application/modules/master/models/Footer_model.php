<?php

class Footer_model extends CI_Model{

    function __construct(){
    	parent::__construct();
  	}

    function getdata(){
        return $this->db->get('cp_footer_config')->result();
    }
}
