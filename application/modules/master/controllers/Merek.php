<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH."libraries/AdminController.php");

class Merek extends AdminController {

	
	function __construct()    
	{
		parent::__construct();    
		$this->_set_action();
		$this->_set_action(array("edit","delete"),"ITEM");
		$this->_set_title( 'Merek List ' );
		$this->load->model('Merek_model','MM');
		$this->folder_view = "Merek";

	}
	
	public function index()
	{
		// $data = [
		// 	'd' => $this->db->get('cp_goods')->result(),
		// ];

		$this->_v($this->folder_view.'/index');
	}

	function add()
	{
		// $data = [
		// 	'd' => $this->db->get('cp_goods')->result(),
		// ];

		$this->_v($this->folder_view.'/form');
	}
}
