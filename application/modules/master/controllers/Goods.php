<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH."libraries/AdminController.php");

class Goods extends AdminController {

	
	function __construct()    
	{
		parent::__construct();    
		$this->_set_action();
		$this->_set_action(array("edit","delete"),"ITEM");
		$this->_set_title( 'Goods List ' );
		$this->load->model('Goods_model','GM');
		$this->folder_view = "Barang";

	}
	
	public function index()
	{
		
		$data = [
			'data' => $this->db->get_where('cp_barang',['is_trash'=>0])->result(),
		];
		// d($data);
		$this->_v($this->folder_view.'/index',$data);
	}

	function add()
	{
		$data = [
			'cat' 	=> $this->GM->getCat(),
		];

		$this->_v($this->folder_view.'/form',$data);
	}

	function detail($id=''){

		if($id != ''){

			$data = $this->GM->getDetail($id);

		}
		// d($data);
		$this->_v($this->folder_view.'/detail',$data);

	}

	function delete($id=''){

		$id=dbClean(trim($id));

		if(trim($id) != ''){
			$o = $this->db->update('cp_barang',["is_trash"=>1],["id"=> idClean($id)] );
		}
		redirect($this->own_link);

	}

	function catChild(){
		
		$cat = $this->GM->getCat($_POST['id']);
		
		echo json_encode($cat);
	}

	function getWarna(){
		$t = '';
		$os = $this->GM->getOpt(1);
		foreach ($os as $k => $v) {
			$t .= '<option value="'.$v->name.'" data-id="'.$v->id.'">'.$v->name.'</option>';
		}

		echo $t;
	}

	function verifyVarian(){

		$opt = '<option value="1">Warna Varian</option>';
		$os = [];

		if($_POST['val'] == 1){
			$opt = '<option value="2">Ukuran Varian</option>';
			$os = $this->GM->getOpt(2,$_POST['tipe']);
		}

		$t = '';
		$t .= '<div class="form-group row" id="varianEle">';
		$t .= '<label class="col-form-label col-md-3">';
		$t .= '<select class="form-control" name="varianType[]">';
		$t .= $opt;
		$t .= '</select>';
		$t .= '</label>';
		$t .= '<label class="col-form-label col-md-8">';
		$t .= '<select  name="ukuran[]" multiple class="form-control chs" placeholder="Select a state...">';
		foreach ($os as $k => $v) {
			$t .= '<option value="'.$v->name.'" data-id="'.$v->id.'">'.$v->name.'</option>';
		}
		$t .= '</select>';
		$t .= '</label>';
		$t .= '<label class="col-form-label col-md-1" >';
		$t .= '<button class="btn btn-danger btn-icon btn-circle cancel" ><i class="fa fa-trash "></i></button>';
		$t .= '</label>';
		$t .= '</div>';

		echo $t;
	}

	// function varianDet(){
	// 	dd($_POST);
	// }
}
