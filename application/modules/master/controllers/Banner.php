<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH."libraries/AdminController.php");

class Banner extends AdminController {

	
	function __construct()    
	{
		parent::__construct();    
		$this->_set_action();
		$this->_set_action(array("edit","delete"),"ITEM");
		$this->_set_title( 'Goods List ' );
		$this->load->helper("file");
		$this->load->model('Goods_model','GM');
		$this->folder_view = 'Banner';

		$this->upload_resize  = array(
			array('name'	=> 'thumb','width'	=> 100, 'height'	=> 100, 'quality'	=> '75%'),
			array('name'	=> 'small','width'	=> 350, 'height'	=> 185, 'quality'	=> '85%'),
			array('name'	=> 'large','width'	=> 730, 'height'	=> 395, 'quality'	=> '100%')
		);

		$this->upload_path="./assets/img/banner/";

	}
	
	public function index()
	{
	    
		$data = [
			'img' => $this->db->get_where('cp_banner',['is_trash' => 0])->result(),
		];
	
		$this->_v($this->folder_view.'/index',$data);
	}

	function add()
	{
		$this->_v($this->folder_view.'/form');
	}

	function delete($id){
		$a = $this->db->get_where('cp_banner',['id' => $id])->row();

		for($i = 0;$i<3;$i++){
			if($i == 0){
				$t = 'large';
			} else if( $i == 1){
				$t = 'small';
			} else {
				$t = 'thumb';
			}
			$path = str_replace(' ','',FCPATH.'assets/img/banner/ '.$t.'/ '.$a->image);
			// $path = str_replace(' ','',FCPATH.'assets\img\banner\ '.$t.'\ '.$a->image);
			$c = unlink($path);
		}		
		
		if($c){
			$d = $this->db->delete('cp_banner',['id' => $id]);
			redirect($this->own_link."?msg=".urldecode('Delete Image success')."&type_msg=success");
		}
		redirect($this->own_link."?msg=".urldecode('Delete Image Error')."&type_msg=Error");
		
	}

	function uploadCarousel(){
		// d($_FILES);

		$data = [
			'status' 	=> $_POST['status'],
			'carousel'	=> $_POST['bannerCarrousel'],
			'side'		=> $_POST['bannerSide'],
			'mid'		=> $_POST['bannerMid'],
			'mid_small'	=> $_POST['bannerMidSmall'],
		];

		$this->DATA->table = 'cp_banner';
		$a = $this->_save_master( 
			$data,
			array(
				'id' => dbClean($_POST['id'])
			),
			dbClean($_POST['id'])			
		);

		$id=$a['id'];

		$upl = $this->_uploaded(
		array(
			'id'		=> $id ,
			'input'		=> array_keys($_FILES)[0],
			'param'		=> array(
							'field' => 'image', 
							'par'	=> array('id' => $id)
						)
		));

		echo json_encode($id);
	}

	function updateUrl(){

		$msg = '';

		if($_POST['value'] != '' && $_POST['id'] != ''){
			$this->db->update('cp_banner',['url' => $_POST['value']],['id' => $_POST['id']]);
			$msg = 200;
		} else {
			$msg = 500;
		}

		echo json_encode($msg);
	}

	// function varianDet(){
	// 	dd($_POST);
	// }
}
