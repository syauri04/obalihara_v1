<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH."libraries/AdminController.php");

class Footer extends AdminController {

	
	function __construct()    
	{
		parent::__construct();    
		$this->_set_action();
		$this->_set_action(array("edit","delete"),"ITEM");
		$this->_set_title( 'Footer Config List ' );
		$this->load->model('footer_model','FM');
		$this->folder_view = "Footer";
		$this->DATA->table = "cp_footer_config";

	}
	
	public function index()
	{
		redirect('master/footer/edit/1');
		$data = [
			'd' => $this->FM->getdata(),
		];

		$this->_v($this->folder_view.'/index',$data);
	}

	function add()
	{
		$this->_v($this->folder_view.'/form');
	}

	function edit($id='') {

		$id=dbClean(trim($id));

		if(trim($id)!=''){
			$this->data_form = $this->DATA->data_id(array(
					'id'	=> $id
				));
			
			$this->_v($this->folder_view."/form");
		}else{
			redirect($this->own_link);
		}

	}

	function delete($id){
		$id=dbClean(trim($id));

		if(trim($id) != ''){
			$c = $this->CM->deleteCheck($id);
			if($c){
				$this->db->update("cp_kategori_barang",array("is_trash"=>1),array("id"=>$id));
			} else {
				redirect($this->own_link."?msg=".urldecode('Delete data Kategori Error')."&type_msg=Error");
			}
			
		}
		redirect($this->own_link."?msg=".urldecode('Delete data Kategori success')."&type_msg=success");
	}

	function save(){
		// d($_POST);
		foreach ($_POST as $k => $v) {
			$d[$k] = $_POST[$k];
		}

		if ($_POST['id'] == '') unset($d['id']);

		$a = $this->_save_master( 
			$d,
			array(
				'id' => dbClean($_POST['id'])
			),
			dbClean($_POST['id'])			
		);

		redirect($this->own_link."?msg=".urldecode('Save data kategori success')."&type_msg=success");
		
	}
}
