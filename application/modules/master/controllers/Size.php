<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH."libraries/AdminController.php");

class Size extends AdminController {

	function __construct()    
	{
		parent::__construct();    
		$this->_set_action();
		$this->_set_action(array("edit","delete"),"ITEM");
		$this->_set_title( 'Size List ' );
		$this->load->model('Size_model','SM');
		$this->folder_view = "Size";
		$this->DATA->table = "cp_ukuran";
	}
	
	public function index()
	{
		$data = [
			'd' => $this->SM->getAll(),
		];

		$this->_v($this->folder_view.'/index',$data);
	}

	function add()
	{
		// $data = [
		// 	'd' => $this->db->get('cp_goods')->result(),
		// ];

		$this->_v($this->folder_view.'/form');
	}
}
