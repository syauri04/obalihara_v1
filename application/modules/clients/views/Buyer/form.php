<form method="POST" enctype="multipart/form-data" action="<?= $this->own_link.'save'; ?>">
<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
	    <div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="form-stuff-1">
				<!-- begin panel-heading -->
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title f-s-16">Data Personal</h4>
                </div>
                <!-- end panel-heading -->
                <!-- begin panel-body -->
                <div class="panel-body">
                		
                		<input type="hidden" name="user_id" value="<?= isset($val)?$val->user_id:'';?>">
                		<input type="hidden" name="buyer_id" value="<?= isset($val)?$val->id:'';?>">
                		<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Photo</label>
							<div class="col-md-9">
								<?php if( isset($val->photo) && trim($val->photo)!="" ){ ?>

									<a href="<?php echo get_image(base_url()."assets/img/buyer/large/".$val->photo);?>" data-lightbox="image-1" data-title="Vendor Photo">
										<img alt="" src="<?php echo get_image(base_url()."assets/img/buyer/thumb/".$val->photo);?>" style="height:90px;width:80px" class="img-polaroid">
									</a>

								<?php } ?>
								<input type="file" name="photo"/>
							</div>
						</div>

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Nama Lengkap</label>
							<div class="col-md-9">
								<input type="text" class="form-control m-b-5" name="nama_lengkap" value="<?= isset($val)?$val->nama_lengkap:'';?>"/>
							</div>
						</div>
						
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">No. Telp</label>
							<div class="col-md-9">
								<input type="text" class="form-control m-b-5" name="telp" value="<?= isset($val)?$val->telp:'';?>"/>
							</div>
						</div>

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Email</label>
							<div class="col-md-9">
								<input type="text" class="form-control m-b-5" name="email" value="<?= isset($val)?$val->email:'';?>"/>
							</div>
						</div>

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-3">Negara</label>
							<div class="col-md-9">
								<select class="form-control select2" name="country">
									<option>- Pilih -</option>
									<?php
										foreach($country as $c => $v){
											$se = isset($val)&&$val->country == $v->country_id?'selected':'';
											echo '<option value="'.$v->country_id.'" '.$se.' >'.$v->short_name.'</option>';
										}
									?>
								</select>
							</div>
						</div>
						

                </div>
			</div>
            <!-- end panel -->
	    </div>
	    <!-- End col-12 -->
	    <!-- begin col-12 -->
	    <div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="form-stuff-2">
				<!-- begin panel-heading -->
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title f-s-16">Data Alamat</h4>
                </div>
                <!-- end panel-heading -->
                <!-- begin panel-body -->
            	<div class="panel-body">

            		<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Title</label>
						<div class="col-md-9">
							<input type="text" class="form-control m-b-5" placeholder="E.g. Alamat Kantor" name="title[]" value="<?= isset($da)?$da->title:'';?>" />
						</div>
					</div>

					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Provinsi</label>
						<div class="col-md-9">
							<select class="form-control chosen" name="provinsi_id[]" id="provinsiID">
								<option>- Pilih Provinsi -</option>
								<?php
									foreach($prov as $c => $v){
										$se = isset($da)&&$da->provinsi_id == $v->id?'selected':'';
										echo '<option value="'.$v->id.'" '.$se.' >'.$v->nama_provinsi.'</option>';
									}
								?>
							</select>
						</div>
					</div>

					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Kota / Kabupaten</label>
						<div class="col-md-9">
							<select class="form-control chosen" name="kota_id[]" id="kotaID">
								<option>- Pilih Kota -</option>
								<?php
									foreach($city as $c => $v){
										$se = isset($da)&&$da->kota_id == $v->id?'selected':'';
										echo '<option value="'.$v->id.'" '.$se.' >'.$v->kota_kab.' '.$v->nama_kota.'</option>';
									}
								?>
							</select>
						</div>
					</div>

					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Alamat</label>
						<div class="col-md-9">
							<textarea class="form-control" name="alamat[]"><?= isset($da)?$da->alamat:'';?></textarea>
						</div>
					</div>

					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Kode Pos</label>
						<div class="col-md-9">
							<input type="text" class="form-control m-b-5" name="post_code[]" value="<?= isset($da)?$da->post_code:'';?>" />
						</div>
					</div>

                </div>
			</div>
            <!-- end panel -->
	    </div>
	    <!-- End col-12 -->
	    <!-- begin col-12 -->
	    <div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="form-stuff-2">
				<!-- begin panel-heading -->
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title f-s-16">Data User</h4>
                </div>
                <!-- end panel-heading -->
                <!-- begin panel-body -->
            	<div class="panel-body">

					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Username</label>
						<div class="col-md-9">
							<input type="text" class="form-control m-b-5" name="username" value="<?= isset($du)?$du->username:'';?>" <?= isset($du)&&!empty($du)?'disabled':'';?> />
						</div>
					</div>

					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3">Password</label>
						<div class="col-md-9">
							<input type="password" class="form-control m-b-5" name="password" value="<?= isset($du)?$du->user_password:'';?>" <?= isset($du)&&!empty($du)?'disabled':'';?>/>
						</div>
					</div>

					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-3"></label>
						<div class="col-md-9">
							<a href="<?= $this->own_link?>" class="btn btn-default cancel">Cancel</a>
							<button type="submit" class="btn btn-primary simpan">Simpan</button>
						</div>
					</div>

                </div>
			</div>
            <!-- end panel -->
	    </div>
	    <!-- End col-12 -->
	</div>
<!-- End Row -->
</form>

<link href="<?= base_url()?>assets/def/plugins/chosen/css/chosen.css" rel="stylesheet" />
<link href="<?= base_url()?>assets/def/plugins/lightbox/css/lightbox.min.css" rel="stylesheet" />

<script src="<?= base_url()?>assets/def/plugins/chosen/js/chosen.jquery.min.js"></script>
<script src="<?= base_url()?>assets/def/plugins/lightbox/js/lightbox.min.js"></script>
<script src="<?= base_url()?>assets/cust/js/clients/buyer.form.js"></script>


<script type="text/javascript">
	TableManageDefault.init();

	$('.simpan').on('click',function(){
		$('form').submit();
	});
</script>

