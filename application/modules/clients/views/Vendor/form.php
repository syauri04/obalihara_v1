<!-- begin row -->
	<div class="row">
		<!-- begin col-12 -->
	    <div class="col-lg-12">
			<!-- begin panel -->
			<div class="panel panel-inverse" data-sortable-id="form-stuff-1">
				<!-- begin panel-heading -->
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title f-s-16">Data Vendor</h4>
                </div>
                <!-- end panel-heading -->
                <!-- begin panel-body -->
                <div class="panel-body">
                	<form method="POST" enctype="multipart/form-data" action="<?= $this->own_link.'save'; ?>" >

                		<input type="hidden" name="vendor_id" value="<?= isset($val)?$val->id:'';?>">

                		<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Username</label>
							<div class="col-md-10">
								<input type="text" class="form-control m-b-5" name="username" value="<?= isset($du)?$du->username:'';?>" disabled/>
							</div>
						</div>

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Password</label>
							<div class="col-md-10">
								<input type="password" class="form-control m-b-5" name="password" value="<?= isset($du)?$du->password:'';?>" disabled/>
							</div>
						</div>

						<hr />

                		<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Photo</label>
							<div class="col-md-10">
								
								<?php if( isset($val->photo) && trim($val->photo)!="" ){?>

									<a href="<?php echo get_image(base_url()."assets/img/vendor/large/".$val->photo);?>" data-lightbox="image-1" data-title="Vendor Photo">
										<img alt="" src="<?php echo get_image(base_url()."assets/img/vendor/small/".$val->photo);?>" style="height:90px;width:auto;" class="img-polaroid">
									</a>

								<?php } ?>

								<!-- <input type="file" name="photo" value="<?= isset($val)?$val->photo:'';?>" /> -->
								
							</div>
						</div>

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Fee</label>
							<div class="col-md-10">
								<input type="text" class="form-control m-b-5" name="fee" value="<?= isset($val)?$val->fee:'';?>" />
							</div>
						</div>

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Nama Toko</label>
							<div class="col-md-10"><input type="text" class="form-control" value="<?= isset($val)?$val->nama_lengkap:'';?>" readonly></div>
						</div>

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Alamat</label>
							<div class="col-md-10"><textarea class="form-control" readonly><?= isset($val)?$val->alamat:'';?></textarea></div>
						</div>

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Telpon</label>
							<div class="col-md-10"><input type="text" class="form-control" value="<?= isset($val)?$val->telp:'';?>" readonly></div>
						</div>

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Email</label>
							<div class="col-md-10"><input type="text" class="form-control" value="<?= isset($val)?$val->email:'';?>" readonly></div>
						</div>

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">PIC</label>
							<div class="col-md-10"><input type="text" class="form-control" value="<?= isset($val)?$val->nama_lengkap:'';?>" readonly></div>
						</div>

						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Kode Pos</label>
							<div class="col-md-10"><input type="text" class="form-control" value="<?= isset($val)?$val->postcode:'';?>" readonly></div>
						</div>
						
						<div class="form-group row m-b-15">
							<label class="col-form-label col-md-2">Domisili</label>
							<div class="col-md-3">
								<input type="text" class="form-control" value="<?= isset($val)?$kec['province']:'';?>" readonly>
							</div>
							<div class="col-md-3">
								<input type="text" class="form-control" value="<?= isset($val)?$kec['type'].' '.$kec['city']:'';?>" readonly>
							</div>
							<div class="col-md-3">
								<input type="text" class="form-control" value="<?= isset($val)?$kec['subdistrict_name']:'';?>" readonly>
							</div>
						</div>
					</form>

					<div class="form-group row m-b-15">
						<label class="col-form-label col-md-2"></label>
						<div class="col-md-10">
							<a href="<?= $this->own_link?>" class="btn btn-default cancel">Cancel</a>&nbsp;
							<button type="submit" class="btn btn-primary simpan">Simpan</button>
						</div>
					</div>
                </div>
			</div>
            <!-- end panel -->
	    </div>
	    <!-- End col-12 -->
	</div>
<!-- End Row -->

<link href="<?= base_url()?>assets/def/plugins/chosen/css/chosen.css" rel="stylesheet" />
<link href="<?= base_url()?>assets/def/plugins/lightbox/css/lightbox.min.css" rel="stylesheet" />

<script src="<?= base_url()?>assets/def/plugins/chosen/js/chosen.jquery.min.js"></script>
<script src="<?= base_url()?>assets/def/plugins/lightbox/js/lightbox.min.js"></script>
<script src="<?= base_url()?>assets/cust/js/clients/vendor.form.js"></script>

<script type="text/javascript">
	TableManageDefault.init();

	$('.simpan').on('click',function(){
		$('form').submit();
	});
</script>