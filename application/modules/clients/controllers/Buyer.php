<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH."libraries/AdminController.php");

class Buyer extends AdminController {

	
	function __construct()    
	{
		parent::__construct();    
		$this->_set_action();
		$this->_set_action(array("edit","delete"),"ITEM");
		$this->_set_title( 'Buyer List ' );
		$this->load->model('Buyer_model','BM');
		$this->folder_view = "buyer";
		$this->DATA->table="cp_buyer";

		$this->upload_path="./assets/img/buyer/";
		$this->upload_resize  = array(
			array('name'	=> 'thumb','width'	=> 250, 'height'	=> 155, 'quality'	=> '75%'),
			array('name'	=> 'small','width'	=> 350, 'height'	=> 200, 'quality'	=> '85%'),
			array('name'	=> 'large','width'	=> 850, 'height'	=> 440, 'quality'	=> '75%')
		);

		$this->image_size_str = "800px x 450px";

	}
	
	function index() {
		$this->breadcrumb[] = array(
			"title"		=> "List"
		);

		/* paging */
		$this->per_page = 1;
		$this->uri_segment = 4;
		$this->data_table = $this->BM->data(array(
			'limit' 	=> $this->per_page, 
			'offset'	=> $this->uri->segment($this->uri_segment)
		));
		$data = $this->_data(array(
			"base_url"	=> $this->own_link.'/index'
		));

		$this->_v($this->folder_view.'/index',$data);
	}

	function add()
	{
		$data = [
			'country' 	=> $this->BM->country(),
			'prov' 	=> $this->BM->province(),
			'city' 	=> $this->BM->city(),
		];

		$this->_v($this->folder_view.'/form',$data);
	}

	function edit($id){

		$this->breadcrumb[] = array(
			"title"		=> "Edit"
		);
		
		if(trim($id)!=''){

			$this->data_form = $this->DATA->data_id(array(
				'id'	=> $id
			));

			$data_address = $this->db->get_where('cp_buyer_address',['buyer_id' => $this->data_form->user_id])->row(); // buyer_id == user_id

			$data = [
				'form_title' => 'FORM VENDOR',
				'country' 	=> $this->BM->country(),
				'du'	=> $this->db->get_where('cp_app_user',['user_id' => $this->data_form->user_id])->row(),
				'da'	=> $data_address,
				'prov'	=> $this->BM->province(),
				'city'	=> isset($data_address)?$this->BM->city($data_address->provinsi_id):'',
			];
			
			$this->_v($this->folder_view.'/form',$data);
		}else{
			redirect($this->own_link);
		}

	}

	function delete($id){
		$id=dbClean(trim($id));

		if(trim($id) != ''){

			$this->db->update("cp_buyer",array("is_trash"=>1),array("id"=>$id));			
			
		}
		redirect($this->own_link."?msg=".urldecode('Delete Data Buyer success')."&type_msg=success");
	}

	function save(){
		
		if(isset($_POST['username'])){
			$data_user = [
				'username' 	=> $_POST['username'],
				'user_password' => md5($_POST['password']),
				'user_status'	=> 1, // aktif
				'user_group'	=> 2, // test id buyer 2 . vendor 3
			];

			$this->DATA->table="cp_app_user";
			$du = $this->_save_master( 
				$data_user,
				array(
					'user_id' => dbClean($_POST['user_id'])
				),
				dbClean($_POST['user_id'])
			);
		}
		
		$data_personal = [
			'user_id'		=> isset($du)?$du['id']:$_POST['user_id'],
			'nama_lengkap' 	=> $_POST['nama_lengkap'],
			'telp' 			=> $_POST['telp'],
			'email' 		=> $_POST['email'],
			'country' 		=> $_POST['country']
		];
		
		$this->DATA->table="cp_buyer";
		$dp = $this->_save_master( 
			$data_personal,
			array(
				'id' => dbClean($_POST['buyer_id'])
			),
			dbClean($_POST['buyer_id'])
		);

		$this->db->delete('cp_buyer_address',['buyer_id' => $dp['id']]);
		foreach($_POST['provinsi_id'] as $k => $v){
			$data_alamat = [
				'title'			=> $_POST['title'][$k],
				'buyer_id'		=> $dp['id'],
				'provinsi_id' 	=> $_POST['provinsi_id'][$k],
				'kota_id' 		=> $_POST['kota_id'][$k],
				'alamat' 		=> $_POST['alamat'][$k],
				'post_code' 	=> $_POST['post_code'][$k]
			];

			$this->db->insert('cp_buyer_address',$data_alamat);
		}		

		$id = $dp['id'];

		$this->_uploaded(
		array(
			'id'		=> $id ,
			'input'		=> 'photo',
			'param'		=> array(
							'field' => 'photo', 
							'par'	=> array('id' => $id)
						)
		));		

		redirect($this->own_link."?msg=".urldecode('Save data article succes')."&type_msg=success");
	}

	function getCity(){
		$tmp    = '';
        $data   = $this->BM->city($_POST['id']);
        if(!empty($data)) {
            $tmp .= "<option value=''>- Pilih Kota -</option>"; 
			foreach($data as $i){ 
                $tmp .= '<option value="'.$i->id.'">'.$i->kota_kab.' '.$i->nama_kota.'</option>';;
            }
        } else {
            $tmp .= "<option value=''>- Pilih Kota -</option>"; 
        }
        die($tmp);
	}
}
