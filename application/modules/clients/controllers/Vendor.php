<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH."libraries/AdminController.php");

class Vendor extends AdminController {

	
	function __construct()    
	{
		parent::__construct();    
		$this->_set_action();
		$this->_set_action(array("edit","delete"),"ITEM");
		$this->_set_title( 'Vendor List ' );
		$this->load->model('Vendor_model','GM');
		$this->folder_view = "vendor";
		$this->DATA->table = "cp_vendor";

		$this->upload_path="./assets/img/vendor/";
		$this->upload_resize  = array(
			array('name'	=> 'thumb','width'	=> 250, 'height'	=> 155, 'quality'	=> '75%'),
			array('name'	=> 'small','width'	=> 350, 'height'	=> 200, 'quality'	=> '85%'),
			array('name'	=> 'large','width'	=> 850, 'height'	=> 440, 'quality'	=> '75%')
		);

		$this->image_size_str = "800px x 450px";
	}
	
	function index(){
		$this->breadcrumb[] = array(
			"title"		=> "List"
		);

		/* paging */
		$this->per_page = 1;
		$this->uri_segment = 4;
		$this->data_table = $this->GM->data(array(
			'limit' 	=> $this->per_page, 
			'offset'	=> $this->uri->segment($this->uri_segment)
		));
		$data = $this->_data(array(
			"base_url"	=> $this->own_link.'/index'
		));
		// d($data);
		$this->_v($this->folder_view.'/index',$data);
	}

	function add(){
	}

	function edit($id){
		$this->breadcrumb[] = array(
			"title"		=> "Edit"
		);

		if(trim($id)!=''){

			$this->data_form = $this->DATA->data_id(array(
				'id'	=> $id
			));

			$data = [
				'form_title' => 'FORM VENDOR',
				'du'	=> $this->db->get_where('cp_user',['id' => $this->data_form->user_id])->row(),
				'kec'	=> raongKec($this->data_form->kecamatan_id,'k'),
			];
			// d($data);
			$this->_v($this->folder_view.'/form',$data);
		}else{
			redirect($this->own_link);
		}
	}

	function delete(){

	}

	function save(){
		
		$d = [];
		foreach ($_POST as $k => $v) {
			$d[$k] = $_POST[$k];
		}
		unset($d['vendor_id']);

		if($_POST['fee'] != ''){
			$d['activate'] = 1;
		}

		// Email Config

		$email = [
			'from' 		=> 'Obalihara.com',
			'title'		=> 'Seller Account Activated',
			'to'		=> getOutlet($_POST['vendor_id'],'email'),
			'subject'	=> 'Seller Account Activated',
			'message'	=> $this->htmlTemplate(getOutlet($_POST['vendor_id'],'nama_lengkap')),
		];

		$e = $this->sendEmail($email);
		
		if($e) {
			$a = $this->_save_master( 
				$d,
				array(
					'id' => dbClean($_POST['vendor_id'])
				),
				dbClean($_POST['vendor_id'])			
			);
		} else {
			redirect($this->own_link."?msg=".urldecode('Save data vendor Error')."&type_msg=failed");
		}

		redirect($this->own_link."?msg=".urldecode('Save data vendor succes')."&type_msg=success");
	}

	function getCity(){
		$tmp    = '';
        $data   = $this->GM->city($_POST['id']);
        if(!empty($data)) {
            $tmp .= "<option value=''>- Pilih Kota -</option>"; 
			foreach($data as $i){ 
                $tmp .= '<option value="'.$i->id.'">'.$i->kota_kab.' '.$i->nama_kota.'</option>';;
            }
        } else {
            $tmp .= "<option value=''>- Pilih Kota -</option>"; 
        }
        die($tmp);
	}

	function htmlTemplate($vendor){
		$a = '
			<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
			<html>
				<head>
					<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">
					<style>
						@import url("https://fonts.googleapis.com/css?family=Heebo:400,500,700");
						@import url("https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css");
						body{
						  font-size: 12px;
						  background: #F5F5F9;
						  font-family: "Heebo" !important;
						  overflow-x: hidden;
						  color: #888888;
						  max-width:600px;
						}
						.red-head{
						    width:100%;
						    background:#8F3248;
						    padding-top:22px;
						    padding-left:14px;
						    padding-right:14px;
						    
						}
						.white-head{
						    border-top-left-radius:10px;
						    border-top-right-radius:10px;
						    text-align:center;
						    background:#fff;
						    padding-top:32px;
						    padding-bottom:10px;
						}
						.content-head{
						    font-size: 14px;
						}
						h1{
						    font-family: Heebo;
						    font-weight:600;
						    font-size: 20px;
						    line-height: 20px;
						    color:#000000;
						}
						.logo{
						    margin-bottom:20px;
						}
						.content-box{
						    padding-top:0px;
						    padding-left:30px;
						    padding-right:30px;
						    background:#fff;
						    width:94.7%;
						}
						.content{
						    padding-top:10px;
						    padding-bottom:10px;
						    padding-left:30px;
						    padding-right:30px;
						}
						a{
						    cursor:pointer;
						    color:#8f3248;
						}
						btn{
						    cursor:pointer;
						}
					</style>
				</head>
				<body>
				    <div class="red-head">
				        <div class="white-head">
				            <img src="<?php echo base_url() ?>assets/img/email/logo.png" alt="" class="logo">
				            <h1>Verifikasi Toko Kamu</h1>
				        </div>
				    </div>
				    <div class="content-box">
				        <div class="content">
				            <h1>Hai '.$vendor.'!</h1>
				            <span style="font-size:14px">Toko kamu telah di verifikasi , selamat bergabung bersama obalihara.com</br>Kamu sekarang sudah bisa menambahkan produk di toko kamu.</span>
				        </div>
				        <div class="content" style="font-size:14px">
				            <span>Silakan cek kembali akun kamu. Selalu jaga keamanan dan kerahasiaan akun Obalihara kamu.</span><br><br>
				            <span>Terima kasih atas perhatiannya.</span>
				        </div>
				        <div style="text-align:center; margin-top:50px">
				            <h1>Ikuti Kami</h1>
				            <span>Jadilah yang pertama tahu promo-promo terbaru Obalihara lewat media <br>sosial kami.</span>
				            <div style="margin-top:10px; text-align:center; width:180px;position:relative; margin:0 auto; padding-top:20px;">
				                <div style="background: #8F3248; width:26px;border-radius:15px;text-align:center;padding-top:7px;padding-bottom:7px; float:left; margin-right:10px;color:#fff;">
				                    <!-- <img src="<?php echo base_url() ?>assets/img/email/ig.png" alt=""> -->
				                    <i class="fa fa-instagram" aria-hidden="true"></i>
				                </div>
				                <div style="background: #8F3248; width:26px;border-radius:15px;text-align:center;padding-top:7px;padding-bottom:7px; float:left; margin-right:10px;color:#fff;">
				                    <!-- <img src="<?php echo base_url() ?>assets/img/email/fb.png" alt=""> -->
				                    <i class="fa fa-facebook" aria-hidden="true"></i>
				                </div>
				                <div style="background: #8F3248; width:26px;border-radius:15px;text-align:center;padding-top:7px;padding-bottom:7px; float:left; margin-right:10px;color:#fff;">
				                    <!-- <img src="<?php echo base_url() ?>assets/img/email/tw.png" alt=""> -->
				                    <i class="fa fa-twitter" aria-hidden="true"></i>
				                </div>
				                <div style="background: #8F3248; width:26px;border-radius:15px;text-align:center;padding-top:7px;padding-bottom:7px; float:left; margin-right:10px;color:#fff;">
				                    <!-- <img src="<?php echo base_url() ?>assets/img/email/in.png" alt=""> -->
				                    <i class="fa fa-linkedin" aria-hidden="true"></i>
				                </div>
				                <div style="background: #8F3248; width:26px;border-radius:15px;text-align:center;padding-top:7px;padding-bottom:7px; float:left;color:#fff;">
				                    <!-- <img src="<?php echo base_url() ?>assets/img/email/yt.png" alt=""> -->
				                    <i class="fa fa-youtube-play" aria-hidden="true"></i>
				                </div>
				                <div style="clear:both"></div>
				            </div>
				            <div style="text-align:center; margin-top:30px; margin-bottom:10px;">
				            Jl. Tegal Parang Utara No.14, RT.5/RW.4, Mampang<br>
				            Prpt., Kec. Mampang Prpt., Kota Jakarta Selatan,<br>
				            Daerah Khusus Ibukota Jakarta 12790<br>
				            </div>
				            <div style="text-align:center; margin-top:30px; margin-bottom:10px;color: #BBBBBB; text-align:center">
				            Copyright © 2019 Obalihara.com. All Rights Reserved
				            </div>
				        </div>
				    </div>
				</body>
			</html>
		';

		return $a;
	}
}
