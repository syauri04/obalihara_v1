<?php

class Vendor_model extends CI_Model{

    function __construct(){
    	parent::__construct();
  	}

    var $tabel = 'cp_vendor';

    function data($p=array()){
    
        $where = '';
        if( isset($p['item_id']) && trim($p['item_id'])!=""){
            $where .=" AND item_id = '".dbClean($p['item_id'])."' ";    
        }
        
        $sql = "
            select 
                SQL_CALC_FOUND_ROWS *
            FROM ".$this->tabel."
            WHERE is_trash != 1
                ".$where." 
            ";   
        
        if( isset($p['limit']) && isset($p['offset']) ){
            $offset = empty($p['offset'])?0:$p['offset'];               
            $sql .= " LIMIT ".$offset.",".$p['limit']." ";
        }

        $query = $this->db->query($sql);
        
        $found_rows = $this->db->query('SELECT FOUND_ROWS() as found_rows');

        return array(
            "data"  => $query->result(),
            "count" => $found_rows->row()
        );

        
    }       

    function province() {

        $this->db->select('id,nama_provinsi')
        ->where('is_trash',0)
        ->order_by('id','ASC');
        return $this->db->get('cp_app_province')->result();

    }

    function city($id) {

        $this->db->select('id,nama_kota,kota_kab')
        ->where('provinsi_id',$id)
        ->where('is_trash',0)
        ->order_by('id','ASC');
        return $this->db->get('cp_app_city')->result();

    }

}
