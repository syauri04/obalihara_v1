<?php

class Welcome_model extends CI_Model{

    function __construct(){
    	parent::__construct();
  	}

    function monthly_sales($p){

        $this->db->select('SUM(c.qty) as total_qty,SUM(cd.total) as total,SUM(cd.courier_cost) as courier_cost,SUM(fee) AS total_fee')
        ->join('cp_checkout_detail cd','cd.cart_id = c.id')
        ->where('c.status_invoice >',4)
        ->where('MONTH(c.tanggal_order)',$p);    
        $data = $this->db->get('cp_cart c')->row();
        // d($data);
        return $data;
    }

    function unactivated_vendor(){

        $this->db->select('*')
        ->where('activate !=',1);
        $d = $this->db->get('cp_vendor')->num_rows();

        return $d;

    }

    // function monthly_fee($p){

    //     $this->db->select('SUM(c.qty) as total_qty,SUM(cd.total) as total,SUM(cd.courier_cost) as courier_cost')
    //     ->join('cp_checkout_detail cd','cd.cart_id = c.id')
    //     ->where('c.status_invoice >',4)
    //     ->where('MONTH(c.tanggal_order)',$p);    

    //     $this->db->group_by('c.vendor_id');
    //     $data = $this->db->get('cp_cart c')->result_array();
        
    //     return $data;
    // }

    // function monthly_item($p){

    //     $this->db->select('SUM(c.qty) as total_qty,SUM(cd.total) as total,SUM(cd.courier_cost) as courier_cost')
    //     ->join('cp_checkout_detail cd','cd.cart_id = c.id')
    //     ->where('c.status_invoice >',4)
    //     ->where('MONTH(c.tanggal_order)',$p);    

    //     $this->db->group_by('c.vendor_id');
    //     $data = $this->db->get('cp_cart c')->result_array();
        
    //     return $data;
    // }

}
