<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once(APPPATH."libraries/AdminController.php");

class Welcome extends AdminController {

	
	function __construct()    
	{
		parent::__construct();    
		$this->_set_action();
		$this->_set_action(array("edit","delete"),"ITEM");
		$this->_set_title( ' Dashboard ' );
		$this->load->model('welcome_model','WM');

	}
	
	public function index()
	{
		$data = [
			'mSales' 	=> $this->WM->monthly_sales(date('m')),
			'uVendor'	=> $this->WM->unactivated_vendor(),
			// 'mFee' 		= $this->WM->monthly_fee(date('M')),
			// 'mItem' 	= $this->WM->monthly_item(date('M')),
		];

		// d($data);

		$this->_v('welcome_message',$data);
	}
}
