<style type="text/css">
  
  .ViewSelector,.ViewSelector2{
    display:block; margin-bottom: 5px;
  }
  .ViewSelector2-item,.ViewSelector table{
    display:block;margin-bottom:1em;width:100%
  }
  .ViewSelector2-item>label,.ViewSelector td:first-child{
    font-weight:700;margin:0 .25em .25em 0;display:block
  }
  .ViewSelector2-item>select{
    width:100%
  }
  .ViewSelector table,.ViewSelector tbody,.ViewSelector td,.ViewSelector tr{
    display:block
  }
  .ViewSelector table{
    height:auto!important
  }
  .ViewSelector table,.ViewSelector td{
    width:auto!important
  }
  .ViewSelector td:last-child *{display:block;text-align:left}
  .ViewSelector td:last-child>div{font-weight:400;margin:0}
  @media (min-width:570px){
    .ViewSelector,.ViewSelector2{
      display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;/*margin:0 0 -1em -1em;*/width:-webkit-calc(100% + 1em);width:calc(100% + 1em)
    }
    .ViewSelector2-item,.ViewSelector table{
      -webkit-box-flex:1;-webkit-flex:1 1 -webkit-calc(100%/3 - 1em);-ms-flex:1 1 calc(100%/3 - 1em);flex:1 1 calc(100%/3 - 1em);margin-left:1em
    }
  }
  .ViewSelector2--stacked,.ViewSelector--stacked{
    display:block;margin:0;width:auto
  }
  .ViewSelector2--stacked .ViewSelector2-item,.ViewSelector--stacked table{
    margin-left:0
  }

</style>
<!-- begin row -->
<div class="row">
    <!-- begin col-3 -->
    <div class="col-lg-3 col-md-6">
        <div class="widget widget-stats bg-gradient-green">
            <div class="stats-icon stats-icon-lg"><i class="fa fa-dollar-sign fa-fw"></i></div>
            <div class="stats-content">
                <div class="stats-title">MONTHLY SALES</div>
                <div class="stats-number"><?= myNum($mSales->total /*+ $mSales->courier_cost*/,'Rp. ');?></div>
                <!-- <div class="stats-progress progress">
                    <div class="progress-bar" style="width: 40.5%;"></div>
                </div> -->
                <!-- <div class="stats-desc">Better than last week (40.5%)</div> -->
            </div>
        </div>
    </div>
    <!-- end col-3 -->
    <!-- begin col-3 -->
    <div class="col-lg-3 col-md-6">
        <div class="widget widget-stats bg-gradient-blue">
            <div class="stats-icon stats-icon-lg"><i class="fa fa-envelope fa-fw"></i></div>
            <div class="stats-content">
                <div class="stats-title">MONTHLY FEE</div>
                <div class="stats-number"><?= myNum($mSales->total_fee,'Rp. ')?></div>
                <!-- <div class="stats-progress progress">
                    <div class="progress-bar" style="width: 76.3%;"></div>
                </div> -->
                <!-- <div class="stats-desc">Better than last week (76.3%)</div> -->
            </div>
        </div>
    </div>
    <!-- end col-3 -->
    <!-- begin col-3 -->
    <div class="col-lg-3 col-md-6">
        <div class="widget widget-stats bg-gradient-purple">
            <div class="stats-icon stats-icon-lg"><i class="fa fa-archive fa-fw"></i></div>
            <div class="stats-content">
                <div class="stats-title">ITEM'S SOLD</div>
                <div class="stats-number"><?= $mSales->total_qty.' ITEM\'S';?></div>
                <!-- <div class="stats-progress progress">
                    <div class="progress-bar" style="width: 54.9%;"></div>
                </div> -->
                <!-- <div class="stats-desc">Better than last week (54.9%)</div> -->
            </div>
        </div>
    </div>
    <!-- end col-3 -->
    <!-- begin col-3 -->
    <div class="col-lg-3 col-md-6">
        <div class="widget widget-stats bg-gradient-black">
            <div class="stats-icon stats-icon-lg"><i class="fa fa-users fa-fw"></i></div>
            <div class="stats-content">
                <div class="stats-title">UNACTIVATED MITRA</div>
                <div class="stats-number"><?= $uVendor.' MITRA';?></div>
            </div>
        </div>  
    </div>
    <!-- end col-3 -->
</div>
<!-- end row -->

<div id="embed-api-auth-container" class="embedapi"></div>
<div class="ViewSelector" id="view-selector-container"></div>
<div id="chart-container"></div>

<!-- begin row -->
<!-- <div class="row"> -->
    <!-- begin col-8 -->
    <!-- <div class="col-lg-8">
        <div class="widget-chart with-sidebar bg-black">
            <div class="widget-chart-content">
                <h4 class="chart-title">
                    Visitors Analytics
                    <small>Where do our visitors come from</small>
                </h4>
                <div id="visitors-line-chart" class="widget-chart-full-width nvd3-inverse-mode" style="height: 260px;"></div>
            </div>
            <div class="widget-chart-sidebar bg-black-darker">
                <div class="chart-number">
                    1,225,729
                    <small>Total visitors</small>
                </div>
                <div id="visitors-donut-chart" class="nvd3-inverse-mode p-t-10" style="height: 180px"></div>
                <ul class="chart-legend f-s-11">
                    <li><i class="fa fa-circle fa-fw text-primary f-s-9 m-r-5 t-minus-1"></i> 34.0% <span>New Visitors</span></li>
                    <li><i class="fa fa-circle fa-fw text-success f-s-9 m-r-5 t-minus-1"></i> 56.0% <span>Return Visitors</span></li>
                </ul>
            </div>
        </div>
    </div> -->
    <!-- end col-8 -->
    <!-- begin col-4 -->
    <!-- <div class="col-lg-4">
        <div class="panel panel-inverse" data-sortable-id="index-1">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Visitors Origin
                </h4>
            </div>
            <div id="visitors-map" class="bg-black" style="height: 179px;"></div>
            <div class="list-group">
                <a href="javascript:;" class="list-group-item list-group-item-inverse d-flex justify-content-between align-items-center text-ellipsis">
                    1. United State 
                    <span class="badge f-w-500 bg-gradient-green f-s-10">20.95%</span>
                </a> 
                <a href="javascript:;" class="list-group-item list-group-item-inverse d-flex justify-content-between align-items-center text-ellipsis">
                    2. India
                    <span class="badge f-w-500 bg-gradient-blue f-s-10">16.12%</span>
                </a>
                <a href="javascript:;" class="list-group-item list-group-item-inverse d-flex justify-content-between align-items-center text-ellipsis">
                    3. Mongolia
                    <span class="badge f-w-500 bg-gradient-grey f-s-10">14.99%</span>
                </a>
            </div>
        </div>
    </div> -->
    <!-- end col-4 -->
<!-- </div> -->
<!-- end row -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.2/d3.min.js"></script>
<script src="<?= base_url()?>assets/def/plugins/nvd3/build/nv.d3.js"></script>
<script src="<?= base_url()?>assets/def/plugins/jquery-jvectormap/jquery-jvectormap.min.js"></script>
<script src="<?= base_url()?>assets/def/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js"></script>
<script src="<?= base_url()?>assets/def/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js"></script>
<script src="<?= base_url()?>assets/def/plugins/gritter/js/jquery.gritter.js"></script>
<script src="<?= base_url()?>assets/def/js/demo/dashboard-v2.min.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<!-- ================== BEGIN PAGE PLUGINS JS ================== -->
<!-- This demo uses the Chart.js graphing library and Moment.js to do date formatting and manipulation. -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script> -->
<!-- Include the ViewSelector2 component script. -->
<!-- <script src="<?= base_url()?>assets/def/plugins/google-analytics/view-selector2.js"></script> -->
<!-- Include the DateRangeSelector component script. -->
<!-- <script src="<?= base_url()?>assets/def/plugins/google-analytics/date-range-selector.js"></script> -->
<!-- Include the ActiveUsers component script. -->
<!-- <script src="<?= base_url()?>assets/def/plugins/google-analytics/active-users.js"></script> -->
<!-- Include the CSS that styles the charts. -->
<!-- <link rel="stylesheet" href="<?= base_url()?>assets/def/plugins/google-analytics/chartjs-visualizations.css"> -->
<!-- ================== END PAGE PLUGINS JS ================== -->
<script>
(function(w,d,s,g,js,fs){
  g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
  js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
  js.src='https://apis.google.com/js/platform.js';
  fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
}(window,document,'script'));
</script>
<script>

    gapi.analytics.ready(function() {

      /**
       * Authorize the user immediately if the user has already granted access.
       * If no access has been created, render an authorize button inside the
       * element with the ID "embed-api-auth-container".
       */
      var CLIENTID = '966280773677-l9s4mhbs9s451n4v7toqum5pmr4r7bvp.apps.googleusercontent.com';

      gapi.analytics.auth.authorize({
        container: 'embed-api-auth-container',
        clientid: CLIENTID
      });


      /**
       * Create a new ViewSelector instance to be rendered inside of an
       * element with the id "view-selector-container".
       */
      var viewSelector = new gapi.analytics.ViewSelector({
        container: 'view-selector-container'
      });

      // Render the view selector to the page.
      viewSelector.execute();


      /**
       * Create a new DataChart instance with the given query parameters
       * and Google chart options. It will be rendered inside an element
       * with the id "chart-container".
       */
      var dataChart = new gapi.analytics.googleCharts.DataChart({
        query: {
          metrics: 'ga:sessions',
          dimensions: 'ga:date',
          'start-date': '30daysAgo',
          'end-date': 'yesterday'
        },
        chart: {
          container: 'chart-container',
          type: 'LINE',
          options: {
            width: '100%'
          }
        }
      });


      /**
       * Render the dataChart on the page whenever a new view is selected.
       */
      viewSelector.on('change', function(ids) {
        dataChart.set({query: {ids: ids}}).execute();
      });

    });
</script>

<script>
    $(document).ready(function() {

        DashboardV2.init();
        $('.embedapi').eq(0).empty();
    });
</script>