<?php 
include("globalModel.php");
class MY_Controller extends CI_Controller{ 
	
	var $DATA;
	var $upload_path	= './assets/files/';
	//var $upload_allowed_types	= 'jpeg|gif|jpg|png|avi|mp4|mpeg|mpg|movie|mov|qt|flv|pdf|doc|docx|xls|xlsx';
	var $upload_allowed_types = '*';
	var $upload_types	= 'file';	// image or file.
	var $upload_resize  = array(
				array('name'	=> 'thumb','width'	=> 200, 'height'	=> 200, 'quality'	=> '85%'),
			array('name'	=> 'small','width'	=> 350, 'height'	=> 350, 'quality'	=> '85%'),
			array('name'	=> 'large','width'	=> 500, 'height'	=> 500, 'quality'	=> '85%')
			);
	var $message = "";
	var $folder_view = "";
	var $prefix_view = "";
	var $per_page 		= 20;
	var $uri_segment 	= 4;
	var $domain = "";
	var $data_table; 
	var $user_online = array();
	var $is_watermark = TRUE;
	
	function __construct(){  
		parent::__construct(); 		  
		
		date_default_timezone_set('Asia/Jakarta');
		
		/*$set=ini_set('mssql.textlimit','65536');
   		ini_set('mssql.textlimit',$set);

   		$set2=ini_set('mssql.textlimit','65536');
   		ini_set('mssql.textsize',$set2); 
		*/ 
   		
		$this->_initConfig();
		$this->output->enable_profiler(false);
		$this->DATA = new globalModel();		

		$this->domain = $_SERVER['SERVER_NAME'];
		//$this->write_log();

		$this->user_online = $this->get_list_user();

		$this->is_watermark = TRUE;
		// d($this->jCfg);
		//die('asd');
	}
	
	function _initConfig(){
		$s = $this->session->userdata("jcfg");
		if(is_array($s))
			$this->jCfg = $s;
		else
			$this->_initSession();			
	}
	
	function isLogin(){
		if($this->jCfg['is_login'] == 1){
			redirect('');
		}else{
			redirect('auth');
		}
	}
	
	function current_session($i=0){
		for($x=0;$x>=$i;$x++){
			$this->_initSession();
		}
	}
	
	function _initSession(){

		$this->jCfg = array(
			'cp_app_id'		=> '1',
			'cp_app_name'		=> '',
			'is_login'		=> 0,
			'client' => array(
				'is_login'	=> 0,
				'id' 		=> '',
				'name'		=> 'guest',
				'fullname'	=> 'Guest',
				'photo'		=> '',
				'email'		=> '',
				'referer'	=> ''
			),
			'view'			=> array(
					"data"	=> "all",
					"t"		=> "all"
				),
			'user' => array(
				'id' 		=> '',
				'name'		=> 'guest',
				'fullname'	=> 'Guest',
				'level'		=> '',
				'is_all'	=> 0,
				'color'		=> 'mine',
				'bg'		=> 'ptrn_e',
				'ujian_type'=> '',
				'email'		=> ''
			),
			'menu'			=> array(),
			'current_class'		=> '',
			'current_funtion' 	=> '',
			'mod_rewrite'	=> 1,
			'theme'			=> '',
			'search'		=> array(
									'class'		=> '',
									'date_start'=> '',
									'date_end'	=> '',
									'status'	=> '',
									'per_page'	=> 20,
									'order_by'  => '',
									'colum'		=> '',
									'keyword'	=> '',
									'order_dir' => 'ASC'
								),
			'referer'		=> '',
			'chat_online'	=> array(),
			'access'		=> array(),
			'lang'			=> 'ind',
			'captcha'		=> array()			
		);
		$this->_releaseSession();
	}
	
	function setReferer($url=''){
		$this->jCfg['referer'] = $url;
		$this->_releaseSession();
	}
	
	function setMenu($menu=array()){
		$this->jCfg['menu'] = $menu;
		$this->_releaseSession();
	}
	
	function setMessage($message=''){
		$this->jCfg['message'] = $message;
		$this->_releaseSession();
	}	
	
	function setLang($lang='eng'){
		$this->jCfg['lang'] = $lang;
		$this->_releaseSession();
	}
	function setAccess($acc=array()){
		$this->jCfg['access'] = $acc;
		$this->_releaseSession();
	}
	
	function getReferer(){
		return $this->jCfg['referer'];
	}
	
	function _releaseSession(){
		$this->session->set_userdata(array("jcfg"=>$this->jCfg));
	}
	
	/*
	* stuff function
	*/
	function _getClass(){ // return name of current class
		return $this->router->fetch_class();
	}
	
	function _getMethod(){ // return name of current methode
		return $this->router->fetch_method();
	}

	
	function sendEmail($p=array()){
		$this->load->library('email');
		
		/*smtp method */

		$config['protocol'] = "smtp";
		$config['smtp_host'] = "ssl://mail.obalihara.com";
		$config['smtp_port'] = "465";
		$config['smtp_user'] = "cs@obalihara.com";
		$config['smtp_pass'] = "Obalihara2019";
		$config['priority']  = 1;
		$config['mailtype']  = "html";
		$config['charset']   = "utf-8";
		$config['wordwrap']  = TRUE;
		$config['newline'] = "\r\n";

		// $config['protocol']  = 'smtp';
		// $config['smtp_host'] = 'ssl://smtp.gmail.com';
		// $config['smtp_port'] = 465;//465;
		// $config['smtp_user'] = 'obalihara@gmail.com';
		// $config['smtp_pass'] = 'siaplestari86';
		// $config['priority']  = 1;
		// $config['mailtype']  = 'html';
		// $config['charset']   = 'utf-8';
		// $config['wordwrap']  = TRUE;
	
		$this->email->initialize($config);
		$this->email->set_newline("\r\n");
		
		$this->email->from('cs@obalihara.com', isset($p['title'])?$p['title']:' Obalihara News');
		// $this->email->from($p['from'], isset($p['title'])?$p['title']:' Obalihara News');
		
		$this->email->to($p['to']);
		// if( isset($p['cc']) && trim($p['cc']) != "" ){			
		// 	$this->email->cc($p['cc']); 
		// }
		$this->email->subject('Obalihara : '.$p['subject']);
		$this->email->message($p['message']);
		
		if(isset($p['alt_message']) && trim($p['alt_message'])!='' ){
			$this->email->set_alt_message($p['alt_message']);
		}
		
		if ($this->email->send()) {
			return true;
		} else {
			d($this->email->print_debugger());
		}
		//d($this->email->print_debugger());
		
	}

	function get_list_user(){
		$ses = $this->db->get("cp_app_sessions")->result();
		$list_user = array();
		if( count($ses) > 0 ){
			foreach($ses as $r){
				$m = $r->user_data;
				$t = explode("username_chat_online",$m);
				$ntmp = isset($t[1])?explode("}",$t[1]):array();
				if( isset($ntmp[0]) ){
					$mx = str_replace(";","",$ntmp[0]);
					$expl = explode('"',trim($mx));
					if(isset( $expl[2] )){
						$list_user[$expl[2]] = 1;
					}
				} 
			}
		}
		
		return($list_user);
	}

	function _uploaded($par=array()){
		$this->load->library('image_lib');

		$uri = $this->upload_path;
		$folder_upload = (isset($par['folder']))?$par['folder']:'';

		//echo "rrr ".$uri;exit;
		if($_FILES[$par['input']]['error']==4)
			return false;
		$uId = uniqid();

		$iname = $_FILES[$par['input']]['name'];
		//$img_name = explode('.',$iname);
		//debugCode($img_name[0]);

		$check_dot = explode('.',$iname); //removing dot character
		$count = count($check_dot);
		if ($count > 2){
			$r = array_pop($check_dot);
			$iname = implode('-',$check_dot).'.'.$r;
		}

		$check_space = strpos($iname, ' ');	//removing space character
		$file_Name = ($check_space === false) ? $iname : str_replace(' ','-',$iname) ;
		$file_Name = preg_replace("/[^a-zA-Z0-9.]/", "_", $file_Name);
		$config['upload_path'] = $uri.$folder_upload."/";
		$config['file_name'] = $file_Name;
		$config['allowed_types'] = $this->upload_allowed_types;

		if(trim($this->upload_types)=='image'){
			$config['max_width']  	= 1024*5;
			$config['max_height'] 	= 768*5;
		}

		$this->load->library('upload');
		$this->upload->initialize($config);

		if( $this->upload->do_upload($par['input']) )
		{
			$media = $this->upload->data($par['input']);

			$fileName = url_title($media["raw_name"],'-',TRUE).'-'.$uId.$media["file_ext"];
            $fileName = strtotime(date("Y-m-d H:i:s"))."-".$file_Name;

			if(file_exists($config['upload_path'].$file_Name)){
				rename($config['upload_path'].$file_Name,$config['upload_path'].$fileName);
			}

			$this->_delte_old_files($par['param']);
			$this->DATA->_update($par['param']['par'],array($par['param']['field']=>$fileName));
			// dd($this->upload_types);
			if(trim($this->upload_types)=='file'){

				$fileNameResize = $config['upload_path'].$fileName;
				// debugCode($fileNameResize);
				$img = getimagesize($fileNameResize);
				$realWidth	= $img[0];
				$realHeight = $img[1];

				$resize = array();

				foreach($this->upload_resize as $r){
					$resize[] = array(
						"width"			=> $r['width'],
						"height"		=> $r['height'],
						"quality"		=> $r['quality'],
						"source_image"	=> $fileNameResize,
						"new_image"		=> $uri.'/'.$r['name']."/".$fileName
					);
				}
				// d($resize);
                foreach($resize as $k=>$v){
					$oriW = $v['width'];
					$oriH = $v['height'];
					$x = $v['width']/$realWidth;
					$y = $v['height']/$realHeight;
					if($x < $y) {
						$v['width'] = round($realWidth*($v['height']/$realHeight));
					} else {
						$v['height'] = round($realHeight*($v['width']/$realWidth));
					}

					$this->image_lib->initialize($v);
					if(!$this->image_lib->resize()){
						d($this->image_lib->display_errors());
						die("Error Resize....");
					}
					$this->image_lib->clear();
				}
				//delete original image
				if(file_exists($config['upload_path'].$fileName)){
					unlink($config['upload_path'].$fileName);
				}
			} // end if this type image
			return $fileName;
		} else {
			d($this->upload->display_errors());
		}
	}

	function _uploaded2($par=array()){ 
		$this->load->library('image_lib');

		$uri = $this->upload_path;
		$folder_upload = (isset($par['folder']))?$par['folder']:'';

		//echo "rrr ".$uri;exit;
		if($_FILES[$par['input']]['error']==4)
			return false;
		$uId = uniqid();

		$iname = $_FILES[$par['input']]['name'];
		//$img_name = explode('.',$iname);
		//debugCode($img_name[0]);

		$check_dot = explode('.',$iname); //removing dot character
		$count = count($check_dot);
		if ($count > 2){
			$r = array_pop($check_dot);
			$iname = implode('-',$check_dot).'.'.$r;
		}

		$check_space = strpos($iname, ' ');	//removing space character
		$file_Name = ($check_space === false) ? $iname : str_replace(' ','-',$iname) ;
		$file_Name = preg_replace("/[^a-zA-Z0-9.]/", "_", $file_Name);
		$config['upload_path'] = $uri.$folder_upload."/";
		$config['file_name'] = $file_Name;
		$config['allowed_types'] = $this->upload_allowed_types;

		if(trim($this->upload_types)=='image'){
			$config['max_width']  	= 1024*5;
			$config['max_height'] 	= 768*5;
		}

		$this->load->library('upload');
		$this->upload->initialize($config);

		if( $this->upload->do_upload($par['input']) )
		{
			$media = $this->upload->data($par['input']);
			//debugCode($media);
			$fileName = url_title($media["raw_name"],'-',TRUE).'-'.$uId.$media["file_ext"];
            $fileName = strtotime(date("Y-m-d H:i:s"))."-".$file_Name;

			if(file_exists($config['upload_path'].$file_Name)){
				rename($config['upload_path'].$file_Name,$config['upload_path'].$fileName);
			}

			$this->_delte_old_files($par['param']);
			$this->DATA->_update($par['param']['par'],array($par['param']['field']=>$fileName));

			return $media;
		} else {
			d($this->upload->display_errors());
		}
	}
	
	function _uploaded3($par=array()){ 
		$this->load->library('image_lib');
		
		//$uri = "./dashboard/assets/plugins/jquery-file-upload/server/php/files";	
		$uri = $this->upload_path;	
		$folder_upload = (isset($par['folder']))?$par['folder']:'';
		//echo "rrr ".$uri;exit;
		if($_FILES[$par['input']]['error']==4)
			return false;
		$uId = uniqid();
		
		$liname = $_FILES[$par['input']]['name'];
		$last_name = explode('.',$liname);
		$iname = $par['new_name'].".".$last_name[1];
		//d($iname);
		//$img_name = explode('.',$iname);
		//d($img_name[0]);
		
		$check_dot = explode('.',$iname); //removing dot character
		$count = count($check_dot);
		if ($count > 2){ 
			$r = array_pop($check_dot);
			$iname = implode('-',$check_dot).'.'.$r;
		}		
		$check_space = strpos($iname, ' ');	//removing space character
		$file_Name = ($check_space === false) ? $iname : str_replace(' ','-',$iname) ;
		
		
		$config['upload_path'] = $uri.$folder_upload."/";
		//d($config['upload_path']);
		$config['file_name'] = $file_Name;
		$config['allowed_types'] = $this->upload_allowed_types;
		//$config['max_size']		= 1024*20;
		if(trim($this->upload_types)=='image'){
			$config['max_width']  	= 1024*5;
			$config['max_height'] 	= 768*5;
		}
		//d($config);
		$this->load->library('upload');
		$this->upload->initialize($config);
		//d($par['input']);
//d($this->upload->do_upload($par['input']));
		if( $this->upload->do_upload($par['input']) )
		{	
	//echo "masul";exit;
			$media = $this->upload->data($par['input']);
			//d($media);
			//$fileName = url_title($media["raw_name"],'-',TRUE).'-'.$uId.$media["file_ext"];
			$fileName = $media["file_name"];
			
			
			//d($fileName);
			//echo $fileName;//die();
			if(file_exists($config['upload_path'].$file_Name)){
				rename($config['upload_path'].$file_Name,$config['upload_path'].$fileName);
			}
			//d($config['upload_path'].$file_Name.' '.$config['upload_path'].$fileName);
			//$img = $uId.$this->upload->file_ext;
			
			$this->_delte_old_files($par['param']);
			//d($par['param']['par']);
			//$this->DATA->_update($par['param']['par'],array($par['param']['field']=>$fileName));
			
			if(trim($this->upload_types)=='image'){ 
			
				$fileNameResize = $config['upload_path'].$fileName;
				//d($fileNameResize);
				$img = getimagesize($fileNameResize);
				$realWidth	= $img[0];
				$realHeight = $img[1];
				
				/* Watermarking */
				/*if($this->is_watermark == TRUE){
					$config = null;
					$config['source_image']	= $fileNameResize;
					$config['image_library'] = 'gd2';
					$config['wm_type'] = 'overlay';
					$config['wm_overlay_path'] = './assets/collections/wm_source.png';
					$config['wm_vrt_alignment'] = 'bottom';
					$config['wm_hor_alignment'] = 'right';
					$config['wm_opacity'] = '80';
					$this->image_lib->initialize($config); 
					$this->image_lib->watermark();
					$this->image_lib->clear();
					unset($config);
				}
*/

				$resize = array();
				foreach($this->upload_resize as $r){
					$resize[] = array(
						"width"			=> $r['width'],
						"height"		=> $r['height'],
						"quality"		=> $r['quality'],
						"source_image"	=> $fileNameResize,
						"new_image"		=> $uri.$r['name']."/".$fileName
					);
				}
				
				foreach($resize as $k=>$v){
					$oriW = $v['width'];
					$oriH = $v['height'];
					$x = $v['width']/$realWidth;
					$y = $v['height']/$realHeight;
					if($x < $y) {
						$v['width'] = round($realWidth*($v['height']/$realHeight));
					} else {
						$v['height'] = round($realHeight*($v['width']/$realWidth));
					}     
				//d($v);
					$this->image_lib->initialize($v); 
					if(!$this->image_lib->resize()){
						d($this->image_lib->display_errors());
						die("Error Resize....");
					}
					$this->image_lib->clear();
					/*if($k==0){
						$config = null;
						$config['image_library'] = 'GD2';
						$im = getimagesize($v['new_image']);
						$toCropLeft = ($im[0] - ($oriW *1))/2;
						$toCropTop = ($im[1] - ($oriH*1))/2;
						
						$config['source_image'] = $v['new_image'];
						$config['width'] = $oriW;
						$config['height'] = $oriH;
						$config['x_axis'] = $toCropLeft;
						$config['y_axis'] = $toCropTop;
						$config['maintain_ratio'] = false;
						
						$this->image_lib->initialize($config);
						 
						if(!$this->image_lib->crop()){
							die("Error Crop..");
						}
						$this->image_lib->clear();
					}*/
				}
				//delete original image
				if(file_exists($config['upload_path'].$fileName)){					
					unlink($config['upload_path'].$fileName);
				}
			} // end if this type image
			return $media;
		}
		else {
			//echo "gaga";exit;
			d($this->upload->display_errors());
		}
	}

	function _delte_old_files($par=array()){
		$uri = $this->upload_path;
		$files = $this->DATA->data_id($par['par']);
		$folder = isset($par['folder'])?$par['folder'].'/':'original/';
		if( !empty( $files->{$par['field']} ) ){
			$ori_file = $uri.$folder.$files->{$par['field']};
			if(file_exists($ori_file)){
				unlink($ori_file);
			}
			if(trim($this->upload_types)=='image' && count($this->upload_resize) > 0){				
				$data = array();
				foreach($this->upload_resize as $m){
					$data[] = $uri.$m['name']."/".$files->{$par['field']};
				}	
				foreach($data as $v){
					if(file_exists($v)){
						unlink($v);
					}
				}
			}				
		}
	}	

	function _data_web($m=array()){
		
		$this->load->library('pagination');
		$config['per_page'] = $this->per_page;
		$data = $this->data_table;
		$config['base_url'] = $m['base_url'];
		$config['total_rows'] = $data['total'];		
		$config['uri_segment'] = $this->uri_segment;
		$config['suffix']	= $this->config->item('url_suffix');
		$config['full_tag_open'] = '<div class="pagination">';
		$config['full_tag_close'] = '</div>';	
		$config['next_link'] = '&rarr;';
		$config['prev_link'] = '&larr;';
		$this->pagination->initialize($config);
		
		return array(
			'data'			=>	$data['data'],			
			'cRec'			=>  $data['total'],
			'no'			=>  $this->uri->segment($this->uri_segment)==''?0:$this->uri->segment($this->uri_segment),
			'cPage'			=>  ceil($data['total']/$this->per_page),
			'paging'		=> 	$this->pagination->create_links()
		);		
	}	
	
	function _data_front($m=array()){
		//d($data);
		
		$this->load->library('pagination');
		$config['per_page'] = $this->per_page;
		$data = $this->data_table;
		//d($data['count']);
		if(isset($data['count']->found_rows) > 0){
			$r = isset($data['count']->found_rows)?$data['count']->found_rows:0;
		}else{
			$r = isset($data['count'])?$data['count']:0;
		}
		//d($r);
		$config['base_url'] = $m['base_url'];
		$config['total_rows'] = $r;		
		$config['uri_segment'] = $this->uri_segment;
		$config['suffix']	= $this->config->item('url_suffix');
		$config['full_tag_open'] = '<nav><ul class="pagination">';
		$config['full_tag_close'] = '</ul></nav>';	
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		$config['next_link'] = '<i class="fa fa-angle-right"></i>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '<i class="fa fa-angle-left"></i>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		return array(
			'data'			=>	$data['data'],			
			'cRec'			=>  $r,
			'no'			=>  $this->uri->segment($this->uri_segment)==''?0:$this->uri->segment($this->uri_segment),
			'cPage'			=>  ceil($r/$this->per_page),
			'paging'		=> 	$this->pagination->create_links()
		);		
	}	

	function _data($m=array()){
		
		$this->load->library('pagination');
		$config['per_page'] = $this->per_page;
		$data = $this->data_table;

		if(isset($data['count']->found_rows) > 0){
			$r = isset($data['count']->found_rows)?$data['count']->found_rows:0;
		}else{
			$r = isset($data['count'])?$data['count']:0;
		}

		$config['base_url'] = $m['base_url'];
		$config['total_rows'] = isset($data['total'])?$data['total']:$r;	
		$config['uri_segment'] = $this->uri_segment;
		$config['suffix']	= $this->config->item('url_suffix');
		$config['full_tag_open'] = '<div class="text-right m-t-15 m-r-15"><ul class="pagination m-t-0 m-b-10">';
		$config['full_tag_close'] = '</ul></div>';	
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['next_link'] = '»';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['prev_link'] = '«';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		// d($config);
		return array(
			'd'			=>	$data['data'],			
			'cRec'		=>  isset($data['total'])?$data['total']:$r,
			'no'		=>  $this->uri->segment($this->uri_segment)==''?0:$this->uri->segment($this->uri_segment),
			'cPage'		=>  ceil(isset($data['total'])?$data['total']:$r/$this->per_page),
			'paging'	=> 	$this->pagination->create_links()
		);		
	}	

	function write_log(){
		
		$class 	= $this->_getClass();
		$method	= $this->_getMethod();
		$name	= $this->jCfg['user']['name'];
		$id 	= $this->jCfg['user']['id'];
		$ip		= $_SERVER['REMOTE_ADDR'];
		$browser= $_SERVER['HTTP_USER_AGENT'];
		$url 	= $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		
		$flag_ins = TRUE;
		if( (!isset($this->jCfg['current_class'])) && (!isset($this->jCfg['current_funtion'])) ){
			$this->jCfg['current_class'] = $class;
			$this->jCfg['current_funtion'] = $method;
			$this->_releaseSession();
		}else{
			
			if($this->jCfg['current_class']==$class && $this->jCfg['current_funtion']==$method){
				$flag_ins = FALSE;				
			}
			
			$this->jCfg['current_class'] = $class;
			$this->jCfg['current_funtion'] = $method;
			$this->_releaseSession();
		}
		
		if(!empty($id) && $flag_ins==TRUE && $this->jCfg['current_class']!="chat" ){
			
			$POST=isset($_POST)?json_encode($_POST):"";
			$GET=isset($_GET)?json_encode($_GET):"";
			$arr_method = array(
				"detail_member","detail_advertiser","cek_advertiser_username",
				"search","get_ads","detail_iklan","get_tag","report","get_tag",
				"get_tag_brand","get_reach","detail","get_ads_info","view_image",
				"get_city","test","index","im_lost","access"
			);
			if(!in_array($method,$arr_method)){
				$this->db->insert("cp_app_log",array(
					"log_date"		=> date("Y-m-d H:i:s"),
					"log_class"		=> $class,
					"log_function" 	=> $method,
					"log_url"		=> $url,
					"log_user_id"	=> $id,
					"log_ip"		=> $ip,
					"log_role"		=> $this->jCfg['user']['level'],
					"log_user_agent"	=> $browser,
					"log_user_name"	=> $name,
					"log_var_get"	=> $GET,
					"log_var_post"	=> $POST			
				));
			}
		}
	}
	
}