
		</div>
		<!-- end #content -->
	
	<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?= base_url()?>assets/def/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?= base_url()?>assets/def/plugins/js-cookie/js.cookie.js"></script>
	<script src="<?= base_url()?>assets/def/js/theme/default.min.js"></script>
	<script src="<?= base_url()?>assets/def/js/apps.js"></script>
	<!-- ================== END BASE JS ================== -->
	
	<script>
		$(document).ready(function() {
			App.init();
			// $('.datepicker').datepicker({});
			
		});
	</script>
	
</body>
</html>