<?php
class FrontController extends My_Controller{ 
	var $meta_keyword 		= "";
	var $meta_title 		= "Home";
	var $meta_desc 			= "";
	var $data_form;
	
	function __construct(){  
		parent::__construct();
		// $m = $this->db->get("cp_template")->row();
		
		$this->jCfg['theme'] = 'templates';
		$this->_releaseSession();  

		$this->load->model('home_model','HM');

		$this->menu = $this->HM->getCatPM();
		$this->company 	= $this->db->get('cp_company_setting')->row();
		$this->cartInfo = produk::cart('',$this->jCfg['client']['id']);
		$this->wishInfo = produk::wishlist('',$this->jCfg['client']['id']);
		$this->bells 	= header::waitingPayment('',$this->jCfg['client']['id']);
	}
	
	function _v($file,$data=array(),$single=true){
		$data["meta_desc"]		= $this->meta_desc;
		$data["meta_title"]		= $this->meta_title;
		$data["meta_keyword"] 	= $this->meta_keyword;
		$data['val'] 			= $this->data_form;

		// $m = $this->db->get("cp_template")->row();
		// $data['themeAssets'] 	= base_url('theme/'.$m->template_name).'/';
		
		if($single)
			$this->load->view($this->jCfg['theme'].'/header',$data);
		
		$this->load->view($file,$data);
		
		if($single)
			$this->load->view($this->jCfg['theme'].'/footer',$data);
	}
	
	function _save_master($data=array(),$par=array(),$vid=0){
		$id 	= 0;
		$act	= FALSE;
		$o = $this->DATA->_cek($par);
		if( $o == 0 ){
			$act = $this->DATA->_add($data);
			$id = $this->db->insert_id();
		}else{
			$act = $this->DATA->_update($par,$data);
			$id = $vid;
		}
		return array(
			'id'	=> $id,
			'msg'	=> ($act)?'Success...':'Fail...'
		);
	}

	function sendEmail($p=array()){
	// d($p);
		$this->load->library('email');
		
		/*smtp method */
		$config['protocol'] = "smtp";
		$config['smtp_host'] = "ssl://mail.obalihara.com";
		$config['smtp_port'] = "465";
		$config['smtp_user'] = "cs@obalihara.com";
		$config['smtp_pass'] = "Obalihara2019";
		$config['priority']  = 1;
		$config['mailtype']  = "html";
		$config['charset']   = "utf-8";
		$config['wordwrap']  = TRUE;
		$config['newline'] = "\r\n";

		// $config['protocol']  = 'smtp';
		// $config['smtp_host'] = 'ssl://smtp.gmail.com';
		// $config['smtp_port'] = 465;//465;
		// $config['smtp_user'] = 'obalihara@gmail.com';
		// $config['smtp_pass'] = 'siaplestari86';
		// $config['priority']  = 1;
		// $config['mailtype']  = 'html';
		// $config['charset']   = 'utf-8';
		// $config['wordwrap']  = TRUE;
	
		$this->email->initialize($config);
		$this->email->set_newline("\r\n");
		$this->email->from('cs@obalihara.com', isset($p['title'])?$p['title']:' Obalihara News');
		// $this->email->from($p['from'], isset($p['title'])?$p['title']:' Obalihara News');
		$this->email->to($p['to']);
		// if( isset($p['cc']) && trim($p['cc']) != "" ){			
		// 	$this->email->cc($p['cc']); 
		// }
		$this->email->subject('Obalihara : '.$p['subject']);
		$this->email->message($p['message']);
		
		if(isset($p['alt_message']) && trim($p['alt_message'])!='' ){
			$this->email->set_alt_message($p['alt_message']);
		}
		
		if ($this->email->send()) {
			return true;
		} else {
			d($this->email->print_debugger());
		}
		//d($this->email->print_debugger());
		
	}
}