<?php defined('BASEPATH') OR exit('No direct script access allowed');

function css_assets($assets_name,$type=''){

    $f = ($type == 'f')?'/front/css/':'/def/css/';

    return '<link href="' . base_url('assets').$f.$assets_name. '" rel="stylesheet" type="text/css" />';
}

function js_assets($assets_name,$type=''){

    $f = ($type == 'f')?'/front/js/':'/def/js/';

    return '<script src="' . base_url('assets').$f.$assets_name. '"  ></script>';
}

function plugin_assets($assets_name,$type='',$file='js'){

    $f = ($type == 'f')?'/front/plugins/':'/def/plugins/';
    	
    if($file == 'js'){
    	$r = '<script src="' . base_url('assets').$f.$assets_name. '"  ></script>';
    }else{
    	$r = '<link href="' . base_url('assets').$f.$assets_name. '" rel="stylesheet" type="text/css" />';
    }

    return $r;
}