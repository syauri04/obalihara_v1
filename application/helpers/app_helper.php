<?php
function getCI(){
	$CI =& get_instance();
	return $CI; 
}

function getHeader(){ 
	$CI =getCI();
	$CI->load->view($CI->jCfg['theme'].'/header');
}

function getFooter(){
	$CI =getCI(); 
	$CI->load->view($CI->jCfg['theme'].'/footer');
} 

function getFormSearch(){
	$CI =getCI();
	$CI->load->view($CI->jCfg['theme'].'/form-search');
} 

function getFormSearchKnow(){
	$CI =getCI();
	$CI->load->view($CI->jCfg['theme'].'/form-search-know');
}  
 
function getTinymce(){ 
	$CI =getCI();
	$CI->load->view($CI->jCfg['theme'].'/tinymce');
}
 
function getView($file="",$par=array()){
	$CI =getCI();
	$CI->load->view($CI->jCfg['theme']."/".$file,$par);
}

function themeUrl(){
	$CI =getCI();
	return base_url().APPPATH."views/".$CI->jCfg['theme']."/";
}

function pTxt($key='',$sep='-'){
	return str_replace($sep,' ', trim($key));
}

function codeEncrypt($t){
	$CI = getCI();
	
	return $CI->encryption->encrypt($t);
}

function codeDecrypt($t){
	$CI = getCI();

	return $CI->encryption->decrypt($t);
}

function urlCheck($url){
	
	// $regex = "((https?|ftp)\:\/\/)?"; // SCHEME 
    // $regex .= "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?"; // User and Pass 
    // $regex .= "([a-z0-9-.]*)\.([a-z]{2,3})"; // Host or IP 
    // $regex .= "(\:[0-9]{2,5})?"; // Port 
    // $regex = "(\/([a-z0-9+\$_-]\.?)+)*\/?"; // Path 
    //$regex .= "(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?"; // GET Query 
    //$regex .= "(#[a-z_.-][a-z0-9+\$_.-]*)?"; // Anchor 
    

    if(strpos($url,'fbsbx')) // `i` flag for case-insensitive
	{ 
       return true; 
	} else {
		return false;	
	}

}

function myNum($num=0,$curr="IDR"){
	$curr2 = strtolower($curr);
	if($curr2=="rp"){
		return $curr.". ".number_format($num,0,",",".");
	}elseif($curr2=="$" || $curr2=="e"){
		return number_format($num,0,".",",")." ".$curr;
	}else{
		return $curr."".number_format($num,0,",",".");
	}
}

function myNum1($num=0,$curr="IDR",$dec=0){
	$curr2 = strtolower($curr);
	if($curr2=="rp"){
		return $curr.". ".number_format($num,0,",",".");
	}elseif($curr2=="$" || $curr2=="e"){
		return number_format($num,0,".",",")." ".$curr;
	}else{
		return number_format($num,0,".",",");
	}
}

function cfg($o='cp_app_name'){ 
	$CI =getCI();
	$return = '';

	$logic = '';
	if(is_array($CI->config->item($o))){
		$logic = count($CI->config->item($o))>0?1:"";
	}else{
		$logic = $CI->config->item($o);
	}

	if(trim($logic)!=""){
		$return = $CI->config->item($o);
	}else{
		$v = $CI->db->get_where("cp_app_config",array(
				'config_name' => $o
			))->row();
		if(isset($v))
			$return = $v->config_value;
	}

	return $return;
}

function myDate($dt,$f="d/m/Y H:i",$s=true){
	$day = array(
		1 => "Senin",
		2 => "Selasa",
		3 => "Rabu",
		4 => "Kamis",
		5 => "Jumat",
		6 => "Sabtu",
		7 => "Minggu"
	);
	if(trim($dt)!="0000-00-00" && trim($dt)!=""){
		$ts = strtotime($dt);
		$dtm = date($f,$ts);
		if( trim($dtm) == "01/01/1970" ){
			return "-";
		}else{
			return ($s)?$day[date("N",$ts)].", ".$dtm:$dtm;
		}
	}else{
		return "-";
	}
}

function get_date_id($date=""){
	
	$date = trim($date)==""?date("Y-m-d"):$date;

	$tgl = myDate($date,"d",false);
	$thn = myDate($date,"Y",false);

	$month = array(
			'01' 	=> "Januari",
			'02' 	=> "Februari",
			'03' 	=> "Maret",
			'04' 	=> "April",
			'05' 	=> "Mei",
			'06' 	=> "Juni",
			'07'	=> "Juli",
			'08' 	=> "Agustus",
			'09' 	=> "September",
			'10'  	=> "Oktober",
			'11'  	=> "November",
			'12' 	=> "Desember"
		);
	$bulan = $month[myDate($date,"m",false)];

	return $tgl." ".$bulan." ".$thn;
}

function d($r=array(),$f=TRUE){
	echo "<pre>";
	print_r($r);
	echo "</pre>";
	
	if($f==TRUE)
		die;
}

function dq($f=TRUE){
	$CI = getCI();
	print_r($CI->db->last_query());


	if($f==TRUE)
		die;
}

function mDate($date="",$v="+1 day",$format='Y-m-d'){
	$date 	= (trim($date)=="")?date("Y-m-d"):$date;
	$nd 	=  strtotime(date("Y-m-d", strtotime($date)) . $v);
	return date($format,$nd);
}

function get_new_image($p=array()){
	$CI =getCI();
	$no_image = base_url()."assets/collections/no_image.jpg";
	$return = $no_image;
	
	$url_source_no_image = base_url()."assets/images/no_image.jpg";
	$p['url'] = trim($p['url'])==""?$url_source_no_image:$p['url'];
	
	if( trim($p['url']) != ""){
		$img_source = "./".str_replace(base_url(),"",$p['url']);
		$width = isset($p['width'])?$p['width']:0;
		$height = isset($p['height'])?$p['height']:0;
		
		if( file_exists($img_source) && !is_dir($img_source)){
			//get file source info.
			$finfo = pathinfo($img_source); 
			$n_width = $width==0?'ori':$width;
			
			$new_image_name = $finfo['filename']."_".$n_width.".".$finfo['extension'];
			if($height>0){
				$new_image_name = $finfo['filename']."_".$n_width."_".$height.".".$finfo['extension'];
			}
			
			$new_path 	= "./assets/images/".$new_image_name;

			if(!file_exists($new_path) && !is_dir($new_path) ){
				$CI->load->library('image_lib');
				$quality = isset($p['quality'])?$p['quality']:'100%';
					
				$v = array(
						"width"                 => $width,
						"height"                => $height,
						"quality"               => $quality,
						"source_image"  		=> $img_source,
						"new_image"             => $new_path
				);
				$img = getimagesize($v['source_image']);
				$realWidth      = $img[0];
				$realHeight 	= $img[1];
				 
				if( $height > 0){
				 					
					//resize
					$oriW = $v['width'];
					$oriH = $v['height'];
					$x = $v['width']/$realWidth;
					$y = $v['height']/$realHeight;
					if($x < $y) {
						$v['width'] = round($realWidth*($v['height']/$realHeight));
					} else {
						$v['height'] = round($realHeight*($v['width']/$realWidth));
					}
					
					$CI->image_lib->initialize($v);
					if(!$CI->image_lib->resize()){
							//debugCode($this->image_lib->display_errors());
							//echo "eror resize ".$new_image_name;
							$return = base_url()."assets/images/no_image.jpg";
					}
					$CI->image_lib->clear();
					
					// CROP..
					$config = null;
					$config['image_library'] = 'GD2';
					$im = getimagesize($v['new_image']);
					$toCropLeft = ($im[0] - ($oriW *1))/2;
					$toCropTop = ($im[1] - ($oriH*1))/2;
					
					$config['source_image'] = $v['new_image'];
					$config['width'] = $oriW;
					$config['height'] = $oriH;
					$config['x_axis'] = $toCropLeft;
					$config['y_axis'] = $toCropTop;
					$config['maintain_ratio'] = false;
					$config['new_image'] = $v['new_image'];
					
					$CI->image_lib->initialize($config);
					 
					if(!$CI->image_lib->crop()){
						die("Error Crop..");
					}
					$CI->image_lib->clear();
					
				}else{
					$CI->image_lib->initialize($v);
					$v['width']		= $v['width']==0?$realWidth:$v['width'];
					$v['height'] 	= $v['width']==0?round($realHeight*($v['width']/$realWidth)):$v['width'];
					//resize...
					if(!$CI->image_lib->resize()){
							//debugCode($this->image_lib->display_errors());
							//echo "eror resize ".$new_image_name;
							$return = base_url()."assets/images/no_image.jpg";
					}
					$CI->image_lib->clear();
				}	

				$return = base_url()."assets/images/".$new_image_name;	
			}else{
				$return = base_url()."assets/images/".$new_image_name;
				//$p['url'] = $url_source_no_image;
				//get_new_image($p);	
			}
		}
		
	}
	return $return;
}

function get_image($url="",$noimage=""){
    // d($noimage);
	if(trim($noimage)==""){
		$no_image = base_url()."assets/noimage.png";
	}else{
		$no_image = themeUrl()."/".$noimage;
	}
	$img = "";
	if(trim($url)!=""){
		$nurl = "./".str_replace(base_url(),"",$url);
		if(file_exists($nurl) && !is_dir($nurl)){
			$img = $url;
		}else
			$img = $no_image;
	}else
		$img = $no_image;


	// d($img);
	
	return $img;
}


function _ac($c='index'){
	if(trim($c)!==''){
		$CI  = getCI();
		$acc = $CI->jCfg['access'];
		if(isset($acc[$c])){
			return TRUE;
		}else
			return FALSE;
	}else{
		return FALSE;
	}
}

function get_info_message(){
	 if( isset($_GET['msg']) ){ 
	 		$type= isset($_GET['type_msg'])?$_GET['type_msg']:'info';
	 	?>

		<div id="gritter-notice-wrapper">
			<div id="gritter-item-4" class="gritter-item-wrapper" role="alert">
				<div class="gritter-top"></div>
				<div class="gritter-item">
					<a class="gritter-close" href="#" tabindex="1" style="display: none;">Close Notification</a>
					<div class="gritter-without-image">
					<span class="gritter-title">Message <?php echo $type;?></span>
					<p><?php echo urldecode($_GET['msg']);?></p>
					</div>
					<div style="clear:both"></div>
				</div>
				<div class="gritter-bottom"></div>
			</div>
		</div>
		<script type="text/javascript">
			function hidden_msg(){
				$('#gritter-notice-wrapper').fadeOut();
			}
			setTimeout('hidden_msg()',4000);
		</script>
	<?php } 
}

function _encrypt($key=""){
	$CI =getCI();
	$CI->load->library('encrypt');
	$nid = "meme-#".$key."#bola-".date("Ymdh");
	return urlencode($CI->encrypt->encode($nid));
}

function _decrypt($key=""){
	$CI =getCI();
	$CI->load->library('encrypt');
	$nid = urldecode($CI->encrypt->decode($key));
	$nid_arr = explode("#",$nid);
	return $nid_arr[1];
}

function get_breadcrumb($par=array()){
	if( count($par) > 1){
		echo '<ol class="breadcrumb pull-right">';
		if(count($par) > 0){
			foreach ($par as $key => $value) {
				if( isset($value['url']) && trim($value['url'])!="" ){
					echo "<li>";
					if(strtolower($value['title'])=="home"){
						echo "<i class='fa fa-home'></i>";
					}else{
						echo "<a href='".$value['url']."'>".$value['title']."</a> <span class='divider'></span>";
					}
					echo "</li>";
				}else{
					echo "<li class='active'>".$value['title']."</li>";
				}
			}
		}
		echo '</ol>';
	}
}

function getLinks($links=array()){
	$CI =getCI();
	$uri =  $CI->uri->segment(3);
	if(count($links)>0){
		rsort($links);
		foreach($links as $v){
			if($v['action']!="bug"){
				if(trim($uri)==''||trim($uri)=='search'||trim($uri)=='access'){
					$fc = 'index';
				}else{		
					$fc = (trim($uri)=='edit'||trim($uri)=='add'||trim($uri)=='upload_excel'||trim($uri)=='print_mail'||trim($uri)=='print_nota')?'add':$uri;
				}
				$class_css = $v['action']=="index"?"list":$v['action'];
				$icon = $v['action']=="add"?'<i class="icon-plus"></i>':'<i class="icon-th-list"></i>';
			?>
				<li class="<?php echo ($fc==$v['action'])?'active':'';?>" ><a href="<?php echo $v['link'];?>" class="ttip_t" title="<?php echo $v['title'];?>"><?php echo $icon." ".$class_css;?></a></li>
			<?php
			}
		}
	}
}

function getLink2($links=array()){
	$CI =getCI();
	$uri =  $CI->uri->segment(3);
	if(count($links)>0){
		rsort($links);
		//debugCode($links);
		foreach($links as $v){
			if($v['action']!="bug"){
				if(trim($uri)==''||trim($uri)=='search'||trim($uri)=='access'){
					$fc = 'index';
				}else{		
					$fc = (trim($uri)=='edit'||trim($uri)=='add'||trim($uri)=='upload_excel'||trim($uri)=='print_mail'||trim($uri)=='print_nota')?'add':$uri;
				}
				$class_css = $v['action']=="index"?"list":$v['action'];
				$icon = $v['action']=="add"?'<i class="icon-plus"></i>':'<i class="icon-th-list"></i>';
				if($v['title'] == "add"){
			?>
				<a href="<?php echo $v['link'];?>" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?php echo $v['title'];?>"><?php echo $icon." ".$class_css;?></a>
			<?php
				}
			}
		}
	}
}

function get_header_table($obj=array()){
	$CI = getCI();
	if( count($obj) > 0 ){
		$direction = $CI->jCfg['search']['order_dir']=="ASC"?"DESC":"ASC";
		foreach ($obj as $key => $value) {
			if(trim($key)!=""){
			echo "<th><a href='".$CI->own_link."/sort?sort_by=".$key."&sort_dir=".$direction."&next=".current_url()."'>".$value."</a></th>";
			}
		}
	}
}

function get_data_table(){ 
?>
	<script src="<?php echo themeUrl();?>lib/datatables/jquery.dataTables.min.js"></script>
	<script src="<?php echo themeUrl();?>lib/datatables/extras/Scroller/media/js/Scroller.min.js"></script>
	<script src="<?php echo themeUrl();?>js/gebo_datatables.js"></script>
<?php
}
function js_validate(){ 
	?>
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/validationEngine.jquery.css" type="text/css" media="screen" title="no title" charset="utf-8" />
		<script src="<?php echo base_url();?>assets/js/languages/jquery.validationEngine-en.js" type="text/javascript"></script>
		<script src="<?php echo base_url();?>assets/js/jquery.validationEngine.js" type="text/javascript"></script>
		<script>	
		$(document).ready(function() {
			$("#form-validated").validationEngine();
		});
		</script>
	<?php
}


function js_picker(){	
	?>
			<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/datepicker.css" />
			<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js" type="text/javascript"></script>
            <script language="javascript">
			$(document).ready(function(){
				$('.picker').datepicker({
					format: 'yyyy-mm-dd'
				});				
			});
			</script>
           <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery-ui.css" />
			<script src="<?php echo base_url();?>assets/js/jquery-1.9.1.js" type="text/javascript"></script>
            <script src="<?php echo base_url();?>assets/js/jquery-ui.js" type="text/javascript"></script>
            <script language="javascript">
			$(document).ready(function(){
				$('.picker').datepicker({
					dateFormat: 'yy-mm-dd',
					//minDate: "-1M", 
					//maxDate: "+10M"
				});				
			});
			</script>-->
	<?php
}

/* ecommerde */

function gebo_choosen(){
	?>
	<script type="text/javascript" src="<?php echo themeUrl();?>lib/chosen/chosen.jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo themeUrl();?>lib/chosen/chosen.css"/>
	<script type="text/javascript">
	$(document).ready(function(){
		gebo_chosen.init();
	});
	gebo_chosen = {
		init: function(){
			$(".chosen_one").chosen({
				allow_single_deselect: true
			});
			$(".chosen_multi").chosen();
		}
	};
	</script>
	<?php
}

function link_action($links=array(),$id=""){
	if(count($links)>0){
		foreach($links as $m){
			$property = "";
			if($m['type']=='simple'){
				$property = " class=\"btn btn-primary btn-icon btn-circle\" ";
			}elseif($m['type']=='confirm'){
				$property = " class=\"btn btn-primary btn-icon btn-circle\" ";
			}else{
				$property = " class=\"btn btn-primary btn-icon btn-circle\" ";	
			}	
		?>
		<a href="<?php echo $m['link']."/".$id;?>" <?php echo $property;?> title="<?php echo ucwords($m['title']);?> Data "><i class="fa fa-<?=$m['image']?> "></i><!-- <img src="<?php echo themeUrl();?>images/<?php echo $m['image'];?>" /> --></a>
		<?php  	
		} 
	}
}

function get_group($id=""){
	$CI = getCI();
	$nama = '';
	$m = $CI->db->get_where("cp_app_acl_group",array(
			'ag_id'	=> $id
		))->row();
	if( count($m) > 0 ){
		$nama = $m->ag_group_name;
	}
	return $nama;
}

function _ajax_cek($par=array()){
	$CI =getCI(); 
	
	if(isset($par['ext']) && trim($par['ext'])!=""){
		$validateValue	= $par['ext'].$_POST['validateValue'];
	}else{
		$validateValue	= $_POST['validateValue'];
	}
	
	if(isset($par['replace_dot'])){
		$validateValue	= str_replace(".","",$validateValue);
	}
	
	$validateId		= $_POST['validateId'];
	$validateError	= $_POST['validateError'];
	
	$arrayToJs = array();
	$arrayToJs[0] = $validateId;
	$arrayToJs[1] = $validateError;
	
	$CI->DATA->table=$par['table'];
	$cek = $CI->DATA->_cek(array(
		$par['field'] => $validateValue
	));
	$tmp = "";
	if($cek > 0){	
		$arrayToJs[2] = "false";		
		$tmp = '{"jsonValidateReturn":'.json_encode($arrayToJs).'}';			
	}else{
		$arrayToJs[2] = "true";
		$tmp = '{"jsonValidateReturn":'.json_encode($arrayToJs).'}';			
	}
	return $tmp;
}


function _get_menu($menu=array()){
	$CI  = getCI();
	if(count($menu)<0) return array();
	$mnn=[];
	foreach($menu as $mn){

		$mnx 	= preg_split("/>/",$mn['acc_menu']);
		$count	= count($mnx);
		$t		= "\$mnn";
		for($i=0;$i<$count;$i++){
			if(($count-1)==$i){				
				$t .= "[]=array('menu'=>'".$mnx[$i]."','id'=>'".$mn['acc_id']."','class_group'=>'".$mn['acc_group']."','group'=>'".$mn['acc_group_controller']."','name'=>'".$mn['acc_controller_name']."','css_class'=>'".$mn['acc_css_class']."');";
			} else {
				$t .= "['".$mnx[$i]."']";
			}
		}
		eval($t);
	}
	return $mnn;
	
}

//SETING MENU

function _menuSet($m,$pId = 0){

	$branch = array();

    foreach ($m as $e) {
        if ($e['parent_id'] == $pId) {
            $children = _menuSet($m, $e['id']);
            if ($children) {
                $e['child'] = $children;
            }
            $branch[] = $e;
        }
    }

    return $branch;

}

function getcategories(){
	$CI = getCI();

	$CI->db->select('id,name,parent_id')
    ->where('is_trash',0)
    ->order_by('id','ASC');
    $p = $CI->db->get('cp_kategori_barang')->result_array();

    $a = _menuSet($p);
    // d($a);

    foreach ($a as $k => $v) {
    	if(isset($v['child'])){
    		$li = '<a href="#submenu'.$k.'" data-toggle="collapse" aria-expanded="false" class="list-group-item list-group-item-action flex-column align-items-start">';
    	}else{
    		$li = '<a href="'.base_url().'category/c?cid='.$v['id'].'&&ct='.url_title($v['name']).'" class="list-group-item list-group-item-action">';
    	}
		echo $li;
		echo '<div class="d-flex w-100 justify-content-start align-items-center">';
		echo '<span class="menu-collapsed">'.$v["name"].'</span>';
		echo '<span class="submenu-icon ml-auto"></span>';
		echo '</div>';
		echo '</a>';
        echo '<div id="submenu'.$k.'" class="collapse sidebar-submenu">';
        if(isset($v['child'])){
        	foreach ($v['child'] as $k => $v) {
		        echo '<a href="'.base_url().'category/c?cid='.$v['id'].'&&ct='.url_title($v['name']).'" class="list-group-item list-group-item-action">';
	            echo '<span class="menu-collapsed">'.$v['name'].'</span>';
	            echo '</a>';
	        }
        }
        
        echo '</div>';
    }
}

function front_menu(){

	$CI = getCI();

    $CI->db->select('id,title,parent_id')
    ->where('status',1)
    ->where('is_trash',0)
    ->order_by('order','ASC');
    $p = $CI->db->get('cp_kategori')->result_array();

	$a = _menuSet($p);

	foreach ($a as $k => $v) {

		if(!isset($v['child'])){
			echo '<li class="nav-item dropdown">';
			echo '<a class="nav-link" href="'.base_url().strtolower($v['title']).'">'.$v['title'].'</a>';
			echo '</li>';
		} else {
			echo '<li class="nav-item dropdown">';
			echo '<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$v['title'];
			echo '</a>';
            echo '<div class="dropdown-menu" aria-labelledby="navbarDropdown">';
            foreach ($v['child'] as $k => $v) {
		        echo '<a class="dropdown-item" href="#">'.$v['title'].'</a>';
            }
            echo '</div>';
			echo '</li>';
		}
	}

}

function dnmcMenu($m,$top=true,$sub=false){
	// d($m);
	$CI  = getCI();
	$c 	= count($m);
	$uris = $CI->uri->segment(1);
	
	echo ($top)?"<ul class='nav'>":"<ul class='sub-menu'>";
	echo ($top)?'<li class="nav-header">Navigation</li>':'';

		$url1 = $CI->uri->segment(1);
		$url2 = $CI->uri->segment(2);
		$url3 = $CI->uri->segment(2)."/".$CI->uri->segment(3);

		if(!empty($m)){
			foreach($m as $k=>$v){
				$active = ($k=='Admin & Setting ')?'':'';
				if(is_array($v) && !isset($v['menu']) && !isset($v['id']) && !isset($v['name']) ){
					$css_class = isset($v[0]['css_class']) && trim($v[0]['css_class'])!=""?$v[0]['css_class']:'';
					if($sub=='true'){
						$css_class = '';
					}else{
						if($k == "Info Management "){
							$css_class = isset(current($v)[0]['css_class']) && trim(current($v)[0]['css_class'])!=""?current($v)[0]['css_class']:'';
						}
					}

					$e = ( strtolower(trim($k))==trim($url1))?'active':'';
					echo "<li class='has-sub ".$e." ".$active." menus' >";
					echo'
							<a href="javascript:;">
								<b class="caret pull-right"></b>
								<i class="'.$css_class.'"></i>
								<span>'.$k.'</span>
							</a>
						';
					dnmcMenu($v,false,true);
					echo "</li>";
				}else{
					if($m[0]['menu'] == "Project"){
						$css_class = isset($v['css_class']) && trim($v['css_class'])!=""?$v['css_class']:'';
						$a=( $v['group']==$url1 && $v['name']==$url2)?'active':'';
						echo "<li class='".$a." menus'> ";
						echo "<a href='".base_url($v['group']."/".$v['name'])."' > <i class='".$css_class."'></i> ".$v['menu']."</a>";
						echo "</li>";
					}else{
						$a=( $v['group']==$url1 && $v['name']==$url2)?'active':'';
						echo "<li class='".$a." menus'> ";
						echo "<a href='".base_url($v['group']."/".$v['name'])."' >".$v['menu']."</a>";
						echo "</li>";
					}
				}		
			
			}
		}
	echo "</ul>";
}

function get_user_list($par=array()){
	$CI = getCI();
	$CI->db->where("is_trash !=",1);
	$CI->db->order_by("user_name","ASC");
	return $CI->db->get("cp_app_user")->result();
}

function getUser($id,$field){
	$CI = getCI();

	$a = $CI->db->get_where('cp_buyer',['user_id' => $id])->row();

	$y = '';
	if(isset($a)){
		$y = $a->$field;
	}

	return $y;
}

function get_category_photo($par=array()){
	$CI = getCI();
	$CI->db->where($par);
	$CI->db->order_by("catp_name","ASC");
	$CI->db->where("is_trash",0);
	return $CI->db->get("oto_category_photo")->result();
}

function getMenu($sec_id = ''){
	$CI = getCI(); 
	$sec  = $CI->db->query("
		SELECT * FROM m_article_section
		WHERE section_status = 1
			AND section_istrash != 1 	
			AND section_id != 5
			AND section_id != 12
		ORDER BY order_by ASC
	")->result();
	//debugCode($sec);
	if($sec_id == 'home'){
		$h = 'set-menu';
	}else{
		$h = '';	
	}
	echo '<ul>';
		echo '<li class="'.$h.'"><a href="'.base_url().'" >HOME</a></li>';
		foreach($sec as $s){
		$cat  = $CI->db->query("
			SELECT a.*,b.* FROM  m_article_category a, m_article_section b
			WHERE a.category_status = 1
				AND a.category_istrash != 1 
				AND a.category_id != 14
				AND a.category_section_id	= ".$s->section_id."
			GROUP BY a.category_id
		")->result();
		//debugCode($cat);
		if($sec_id == $s->section_id){
			$ch = 'set-menu';
		}else{
			$ch = '';
		}
		if(!empty($cat)){
			$cs = 'has-dd';
		}else{
			$cs = '';	
		}
		$section_id = isset($s->section_id)?$s->section_id:'';
		//debugCode($section_id);
		if($section_id == '8'){
			echo '<li class="'.$cs.' '.$ch.'"><a href="'.base_url().'multimedia"  >'.strtoupper($s->section_title).'</a>';
		}else{
			echo '<li class="'.$cs.' '.$ch.'"><a href="'.base_url().'topic/s/'.$s->section_id.'-'.url_title($s->section_title).'" >'.strtoupper($s->section_title).'</a>';
		}
		if(!empty($cat)){
			echo '<ul>';
			foreach($cat as $c){
				$category_id = isset($c->category_id)?$c->category_id:'';
				if($category_id == '12'){
					echo '<li><a href="'.base_url().'multimedia/foto">'.$c->category_title.'</a></li>';		
				}elseif($category_id == '13'){
					echo '<li><a href="'.base_url().'multimedia/video">'.$c->category_title.'</a></li>';		
				}else{
					echo '<li><a href="'.base_url().'topic/c/'.$c->category_id.'-'.url_title($c->category_title).'">'.$c->category_title.'</a></li>';		
				}
				echo '<div class="clear"></div>';
			}
			echo '</ul>';
		}
		echo '</li>';
		}
		echo '</ul>';
}

function getMenuLeft($id){
	$CI = getCI();
	$where = '';
	//debugCode($id);
	if($id != ''){
		$where = 'AND sec_id = '.$id; 
	
	
	$sec  = $CI->db->query("
		SELECT * FROM section_toko
		WHERE is_publish = 1
			AND is_trash != 1
			".$where." 	
		ORDER BY sec_id ASC
	")->result();
	
	echo '<ul>';
		foreach($sec as $s){
		echo '<li><strong>'.$s->sec_name.'</strong>';
		$cat  = $CI->db->query("
			SELECT a.*,b.* FROM product_cat a, section_toko b
			WHERE a.is_publish = 1
				AND a.is_trash != 1 
				AND a.catpr_section	= ".$s->sec_id."
			GROUP BY a.catpr_id
			ORDER BY a.catpr_order
		")->result();
		//debugCode($cat);	
			echo '<ul>';
			foreach($cat as $c){
			echo '<li><a href="'.base_url().'category/c/'.$c->catpr_id.'/'.url_title($c->catpr_name).'">'.$c->catpr_name.'</a>';
			$scat  = $CI->db->query("
				SELECT a.*,b.* FROM product_subcat a, product_cat b
				WHERE a.is_publish = 1
					AND a.is_trash != 1 
					AND a.subpr_category = ".$c->catpr_id."
				GROUP BY a.subpr_id	
				ORDER BY a.subpr_order
			")->result();
			//debugCode($scat);
				if(!empty($scat)){
				echo '<ul>';
					foreach($scat as $sc){
						echo '<li><a href="'.base_url().'category/sc/'.$sc->subpr_id.'/'.url_title($sc->subpr_name).'">'.$sc->subpr_name.'</a></li>';
					}
				echo '</ul>';
				}
			echo '</li>';		
			}
			echo '</ul>';
		echo '</li>';
		}
	echo '</ul>';
	}
}

function fixdate($date_art = ''){
	$CI = getCI();
	
	if(!empty($date_art)){
		$d = explode(' ',$date_art);
		$t = explode('-',$d[0]);
		$w = explode(':',$d[1]);
	
		$jam  = $w[0].':'.$w[1];
		$tgl  = $t[2];
		$bln  = $t[1];
		$thn  = $t[0];
		
		$x = mktime(0, 0, 0, $bln, $tgl, $thn);
		$hari = date("w", $x);
	}else{
		$hari = date('w');
		$tgl  = date('d');
		$bln  = date('m');
		$thn  = date('Y');
	}
	
    switch($hari){      
        case 0 : {
                    $hari='Minggu';
                }break;
        case 1 : {
                    $hari='Senin';
                }break;
        case 2 : {
                    $hari='Selasa';
                }break;
        case 3 : {
                    $hari='Rabu';
                }break;
        case 4 : {
                    $hari='Kamis';
                }break;
        case 5 : {
                    $hari="Jum'at";
                }break;
        case 6 : {
                    $hari='Sabtu';
                }break;
        default: {
                    $hari='UnKnown';
                }break;
    }
     
	switch($bln){       
        case 1 : {
                    $bln='Januari';
                }break;
        case 2 : {
                    $bln='Februari';
                }break;
        case 3 : {
                    $bln='Maret';
                }break;
        case 4 : {
                    $bln='April';
                }break;
        case 5 : {
                    $bln='Mei';
                }break;
        case 6 : {
                    $bln="Juni";
                }break;
        case 7 : {
                    $bln='Juli';
                }break;
        case 8 : {
                    $bln='Agustus';
                }break;
        case 9 : {
                    $bln='September';
                }break;
        case 10 : {
                    $bln='Oktober';
                }break;     
        case 11 : {
                    $bln='November';
                }break;
        case 12 : {
                    $bln='Desember';
                }break;
        default: {
                    $bln='UnKnown';
                }break;
    }
	
	
	if(!empty($date_art)){
		$date = $hari.", ".$tgl." ".$bln." ".$thn." ".$jam." WIB";
	}else{
		$date = $hari.", ".$tgl." ".$bln." ".$thn;
	}
	
	if(!empty($date_art)){
		return $date;
	}else{
		echo $date;
	}

}

function terbilang($angka){

    $angka = (float)$angka;
    $bilangan = array(
            '',
            'satu',
            'dua',
            'tiga',
            'empat',
            'lima',
            'enam',
            'tujuh',
            'delapan',
            'sembilan',
            'sepuluh',
            'sebelas'
    );
     
    if ($angka < 12) {
        return $bilangan[$angka];
    } else if ($angka < 20) {
        return $bilangan[$angka - 10] . ' belas';
    } else if ($angka < 100) {
        $hasil_bagi = (int)($angka / 10);
        $hasil_mod = $angka % 10;
        return trim(sprintf('%s puluh %s', $bilangan[$hasil_bagi], $bilangan[$hasil_mod]));
    } else if ($angka < 200) {
        return sprintf('seratus %s', terbilang($angka - 100));
    } else if ($angka < 1000) {
        $hasil_bagi = (int)($angka / 100);
        $hasil_mod = $angka % 100;
        return trim(sprintf('%s ratus %s', $bilangan[$hasil_bagi], terbilang($hasil_mod)));
    } else if ($angka < 2000) {
        return trim(sprintf('seribu %s', terbilang($angka - 1000)));
    } else if ($angka < 1000000) {
        $hasil_bagi = (int)($angka / 1000);
        $hasil_mod = $angka % 1000;
        return sprintf('%s ribu %s', terbilang($hasil_bagi), terbilang($hasil_mod));
    } else if ($angka < 1000000000) {
        $hasil_bagi = (int)($angka / 1000000);
        $hasil_mod = $angka % 1000000;
        return trim(sprintf('%s juta %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
    } else if ($angka < 1000000000000) {
        $hasil_bagi = (int)($angka / 1000000000);
        $hasil_mod = fmod($angka, 1000000000);
        return trim(sprintf('%s milyar %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
    } else if ($angka < 1000000000000000) {
        $hasil_bagi = $angka / 1000000000000;
        $hasil_mod = fmod($angka, 1000000000000);
        return trim(sprintf('%s triliun %s', terbilang($hasil_bagi), terbilang($hasil_mod)));
    } else {
        return 'Wow...';
    }
}

function date_intval($a='',$b=''){
	
	$date1=date_create($a);
	$date2=date_create($b);
	$diff=date_diff($date1,$date2);
	return $diff->format("%R%a days");;
}

function dateRe($d,$r=''){
	$date ='';
	if($r != 'reverse'){
		$date = date('Y-m-d',strtotime($d));
		
	}else{
		$date = date('d-m-Y',strtotime($d));
	}
	return $date;
}

function loadImage($con,$tipe,$img){

    return base_url().'assets/img/'.$con.'/'.$tipe.'/'.$img;

}

class catMenuClass {

	static function getCatParent($id){
		$t = getCI();
		$a = $t->db->get_where('cp_kategori_barang',['id'=>$id])->row();

		$y = '-';
		if(isset($a)){
			$y = $a->name;
		}

		return $y;
	}

	static function getMenuParent($id){
		$t = getCI();
		$a = $t->db->get_where('cp_kategori',['id'=>$id])->row();

		$y = '-';
		if(isset($a)){
			$y = $a->title;
		}

		return $y;
	}

	static function getCatTipe($id){
		
		$tipeId = $id;

		switch($tipeId){       
	        case 1 : {
	                    $tipeId='Ukuran Baju & Warna';
	                }break;
	        case 2 : {
	                    $tipeId='Ukuran Sepatu & Warna';
	                }break;
	        case 3 : {
	                    $tipeId='Warna Saja';
	                }break;
	        default: {
	                    $tipeId=' - ';
	                }break;
	    }

	    return $tipeId;
		
	}

}

function statusInvoice($id){

	$tipeId = $id;

	switch($tipeId){       
        case 0 : {
                    $tipeId='New Order';
                }break;
        case 1 : {
                    $tipeId='Waiting For Payment';
                }break;
        case 2 : {
                    $tipeId='Paid';
                }break;
        case 3 : {
                    $tipeId='On Process By Seller';
                }break;
        case 4 : {
                    $tipeId='On Process By Courier';
                }break;
        case 5 : {
                    $tipeId='Arrive at Buyers Address';
                }break;
        case 6 : {
                    $tipeId='Done';
                }break;
        case 7 : {
                    $tipeId='Cancel';
                }break;
        default: {
                    $tipeId=' - ';
                }break;
    }

    return $tipeId;
}

function getChild($id,$table){
	$ci = getCI();
	$a = array();

	$sql = '
		SELECT id
		FROM (SELECT * FROM '.$table.' ORDER BY parent_id, id) parent_sorted,
			 (select @pv := "'.$id.'") initialisation
		WHERE find_in_set(parent_id, @pv) AND length(@pv := concat(@pv, ",", id))';
	$query = $ci->db->query($sql)->result();
	foreach ($query as $k => $v) {
		$a[] = $v->id;
	}
	
	return $a;
}

//HEADER HELPER

class header{

	static function checkUserVendor($id){ // id user

		$CI = getCI();
		$a = $CI->db->get_where('cp_vendor',['user_id'=>$id])->result();

		if(count($a) > 0){
			return true;
		} else {
			return false;
		}

	} 

	static function waitingPayment($user_id){

		$CI = getCI();
		$CI->db->where('status_invoice >',0)->where('status_invoice <',6)->where('user_id',$user_id);
		$a = $CI->db->get('cp_cart')->result();

		return count($a);

	}

}

//REGIONAL HELPER

class regional{

	static function province($id){
		$CI = getCI();
		$a = $CI->db->get_where('cp_app_province',['id'=>$id])->row();

		$y = '';
		if(isset($a)){
			$y = $a->nama_provinsi;
		}

		return $y;
	}

	static function kota($id){
		$CI = getCI();
		$a = $CI->db->get_where('cp_app_city',['id'=>$id])->row();

		$y = '';
		if(isset($a)){
			$y = $a->kota_kab.' '.$a->nama_kota;
		}

		return $y;
	}

	static function country($id){
		$CI = getCI();
		$a = $CI->db->get_where('cp_app_country',['country_id'=>$id])->row();

		$y = '';
		if(isset($a)){
			$y = $a->short_name;
		}

		return $y;
	}

}

//CHECKOUT HELPER

function getCheckoutbyCart($id,$field){

	$CI = getCI();
	$q = 'SELECT * FROM `cp_checkout_detail` WHERE cart_id RLIKE "[[:<:]]'.$id.'[[:>:]]"';
	$a = $CI->db->query($q)->row();
	$b = $CI->db->get_where('cp_checkout',['id' => $a->header_id])->row();
	
	return $b->$field;
}

function getCheckoutdetailbyCart($id,$field){

	$CI = getCI();
	$q = 'SELECT * FROM `cp_checkout_detail` WHERE cart_id RLIKE "[[:<:]]'.$id.'[[:>:]]"';
	$a = $CI->db->query($q)->row();

	$y = '';
	if(isset($a)){
		$y = $a->$field;
	}
	
	return $y;
}

//OUTLET HELPER

function getOutlet($id,$field){

	$CI = getCI();
	$a = $CI->db->get_where('cp_vendor',['id'=>$id])->row();
	
	$y = '';
	if(isset($a)){
		$y = $a->$field;
	}

	return $y;

}

function getOutletByUser($id,$field){

	$CI = getCI();
	$a = $CI->db->get_where('cp_vendor',['user_id'=>$id])->row();

	$y = '';
	if(isset($a)){
		$y = $a->$field;
	}

	return $y;

}

//PRODUK HELPER

class produk {

	static function catRef($id){
		$ci = getCI();
		$a = array();
		$a[] = $id;

		$sql = '
			SELECT id
			FROM (SELECT * FROM cp_kategori_barang ORDER BY parent_id, id) parent_sorted,
				 (select @pv := "'.$id.'") initialisation
			WHERE find_in_set(parent_id, @pv) AND length(@pv := concat(@pv, ",", id))';
		$query = $ci->db->query($sql)->result();
		foreach ($query as $k => $v) {
			$a[] = $v->id;
		}

		return $a;
	}

	static function catRefASC($id){
		$ci = getCI();
		$a = array();
		$a[] = $id;

		$sql = 'SELECT id,parent_id FROM cp_kategori_barang WHERE id = '.$id.' ORDER BY id DESC';
		$x = $ci->db->query($sql)->result();
		foreach ($x as $k => $v) {
			$a[] = $v->parent_id;
			if($v->parent_id != 0){
				$l = $ci->db->get_where('cp_kategori_barang',['id' => $v->parent_id])->result();
				$a[] = $l[$k]->parent_id;
			}	
		}

		return $a;
	}

	static function catList($id,$tf = true){

		$a = produk::catRefASC($id);

		// sort($a);
		// array_filter($a);

		$c = count($a);
		$ele = [];
		for($i = 0;$i < $c;$i++){
			if($tf == true){
				if($a[$i] == end($a)){
					echo produk::kategori($a[$i]).'.';
				} else {
					echo produk::kategori($a[$i]).',';	
				}	
			} else {
				$ele[] = $a[$i];
			}
			
		}

		sort($ele);
		
		if($tf == false) { return array_filter($ele); } 
	}

	static function goods($id,$field){

		$CI = getCI();
		$a = $CI->db->get_where('cp_barang',['id'=>$id])->row();

		$y = '';
		if(isset($a)){
			$y = $a->$field;
		}

		return $y;

	}

	static function detail($id,$field){

		$CI = getCI();
		$a = $CI->db->get_where('cp_barang_detail',['barang_id'=>$id])->row();

		$y = '';
		if(isset($a)){
			$y = $a->$field;
		}

		return $y;

	}

	static function maxDiscount(){

		$CI = getCI();
		$CI->db->select_max('discount');
		$a = $CI->db->get('cp_barang_detail')->row();

		$y = '';
		if(isset($a)){
			$y = $a->discount;
		}

		return $y;

	}

	static function wishlist($barang_id='',$user_id){

		$CI = getCI();
		if($barang_id != ''){
			$a = $CI->db->get_where('cp_wishlist',['barang_id' => $barang_id, 'user_id' => $user_id])->result();
		} else {
			$a = $CI->db->get_where('cp_wishlist',['user_id' => $user_id])->result();
		}

		return count($a);

	}

	static function cart($barang_id='',$user_id){

		$CI = getCI();
		if($barang_id != ''){
			$a = $CI->db->get_where('cp_cart',['barang_id' => $barang_id, 'user_id' => $user_id])->result();
		} else {
			$a = $CI->db->get_where('cp_cart',['user_id' => $user_id,'status_invoice' => 0])->result();
		}
		
		return count($a);

	}

	static function getCart($barang_id,$user_id){

		$CI = getCI();
		$a = $CI->db->get_where('cp_cart',['barang_id' => $barang_id, 'user_id' => $user_id])->result();

		return $a;

	}

	static function kategori($id){

		$CI = getCI();
		$a = $CI->db->get_where('cp_kategori_barang',['id'=>$id])->row();

		$y = '';
		if(isset($a)){
			$y = $a->name;
		}

		return $y;

	}

	static function varPrice($id,$u,$w){

		$CI = getCI();
		$a = $CI->db->get_where('cp_barang_varian_detail',['barang_id'=>$id,'ukuran_id' => $u,'warna_id' => $w])->row();

		$y = '';
		if(isset($a)){
			$y = myNum($a->harga,'Rp');
		}

		return $y;

	}

	static function gambar($id,$tipe){

		$CI = getCI();
		$a = $CI->db->get_where('cp_barang_gambar',['barang_id'=>$id])->row();

		$y = '';

		if(isset($a)){
			$y = get_image(base_url()."assets/img/goods/".$tipe."/".$a->gambar_name);
		}

		return $y;

	}

	static function gambarCOM($id,$tipe){

		$CI = getCI();
		$a = $CI->db->get_where('cp_barang',['kategori_barang'=>$id])->row();
		
		if(isset($a)){
			$b = $CI->db->get_where('cp_barang_gambar',['barang_id'=>$a->id])->row();

			$y = '';

			if(isset($b)){
				$y = get_image(base_url()."assets/img/goods/".$tipe."/".$b->gambar_name);
			}
		}else{
			$y = base_url()."assets/noimage.png";
		}
		

		return $y;

	}

	static function warna($id){

		$CI = getCI();
		$a = $CI->db->get_where('cp_warna',['id' => $id])->row();

		$y = '';
		if(isset($a)){
			$y = $a->value;		
		}

		return $y;
	}

	static function ukuran($id){

		$CI = getCI();
		$a = $CI->db->get_where('cp_ukuran',['id' => $id])->row();

		$y = '';
		if(isset($a)){
			$y = $a->name;		
		}

		return $y;
	}

	static function disPrice($p,$d){

		$a = $p - ($p * $d/100);
		
		return $a;

	}

}

// INVOICE HELPER

function feePrice($price,$fee){
	$a = 0;

	if($price != 0 && $fee != 0){
		$a = $price * ($fee/100);
	}

	return $a;
}

function disburse_code(){

	$t = getCI();

	$q = $t->db->query(" select max(right(external_id,6)) as exid from cp_vendor_disburse ")->row();
	$v = $q->exid+1;
	$d = date('d-m-y H:i:s');
	$prc = "DBS".strtotime($d).str_repeat("0",6-strlen($v)).$v;

	return $prc;

}

function courierSwitch($id){
		
	$tipeId = $id;

	switch($tipeId){       
        case 'j&t' : {
                    $tipeId='jnt';
                }break;
        case 'J&T' : {
                    $tipeId='jnt';
                }break;
        default: {
                    $tipeId=$id;
                }break;
    }

    return $tipeId;
	
}

//REGIONAL HELPER

class image{

	static function shipping($logo){

		$CI = getCI();

		$name = '';

		$html ='';
		if(isset($logo)){
			$im = explode(",", $logo);
			
			foreach ($im as $key => $value) {

				if($value == "tiki"){
					$name = base_url()."assets/front/img/Logo-TIKI.png";
				}else if($value == "j&t"){
					$name = base_url()."assets/front/img/j&t.png";
				}else if($value == "pos"){
					$name = base_url()."assets/front/img/pos-indonesia.png";
				}else if($value == "jne"){
					$name = base_url()."assets/front/img/jne.png";
				}else if($value == "lion"){
					$name = base_url()."assets/front/img/lion-parcel-logo.png";
				}
				$html .= '<img width="50px" style="margin-top:10px;" src="'.$name.'" alt="">';
			}
			
		}else{
			$name = base_url()."assets/noimage.png";
			$html .= '<span>Not Available</span>';
		}
		
		// d($html);

		return $html;
	}

}

// RAONG CONFIG
function raongCity($id=''){

	if($id != ''){
		$url = "https://pro.rajaongkir.com/api/city?id=".$id;
	} else {
		$url = "https://pro.rajaongkir.com/api/city";
	}
	

	$curl = curl_init();

	curl_setopt_array($curl, array(
	CURLOPT_URL => $url,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => array(
		"key: f5e8ac7da7095787b8c8892cd644e7f1"
		),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);
	$status = json_decode($response,true);

	$a = '';

	if($status['rajaongkir']['status']['code'] == 400){
		$a = $status['rajaongkir']['status']['description'];
	} else {
		$a = json_decode($response,true)['rajaongkir']['results'];
	}

	// d(json_decode($response,true));

	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	  return $a;
	}

}

function raongKec($id='',$t){

	if($id != ''){
		if($t == 'c'){
			$url = "https://pro.rajaongkir.com/api/subdistrict?city=".$id;
		} else {
			$url = "https://pro.rajaongkir.com/api/subdistrict?id=".$id;
		}
		
	} else {
		$url = "https://pro.rajaongkir.com/api/subdistrict";
	}

	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => $url,
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 30,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
	    "key: f5e8ac7da7095787b8c8892cd644e7f1"
	  ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);
	$status = json_decode($response,true);

	$a = '';

	if($status['rajaongkir']['status']['code'] == 400){
		$a = $status['rajaongkir']['status']['description'];
	} else {
		$a = json_decode($response,true)['rajaongkir']['results'];
	}

	curl_close($curl);

	if ($err) {
	  echo "cURL Error #:" . $err;
	} else {
	  return $a;
	}
}

// SALES REPORT HELPER
function sumTotalCourier($vendor_id,$type){

	$ci = getCI();

	if($type == 'total'){
		$a = $ci->db->select('SUM(total) as total')->where('vendor_id' , $vendor_id)->get('cp_checkout_detail')->row();
	} else {
		$a = $ci->db->select('SUM(courier_cost) as total')->where('vendor_id' , $vendor_id)->get('cp_checkout_detail')->row();
	}

	return $a->total;

}

// +6281382220134
// 350180012251019